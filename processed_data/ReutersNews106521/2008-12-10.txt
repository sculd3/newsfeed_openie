2008-12-09T22:41:00Z,NY Times,brace for,tough 09
2008-12-10T00:07:00Z,Electronics Arts,cuts outlook on,holiday sales
2008-12-10T01:40:00Z,Sony,restructuring,plans
2008-12-10T03:16:00Z,Labor groups,urge,stimulus
2008-12-10T06:48:00Z,Rio Tinto,slash,capex
2008-12-10T07:52:00Z,Fed,issuing,debt
2008-12-10T08:17:00Z,Rio,cuts,halves
2008-12-10T09:32:00Z,Job cuts,mount,downturn bites
2008-12-10T09:35:00Z,Critics,urge,ouster
2008-12-10T13:38:00Z,T-bill sale,produces,dash
2008-12-10T13:55:00Z,Daimler,seeks,clearance for Kamaz deal
2008-12-10T14:36:00Z,Yahoo shareholder,urges,sale to Microsoft
2008-12-10T14:36:00Z,Yahoo workers,will learn of,layoffs
2008-12-10T15:01:00Z,Credit Crisis II,feared,U.S. automakers fail
2008-12-10T15:14:00Z,GMAC 's bonds,fall,exchange offer fizzles
2008-12-10T16:19:00Z,Conceptual agreement,reached on,auto aid
2008-12-10T16:19:00Z,agreement,reached on,auto aid
2008-12-10T18:07:00Z,oil,gone in,25 years
2008-12-10T18:07:00Z,priced oil,gone in,25 years
2008-12-10T18:20:00Z,Ford shares,decline after,Shelby comments
2008-12-10T18:20:00Z,GM,decline after,Shelby comments
2008-12-10T18:39:00Z,car factories,hit across,Europe
2008-12-10T18:39:00Z,economy,hits,wall
2008-12-10T19:13:00Z,AIG,cover,derivatives with Fed plan
2008-12-10T20:07:00Z,Lawmaker,tells,Treasury 's Kashkari
2008-12-10T20:25:00Z,Oil,rises as,cuts
2008-12-10T20:47:00Z,Spending oversight,irk,Elan investors
2008-12-10T20:54:00Z,Nortel,shares,dive on bankruptcy advice report
2008-12-10T20:59:00Z,15-20 percent profit growth,in,China
2008-12-10T20:59:00Z,Yum,can reach,15-20 percent profit growth
2008-12-10T23:24:00Z,GMAC bond exchange flop,threatens,bank bid
2008-12-10T23:31:00Z,U.S. auto deal,hits,snag
2008-12-10T23:36:00Z,Republicans,criticize,auto bailout
2008-12-11T05:08:00Z,AIG,owes,$ 10 billion for soured trades
