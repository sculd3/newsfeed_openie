2007-12-11T00:32:00Z,Singapore,injects,capital
2007-12-11T03:09:00Z,MBIA,gets,bln investment
2007-12-11T03:44:00Z,General Growth,cuts,forecast
2007-12-11T03:53:00Z,Delphi,sell,steering business
2007-12-11T07:30:00Z,OFT,sees,risks in exclusive drug distribution deals
2007-12-11T07:30:00Z,risks,in,exclusive drug distribution deals
2007-12-11T09:01:00Z,car,aimed for,Europe 2008/09
2007-12-11T09:01:00Z,new car,aimed for,Europe 2008/09
2007-12-11T10:03:00Z,Toyota head,sees,annual cost cuts
2007-12-11T10:26:00Z,Toyota,'s record,profits
2007-12-11T10:28:00Z,2007 sales growth,beating,its target
2007-12-11T10:28:00Z,Cadbury,sees,2007 sales growth
2007-12-11T10:37:00Z,Novartis,plans,restructuring
2007-12-11T10:41:00Z,China 's inflation rate,hits,high
2007-12-11T11:51:00Z,Suzuki,raise India production with,new car
2007-12-11T12:51:00Z,German investor morale,drops to,15-year low
2007-12-11T12:51:00Z,investor morale,drops to,near low
2007-12-11T13:24:00Z,AT&T,lifts,dividend
2007-12-11T13:34:00Z,Biovail,settles,U.S. class action suit
2007-12-11T14:35:00Z,U.S. electronics,unaffected by,economy
2007-12-11T15:34:00Z,HP,sees,2009 revenue
2007-12-11T16:22:00Z,Fannie Mae,preparing for,long winter
2007-12-11T16:52:00Z,Toyota,sees annually,cost savings
2007-12-11T16:52:00Z,Wyndham shares,tumble on,profit outlook
2007-12-11T16:53:00Z,Freddie Mac,expects,credit losses
2007-12-11T17:11:00Z,6 1/2 years,in,jail
2007-12-11T17:11:00Z,Conrad Black,gets,6 1/2 years
2007-12-11T17:58:00Z,product safety,hurts China image in,U.S
2007-12-11T18:14:00Z,Judge,declares,mistrial
2007-12-11T18:14:00Z,mistrial,in,insider trading case
2007-12-11T20:07:00Z,Fed,fed,funds rate 0.25 pct
2007-12-11T21:30:00Z,Wall St,govt,help
2007-12-11T21:59:00Z,Citigroup,names,Pandit CEO
2007-12-11T22:41:00Z,HP,sees up,margins
2007-12-11T22:47:00Z,Fed,after,modest rate move
2007-12-11T22:47:00Z,Stocks,tumble after,Fed 's rate move
2007-12-11T22:59:00Z,Fed,after,modest rate move
2007-12-11T22:59:00Z,Pandit,has road ahead at,Citigroup
2007-12-11T22:59:00Z,Stocks,tumble after,Fed 's rate move
2007-12-11T23:02:00Z,GE,sees,2008 profit
2007-12-11T23:33:00Z,China,spar over,trade
2007-12-11T23:33:00Z,U.S.,spar over,trade
2007-12-11T23:41:00Z,Fed,actively considering,liquidity measures
2007-12-11T23:54:00Z,Investors,dump,stocks
2007-12-12T05:31:00Z,Ford,sees,Jaguar
2007-12-12T05:31:00Z,Jaguar,sale in,early 2008
2007-12-12T05:31:00Z,Land Rover sale,in,early 2008
