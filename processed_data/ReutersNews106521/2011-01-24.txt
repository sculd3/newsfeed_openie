2011-01-24T00:09:00Z,EU,must decide,new euro measures
2011-01-24T00:41:00Z,Britain 's Serco,abandons,SRA bid
2011-01-24T06:37:00Z,Rio Tinto,wins Riversdale support for,$ 3.9 billion
2011-01-24T07:07:00Z,GM,falls,just shy
2011-01-24T09:48:00Z,Macau 's Stanley Ho,loosens,hold on casino empire
2011-01-24T11:13:00Z,Euro zone PMIs,indicate,inflation pressure
2011-01-24T13:36:00Z,Sara Lee shares,rise on,equity bid report
2011-01-24T13:39:00Z,Treasury 's asset funds,gain,27 percent
2011-01-24T13:39:00Z,Treasury 's toxic asset funds,gain,27 percent
2011-01-24T13:59:00Z,warrants,buy,Citigroup stock
2011-01-24T14:17:00Z,Facebook,sees,advertising prospects
2011-01-24T16:45:00Z,Luxottica,sees,solid 2011
2011-01-24T18:27:00Z,Halliburton,tops,profit forecast
2011-01-24T18:39:00Z,Bank lobby group IIF,slams,capital top-up plans
2011-01-24T18:50:00Z,Steel Dynamics,sees,2011 sales
2011-01-24T19:01:00Z,Judge,temporarily delays,loan document shredding
2011-01-24T21:52:00Z,Euro,lift stocks at,high
2011-01-24T21:52:00Z,growth bets,lift,stocks
2011-01-24T22:03:00Z,Wall Street,rises on,earnings optimism
2011-01-24T22:18:00Z,SEC,eyes,ties of ex-Mass
2011-01-24T22:22:00Z,Intel,sets aside,$ 10 billion
2011-01-25T05:27:00Z,Morgan Stanley banker,drawn into,Galleon investigation
