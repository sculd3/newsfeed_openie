2012-11-08T02:42:00Z,Kia,face,lawsuit over false fuel economy claims
2012-11-08T03:51:00Z,Sandy,scrap,heap
2012-11-08T06:34:00Z,Trade panel,approves,duties
2012-11-08T06:58:00Z,EADS earnings,beat,forecasts
2012-11-08T06:58:00Z,EADS quarterly earnings,beat,forecasts
2012-11-08T10:15:00Z,Adidas,cuts,2012 sales forecast on Reebok woes
2012-11-08T10:15:00Z,Adidas CEO,sees,first signs of Reebok improvement
2012-11-08T10:17:00Z,Lenovo,expects,its China smartphone business
2012-11-08T10:43:00Z,UBS Germany,probed over,suspected tax fraud
2012-11-08T11:20:00Z,Emirates,may replace fleet with,model
2012-11-08T11:37:00Z,Obama,buoyed by,election
2012-11-08T12:03:00Z,it,courts,gadget shoppers
2012-11-08T12:07:00Z,Siemens,sets,aside 3-digit million eur sum
2012-11-08T12:48:00Z,ECB,holds,interest rates
2012-11-08T13:11:00Z,German funding row,hits,EADS
2012-11-08T13:11:00Z,funding row,hits,EADS
2012-11-08T13:19:00Z,Kellogg,sees,2013 growth
2012-11-08T13:23:00Z,Duke Energy profit,beats,Street
2012-11-08T13:23:00Z,Street,boosted by,Progress
2012-11-08T13:29:00Z,Carlyle,returns to,Q3 profit
2012-11-08T13:33:00Z,storm,distorts,data
2012-11-08T13:59:00Z,Green Mountain,gives,details of Luigi Lavazza espresso machine
2012-11-08T14:13:00Z,Trade deficit,narrows as,exports climb
2012-11-08T14:26:00Z,ECB,holds,rates
2012-11-08T14:34:00Z,JPMorgan,reaches,deal
2012-11-08T16:07:00Z,Venezuela Amuay refinery operations,seen,normal
2012-11-08T17:07:00Z,Walmart,moves earlier,Black Friday
2012-11-08T17:09:00Z,Ford,builds plug-ins at,flexible Michigan plant
2012-11-08T18:13:00Z,Google Ventures,beefs up,fund size
2012-11-08T18:40:00Z,Image revamp,helps,Wendy
2012-11-08T18:50:00Z,economy,resists,chill
2012-11-08T19:18:00Z,Carlyle,says,reports profit
2012-11-08T19:18:00Z,Draghi,helping,Greece
2012-11-08T19:18:00Z,Draghi open,helping,Greece
2012-11-08T20:20:00Z,AMR,avoids investigation into,$ 2.26 billion
2012-11-08T21:44:00Z,euro,falls on,ECB
2012-11-08T22:32:00Z,Wall Street,drops for,day
2012-11-08T22:52:00Z,U.S.,sees,talks on corporate audits
2012-11-08T22:52:00Z,talks,with,China
2012-11-08T23:12:00Z,Walmart,moves earlier,Black Friday
2012-11-08T23:37:00Z,Abu Dhabi fund,challenges,Citi victory
2012-11-08T23:42:00Z,stock,hits,record low
2012-11-08T23:49:00Z,U.S.,in,talks nations
2012-11-09T05:16:00Z,United Continental,says,profit
2012-11-09T05:19:00Z,Boeing,confirms,20 orders
2012-11-09T05:48:00Z,JPMorgan,gets,nod
2012-11-09T05:48:00Z,Kohl 's holiday profit forecast,misses,mark
2012-11-09T05:48:00Z,nod,resume,stock buys
