2009-08-05T00:23:00Z,Kraft,slips following,results
2009-08-05T00:23:00Z,Kraft profit,climbs,11 percent
2009-08-05T01:59:00Z,Global automakers,beat,forecasts
2009-08-05T01:59:00Z,automakers,beat,forecasts
2009-08-05T05:35:00Z,Board,endorses,direction of GM
2009-08-05T05:35:00Z,Honda,says,car business return
2009-08-05T09:24:00Z,Citi,sell,20 consumer finance businesses
2009-08-05T09:27:00Z,SocGen profits,bolstered by,market rebound
2009-08-05T09:50:00Z,Lloyds posts loss,hit,13 billion pounds
2009-08-05T12:47:00Z,Planned layoffs,accelerate in,July
2009-08-05T14:13:00Z,Service sector,shrinks in,July
2009-08-05T14:36:00Z,Factory orders,rise in,June
2009-08-05T15:07:00Z,Service sector,shrinks in,July
2009-08-05T15:09:00Z,American Axle,cites bankruptcy risk after,deep loss
2009-08-05T15:09:00Z,Axle,cites bankruptcy risk after,loss
2009-08-05T15:16:00Z,Marsh forecast,sends,shares
2009-08-05T17:24:00Z,Government,queries Goldman about,compensation
2009-08-05T17:29:00Z,Treasury,wants,stay
2009-08-05T18:47:00Z,EA shares slide,concerns about,industry
2009-08-05T19:29:00Z,Oil,rises on,U.S. stock drop
2009-08-05T20:49:00Z,Americredit,posts,Q4 profit
2009-08-05T20:51:00Z,1st qtrly profit,in,year
2009-08-05T21:00:00Z,U.S. government,queries,Goldman
2009-08-05T21:12:00Z,half,seen,underwater
2009-08-05T22:42:00Z,AIG,soar amid,financial rally
2009-08-05T22:42:00Z,CIT shares,soar amid,financial rally
2009-08-05T22:42:00Z,Former AmEx head,is,close
2009-08-05T23:17:00Z,U.S. jobs,raise,recovery worries
2009-08-05T23:59:00Z,Cisco,cautious on,shares
