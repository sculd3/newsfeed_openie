2007-01-12T01:18:00Z,Samsung Elec Q4 profit,falls on,flash chips
2007-01-12T02:37:00Z,NAND prices,falling in,Q1
2007-01-12T02:37:00Z,Samsung Elec,sees,DRAM
2007-01-12T03:42:00Z,Pool Corp,says,2007 earnings
2007-01-12T03:43:00Z,REFILE-Samsung,sees Q1 despite,seasonal weakness
2007-01-12T06:12:00Z,BMW,says,2006 China car sales
2007-01-12T07:13:00Z,Toyota,may add,5 N. America car plants
2007-01-12T07:25:00Z,NYSE,plans,test of Web quotes
2007-01-12T09:19:00Z,Samsung Elec Q4 profit,falls,H1 outlook tough
2007-01-12T12:18:00Z,Chip giant Intel,plans,new China plant
2007-01-12T12:43:00Z,AMD,sees,Q4 rev
2007-01-12T14:20:00Z,PepsiCo,sets,ad campaign
2007-01-12T15:26:00Z,AOL,signs Napster as,music subscription service
2007-01-12T16:04:00Z,EMI bosses,face,music
2007-01-12T16:04:00Z,Two EMI bosses,face music after,profit warning
2007-01-12T17:40:00Z,AMD,sees,Q4 rev
2007-01-12T19:35:00Z,U.S. judge,OKs,Delphi equity investment plan
2007-01-12T19:39:00Z,U.S.,investigates options grant to,Apple CEO
2007-01-12T19:48:00Z,Northwest Airlines,files,reorganization plan
2007-01-12T20:13:00Z,Oil,rises after,15 pct slide
2007-01-12T21:17:00Z,Dow,ends at,record as energy up
2007-01-12T21:48:00Z,Dolans,raise,offer take
2007-01-12T21:52:00Z,Stocks,focus,inflation
2007-01-12T21:55:00Z,Nasdaq,calls,desperate
2007-01-12T22:03:00Z,merger,with,ClientLogic
2007-01-12T22:26:00Z,Wal-Mart ad account,goes to,Interpublic
