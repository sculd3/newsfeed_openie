2009-03-26T23:48:00Z,Economic optimism,boosts,Wall Street
2009-03-27T00:55:00Z,National Amusements theater auction,sparks,interest
2009-03-27T07:12:00Z,Bristol-Myers,settles,FTC probe
2009-03-27T13:26:00Z,euro,falls on,German warning
2009-03-27T14:36:00Z,U.S. consumers ' mood,improves in,March
2009-03-27T15:06:00Z,Johnson Controls,cut,jobs
2009-03-27T15:09:00Z,33 pct drop,in,first-quarter M&A
2009-03-27T15:09:00Z,Crisis,sparks,33 pct drop in first-quarter M&A
2009-03-27T15:09:00Z,M&A,leaps in,Q1
2009-03-27T15:09:00Z,State-backed M&A,leaps in,Q1
2009-03-27T15:09:00Z,Troubled auctions,highlight,hurdles
2009-03-27T15:09:00Z,auctions,highlight,hurdles to M&A
2009-03-27T15:09:00Z,bank,rescues,surge
2009-03-27T17:19:00Z,Tension,simmers,Lehman bonuses loom
2009-03-27T17:23:00Z,Morgan Stanley,recovers,ground in M&A rankings
2009-03-27T17:23:00Z,lost ground,in,M&A rankings
2009-03-27T19:26:00Z,Disney U.S. parks,cutting,jobs
2009-03-27T20:14:00Z,Jack Dreyfus,dies at,95
2009-03-27T20:43:00Z,Chrysler plant,aired with,Geithner
2009-03-27T20:57:00Z,Debt load,pushes Charter into,bankruptcy
2009-03-27T21:09:00Z,Obama,save,Detroit
2009-03-27T22:41:00Z,Google,hiring after,layoffs
2009-03-27T23:12:00Z,attending meeting,with,Obama
2009-03-27T23:56:00Z,Obama,for,economic plans
2009-03-27T23:56:00Z,Optimism,might,might misplaced
2009-03-28T04:18:00Z,U.S. banking market,set for,take-off
2009-03-28T04:18:00Z,U.S. mobile banking market,set for,take-off
2009-03-28T04:29:00Z,Las Vegas venture CityCenter,may file for,bankruptcy
2009-03-28T04:32:00Z,U.S.,lead on,deal
