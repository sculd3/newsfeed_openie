2008-01-16T00:09:00Z,legislation,stimulate,economy
2008-01-16T00:55:00Z,outlook,miss,estimates
2008-01-16T01:05:00Z,Apple,shows,high-def Apple TV
2008-01-16T01:05:00Z,Jobs,unveils,backup device
2008-01-16T01:05:00Z,high-def Apple TV,focus on,movies
2008-01-16T01:05:00Z,iTunes,rent,movies over Web
2008-01-16T08:00:00Z,Former Mitsubishi Motors head,convicted for,death
2008-01-16T08:57:00Z,Sony Ericsson,beats,forecasts
2008-01-16T10:10:00Z,Airbus,trails,Boeing
2008-01-16T11:27:00Z,Carlyle,in,talks buy
2008-01-16T11:42:00Z,Settlement,hopes,dim
2008-01-16T11:42:00Z,dim,in,Boeing-Airbus trade clash
2008-01-16T11:45:00Z,China,tells,banks
2008-01-16T11:45:00Z,banks,hold,cash
2008-01-16T11:51:00Z,Cathay,backs,Air China bid for China Eastern
2008-01-16T12:25:00Z,Rio Tinto,runs mines hard,BHP offer looms
2008-01-16T12:37:00Z,JPMorgan,takes,writedown
2008-01-16T13:08:00Z,Apple,unveils,Air laptop
2008-01-16T13:22:00Z,Wells Fargo,sees charge-offs in,2008
2008-01-16T14:07:00Z,Airbus,posts,record 2007 orders
2008-01-16T14:17:00Z,Icahn,supports,Oracle purchase
2008-01-16T14:20:00Z,Industrial output,unchanged in,December
2008-01-16T14:20:00Z,output,unchanged in,December
2008-01-16T14:46:00Z,Net inflows,rise to,highest
2008-01-16T14:46:00Z,inflows,rise to,highest
2008-01-16T14:47:00Z,Consumer prices,see,rise
2008-01-16T15:29:00Z,Customer satisfaction top U.S. issue,in,2008
2008-01-16T16:19:00Z,Home loan demand,surges to,to four-year high
2008-01-16T18:03:00Z,Bernanke,grilling on,fiscal stimulus stance
2008-01-16T18:03:00Z,Volcker,chides Fed for,bubbles
2008-01-16T18:11:00Z,Brocade ex-CEO,fined,$ 15 million
2008-01-16T18:25:00Z,Commerce Bancorp,sued by,founder Hill
2008-01-16T18:47:00Z,Brocade ex-CEO,sentenced to,21 months
2008-01-16T19:48:00Z,BEA,accepts,sweetened Oracle offer
2008-01-16T21:49:00Z,Market,ends on,Intel disappointment
2008-01-16T22:19:00Z,Brocade ex-CEO,sentenced to,21 months
2008-01-16T22:32:00Z,Citigroup,sees,$ 15 bln writedown
2008-01-16T22:48:00Z,JPMorgan consumer woes,hurt,profit
2008-01-16T22:48:00Z,consumer woes,hurt,profit
2008-01-16T23:00:00Z,Lawmakers,push for,economic stimulus plan
2008-01-16T23:13:00Z,Consumers,strained by,price surge year
2008-01-16T23:52:00Z,Delta,in,merger talks with Northwest
2008-01-16T23:52:00Z,merger talks,with,Northwest
2008-01-17T05:14:00Z,Economic stimulus,could pass,quickly-Rep
2008-01-17T05:46:00Z,S&P 500,fall as,Intel fuels economy fears
