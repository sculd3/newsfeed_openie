2009-05-11T07:10:00Z,U.S.,make,antitrust policy tougher
2009-05-11T11:11:00Z,AIG,expects,long restructuring process
2009-05-11T11:25:00Z,Soros,says,downward trend easing
2009-05-11T13:00:00Z,HSBC,says,first-quarter profits
2009-05-11T13:50:00Z,Intel 's antitrust battle,with,EU
2009-05-11T14:17:00Z,Fed,'s Lacker,Government safety net encouraged
2009-05-11T15:00:00Z,AstraZeneca,shares,leap
2009-05-11T15:00:00Z,its drug,beats,Plavix
2009-05-11T15:02:00Z,Nortel loss,deepens,revenue falls
2009-05-11T15:02:00Z,revenue,falls,37 percent
2009-05-11T17:41:00Z,Citi investors,withhold,votes
2009-05-11T17:45:00Z,Obama,tripling,U.S. FDIC 's borrowing power
2009-05-11T18:04:00Z,Microsoft,says,Windows 7 on track for holidays
2009-05-11T18:10:00Z,Goldman,in,$ 60 million
2009-05-11T18:10:00Z,Massachusetts,Goldman in,$ 60 million
2009-05-11T19:36:00Z,Oil,slips as,U.S. equities
2009-05-11T19:40:00Z,U.S. official,vows,tougher antitrust enforcement
2009-05-11T20:29:00Z,Obama,seeks,corporate loophole cuts
2009-05-11T20:36:00Z,Big U.S. banks,selling,stock
2009-05-11T20:36:00Z,U.S. banks,selling,stock
2009-05-11T20:41:00Z,Chrysler nonunion retirees,begin,fight for benefits
2009-05-11T20:41:00Z,Chrysler retirees,begin,fight
2009-05-11T20:41:00Z,U.S. interventions,may usher in,bond chill effect
2009-05-11T20:45:00Z,White House forecasts,higher,budget deficit
2009-05-11T21:01:00Z,Wall Street,drops on,profit-taking
2009-05-11T22:00:00Z,Ackman,says,Target proxy
2009-05-11T22:21:00Z,GM executives,dump stock,trading window opens
2009-05-11T23:40:00Z,Geffen,buy,NY Times stake
2009-05-11T23:47:00Z,Bernanke,says,response
2009-05-12T04:44:00Z,GM,settle Europe sale plan by,month-end
