2008-04-08T09:12:00Z,dollar,halve,profit
2008-04-08T09:12:00Z,weak dollar,halve,profit
2008-04-08T12:04:00Z,Dell CEO,sees,growth in 08
2008-04-08T12:04:00Z,healthy growth,in,08
2008-04-08T12:04:00Z,hybrid car buyers,in,U.S.
2008-04-08T12:04:00Z,improved profits,growth in,08
2008-04-08T12:10:00Z,shares,drop after,rare stumble
2008-04-08T13:01:00Z,First Marblehead,sinks,client goes
2008-04-08T13:01:00Z,big client,goes,bankrupt
2008-04-08T13:01:00Z,client,goes,bankrupt
2008-04-08T14:05:00Z,Housing crunch,boosts,apartment demand
2008-04-08T14:31:00Z,Pending home sales fall,in,February
2008-04-08T18:12:00Z,Credit losses,could approach,$ 1 trillion
2008-04-08T18:36:00Z,Comcast,asks,court
2008-04-08T18:36:00Z,court,reverse,FCC set-top box rule
2008-04-08T19:11:00Z,12 percent,in,quarter
2008-04-08T20:11:00Z,EIA,offers,grim oil supply picture
2008-04-08T21:11:00Z,Weak retail sales,may spur,warnings
2008-04-08T21:11:00Z,Weak sales,may spur,warnings
2008-04-08T21:11:00Z,retail sales,may spur,more warnings
2008-04-08T21:11:00Z,sales,may spur,more warnings
2008-04-08T21:27:00Z,U.S. rules,seen in,April
2008-04-08T21:46:00Z,American Air,cancels,flights for MD-80 inspections
2008-04-08T21:50:00Z,12 percent,in,quarter
2008-04-08T22:32:00Z,WaMu,gets,cuts jobs
2008-04-08T23:14:00Z,Citigroup,seen near,sale of bln loans
2008-04-08T23:21:00Z,Govt,should do,more
2008-04-08T23:21:00Z,U.S.,in,recession
2008-04-08T23:31:00Z,China sales,in,first qtr
2008-04-08T23:31:00Z,Ford,says,China sales up 47 pct in qtr
2008-04-09T04:14:00Z,U.S. car makers,repeat,green halo
