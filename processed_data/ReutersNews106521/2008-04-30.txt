2008-04-30T05:57:00Z,Sanofi Q1 oper profit,falls,7 pct
2008-04-30T06:32:00Z,Citibank,co. in,S.Korea
2008-04-30T06:32:00Z,holding co.,in,S.Korea
2008-04-30T07:35:00Z,ANALYST VIEW-BOJ,cuts,growth outlook
2008-04-30T08:29:00Z,China earnings outlook,regains,luster
2008-04-30T11:38:00Z,Time Warner Cable profit,beats,expectations
2008-04-30T11:52:00Z,BOJ,drops bias in,face of growth
2008-04-30T12:06:00Z,GM,posts,loss on strike
2008-04-30T13:06:00Z,INSTANT VIEW 4-Q1 GDP,surprise on,high side
2008-04-30T13:20:00Z,NYC economy,shrinks,straight month
2008-04-30T14:21:00Z,Australia,for,Origin
2008-04-30T14:31:00Z,P&G profit,helped by,cost controls
2008-04-30T14:33:00Z,Wachovia,may face bln charge after,court ruling
2008-04-30T14:44:00Z,Chicago April PMI,seen on,auto woes
2008-04-30T15:18:00Z,SEC,brings,civil charges against execs
2008-04-30T15:35:00Z,Kellogg 1st-quarter earnings,beat,estimates
2008-04-30T15:35:00Z,Kellogg earnings,beat,estimates
2008-04-30T16:56:00Z,Investors,see,recession
2008-04-30T17:16:00Z,Virgin Mobile,declines comment on,share rise
2008-04-30T17:42:00Z,Exxon,raises,second-quarter dividend
2008-04-30T18:25:00Z,Bear Stearns,sending,redundancy letters
2008-04-30T18:30:00Z,Consumer products companies,see,results
2008-04-30T18:58:00Z,BA,says in,talks
2008-04-30T18:58:00Z,talks,with,American
2008-04-30T19:00:00Z,Time Warner,split off,cable services
2008-04-30T19:35:00Z,Oil,falls on,U.S. crude inventory rise
2008-04-30T19:40:00Z,Federal Reserve,cuts,rate 1/4 point
2008-04-30T19:43:00Z,JPMorgan,sets,markets team
2008-04-30T20:14:00Z,Kellogg earnings,beat,stock dips
2008-04-30T20:45:00Z,Fed,approves discount rate cuts at,three banks
2008-04-30T20:47:00Z,Rockefellers,call for,change at Exxon Mobil
2008-04-30T21:02:00Z,Fed 's adjusted wording flags,pause in,rate cuts
2008-04-30T21:02:00Z,Fed 's wording flags,pause in,rate cuts
2008-04-30T21:02:00Z,Fed tone,may send higher,food prices
2008-04-30T21:05:00Z,risk,at debt,GMAC weakness
2008-04-30T21:17:00Z,Starbucks profit,falls,consumer weakens
2008-04-30T21:25:00Z,Sunoco,posts,Q1 loss on weak refining margins
2008-04-30T21:53:00Z,Glaxo,wins,FDA approval
2008-04-30T22:20:00Z,BlackRock 's Bob Doll,sees,end of Fed rate cuts
2008-04-30T22:43:00Z,Fed,signals,pause
2008-04-30T23:22:00Z,Wall Street,ends lower on,rate uncertainty
2008-04-30T23:25:00Z,hints cuts,may at,may end
2008-05-01T04:51:00Z,Dean,beat,estimates
2008-05-01T04:56:00Z,GM Wagoner,sees,tough quarter
