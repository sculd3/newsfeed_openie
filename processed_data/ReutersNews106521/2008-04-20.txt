2008-04-20T02:11:00Z,Venture capitalists,invest in,quarter
2008-04-20T03:15:00Z,China,to,economy
2008-04-20T03:15:00Z,Inflation,seen as,top risk China economy
2008-04-20T08:49:00Z,GM,sees,China sales
2008-04-20T08:49:00Z,Mercedes,takes SUV battle with,BMW
2008-04-20T09:54:00Z,Hankook Tire,mulls,price hike
2008-04-20T09:54:00Z,Nissan CEO,holds to,U.S. industry forecast
2008-04-20T12:54:00Z,Mercedes,overtake BMW in,China
2008-04-20T14:59:00Z,Jaguar Land Rover CEO,dies after,illness
2008-04-20T15:31:00Z,BMW China venture,plans to,triple capacity
2008-04-20T15:42:00Z,U.S. rails,seen,rolling
2008-04-20T15:59:00Z,Dow,nears,turnaround
2008-04-20T15:59:00Z,Earnings,are,key
2008-04-20T17:06:00Z,Growing world,needs,form of energy
2008-04-20T17:06:00Z,world,needs,form of energy
2008-04-20T17:43:00Z,Automakers,step up in,China
2008-04-20T17:43:00Z,BMW,plays,green card
2008-04-20T17:43:00Z,Eni CEO,sees,time for international oil firms
2008-04-20T17:43:00Z,Eni returns,with,oil
2008-04-20T17:43:00Z,Indonesia,sees,costly oil
2008-04-20T17:43:00Z,costly oil,stretching,fuel subsidy
2008-04-20T17:43:00Z,oil,stretching,fuel subsidy
2008-04-20T18:23:00Z,Earnings onslaught,offers,reality check
2008-04-20T23:08:00Z,Drivers,paying,record pump prices
2008-04-21T04:20:00Z,Ford China car JV,sees,2008 sales
