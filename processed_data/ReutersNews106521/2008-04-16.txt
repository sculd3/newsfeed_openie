2008-04-16T00:00:00Z,WaMu investor vote,favors,new chair
2008-04-16T06:16:00Z,McDonald 's U.S. sales,in,March
2008-04-16T07:07:00Z,SEC,step up,ratings scrutiny
2008-04-16T07:07:00Z,U.S. senator,asks,SEC
2008-04-16T10:38:00Z,Basel bank watchdog,plans,crackdown
2008-04-16T12:39:00Z,OPEC,cool,oil prices
2008-04-16T12:54:00Z,Home,starts,fall 11.9 pct
2008-04-16T12:57:00Z,China economy,slows,touch
2008-04-16T14:04:00Z,Tommy Hilfiger,look at,end 2009
2008-04-16T14:58:00Z,L'Oreal shares,plummet on,economy jitters
2008-04-16T15:54:00Z,JPMorgan net,falls,50 percent
2008-04-16T16:37:00Z,Microsoft,lend,more
2008-04-16T17:28:00Z,Many banks,dragging,their feet
2008-04-16T17:28:00Z,banks,dragging,their feet
2008-04-16T17:41:00Z,BlackRock shares,fall after,disappointing profit
2008-04-16T17:51:00Z,Judge,approves,Ford Explorer settlement
2008-04-16T18:13:00Z,Oil,hits,record
2008-04-16T18:38:00Z,Hedge fund manager Paulson,earns,$ 3.7 billion
2008-04-16T18:38:00Z,fund manager Paulson,earns,$ 3.7 billion
2008-04-16T18:48:00Z,Bank results,soothe,investors
2008-04-16T18:48:00Z,Hedge fund managers,set,payout records
2008-04-16T18:48:00Z,fund managers,set,new payout records
2008-04-16T19:07:00Z,Coca-Cola tops,estimates with,sales
2008-04-16T19:21:00Z,BlackRock,sees,dramatic consolidation
2008-04-16T19:27:00Z,U.S. economy,feels,bad
2008-04-16T20:16:00Z,EBay profit,rises,22 pct fueled
2008-04-16T20:24:00Z,IBM profit,rises on,software strength
2008-04-16T21:05:00Z,Stimulus plan,should boost,growth
2008-04-16T21:41:00Z,EBay,sees,results
2008-04-16T22:39:00Z,GM,revamps,marketing
2008-04-16T23:15:00Z,EBay profit,rises,CFO cautious on economy
2008-04-16T23:22:00Z,corporate profits,reassure,investors
2008-04-16T23:22:00Z,profits,reassure,investors
2008-04-16T23:37:00Z,IBM,sees,solid opportunities for acquisitions
2008-04-16T23:54:00Z,Wall St.,jumps,profits reassure
2008-04-17T04:17:00Z,Cheaper clothes,keep inflation in,check
2008-04-17T04:17:00Z,clothes,keep,inflation
