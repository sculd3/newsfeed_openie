2008-07-02T00:42:00Z,Toyota,meet,hybrid
2008-07-02T00:53:00Z,U.S.,wins,court order
2008-07-02T01:33:00Z,Wal-Mart,violated,Minnesota labour laws
2008-07-02T08:01:00Z,Auto sales,hit,low
2008-07-02T08:07:00Z,Nikkei,hits,longest losing streak
2008-07-02T08:22:00Z,Taylor Wimpey,raise,cash
2008-07-02T11:22:00Z,Microsoft,seeks,allies for new Yahoo move
2008-07-02T14:00:00Z,May construction spending,fell,0.4 percent
2008-07-02T14:09:00Z,$ 5.8 bln,in,Q2
2008-07-02T14:09:00Z,Analysts,see,Q2 writedowns for Citi
2008-07-02T14:09:00Z,Merrill,may write down,$ 5.8 bln
2008-07-02T14:57:00Z,Blockbuster,pulls,bid
2008-07-02T15:01:00Z,U.S.,facing,Q2
2008-07-02T15:24:00Z,UnitedHealth,lowers,outlook
2008-07-02T19:17:00Z,Paulson,wants,bank failures process
2008-07-02T20:40:00Z,Dow industrials,end session in,bear market
2008-07-02T20:40:00Z,INSTANT VIEW,end in,bear market
2008-07-02T20:55:00Z,GM debt protection costs,surge,record
2008-07-02T21:08:00Z,Oil,hits record on,U.S. inventory drop
2008-07-02T21:58:00Z,Airline pilots,downsizing,career change
2008-07-02T22:43:00Z,AMR,offset,fuel prices
2008-07-02T22:54:00Z,Dow 's bear market,run spells trouble for,Wall St.
2008-07-02T23:05:00Z,Dow,enters bear market as,stocks slide
2008-07-02T23:11:00Z,Mattel,had,morale
2008-07-02T23:15:00Z,Weather Channel sale,could come in,2 days
2008-07-02T23:40:00Z,Ex-hedge fund manager surrenders,sent to,New York
2008-07-02T23:40:00Z,fund manager surrenders,sent to,New York
2008-07-02T23:47:00Z,250 jobs,150 in,editorial
2008-07-02T23:47:00Z,L.A. Times,cut,150 in editorial
