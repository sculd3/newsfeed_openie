2008-06-24T00:50:00Z,United Airlines,lay off,950 pilots
2008-06-24T06:06:00Z,Rio iron ore deal,may sink,BHP price plan
2008-06-24T06:16:00Z,Syngenta,in,crop protection pact
2008-06-24T07:14:00Z,UK 's BG,makes,hostile Origin bid
2008-06-24T09:28:00Z,Barr,agree,oral contraceptive deal
2008-06-24T09:28:00Z,Bayer,agree,contraceptive deal
2008-06-24T09:46:00Z,Lone Star,cleared of,KEB unit price manipulation
2008-06-24T11:28:00Z,Paramount domestic ticket sales,pass,$ 1 billion
2008-06-24T11:28:00Z,Paramount ticket sales,pass,$ 1 billion
2008-06-24T11:54:00Z,UPS slashes,view on,fuel costs
2008-06-24T12:17:00Z,stake,in,Doha exchange
2008-06-24T12:29:00Z,SEC proposals,may diminish,credit ratings role
2008-06-24T13:37:00Z,Paulson,says,economy sound despite strains
2008-06-24T13:49:00Z,ConAgra,sees,quarterly profit
2008-06-24T14:11:00Z,Consumer confidence,falls in,June
2008-06-24T14:57:00Z,GM,may burn cash by,end of 2010
2008-06-24T15:31:00Z,Home prices,fall in,April
2008-06-24T15:52:00Z,WaMu,may face,bln credit losses
2008-06-24T15:53:00Z,Consumer confidence,tumbles in,June
2008-06-24T15:53:00Z,Household morale,drops in,U.S.
2008-06-24T17:03:00Z,Kroger 's discount push,lifts,profits
2008-06-24T17:18:00Z,AT&T,boost,content distribution
2008-06-24T17:20:00Z,Lowe 's Cos,gain,market share
2008-06-24T18:24:00Z,Kodak,sets,$ 1 billion stock buyback
2008-06-24T20:34:00Z,Market,drops as,UPS fan economic fears
2008-06-24T20:48:00Z,Fed,starts,policy meeting
2008-06-24T20:48:00Z,U.S. growth,rates on,hold
2008-06-24T20:48:00Z,rates,seen on,hold
2008-06-24T21:06:00Z,BlackRock,sees,slowdown
2008-06-24T21:06:00Z,global slowdown,worsening in,2009
2008-06-24T21:06:00Z,slowdown,worsening in,2009
2008-06-24T21:13:00Z,Delta,reach,tentative labor deal
2008-06-24T21:13:00Z,Northwest pilots,reach,labor deal
2008-06-24T21:19:00Z,Dow Chemical,sets,price hikes
2008-06-24T21:26:00Z,Circuit City,may have,several bidders
2008-06-24T21:35:00Z,Blockbuster shares,rise on,talk of deal bust
2008-06-24T21:41:00Z,Circuit City,may have,several bidders
2008-06-24T22:43:00Z,Lehman Brothers,brings back,former executives
2008-06-24T23:11:00Z,Olive Garden,sees,strong growth
2008-06-25T04:09:00Z,firms,should,should allowed
2008-06-25T04:55:00Z,BHP,may seek,iron ore price rise
