2013-01-02T01:04:00Z,South Korea,'s Hyundai,Kia
2013-01-02T01:04:00Z,South Korea 's Hyundai,expect,sales growth
2013-01-02T01:04:00Z,slowest sales growth,in,10 years
2013-01-02T01:23:00Z,House,vote on,Senate fiscal cliff bill
2013-01-02T01:44:00Z,Top House Republican,predicts,final passage of cliff bill
2013-01-02T04:52:00Z,Key points,in,bill passed by Congress
2013-01-02T10:26:00Z,Russia,allowing,grain exports
2013-01-02T10:39:00Z,Goldman Sachs executives,get shares on,New Year 's Eve
2013-01-02T10:39:00Z,New Year,on,Eve
2013-01-02T10:39:00Z,Top Goldman Sachs executives,get shares on,New Year 's Eve
2013-01-02T10:58:00Z,$ 1.1 billion stake,in,Canadian unit
2013-01-02T11:17:00Z,UK factory activity,rebounds in,December
2013-01-02T14:04:00Z,Small-business borrowing,rises in,November
2013-01-02T14:09:00Z,Global banks,rethink,Middle East model
2013-01-02T14:09:00Z,banks,rethink,Middle East model
2013-01-02T15:12:00Z,Construction spending posts,decline in,eight months
2013-01-02T15:12:00Z,Construction spending posts first decline,in,eight months
2013-01-02T15:15:00Z,ISM index 50.7,in,December
2013-01-02T16:14:00Z,DoubleLine,launches,stock management division
2013-01-02T16:59:00Z,Global factory activity,rises since,May
2013-01-02T16:59:00Z,factory activity,rises for,time
2013-01-02T18:03:00Z,IPOs,acquisitions of,venture-backed companies
2013-01-02T18:33:00Z,Euro zone factory slump,deepens,U.S.
2013-01-02T19:10:00Z,Car slump,in,France
2013-01-02T19:16:00Z,Senate,confirms Berner to,Treasury Office of Financial Research
2013-01-02T19:26:00Z,Judge,rejects,part of Apple App Store suit
2013-01-02T19:48:00Z,Manufacturing,ends up,2012
2013-01-02T21:08:00Z,economy,boosts,business
2013-01-02T21:08:00Z,slow economy,boosts,car-share business
2013-01-02T22:06:00Z,Wall Street,starts with,bang
2013-01-02T22:53:00Z,$ 2.5 billion deal,with,Buffett utility
2013-01-02T23:02:00Z,Budget deal,leaves U.S. defense sector in,limbo
2013-01-02T23:24:00Z,Goldman,accelerated ahead,share awards
2013-01-02T23:49:00Z,Bigger fights,loom deal after,fiscal cliff
2013-01-02T23:49:00Z,fights,loom after,cliff
2013-01-02T23:50:00Z,Global Hawk maker,skip,Paris air show
2013-01-02T23:50:00Z,Hawk maker,skip,Paris air show
2013-01-02T23:58:00Z,Global stocks,rise on,U.S. deal
2013-01-02T23:58:00Z,commodities,rise on,U.S. deal
2013-01-02T23:58:00Z,stocks,rise on,U.S. fiscal deal
2013-01-03T05:01:00Z,Apple,testing,iOS 7
