2008-03-03T00:39:00Z,BHP,invests,$ 975 million in South Africa coal project
2008-03-03T00:58:00Z,United Tech,makes,bid
2008-03-03T04:08:00Z,Bayer drugs unit,sees,double-digit Asia sales growth
2008-03-03T06:52:00Z,Oxiana,launches,agreed bid for Zinifex
2008-03-03T07:24:00Z,China,sees,hit
2008-03-03T07:55:00Z,small growth,in,Q1
2008-03-03T09:05:00Z,aims,bring MAN into,Scania alliance
2008-03-03T09:38:00Z,Xstrata,sees output growth in,2008
2008-03-03T09:53:00Z,VW building auto parts plant,in,northeast China
2008-03-03T10:08:00Z,Oxiana,launches,agreed bid for Zinifex
2008-03-03T10:32:00Z,Scania CEO-to,stay into,year
2008-03-03T12:27:00Z,S.Africa 's Merafe,declares,force majeure
2008-03-03T12:31:00Z,HSBC earner,paid near,$ 20 million
2008-03-03T12:31:00Z,HSBC top earner,paid near,$ 20 million
2008-03-03T12:47:00Z,dollar,tumble on,bank
2008-03-03T13:49:00Z,Buffett,sees,headwinds for rating agencies
2008-03-03T13:52:00Z,Investor AB,may use,Scania proceeds
2008-03-03T14:02:00Z,Countrywide,may face,more credit woes
2008-03-03T15:34:00Z,E Trade Chairman Layton,adds,CEO post
2008-03-03T16:07:00Z,Treasury 's Paulson,again backs,dollar
2008-03-03T16:07:00Z,institutions,need,capital
2008-03-03T16:09:00Z,bad debts,hit,$ 17 billion
2008-03-03T16:09:00Z,debts,hit,$ 17 billion
2008-03-03T16:40:00Z,Boeing,faces questions after,tanker loss
2008-03-03T17:59:00Z,Russia 's Usmanov,seen,building stake in Norilsk
2008-03-03T17:59:00Z,building stake,in,Norilsk
2008-03-03T19:24:00Z,GM posts drop,in,U.S. sales
2008-03-03T19:24:00Z,Honda posts,adjusted,1 pct
2008-03-03T19:24:00Z,gain,in,U.S. February sales
2008-03-03T19:37:00Z,Former press baron,begins,U.S. prison term
2008-03-03T19:37:00Z,Former press baron Black,begins,U.S. prison term
2008-03-03T19:55:00Z,Chrysler U.S. Feb sales,fall,14 pct unadjusted
2008-03-03T20:15:00Z,2nd-half rebound,in,U.S. auto market
2008-03-03T20:15:00Z,Toyota,sees,2nd-half rebound in U.S. auto market
2008-03-03T20:21:00Z,Thornburg Mortgage,plunges on,bankruptcy worry
2008-03-03T20:34:00Z,Citigroup,reorganizes,U.S. wealth management unit
2008-03-03T20:35:00Z,Oil,hits,new peak
2008-03-03T21:10:00Z,OPEC,be moved by,oil
2008-03-03T22:46:00Z,Fannie,agree to,appraisal standards
2008-03-03T22:46:00Z,Freddie,agree to,appraisal standards
2008-03-03T23:28:00Z,U.S. gasoline price,nears,record
2008-03-03T23:28:00Z,U.S. retail gasoline price,nears,record
2008-03-04T05:11:00Z,Whitman,holds,firm on MBIA
2008-03-04T05:13:00Z,Market woes,warrant,rates
2008-03-04T05:56:00Z,Buffett,says,U.S.
2008-03-04T05:56:00Z,U.S.,in,recession
