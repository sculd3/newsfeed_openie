2007-10-08T00:15:00Z,10 pct stake,in,Winton
2007-10-08T00:15:00Z,Goldman fund,buys,10 pct stake in Winton
2007-10-08T05:03:00Z,N.Rock,stay,independent
2007-10-08T07:57:00Z,Apollo,interested in,Northern Rock
2007-10-08T07:57:00Z,Blackstone,interested in,Northern Rock
2007-10-08T08:17:00Z,Northern Rock,climbs on,report of further suitors
2007-10-08T08:43:00Z,BNP Paribas,grows,Asia onshore private banking
2007-10-08T11:33:00Z,Julius Baer,has,targets
2007-10-08T12:20:00Z,U.S. downturn,worries,ease among investors
2007-10-08T12:25:00Z,bigger share,in,India
2007-10-08T14:13:00Z,Business Objects,was,approached
2007-10-08T15:36:00Z,UAW,sets,Wednesday deadline
2007-10-08T15:39:00Z,Pfizer,decline,comment on stake talk
2007-10-08T15:39:00Z,Sanofi,decline,comment on stake talk
2007-10-08T16:55:00Z,China 's Minsheng Bank,buy into,UCBH
2007-10-08T18:04:00Z,EU,deepens,probe
2007-10-08T18:24:00Z,Broadcom shares,rise on,Samsung shipment
2007-10-08T18:55:00Z,Big write-downs,loom at,JPMorgan
2007-10-08T18:55:00Z,write-downs,loom at,JPMorgan
2007-10-08T19:30:00Z,UAW,sets,Wednesday deadline
2007-10-08T19:30:00Z,Wednesday deadline,in,Chrysler talks
2007-10-08T19:33:00Z,Chevron,seeks,dismissal
2007-10-08T20:18:00Z,Connecticut,sues,Marsh unit
2007-10-08T20:20:00Z,Oil,falls,nearly 3 percent
2007-10-08T21:42:00Z,Google shares,cross threshold for,time
2007-10-08T21:46:00Z,Dow,end down with,oil stocks
2007-10-08T21:46:00Z,S&P,end down with,oil stocks
2007-10-08T21:51:00Z,Yum Brands posts,boosts,forecast
2007-10-08T23:09:00Z,U.S. funds,made,money
2007-10-08T23:09:00Z,U.S. hedge funds,made money in,September
2007-10-09T04:32:00Z,Vonage,settle,patent dispute
2007-10-09T04:39:00Z,SAP,defends,price offered
2007-10-09T04:39:00Z,price,offered for,Business Objects
2007-10-09T04:40:00Z,Ryder,cites,market
2007-10-09T04:52:00Z,Reuters,expects,ruling on Thomson deal by Jan
2007-10-09T04:54:00Z,RBS trio,has,86 percent of ABN
