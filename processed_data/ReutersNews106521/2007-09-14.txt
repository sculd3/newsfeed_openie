2007-09-14T03:43:00Z,Humberto,shuts,3 refineries
2007-09-14T05:42:00Z,AWB,says,U.S. class action filed
2007-09-14T07:17:00Z,new factory,in,Japan
2007-09-14T07:40:00Z,Campbell,seeking,$ 1.5 bln for Godiva
2007-09-14T07:42:00Z,South Korea,sends,Intel antitrust statement
2007-09-14T09:36:00Z,Cadbury,rejects drinks bid due to,financing
2007-09-14T10:49:00Z,U.S.-China trade tensions,flare in,WTO disputes
2007-09-14T12:13:00Z,Savers,pull,funds
2007-09-14T12:51:00Z,TD Ameritrade,finds,breach of database
2007-09-14T13:06:00Z,AnnTaylor,roll out line,week
2007-09-14T13:16:00Z,Dow Jones union,says close to,contract deal
2007-09-14T13:39:00Z,UAW,sets,Friday deadline
2007-09-14T14:06:00Z,News Corp,expects,tough Apple negotiations
2007-09-14T14:57:00Z,Beazer,asks,court
2007-09-14T15:16:00Z,iPhone,headed for,Germany
2007-09-14T15:41:00Z,Lilly,gets FDA nod on,new osteoporosis drug use
2007-09-14T16:35:00Z,UAW singles,rallies for,possible strike
2007-09-14T17:25:00Z,UAW,girds workers for,possible strike against GM
2007-09-14T17:47:00Z,China,with,pace of change
2007-09-14T17:47:00Z,Paulson,says,impatient
2007-09-14T18:19:00Z,Merrill,warns of,asset value cuts
2007-09-14T18:57:00Z,Bank,wins,Fed OK of LaSalle purchase
2007-09-14T19:50:00Z,supply concerns,spur,record
2007-09-14T22:34:00Z,Icahn,boosts,BEA stake
2007-09-15T04:09:00Z,Northern Rock share plunge,turns,lender
2007-09-15T04:59:00Z,Oil spike,undermine,airline recovery
