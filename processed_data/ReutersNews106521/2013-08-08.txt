2013-08-08T00:20:00Z,revenue,exceeds,estimates
2013-08-08T00:40:00Z,JPMorgan,faces,probes
2013-08-08T02:03:00Z,Hilton,selects,banks
2013-08-08T02:03:00Z,banks,lead,initial offering
2013-08-08T07:39:00Z,Costco July sales,miss,estimates
2013-08-08T07:39:00Z,Costco July same-store sales,miss,estimates
2013-08-08T07:40:00Z,Italy parliament,approves,youth jobs measures
2013-08-08T07:41:00Z,BOJ 's Kuroda,easing up on,fiscal discipline
2013-08-08T08:02:00Z,China commodities imports,rebound in,July
2013-08-08T08:02:00Z,Volatile China commodities imports,rebound in,July
2013-08-08T08:15:00Z,Unexpected strength,eases,gloom
2013-08-08T08:15:00Z,strength,eases,gloom
2013-08-08T10:42:00Z,Greek unemployment,hit record in,May of 27.6 percent
2013-08-08T10:42:00Z,unemployment,hit record in,May
2013-08-08T11:48:00Z,Apollo 's earnings,soar on,higher fund values
2013-08-08T11:48:00Z,Apollo 's second-quarter earnings,soar on,higher fund values
2013-08-08T12:14:00Z,Rio,sell,aluminium
2013-08-08T12:55:00Z,Herbalife,gets,license sell in parts of China
2013-08-08T14:32:00Z,$ 720 million,in Conoco,Imperial
2013-08-08T14:32:00Z,Conoco,in,$ 720 million
2013-08-08T15:00:00Z,third time,in,seven months
2013-08-08T15:22:00Z,Groupon shares,leap,comeback effort accelerates
2013-08-08T15:26:00Z,Russia 's Megafon,in,ahead wireless race with $ 1.2 billion
2013-08-08T15:26:00Z,wireless race,with,$ 1.2 billion
2013-08-08T19:32:00Z,first subscriber growth,in,four years
2013-08-08T19:45:00Z,Carl Icahn,increases Nuance stake to,16 percent
2013-08-08T20:33:00Z,T.Rowe slashes stake,in,Dell
2013-08-08T20:43:00Z,Fed officials,cutting,QE3
2013-08-08T21:16:00Z,Borrowers,can sue Wells Fargo over,mortgage modifications
2013-08-08T21:45:00Z,U.S.,slowly opening up,drone industry
2013-08-08T21:50:00Z,Fannie Mae,sending,$ 10.2 billion
2013-08-08T21:52:00Z,CEO,calls for,strategy
2013-08-08T22:35:00Z,Wall Street,end,three-day drop as Microsoft gains
2013-08-08T22:59:00Z,Ackman,pushes,J.C. Penney
2013-08-08T22:59:00Z,J.C. Penney,speed up,CEO search
2013-08-08T23:29:00Z,Fine,must,must cut
2013-08-08T23:55:00Z,Time Warner Cable,barbs on,day seven of blackout
2013-08-09T04:13:00Z,Hillshire,gives outlook on,costs
