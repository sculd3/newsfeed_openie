2009-08-13T05:21:00Z,Citi,hires,external help for management review
2009-08-13T10:20:00Z,UBS shares,climb on,U.S. tax case deal
2009-08-13T11:20:00Z,Opel,seal,deal
2009-08-13T12:56:00Z,U.S. retail sales,unexpectedly fall in,July
2009-08-13T12:56:00Z,U.S. sales,fall in,July
2009-08-13T14:06:00Z,Machinists union,seeks,Delta Air Lines election
2009-08-13T15:46:00Z,Walmart 's flat profit,beats,estimates
2009-08-13T15:46:00Z,Walmart 's profit,beats,estimates
2009-08-13T17:00:00Z,Lawsuit claims Textron,misled investors on,backlog
2009-08-13T17:30:00Z,U.S. food companies,seek,sugar quotas
2009-08-13T17:32:00Z,We,goofed on,derivative risks
2009-08-13T19:29:00Z,Fed,bought net in,latest week
2009-08-13T19:30:00Z,Oil,tracking,Wall Street
2009-08-13T20:02:00Z,CFTC,proposes,new account class for OTC derivatives
2009-08-13T20:08:00Z,UBS,still faces,hurdles
2009-08-13T20:08:00Z,UBS tax deal,forged in,sweat-drenched talks
2009-08-13T20:30:00Z,claims,rise,Retail sales fall
2009-08-13T20:30:00Z,jobless claims,rise,sales fall
2009-08-13T20:30:00Z,new claims,rise,Retail sales fall
2009-08-13T20:30:00Z,new jobless claims,rise,sales fall
2009-08-13T20:49:00Z,Nordstrom,meets,Street
2009-08-13T20:54:00Z,U.S. data,ignored by,stocks
2009-08-13T20:54:00Z,Weak U.S. data,embraced by,bonds
2009-08-13T21:26:00Z,Ford,raises output as,clunker sales surge
2009-08-13T21:47:00Z,Magna,agrees over,Opel
2009-08-13T22:35:00Z,Bank,ends,arbitration
