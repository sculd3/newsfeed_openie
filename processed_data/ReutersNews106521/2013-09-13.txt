2013-09-13T00:56:00Z,U.S. judge,hears chorus,credit card pact
2013-09-13T01:26:00Z,Lockheed Martin,wins,contract
2013-09-13T02:37:00Z,KKR mulls,teaming with,Japan state fund
2013-09-13T05:50:00Z,UK recovery,may,may kiss of death for zombie firms
2013-09-13T06:19:00Z,South Africa union,building strike with,12 percent wage deal
2013-09-13T06:39:00Z,KPN,holds,talks
2013-09-13T06:39:00Z,talks,with,America Movil
2013-09-13T07:09:00Z,Malaysia 's 1MDB,lifts,IPO target
2013-09-13T07:16:00Z,Ex-RBS trader,sentenced in,jail
2013-09-13T07:23:00Z,Portugal,should stick to,deficit reduction goals
2013-09-13T07:35:00Z,Japan ministers,disagree on,sales tax
2013-09-13T07:48:00Z,KKR mulls,teaming with,Japan state fund
2013-09-13T07:55:00Z,Chinese authorities,probing,German drugmaker Bayer
2013-09-13T07:55:00Z,authorities,probing,German drugmaker Bayer
2013-09-13T08:14:00Z,Spain house prices,fall at,pace
2013-09-13T08:14:00Z,end-2010,in,Q2
2013-09-13T08:20:00Z,Spain 's debt,hits,92.2 percent of GDP
2013-09-13T08:32:00Z,VW group sales,flat in,August
2013-09-13T09:06:00Z,Euro zone employment,falls in,second quarter
2013-09-13T09:11:00Z,Japan exports,in,three years
2013-09-13T10:02:00Z,Germany,oppose Commission as,future bank resolution body
2013-09-13T10:12:00Z,asset management,ranks as,Fusenig exits
2013-09-13T10:39:00Z,Equity issuance,may reflect,growth picture
2013-09-13T10:44:00Z,Fiat CEO,sees,Chrysler IPO
2013-09-13T13:01:00Z,Germany,role in,bank decisions
2013-09-13T13:01:00Z,query Commission 's planned role,in,bank decisions
2013-09-13T13:57:00Z,U.S. consumer sentiment,sinks on,interest rate fears
2013-09-13T13:58:00Z,Perella fund,sell,rail leasing company
2013-09-13T15:34:00Z,Occidental,seeks buyers for,40 percent
2013-09-13T17:41:00Z,U.S. retail sales,point to,soft economy
2013-09-13T17:59:00Z,Kerimov,selling,$ 3.7 billion Uralkali stake
2013-09-13T18:50:00Z,Passive fund manager Vanguard,turns activist in,board votes
2013-09-13T18:50:00Z,fund manager Vanguard,turns,activist
2013-09-13T20:06:00Z,Honeywell,on,Intermec purchase
2013-09-13T20:06:00Z,U.S.,puts,conditions
2013-09-13T20:11:00Z,Vornado,'s chairman,Roth
2013-09-13T20:11:00Z,Vornado 's chairman,resigns from,J.C. Penney board
2013-09-13T20:22:00Z,SAC Capital investment staff,jumping to,rival firm
2013-09-13T20:35:00Z,Twitter,takes,first step going
2013-09-13T20:48:00Z,Stocks,rise,gold in worst week
2013-09-13T21:00:00Z,Icahn,sells,remaining stake
2013-09-13T21:00:00Z,remaining stake,in,Hain Celestial
2013-09-13T21:01:00Z,Miami hospital operator,settles with,SEC
2013-09-13T21:02:00Z,he,following,orders
2013-09-13T21:27:00Z,PNC Chairman Rohr,leave,BlackRock board
2013-09-13T21:37:00Z,Bankruptcy court,approves,ResCap 's $ 596.5 million deal
2013-09-13T21:37:00Z,Twitter,adds banks to,underwriting lineup
2013-09-13T21:50:00Z,Fed,causing,market tantrum
2013-09-13T21:54:00Z,IRS,broadens,scope
2013-09-13T22:24:00Z,Bombardier,targets,Monday
2013-09-13T22:28:00Z,Ackman,cuts,stake in mall owner General Growth Properties
2013-09-13T22:28:00Z,stake,in,mall owner General Growth Properties
2013-09-13T22:51:00Z,IPO,puts investors for,now
2013-09-13T22:51:00Z,Twitter,puts investors in,dark
2013-09-13T23:20:00Z,Sanofi,could acquire,L'Oreal 's stake
2013-09-13T23:55:00Z,Twitter,may kickstart,consumer-tech IPO train
2013-09-13T23:57:00Z,BlackBerry bidders,carve up,business
2013-09-14T04:08:00Z,UAW,represent,Tennessee workers
