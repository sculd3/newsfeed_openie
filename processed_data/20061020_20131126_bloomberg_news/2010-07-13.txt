2010-07-13T00:12:44Z,Professor,Writes in,Daily
2010-07-13T00:15:33Z,Pimco 's Fisher,Says,Avoid Euro Periphery Countries
2010-07-13T00:20:23Z,CNPC,Increase,Crude Oil
2010-07-13T00:20:23Z,Natural Gas Supplies,in,Second
2010-07-13T00:31:23Z,Neff Wins Court Approval,Vote Over,Objections
2010-07-13T00:42:44Z,Talks,with,United Nations Command
2010-07-13T00:53:59Z,Alumina Shares Gain,Tops,Profit Projections
2010-07-13T00:56:46Z,BP,Completes,Installation of New Sealing Cap on Well Spilling Oil in Gulf
2010-07-13T00:56:46Z,Installation,in,Gulf
2010-07-13T01:00:00Z,Russian Spies Might,Sell,Tips
2010-07-13T01:00:00Z,Spies Might,Sell Tips to,Capitalists
2010-07-13T01:00:15Z,Order,Blocking,Assets
2010-07-13T01:05:56Z,Pfizer Medicine Price-Fixing Suit,Reinstated by,California High Court
2010-07-13T01:07:47Z,Global Recovery Signs,Spur,Risk-Taking
2010-07-13T01:07:47Z,Korean Won,Rises as,Alcoa Earnings
2010-07-13T01:07:47Z,Recovery Signs,Spur,Risk-Taking
2010-07-13T01:16:24Z,More Consolidation,in,Steel
2010-07-13T01:30:00Z,Australian Business Confidence Little Changed,Declines in,June
2010-07-13T01:34:45Z,Rubber Futures,Extend,Slump
2010-07-13T01:49:57Z,China,in Slow,Center
2010-07-13T01:49:57Z,Livestock Feed Production Growth May Slow,in,China
2010-07-13T01:53:24Z,Cavaliers Owner Gilbert,Gets Fine for,Comments
2010-07-13T02:11:54Z,Energy Projects,in,Africa
2010-07-13T02:38:12Z,North Korea,Cancels Talks,Sinking
2010-07-13T02:38:12Z,South,of,Warship
2010-07-13T02:39:39Z,Vedanta Resources ' Credit Outlook,Raised at,Fitch
2010-07-13T02:45:19Z,0.3 %,Drops in,Four Days
2010-07-13T02:45:19Z,Drops,in,Four Days
2010-07-13T02:48:25Z,Executive Director Horii,Joins,Canon Research Group
2010-07-13T02:49:41Z,Dubai Crisis,Prompts,Sukuk Holders
2010-07-13T03:10:53Z,Delay,in,Receiving Engine Parts
2010-07-13T03:18:43Z,Senate Conservatives Restore Canada,'s Legislation,Avoid Showdown
2010-07-13T03:24:30Z,Australian Federal Police,Seize,240 Kilograms
2010-07-13T03:24:30Z,Federal Police,Seize,240 Kilograms of Cocaine
2010-07-13T03:34:22Z,Decline,Cuts,Recommendation
2010-07-13T03:34:22Z,Recommendation,Decline After,UBS AG
2010-07-13T03:38:12Z,Japan Needs,Discuss Tax Changes After,Even Election Defeat
2010-07-13T03:38:12Z,Noda,Says,Japan Needs
2010-07-13T03:51:01Z,Concern China,on,Economic Growth May
2010-07-13T03:53:07Z,Shares Slump,in,Sydney
2010-07-13T03:53:47Z,Asian Stocks Fall,Gains on,China Real-Estate Policy
2010-07-13T03:54:08Z,20 China Cities Drops,in Supply,China Index Academy
2010-07-13T03:54:08Z,Land Supply,in,20 China Cities Drops
2010-07-13T04:00:00Z,European Job Advertisements Advance,in,16 Months
2010-07-13T04:00:00Z,Highest,to Advance,Monster
2010-07-13T04:00:00Z,Lowest Trading Obstacle,in,U.S.
2010-07-13T04:00:00Z,Trade Deficit,Narrowed,Oil Prices Declined
2010-07-13T04:00:01Z,Seller 's Researcher,Wrote,Attack
2010-07-13T04:00:01Z,Short Seller 's Researcher,Wrote,Attack on For-Profit Colleges
2010-07-13T04:00:30Z,Bank Credit,Delay of,Rebates
2010-07-13T04:00:30Z,Minnesota Cash-Flow Plan,Involves,Delay of Rebates
2010-07-13T04:01:00Z,Ape Testicles,Geezers in,Quest
2010-07-13T04:01:00Z,California Wine-Tasting Room,Aiding,Charity
2010-07-13T04:01:00Z,Ma Peche,Brings Momofuku With,Steak
2010-07-13T04:01:00Z,Red Wine Aid Geezers,in,Quest
2010-07-13T04:01:01Z,Riviera Holdings,Files for,Bankruptcy Protection
2010-07-13T04:21:06Z,$ 18.5 Billion,in,Clean Energy Projects
2010-07-13T04:47:18Z,Taiwan 's Stabilization Fund,Sold,Shares
2010-07-13T04:57:24Z,Farmers,Expect,Drought
2010-07-13T04:57:24Z,Russian Farmers,Expect Drought to,Cost Them
2010-07-13T05:02:42Z,Planning,Acquire,Business Day Reports
2010-07-13T05:04:02Z,Japanese Consumer Confidence Advanced,in,June
2010-07-13T05:11:02Z,Export,to BMW,FTD
2010-07-13T05:14:15Z,Putin,Extends Discount Amid,Drought
2010-07-13T05:15:36Z,Palm Oil Drops,in,Four Days on Concern Supplies to Rise
2010-07-13T05:21:22Z,Egypt Finance Ministry Aims,in,5 Years
2010-07-13T05:30:44Z,Food Prices,in,South Africa May Rise on Bad Weather
2010-07-13T05:32:51Z,Shell 's Retail Fuel Outlets,in,India
2010-07-13T05:32:53Z,China,on,Growth Concerns
2010-07-13T05:44:18Z,$ 147 Million Development,With,Sahel
2010-07-13T05:53:51Z,Latin America,Give,45 % of Santander Profit
2010-07-13T05:53:51Z,Luzon,Tells,El Pais
2010-07-13T06:00:22Z,CEO Garrone,Tells,Il Sole
2010-07-13T06:00:22Z,ERG May Exercise,Put Option on,Priolo Refinery
2010-07-13T06:04:50Z,African Minerals,Gets,$ 1.5 Billion Project Investment
2010-07-13T06:04:50Z,Minerals,Gets,$ 1.5 Billion Project Investment From Shandong Iron
2010-07-13T06:07:29Z,Komatsu,Raises,Forecast
2010-07-13T06:26:59Z,China Landslides,Kill People at,Least 16
2010-07-13T06:26:59Z,Western China Landslides,Kill,People
2010-07-13T06:33:32Z,Access Bank,With,Africa Finance Corp
2010-07-13T06:42:36Z,Its,Option,MF Reports
2010-07-13T06:42:36Z,Telecom Italia May,Drop,MF Reports
2010-07-13T06:48:20Z,Komatsu,Raises,First-Half Net Forecast
2010-07-13T06:58:06Z,Japan Pension Fund Becomes Net Bond Seller,in,Risk for Government Finances
2010-07-13T06:59:54Z,Sappi South Africa Sells Kangas Mill Buildings,in,Finland Back
2010-07-13T07:00:00Z,Economy,Emerged From,Recession
2010-07-13T07:00:00Z,Inflation,Accelerated in,June
2010-07-13T07:00:00Z,Spanish Inflation,Accelerated,Economy Emerged
2010-07-13T07:05:55Z,Talks,for Push,Group
2010-07-13T07:13:11Z,Abu Dhabi,at,Venture
2010-07-13T07:13:11Z,Masdar Announces Second Senior Resignation,With,MIT
2010-07-13T07:13:27Z,989,to Triples,Demand
2010-07-13T07:14:47Z,BP,Surges to,One-Month High
2010-07-13T07:17:07Z,Lowers Portugal,as,Rating
2010-07-13T07:19:13Z,Sasol Competition Settlement,be,Decided
2010-07-13T07:23:39Z,China,as,Equities Decline
2010-07-13T07:24:30Z,Outlook,Is,Stable
2010-07-13T07:27:14Z,European Stocks,Pare Gains After,Moody 's Downgrades Portugal Credit Rating
2010-07-13T07:27:14Z,Moody,After,Downgrades Portugal Credit Rating
2010-07-13T07:27:14Z,Stocks,Pare Gains After,Moody 's Downgrades Portugal Credit Rating
2010-07-13T07:27:28Z,China,Enforce,Curbs on Real Estate
2010-07-13T07:27:28Z,Copper Declines,in,London
2010-07-13T07:27:54Z,Deep-Water Drilling,in,Black Sea
2010-07-13T07:29:55Z,Euro,Extends Decline After,Moody 's Downgrades Portugal Debt
2010-07-13T07:29:55Z,Moody,After,Downgrades Portugal Debt
2010-07-13T07:40:04Z,KB Financial 's New Chairman Euh Laments Cost,Vows,Reductions
2010-07-13T07:41:53Z,Tesco,Make,Lowball Bid For Carrefour Assets
2010-07-13T07:49:23Z,Zijin,Closes,Copper Smelter
2010-07-13T07:57:41Z,Infosys,Leads,Indian Software Stocks Lower After Earnings Miss Estimates
2010-07-13T08:00:22Z,Exports,Climbed at,Faster Pace
2010-07-13T08:00:22Z,Philippine Exports,Climbed in,May
2010-07-13T08:10:17Z,China,Korea for,First Time
2010-07-13T08:10:17Z,May,Buy Steel Plates From,South Korea
2010-07-13T08:10:23Z,Hungarian Inflation,Unexpectedly Accelerated on,Food
2010-07-13T08:10:23Z,Inflation,Accelerated in,June
2010-07-13T08:18:55Z,One-Month Low,to Drops,Thai Supply
2010-07-13T08:18:55Z,Third Day,for Drops,Thai Supply
2010-07-13T08:20:41Z,Economy,Represented,as Much as 17.5 % of GDP
2010-07-13T08:20:41Z,Italian Economy,Represented,as Much as 17.5 %
2010-07-13T08:20:41Z,Italian Underground Economy,Represented,as Much as 17.5 % of GDP
2010-07-13T08:20:41Z,Underground Economy,Represented,as Much as 17.5 % of GDP
2010-07-13T08:21:57Z,Hong Kong High-Speed Railway May,Be,Completed Early
2010-07-13T08:26:47Z,13 Cents,to Slump,F.O. Licht
2010-07-13T08:38:47Z,EU-Skeptic Vaclav Klaus,Says,Euro
2010-07-13T08:38:47Z,Euro,Survive Debt Crisis at,Extreme
2010-07-13T08:38:47Z,Vaclav Klaus,Says,Euro
2010-07-13T08:46:29Z,Budd,Denies,Budget Office Came
2010-07-13T08:49:06Z,U.K. Inflation,Declining Than,Forecast
2010-07-13T08:49:44Z,Double Capacity,to Plans,Overseas Demand
2010-07-13T08:53:34Z,South Africa 's Rand,Strengthens Against,Snapping Two-Day Fall
2010-07-13T08:54:49Z,Russia Health Official,Urges Siesta Regime for,Workers
2010-07-13T08:54:49Z,Workers,in,Record Heat Wave
2010-07-13T08:55:47Z,Crude Oil Futures,Erase Losses in,New York
2010-07-13T08:57:30Z,City,in,East
2010-07-13T08:57:30Z,Construction,in,City 's East
2010-07-13T08:57:30Z,Jerusalem Committee,Approves,Construction
2010-07-13T09:04:18Z,AngloGold,Says,Suspension at Its Tau Lekoa Mine Lifted
2010-07-13T09:08:46Z,Donald Trump Eyes Profit,Golf Including,Casinos
2010-07-13T09:12:12Z,Spain 's Bank Rescue Fund,Is Put for,Downgrade by Moody 's
2010-07-13T09:12:58Z,BP Shares Advance,Leaking,Gulf of Mexico Oil Well
2010-07-13T09:20:51Z,BMW,Raises,2010 Earnings
2010-07-13T09:21:35Z,Zijin,Closes,Copper Smelter
2010-07-13T09:21:45Z,Add,500 in,India
2010-07-13T09:21:45Z,Airbus Vendor Symphony,Add,500 in India
2010-07-13T09:22:08Z,Yuan Forwards,Weaken as,Europe Debt Crisis Bolster Dollar
2010-07-13T09:29:24Z,Gulf,in,2010 2011
2010-07-13T09:29:24Z,Mexico Project Delays Will Cut U.S. Output,of Gulf,IEA
2010-07-13T09:31:12Z,El Paso Discover Natural Gas,in,Offshore Espirito Santo Well
2010-07-13T09:31:12Z,Petrobras,Gas in,Offshore Espirito Santo Well
2010-07-13T09:32:45Z,Bonds,Drop as,Auctions Bills
2010-07-13T09:32:45Z,German Bonds,Drop as,Auctions Bills at Lower Rate
2010-07-13T09:35:52Z,SEB,Swings in,Quarter
2010-07-13T09:39:33Z,China 's Prime Office-Property Market,Recovers as,Economy Fuels Investment
2010-07-13T09:44:35Z,Typhoon Conson,Strengthens Off,Philippines
2010-07-13T09:46:15Z,Threadneedle,Hires,Legal 's Burgess
2010-07-13T09:52:06Z,Pakistan-Australia Cricket Test,Is,Delayed
2010-07-13T09:52:06Z,at Lord,in,London
2010-07-13T10:00:00Z,White House,Invites CEO Proposals on,Taxes
2010-07-13T10:02:13Z,Cofco,Buys,Bringing Total to 14
2010-07-13T10:02:13Z,One More U.S. Corn Cargo,Total to,14
2010-07-13T10:03:13Z,Rose 1.3 Percent,in,June
2010-07-13T10:04:37Z,Citigroup,HSBC For,Foreign-Currency Bond Sale
2010-07-13T10:04:37Z,State Bank,Hires,HSBC For Foreign-Currency Bond Sale
2010-07-13T10:06:31Z,75 %,in,Five Years
2010-07-13T10:08:48Z,Forecast,in,June on Gas
2010-07-13T10:08:48Z,June,in Forecast,Food Drop
2010-07-13T10:09:30Z,Asia,on Rise,Beating Estimates
2010-07-13T10:10:56Z,New Base,in,Darfur Region
2010-07-13T10:11:57Z,Palm,Imports by,India
2010-07-13T10:18:26Z,National Bank,Rises,10 %
2010-07-13T10:20:08Z,Harel Freres,Makes,Offer
2010-07-13T10:27:12Z,China,Reiterates,Policies
2010-07-13T10:30:00Z,Pennsylvania Governor,on,Race
2010-07-13T10:30:00Z,Republican Corbett,Leads Onorato in,Poll
2010-07-13T10:41:50Z,$ 107 Million,in,Share Sale
2010-07-13T10:41:50Z,Era Infra May,Raise,$ 107 Million in Share Sale
2010-07-13T10:42:40Z,IEA Forecasts World Crude Oil Demand Growth,Will Slow,1.6 %
2010-07-13T10:43:53Z,Fields,in,First Test
2010-07-13T10:43:53Z,First Test,in,London
2010-07-13T10:43:53Z,Pakistan,Wins,Fields Against Australia in First Test in London
2010-07-13T10:43:53Z,Toss,Fields Against,Australia
2010-07-13T10:45:40Z,Kenya Commercial Bank,Says,17 %
2010-07-13T10:47:06Z,Intesa Sanpaolo Chief Passera,Backs,EU 's Proposal
2010-07-13T10:52:28Z,Court News,in Total,Update1
2010-07-13T10:52:28Z,Goldman,Total in,Court News
2010-07-13T10:52:28Z,Total,in,Court News
2010-07-13T10:54:08Z,Bank,Expand,Wealth Unit in U.K.
2010-07-13T10:54:08Z,Capital Markets,Unit in,U.K.
2010-07-13T10:54:08Z,RBC 's Chief Nixon,Says,Bank
2010-07-13T10:54:08Z,Wealth Unit,in,U.K.
2010-07-13T10:55:39Z,Slovakia 's New Government,Opposes,Contributing
2010-07-13T11:00:10Z,Macquarie Group,Hires,Six Senior Traders for Equity Business
2010-07-13T11:03:10Z,Foxconn,Urges,Hong Kong Judge
2010-07-13T11:03:10Z,Hong Kong Judge,Throw Out,Conspiracy Suit Filed
2010-07-13T11:04:25Z,Its,Identity,Polish Regulator
2010-07-13T11:17:46Z,BMW,Raises,2010 Earnings
2010-07-13T11:18:50Z,England 's Ian Bell,Is Ruled Out of,Pakistan Test Series
2010-07-13T11:30:00Z,Small-Business Confidence,in,U.S. Drops to Three-Month Low
2010-07-13T11:32:20Z,Outlook,Is,Stable
2010-07-13T11:33:51Z,Afghan Soldier,Kills,Three U.K. Soldiers
2010-07-13T11:37:17Z,Credit Agricole Foreign-Exhange Strategist Stuart Bennett,Has Left,Bank
2010-07-13T11:41:36Z,Calabria,on,Ndrangheta Mob Network
2010-07-13T11:41:36Z,Italy Police Arrest Hundreds,in,Raid on Calabria 's Ndrangheta Mob Network
2010-07-13T11:41:54Z,Copper,Erases Decline in,Trades Unchanged
2010-07-13T11:42:43Z,Housing Development,Propel Benchmark Stock Index,Hindalco
2010-07-13T11:51:56Z,U.S. Oil Inventories Seen,Increasing,Crude Futures Advance
2010-07-13T11:54:45Z,Reserve Bank May Raise Interest Rates,Signal 's,Singh
2010-07-13T11:54:50Z,Fall,in,Infosys Profit
2010-07-13T11:54:50Z,Rupee,Holds,Losses
2010-07-13T11:56:35Z,UN Supply,in,Up July on New Procedures
2010-07-13T12:03:37Z,Tiger Woods,Switches to,Nike Putter
2010-07-13T12:12:51Z,Ex-IKB Chief Ortseifen,Be,Acquitted
2010-07-13T12:16:45Z,Copper,May Drop on,Speculation About Weakening Demand
2010-07-13T12:19:53Z,Australia,Reaches,36-1 Against Pakistan
2010-07-13T12:20:24Z,10.2 %,in,May
2010-07-13T12:22:45Z,ECB,Criticizes,Hungary Government 's Plan
2010-07-13T12:26:02Z,Brazil,in,Center South Will Pare Output
2010-07-13T12:26:02Z,Sugar-Cane Dryness,in,Brazil 's Center South Will Pare Output
2010-07-13T12:30:01Z,Trade Gap,in,U.S.
2010-07-13T12:30:31Z,Merrill Survey,Says as,Growth Outlook Drops
2010-07-13T12:42:06Z,BT Pension Trustees,Seek Court Ruling on,Billion Deficit Liability
2010-07-13T12:42:10Z,Congo,Defends Sale to,Companies
2010-07-13T12:42:10Z,South Africa,by,Zuma
2010-07-13T12:42:51Z,South Africa Gain,Led By,Anglo Platinum
2010-07-13T12:42:51Z,Stocks,in,South Africa Gain
2010-07-13T12:43:43Z,Kumba,Assore After,Analyst Naidoo Resigns
2010-07-13T12:44:00Z,Gas,Spend,$ 1 Billion
2010-07-13T13:00:00Z,Clean Energy Investment,Holds Steady in,Q2
2010-07-13T13:00:31Z,Crude Oil,on,Advance
2010-07-13T13:00:31Z,Mexican Peso,Rises in,Three Days
2010-07-13T13:00:31Z,Peso,Rises on,Oil 's Advance
2010-07-13T13:16:27Z,Libyan Organizers,Say,Israeli Navy Boat Intercepts Gaza-Bound Aid Vessel
2010-07-13T13:16:27Z,Organizers,Say,Israeli Navy Boat Intercepts Gaza-Bound Aid Vessel
2010-07-13T13:17:49Z,New York Yankees Owner Steinbrenner,Suffers,WABC-TV Reports
2010-07-13T13:21:27Z,Tax Cuts,Get,Reprieve
2010-07-13T13:24:58Z,Transgene,in,Talks to License Experimental Antibody Showing
2010-07-13T13:30:25Z,Renova Energia Declines,in,First Day of Trading
2010-07-13T13:41:33Z,33 % Increase,in,Second-Quarter Profit
2010-07-13T13:44:50Z,Estonian Unit,From,Ex-Owners
2010-07-13T13:50:47Z,Localiza Gains,Jump,30 Percent
2010-07-13T13:50:51Z,Poland,in,Zachodni
2010-07-13T13:51:23Z,5.7 % Gain,in,Imports
2010-07-13T13:51:23Z,Canada Records Third Straight Trade Deficit,in,May
2010-07-13T13:58:41Z,George Steinbrenner,Dies at,80
2010-07-13T13:59:36Z,Citigroup,Says,StepStone Will Run Private-Equity Unit
2010-07-13T14:02:58Z,HIV Infections Fall,in,Africa
2010-07-13T14:12:21Z,U.S.,Asks,Courts
2010-07-13T14:16:18Z,Rates,Sink on,Rate-Cut Speculation
2010-07-13T14:16:35Z,New York Yankees Baseball Owner George Steinbrenner,Dies at,Age 80
2010-07-13T14:20:27Z,Dr Pepper Snapple,Authorizes,Billion Share Buyback
2010-07-13T14:29:26Z,China Deaths,Reach,More Rainfall Expected
2010-07-13T14:31:26Z,Italy,Abolish,Bill
2010-07-13T14:34:50Z,Treasury 10-Year Note Falls,Decline Since,August
2010-07-13T14:37:30Z,Brazil Retail Sales Rise,Are,Less Than Expected Removed
2010-07-13T14:52:14Z,Partner,Develop Business in,Poland
2010-07-13T14:59:41Z,Flaherty,Says,Passage of Intact Canada Budget Will Lead to Fiscal Balance
2010-07-13T15:02:24Z,Governor,Quarrel Over,State Debt
2010-07-13T15:04:32Z,Fidel Castro,Makes,TV Appearance
2010-07-13T15:06:31Z,Stock Market Makers,in,U.S. Need Tougher Rules
2010-07-13T15:06:31Z,U.S. Need Tougher Rules,in Makers,Brokerages
2010-07-13T15:08:01Z,Concern Romania,on,Recession
2010-07-13T15:08:01Z,Leu,Depreciates on,Concern Romania 's Recession
2010-07-13T15:11:49Z,U.S. Derivatives Strategist,in,New York
2010-07-13T15:11:49Z,UBS,Hires,Citigroup 's Revsine
2010-07-13T15:16:55Z,Andy Schleck,Takes,Lead
2010-07-13T15:16:55Z,Sandy Casar,Wins Alpine Stage,Andy Schleck Takes
2010-07-13T15:17:59Z,Debt Sale,Reduces,Demand
2010-07-13T15:17:59Z,Greek Debt Sale,Reduces,Demand for Safest Assets
2010-07-13T15:23:30Z,Kagan,on,Supreme Court Nomination
2010-07-13T15:44:24Z,ERG,Move in,Milan Trading
2010-07-13T15:49:37Z,German Stocks,Gain as,BMW
2010-07-13T15:49:37Z,Stocks,Gain as,BMW
2010-07-13T15:52:38Z,RBC,Hires,Bankers
2010-07-13T15:56:29Z,African Stocks,Rise to,Three-Week High
2010-07-13T15:56:29Z,South African Stocks,Rise to,CIC
2010-07-13T15:56:29Z,Stocks,Rise to,Three-Week High Led by Anglo Platinum
2010-07-13T15:57:59Z,Pound,Rises After,Data Show U.K. Inflation Month
2010-07-13T15:58:55Z,Fuel Oil Losses,in,Europe Decline
2010-07-13T16:00:25Z,Transocean,Leads,Increase
2010-07-13T16:08:13Z,Advance,Paced by,Peugeot
2010-07-13T16:08:17Z,CFO Nelson,Quits,Days
2010-07-13T16:08:48Z,Phoenix Solar,Climbs to,Institutional Investors
2010-07-13T16:11:34Z,European Stocks,Rise,BP Surges
2010-07-13T16:11:34Z,Stocks,Rise for,Sixth Day
2010-07-13T16:17:35Z,Their,Finances,Survey
2010-07-13T16:17:58Z,South African Rand,Climbs to,Two-Week High Versus Dollar
2010-07-13T16:18:10Z,China,Surges,Europe
2010-07-13T16:18:10Z,Financing,in U.S.,Europe
2010-07-13T16:18:10Z,Past U.S.,in,Financing
2010-07-13T16:18:39Z,Banks,Drive,Bill Rates Below EU Loans
2010-07-13T16:18:39Z,Bill Rates Below EU Loans,in,Post-Crisis Sale
2010-07-13T16:18:39Z,Greek Banks,Drive,Bill Rates Below EU Loans in Post-Crisis Sale
2010-07-13T16:23:12Z,$ 1 Billion Debt Sale,Offering Since,January 2008
2010-07-13T16:28:20Z,Brazil,at,Santos Port
2010-07-13T16:28:35Z,JPMorgan Chase,'s Miles,Adviser to UAL
2010-07-13T16:28:35Z,JPMorgan Chase 's Miles,Adviser to,UAL
2010-07-13T16:28:35Z,Morgan Stanley,Hires,Adviser
2010-07-13T16:28:36Z,Body,Asks,Oil Companies
2010-07-13T16:28:36Z,Nigerian Body,Improve,Reporting
2010-07-13T16:32:01Z,Kenya Commercial Bank,Bank for,Africa
2010-07-13T16:46:48Z,Federal Appeals Court,in,New York
2010-07-13T16:47:35Z,Rain,Threatens,Northeast
2010-07-13T16:48:30Z,Buyout-Loan Rebound,Is Led by,Providence
2010-07-13T16:58:32Z,Congo Army Clashes,With,Ugandan Rebels
2010-07-13T17:10:14Z,First Quantum Seeks Workers,Sees,March Completion
2010-07-13T17:14:40Z,Texas Rangers,Seek Auction as,Bidder for Team
2010-07-13T17:14:55Z,$ 200 Million,in,Contest
2010-07-13T17:14:55Z,Ideas,$ Million in,Contest
2010-07-13T17:17:08Z,Royal Holdings,Bid for,JAL In-Flight Meal Unit
2010-07-13T17:23:08Z,Steinbrenner Ownership Legacy,Continues After,Death
2010-07-13T17:24:15Z,Harry Redknapp,Extends,Coaching Contract
2010-07-13T17:26:54Z,Barclays Capital 's Equities Chief Dixit Joshi,Is,Said Depart
2010-07-13T17:26:54Z,Barclays Capital 's European Equities Chief Dixit Joshi,Is,Said Depart
2010-07-13T17:35:32Z,Brazil,Starts New Libra Oil Well as,Mechanical Problem Halts First Attempt
2010-07-13T17:36:13Z,Canada Targets July 2012,With,Head Office
2010-07-13T17:39:40Z,Host Bank Authority,in,London
2010-07-13T17:40:30Z,Conson,Weakens to,Storm
2010-07-13T17:40:30Z,it,Crosses,Philippine Island
2010-07-13T17:42:43Z,$ 8.5 Billion Restructuring Plan,Split in,Two
2010-07-13T17:42:43Z,Plans Split,in,Two
2010-07-13T18:03:25Z,Coffee Crop,in,Brazil Region
2010-07-13T18:11:19Z,Fans,in,Uganda
2010-07-13T18:11:19Z,New York Police,Send Investigators on,Fans in Uganda
2010-07-13T18:25:44Z,Gold,Rises,Most in Three Weeks
2010-07-13T18:26:54Z,June Budget Deficit,in,U.S.
2010-07-13T18:27:57Z,Nomura Holdings,Hires Paul Lejuez as,U.S. Retail Analyst
2010-07-13T18:28:05Z,Ignatieff,Hits,Highway
2010-07-13T18:32:52Z,Alcoa Profit Gain,Optimism for,Metals Demand
2010-07-13T18:41:27Z,Business Roundtable 's Castellani,Named,CEO of Drug Industry 's Lobby Group
2010-07-13T18:41:27Z,Drug Industry,of,Lobby Group
2010-07-13T18:56:03Z,Playboy,Take Private,Publishing Company
2010-07-13T19:05:36Z,Nuclear Scientist,Takes,Refuge
2010-07-13T19:05:36Z,Scientist,Takes Refuge in,Iran TV
2010-07-13T19:06:46Z,UBS,Hires McLoughlin to,Head Municipal-Bond Research
2010-07-13T19:16:08Z,Credit,Hedge,Funds
2010-07-13T19:18:36Z,Cocoa,Rises on,Expectations Commodity Demand
2010-07-13T19:29:57Z,Heavy Rain May Curb Yields,in,Midwest
2010-07-13T19:35:24Z,Peru 's Sol,Rises to,23-Month High
2010-07-13T19:38:38Z,Pfizer Officials,Sued Over,Billion Marketing Settlement
2010-07-13T19:49:56Z,Improved Demand Prospects,in,U.S.
2010-07-13T19:51:52Z,3.5 %,to %,Double-Dip Recession
2010-07-13T19:51:52Z,Blinder,Sees,U.S. Growth
2010-07-13T19:54:35Z,Pepsi 's Mexico Chief,Sees Sales,Economy Recovers
2010-07-13T19:54:35Z,Sales,Climbing,4 % Year
2010-07-13T19:55:28Z,Colombia 's Peso,Rises to,Nine-Month High on Greater Dollar Inflow Bets
2010-07-13T20:00:39Z,U.S.,Gain on,Alcoa Earnings
2010-07-13T20:04:19Z,BP Pipelines,Tanks for,289
2010-07-13T20:05:49Z,Share,Beats,Analysts ' Estimates
2010-07-13T20:09:15Z,Diet Pill,of,Safety
2010-07-13T20:09:40Z,Applied Materials,Says,Demand
2010-07-13T20:15:31Z,Intel Posts Second-Quarter Profit,Exceeding,Estimates
2010-07-13T20:18:36Z,Hefner,Says,Playboy Offer in Holders ' Interest
2010-07-13T20:18:36Z,Playboy Offer,in,Holders ' Best Interest
2010-07-13T20:19:22Z,Activision,Hires Deutsch Executive to,Lead Publishing
2010-07-13T20:19:45Z,Author,by,Estate for Harry Potter Similarities
2010-07-13T20:23:03Z,Rio,Limiting Scope for,New Investment
2010-07-13T20:23:27Z,Homebuilder Shares,Poised to,Rally
2010-07-13T20:23:27Z,Housing Recovery,Begins in,Mexico
2010-07-13T20:31:14Z,Lorillard,Urges FDA to,Review Testimony
2010-07-13T20:33:23Z,Chile 's BCI,Rises,Most in 15 Years
2010-07-13T20:37:57Z,Trade Gap,in,U.S.
2010-07-13T20:38:30Z,U.S. Appeals Court,Strikes,Down FCC Indecency Policy
2010-07-13T20:39:18Z,Treasury 10-Year Note,Tumbles Longest Decline for,Day
2010-07-13T20:39:18Z,Treasury Note,Tumbles,Longest Decline
2010-07-13T20:47:18Z,U.S. Stocks,Jump on,Strength
2010-07-13T20:47:30Z,Arab Bank,Sanctioned for,Withholding Records
2010-07-13T20:47:30Z,Withholding Records,in,Terrorism Suits
2010-07-13T20:51:05Z,Christie,Signs,Annual Property-Tax Cap on New Jersey Towns
2010-07-13T20:52:48Z,U.S. Oil Inventories Seen,Increasing,Crude Futures Advance
2010-07-13T20:54:05Z,Bayer,Fouled,U.S. Long-Grain Rice Crop
2010-07-13T20:55:43Z,Airline Challenge,Is,Rejected
2010-07-13T20:59:29Z,Ex-Blagojevich Aide,Testifies to,Pressure
2010-07-13T21:00:22Z,53 Percent Surge,in,Second-Quarter Contracted Sales
2010-07-13T21:00:59Z,It,'s Diverted to,Egypt
2010-07-13T21:00:59Z,Libyans,Say on,Course
2010-07-13T21:07:49Z,Baidu,Climbs on,First Global Outperform
2010-07-13T21:09:12Z,Canada 's Dollar Advances,in,Almost Three Weeks on Equity Rally
2010-07-13T21:17:18Z,Benihana,Sale of,Japanese-Themed Steakhouses
2010-07-13T21:19:22Z,Argentine Bonds,Climb by,Fitch Ratings
2010-07-13T21:19:22Z,Bonds,Climb After,Upgrade
2010-07-13T21:29:53Z,Fairholme Discloses Stake,in,Insurer
2010-07-13T21:29:53Z,MBIA,Rises After,Fairholme Discloses Stake
2010-07-13T21:29:55Z,Author,by,Estate
2010-07-13T21:29:55Z,Scholastic,Sued for,Similarities to Harry Potter Books
2010-07-13T21:30:14Z,Siemens Energy,Deals on,Russia Visit
2010-07-13T21:34:42Z,Sprott,Appoints,Peter Grosskopf
2010-07-13T21:38:31Z,Itau Tops Goldman Sachs,in,Brazilian Bond Underwriting
2010-07-13T21:41:36Z,STMicroelectronics,Caused,Hitachi 's Supply Delay
2010-07-13T21:44:21Z,10.5-Year Bonds,in,International Credit Markets
2010-07-13T21:49:43Z,Laos Minister Thongloun,Makes Highest-Level Visit to,Washington
2010-07-13T22:00:00Z,Wall Street,Fix,Seen Ineffectual
2010-07-13T22:05:12Z,New Zealand 's June House Price Index,Rises From,Previous Month
2010-07-13T22:15:26Z,North Korea,Proposes,UN Talks
2010-07-13T22:29:05Z,Ghailani,Avoid,Prosecution
2010-07-13T22:43:04Z,Energy Bill,in,Senate May Reduce Pollution
2010-07-13T22:43:04Z,Senate May Reduce Pollution,in Bill,Reid
2010-07-13T22:43:11Z,Yen Declines,in,Three Weeks on U.S. Profit Outlook
2010-07-13T22:43:38Z,Aid Ship,Bound for,Gaza
2010-07-13T22:43:38Z,Libyan Aid Ship,Bound for,Gaza
2010-07-13T22:47:42Z,Levi Strauss Targets China,Emerging,Middle Class
2010-07-13T22:55:11Z,Northrop,Combine,Yards
2010-07-13T23:01:00Z,Ireland,Almost Halve,ESRI
2010-07-13T23:01:00Z,Rich,Get At,Poverty Inc.
2010-07-13T23:01:00Z,World War,' Year,ESRI
2010-07-13T23:01:01Z,Buy Stocks,in,Bernanke Research
2010-07-13T23:01:01Z,Economy,Says,Stocks on Uncertainty From Stanford
2010-07-13T23:09:59Z,0.4 %,in,Chicago
2010-07-13T23:09:59Z,Wheat Futures Drop,Declining in,Four Days
2010-07-13T23:14:25Z,Kokusai Global Fund Cuts Investment,in,Euro Government Bonds
2010-07-13T23:30:00Z,U.K.,Lending 's,Moody
2010-07-13T23:34:08Z,FAW Group May,Be,Restructuring Business for IPO
2010-07-13T23:43:32Z,Middle Course,in,Strategy
2010-07-13T23:44:32Z,Blinder Cuts U.S. Forecast,Says,Easing
2010-07-13T23:53:39Z,Lions Gate,Said on,Purchase
2010-07-13T23:59:22Z,China,Has Removed,Business News
2010-07-14T00:26:45Z,Fans,' Tears,Senate Proclamations
2010-07-14T00:26:45Z,George Steinbrenner,Is Remembered With,Fans ' Tears
2010-07-14T01:11:48Z,Jetstar,Adds,Flights
2010-07-14T01:11:48Z,Qantas,Accelerates,Boeing 787 Delivery
2010-07-14T01:22:39Z,May,Hold,Policy Stance
2010-07-14T01:25:56Z,Barnes 's Defensive,Move,Proper
2010-07-14T01:42:38Z,New Zealand,'s Retail,Housing Data Back Case for Gradual Rate Increases
2010-07-14T01:42:38Z,New Zealand 's Retail,Case for,Gradual Rate Increases
2010-07-14T01:54:05Z,EBay,Sued by,XPRT Over Infringement
2010-07-14T01:58:24Z,Asian Stocks,Climb on,Singapore Growth
2010-07-14T01:58:24Z,Currencies,Climb on,Intel Sales
2010-07-14T01:58:24Z,Stocks,Climb on,Singapore Growth
2010-07-14T02:44:15Z,Expansion Moderates,in,Second Half
2010-07-14T03:29:06Z,Increase,in,U.S. Crude Stockpiles
2010-07-14T03:29:06Z,Oil Falls,in,Gains on Increase
2010-07-14T04:01:00Z,Chamber,Urges,Obama
2010-07-14T05:41:48Z,Philippines May,Keep Key Interest Rate at,Record-Low
2010-07-14T07:31:22Z,Armbands,Lowered,Flags Mark Death
2010-07-14T07:31:22Z,Black Armbands,Lowered,Flags Mark Death
2010-07-14T08:04:39Z,PetroChina $ 3.1 Billion Takeover,Approved by,Arrow Energy Holders
2010-07-14T08:04:39Z,PetroChina Billion Takeover,Approved by,Arrow Energy Holders
2010-07-14T09:42:47Z,U.K. House Prices,May Decline as,Cuts Sap Confidence
2010-07-14T10:27:56Z,Stocks,in,India
2010-07-14T10:33:28Z,Asian Stocks,Climbs on,Intel 's Record Revenue
2010-07-14T10:33:28Z,Intel,on,Record Revenue
2010-07-14T10:33:28Z,Samsung,Climbs on,Intel 's Record Revenue
2010-07-14T10:33:28Z,Stocks,Climbs on,Intel 's Record Revenue
2010-07-14T10:39:08Z,Daewoo,Buys,Stake
2010-07-14T10:39:08Z,President Zuma,by,Nephew
2010-07-14T12:10:02Z,Muslim Ally,in,Gaza Crisis
2010-07-14T12:10:02Z,Turks,Binds Israel to,Muslim Ally
2010-07-14T15:06:44Z,Baghdad,Is on,Map for Lufthansa
2010-07-14T15:51:10Z,U.K. 's FTSE 100 Index Declines,in,Seven Days
2010-07-14T16:48:37Z,Bonds,in,Longest Rally
2010-07-14T17:23:36Z,Most Sudden Acceleration Crashes,Are,Driver Error
2010-07-14T17:23:36Z,Sudden Acceleration Crashes,Are,Driver Error
2010-07-14T19:45:32Z,Canada Stocks,Slip as,Fed Minutes
2010-07-21T15:51:21Z,Deutsche Bank,Hires Lampard to,Head Canada Sales
