2011-02-14T00:00:00Z,Beer Buyer,Is,Smarter
2011-02-14T00:00:00Z,Demand,May Remains,Advance
2011-02-14T00:00:00Z,Global Demand,May Remains on,Bets
2011-02-14T00:00:00Z,Soybeans,May Remains on,Bets
2011-02-14T00:00:01Z,Egyptian Women,Silently Endure,Movie
2011-02-14T00:00:01Z,Elisabeth Moss Star,in,London Lesbian Drama
2011-02-14T00:00:01Z,Heston Blumenthal ’s Dinner,Has,Top London Chefs Abuzz
2011-02-14T00:00:01Z,Keira Knightley,Star in,London Lesbian Drama
2011-02-14T00:00:01Z,Women,Endure,Sexual Harassment
2011-02-14T00:00:07Z,Laffer Curve,Pays,Billions
2011-02-14T00:01:00Z,Cable,Says,U.K. Banking Panel Will Prompt Radical Surgery
2011-02-14T00:05:00Z,CEBR Cuts U.K. House Price Forecast,Says to,Fall
2011-02-14T00:28:48Z,Plan Sale,in,U.S. Dollars
2011-02-14T00:33:24Z,Siemens CFO,Says,Company Seeks Acquisitions
2011-02-14T00:59:57Z,Suzlon Energy,Build,Wind Power Plant
2011-02-14T00:59:57Z,Wind Power Plant,in,New South Wales
2011-02-14T01:01:01Z,Prosecutor May,Appearance of,Mud
2011-02-14T01:27:07Z,FARC,Frees,One Hostage
2011-02-14T01:38:25Z,Gbagbo Remains,Tells,FT
2011-02-14T01:38:59Z,U.K. Defense Equipment,Scrapping,$ 19 Billion
2011-02-14T01:43:16Z,2.1 %,in,December
2011-02-14T02:03:24Z,Japan Economy,Shrinks,Less-Than-Estimated 1.1 %
2011-02-14T02:37:44Z,Toledo,Widens,Lead in Peru Presidential Poll
2011-02-14T02:40:19Z,Brazil,'s Retirement,Estado
2011-02-14T02:50:06Z,Weber,’s Chances,FT
2011-02-14T03:00:45Z,General Electric,Is to,Purchase John Wood Extraction Unit
2011-02-14T03:03:25Z,London,in Stay,FT
2011-02-14T03:03:25Z,NYSE Euronext ’s Liffe May Stay,in,London
2011-02-14T03:18:06Z,Barratt Mulls,Shifting to,London Apartment Market
2011-02-14T03:18:32Z,Motorola Mobility,May Buy,WSJ
2011-02-14T03:25:59Z,Batista Bid,Has,Brazil ’s Richest Make Explorers Cheap
2011-02-14T03:51:28Z,Ambani,'s Communications,Reliance Infrastructure Fall in Mumbai
2011-02-14T03:51:28Z,Ambani 's Reliance Communications,Fall in,Mumbai
2011-02-14T03:51:28Z,Reliance Infrastructure Fall,in,Mumbai
2011-02-14T04:28:15Z,Asustek Advances,Increase,Stock Ratings
2011-02-14T04:36:19Z,Tasweeq,Offers,2.4 Million Barrels
2011-02-14T04:47:45Z,Zain Group,Receives,Third Offer
2011-02-14T04:53:23Z,SAIC Motor,'s Said,Increasing Production
2011-02-14T04:56:59Z,Fuel Oil,More,49 %
2011-02-14T04:56:59Z,Japan ’s 10 Power Utilities,Burn,Fuel Oil
2011-02-14T04:56:59Z,Japan ’s Power Utilities,Burn,49 % More Fuel Oil
2011-02-14T05:01:00Z,Millions,in,Gold Fuel Historical Crime
2011-02-14T05:01:00Z,Technology Firms,Mute Earnings Noise,Adjusted Numbers Fade
2011-02-14T05:01:00Z,Wall Street Bomb,Millions in,Gold Fuel Historical Crime
2011-02-14T05:17:32Z,Kenya,Borrows,54.5 Billion Shillings
2011-02-14T05:23:32Z,Investors Pipeline Costs,are,Cut
2011-02-14T05:30:19Z,Hong Kong Short Selling Turnover,Recorded,02/14/2011
2011-02-14T05:31:38Z,Sanofi,Is Studying,Ophthalmology Acquisitions
2011-02-14T05:38:16Z,Glaxo Plans Partnerships,With,Academic Scientists
2011-02-14T05:39:21Z,Imports,Increase,51 %
2011-02-14T05:41:43Z,Cricket South Africa,Fires,Business Day Reports
2011-02-14T05:43:20Z,Satyam Computer Jumps,in,Mumbai
2011-02-14T06:08:25Z,Wal-Mart,'s Bid,Terrorism Suspect
2011-02-14T06:09:25Z,$ 392 Million Australian Wind Farm,in,New South Wales
2011-02-14T06:16:23Z,SCA,Are Among,Duropack Suitors
2011-02-14T06:19:06Z,Drake,Rises Since,October 2009
2011-02-14T06:19:06Z,Scull,Rises to,Highest
2011-02-14T06:41:07Z,Norway Wages,Grew,Aftenposten Reports
2011-02-14T06:46:13Z,Lowest,to Falls,Hospodarske
2011-02-14T06:51:35Z,Boost Profit,in,Russia
2011-02-14T06:52:17Z,Copper Imports,Climb,5.7 %
2011-02-14T07:12:16Z,China,Boosts,Imports
2011-02-14T07:12:16Z,Tin,Climbs,Record
2011-02-14T07:17:47Z,Schaeuble,Tells,El Pais
2011-02-14T07:28:26Z,Cement Majority,to Coalition,Postimees
2011-02-14T07:28:26Z,Estonia ’s Coalition,in,Vote
2011-02-14T07:29:55Z,Croatia,Ask,Banks
2011-02-14T07:35:00Z,Czech Solar Investors May,Sue for,$ 2 Billion Koruna
2011-02-14T07:36:42Z,Nina Wang,'s Adviser,Lover Loses for $ 12 Billion Estate
2011-02-14T07:36:42Z,Nina Wang 's Ex-Feng Shui Adviser,Loses for,$ 12 Billion Estate
2011-02-14T07:41:10Z,African,Threatened to,Spread Foot-And-Mouth Disease
2011-02-14T07:41:10Z,South African,Threatened to,Spread Foot-And-Mouth Disease
2011-02-14T07:41:10Z,Spread Foot-And-Mouth Disease,in,U.S.
2011-02-14T07:49:33Z,South African Stock Benchmark,Reaches,Its Highest
2011-02-14T07:59:00Z,Greenhouse Investment Fund Eyes KBC Unit,in,Serbia
2011-02-14T07:59:59Z,Copper,Rises After,Imports Rise
2011-02-14T07:59:59Z,Imports Rise,in,China
2011-02-14T08:01:31Z,Australia,for,Whitehaven Coal
2011-02-14T08:01:31Z,Korea Resources,Submits,Preliminary Bid for Australia 's Whitehaven Coal
2011-02-14T08:03:39Z,Bundesbank Tradition,in,Choosing Weber Replacement
2011-02-14T08:03:39Z,Merkel May Stick,With,Bundesbank Tradition in Choosing Weber Replacement
2011-02-14T08:08:50Z,CEO Son,After,Death
2011-02-14T08:19:49Z,Lukoil,Prepares in,Future
2011-02-14T08:27:14Z,Germany ’s Feld,Sees,Debt Restructuring
2011-02-14T08:50:11Z,Haniel,Is in,in Advanced Talks on Sale of Metro AG Real Estate
2011-02-14T08:50:18Z,Alumina Trade,in,Three Years
2011-02-14T08:50:18Z,Shift,to Trade,Rusal
2011-02-14T08:50:18Z,Spot Price,to Trade,Rusal
2011-02-14T08:51:29Z,Nissan Plant,in,Portugal May Make Other Components
2011-02-14T08:53:19Z,Hong Kong,Starts,25-Day Inquest Into Manila Bus Hostage Crisis
2011-02-14T08:55:13Z,India May Save Money,Replacing Diesel With,Wind
2011-02-14T09:12:10Z,Pakistan ’s Engro Full-Year Profit,Rises on,Sales
2011-02-14T09:20:33Z,Russia,Begins,Anti-Dumping Probe Into Chinese Cold-Rolled Steel
2011-02-14T09:22:43Z,Credit Suisse,Raises,$ 6.2 Billion
2011-02-14T09:30:26Z,Poland,’s Account,BNP Paribas
2011-02-14T09:30:26Z,Zloty May,Weaken on,Poland ’s Current Account
2011-02-14T09:35:17Z,Slovenia Pensioners Party,Overtakes,Premier ’s Group
2011-02-14T09:57:18Z,Dong,Slips to,Record Low
2011-02-14T09:57:18Z,Vietnamese Dong,Slips to,Record Low
2011-02-14T10:00:05Z,17 %,in,2011
2011-02-14T10:03:42Z,London,in,Knightsbridge District
2011-02-14T10:16:39Z,Israel ’s Ayalon,Says,Iran
2011-02-14T10:18:52Z,Indonesia Economy,Expands,Egypt Crisis Eases
2011-02-14T10:18:52Z,Rupiah Gains,Expands,Egypt Crisis Eases
2011-02-14T10:28:11Z,West End Landlord Grosvenor,Invest in,London Assets
2011-02-14T10:31:04Z,Japan 's GDP,Beats,Estimates
2011-02-14T10:38:02Z,$ 12.8 Million,in,Domestic Market Last Week
2011-02-14T10:40:34Z,Tanzania ’s Benchmark Coffee Price,Climbs at,Auction
2011-02-14T10:45:17Z,152,to Fall,Global Ports
2011-02-14T10:50:49Z,Polish Zloty,Extends Loss,Month
2011-02-14T10:51:43Z,South African Corn Advances,Gains by,U.S. Benchmark
2011-02-14T10:54:36Z,Epic,Close,$ 1.3 Billion Ukrtelecom Financing
2011-02-14T11:03:38Z,Spot Trade,Starts,April
2011-02-14T11:06:19Z,Andritz,Announcing,Talks for Biggest-Ever Order
2011-02-14T11:13:26Z,7.8-Megawatt Solar Project,in,New Jersey
2011-02-14T11:18:47Z,Forint,Weakens as,Government Conflict
2011-02-14T11:24:34Z,Palm Oil Declines,Tracking,Crude Oil
2011-02-14T11:25:04Z,Global Investors,Sell,Net 4.34 Billion Rupees of Stocks
2011-02-14T11:26:55Z,Chile,Says,Emergencies from 6.6 Quake Overnight
2011-02-14T11:39:37Z,Record,in,China
2011-02-14T11:39:37Z,Wheat,Surges Imports to,Record in China
2011-02-14T11:40:09Z,Abu Dhabi Islamic,Has,Profit
2011-02-14T11:40:11Z,China May,Replenish,Stockpiles
2011-02-14T11:40:11Z,Rubber,Climbs,Record
2011-02-14T11:48:14Z,Egypt Bourse,Extends Trading Suspension by,Two Days
2011-02-14T11:51:24Z,Ukraine ’s Central Bank,Extends,Temporary Control
2011-02-14T11:51:34Z,Caisse De Depot Largest Holdings,in,4th Quarter
2011-02-14T11:55:12Z,Kingdon Capital Mgmt Largest Holdings,in,4th Qtr
2011-02-14T12:04:56Z,Fed Funds Projected,Open at,0.16 ICAP Says
2011-02-14T12:10:16Z,Batista 's AUX Canada,Agrees to,Purchase Ventana Gold
2011-02-14T12:25:49Z,African,Threatened to,Spread Foot-And-Mouth Disease
2011-02-14T12:25:49Z,South African,Threatened to,Spread Foot-And-Mouth Disease
2011-02-14T12:25:49Z,Spread Foot-And-Mouth Disease,in,U.S.
2011-02-14T12:30:12Z,2 Deposits,in,East Siberia
2011-02-14T12:30:12Z,Rosneft,Finds,2 Deposits
2011-02-14T12:34:11Z,Portugal,Leads Rise on,Growth Concerns
2011-02-14T12:34:11Z,Rise,in,Government Debt Risk
2011-02-14T12:36:10Z,Jana Partners Largest Holdings,in,4th Quarter
2011-02-14T12:36:15Z,Ukraine,’s Changed,Researcher
2011-02-14T12:36:24Z,Brazilian Economists,Raise,Inflation
2011-02-14T12:36:24Z,Cut GDP Forecasts,in,Survey
2011-02-14T12:36:24Z,Economists,Raise,Inflation
2011-02-14T12:51:19Z,Geithner,Tells Obama Debt Expense,Record
2011-02-14T12:51:42Z,33 %,in,Year
2011-02-14T12:51:42Z,JPMorgan,Buying,Bovespa Correction
2011-02-14T12:58:25Z,Cape Town Mall,Owned by,Istithmar
2011-02-14T12:58:25Z,Growthpoint,Buy,Cape Town Mall
2011-02-14T12:58:25Z,PIC,Buy,Cape Town Mall Owned
2011-02-14T12:58:55Z,Start,to PGE,Grid
2011-02-14T13:00:01Z,Intel,Sampling,Medfield Smartphone Chip
2011-02-14T13:05:45Z,Japan Tobacco Inc.,Bid for,Bulgartabak
2011-02-14T13:08:03Z,Karsan,Surges to,High
2011-02-14T13:10:15Z,Ivory Coast Budget Minister,Denounces,Citigroup Bank Closures
2011-02-14T13:17:35Z,U.S. Treasury Department,Schedule for,Monday
2011-02-14T13:18:30Z,$ 50 Million,in,Zambian Copper
2011-02-14T13:20:39Z,2.06 Billion Liras,in,Tax Filing
2011-02-14T13:21:53Z,South Africa Stock Benchmark,Rises on,Metal Prices
2011-02-14T13:22:44Z,Oil,Rises on,China Crude Imports
2011-02-14T13:22:45Z,Spar,Says Turnover Rose to,$ 1.3 Billion
2011-02-14T13:36:39Z,3.66 Percent,in,New York Trading
2011-02-14T13:37:56Z,Foreigners,Buy Cedis for,Bond Sale
2011-02-14T13:43:16Z,Jana Partners,Buys,New Stakes in Gold Fund
2011-02-14T13:43:16Z,New Stakes,in,Fund
2011-02-14T13:44:04Z,Croatia Podravka,Had,84 Million Kuna Profit
2011-02-14T13:45:16Z,Fight,in,Congress
2011-02-14T13:45:16Z,Obama 's $ 3.7 Trillion Budget,Sets,Fight in Congress
2011-02-14T13:45:16Z,Obama 's $ Trillion Budget,Sets,Fight in Congress
2011-02-14T13:45:16Z,Obama 's Trillion Budget,Sets,Fight in Congress
2011-02-14T13:52:08Z,Magal Security,Rises,Most in Three Weeks on Million Orders
2011-02-14T13:57:22Z,Businesses,Pay Bills With,Dollars
2011-02-14T13:57:22Z,Kenya Shilling,Weakens for,Second Day
2011-02-14T14:00:00Z,TPG ’s Petro Harvester,Buy,North Dakota Oil Properties
2011-02-14T14:22:07Z,Andritz,Are Active in,Vienna
2011-02-14T14:29:55Z,Highbridge Capital Mgmt Largest Holdings,in,4th Qtr
2011-02-14T14:31:47Z,Aksel Lund Svindal,Retains,World Super-Combined Skiing Gold for Norway
2011-02-14T14:35:04Z,Parallel Fd I Largest Holdings,in,4th Qtr
2011-02-14T14:39:03Z,Absa,'s Unit,Citizen
2011-02-14T14:39:47Z,Trian Credit Partners Largest Holdings,in,4th Qtr
2011-02-14T14:42:45Z,Commodities,Climb on,China Exports
2011-02-14T14:42:54Z,Berezovsky Papers Found,in,Moscow Gambling Raid
2011-02-14T14:42:54Z,Picasso Work,Found in,Moscow Gambling Raid
2011-02-14T14:44:47Z,Trian Partners General Partner Largest Holdings,in,4th Qtr
2011-02-14T14:52:25Z,Farallon Capital Mgmt Largest Holdings,in,4th Qtr
2011-02-14T14:56:21Z,Congo,'s Province,Local Leader
2011-02-14T14:56:21Z,Ugandan Rebels Murder 12,in,Congo 's North Kivu Province
2011-02-14T15:00:00Z,UPS Store,Makes,Million Available
2011-02-14T15:04:23Z,Nigerian Labor Groups Picket Union Bank,'s Offices,Allege Ban on Unionism
2011-02-14T15:04:23Z,Nigerian Labor Groups Picket Union Bank 's Offices,Ban on,Unionism
2011-02-14T15:09:32Z,Shekel,Strengthens,Egypt Crisis Eases
2011-02-14T15:13:16Z,Southeastern Asset Mgmt Largest Holdings,in,4th Qtr
2011-02-14T15:15:07Z,Transelectrica,Sees,Revenue
2011-02-14T15:15:55Z,Lithuanian State Budget Revenue,Exceeded,Target Month
2011-02-14T15:15:55Z,State Budget Revenue,Exceeded,Target Month
2011-02-14T15:18:54Z,Wertheim,Buys,Calcalist
2011-02-14T15:24:59Z,Thales,Surges on,Forecast of Earnings Gain
2011-02-14T15:27:01Z,Brahman Capital Corp Largest Holdings,in,4th Quarter
2011-02-14T15:28:31Z,Merck,Wins First State-Court Trial in,Lawsuit
2011-02-14T15:30:00Z,53 %,in,Administration Projections
2011-02-14T15:30:15Z,Homeland Security Budget,Would Increase,1 %
2011-02-14T15:32:19Z,$ 89 Billion,Cut for,Higher Education
2011-02-14T15:32:19Z,Obama,Seeks,10-Year Cut for Higher Education
2011-02-14T15:36:49Z,Tour de France Champ Alberto Contador,Is,Cleared
2011-02-14T15:37:44Z,War Crimes Investigation Needed,in,Somali Capital
2011-02-14T15:42:54Z,HFC Bank Ghana,Rises on,State Housing Project
2011-02-14T15:43:41Z,Benchmark,as Dead,Raymond James
2011-02-14T15:49:08Z,Owners,Elect,New Board
2011-02-14T15:50:16Z,Top High School Football Recruit Jadeveon Clowney,Chooses,South Carolina
2011-02-14T15:52:44Z,1.3 %,in,Convalescence
2011-02-14T15:54:42Z,Serbia Will Discuss Future Cooperation,With,IMF
2011-02-14T15:56:59Z,Pasadena Refining,Foaming at,Texas Plant
2011-02-14T15:58:48Z,S. Africa Stocks,Close at,3-Year High
2011-02-14T16:00:02Z,Housing Budget,Provides Money for,Homeless Less
2011-02-14T16:04:09Z,Ventana,Accept,Batista 's Increased Takeover Offer
2011-02-14T16:24:05Z,Buyers,Pare,Winners
2011-02-14T16:24:05Z,Sprint,Says,U.S. Tablet Craze
2011-02-14T16:24:58Z,Congo,Investigates,Foreigners for Gold Smuggling
2011-02-14T16:27:36Z,Mol,After,Iraq Output
2011-02-14T16:29:52Z,Russia,Raises,10 % of VTB Group
2011-02-14T16:32:04Z,Sunstone Names Arabia,Analyst From,Green Street
2011-02-14T16:44:02Z,0.3 %,in,Year
2011-02-14T16:44:21Z,Germany Mulls Changes,in,Loss Carry-Forward Rules
2011-02-14T16:45:12Z,Sirios Capital Mgmt Largest Holdings,in,4th Qtr
2011-02-14T16:51:07Z,Bank Handlowy,Move on,Warsaw Bourse
2011-02-14T16:51:18Z,Army,Hold,Elections
2011-02-14T16:51:31Z,Carlson Capital LP Largest Holdings,in,4th Quarter
2011-02-14T16:54:33Z,British Pound,Strengthens,Versus Euro
2011-02-14T16:54:33Z,Pound,Strengthens Versus Euro Before,Inflation Data
2011-02-14T16:55:07Z,Palisade Capital Mgmt Largest Holdings,in,4th Qtr
2011-02-14T16:55:42Z,German Stocks,Rise to,Three-Year High
2011-02-14T16:55:42Z,Stocks,Rise to,Three-Year High
2011-02-14T17:01:16Z,RWE,Holds,Quantity of Suspect ’ Carbon
2011-02-14T17:05:12Z,Credit Agricole,Set,EU20 Million
2011-02-14T17:06:14Z,U.K. Gas Falls,Resumes,Production
2011-02-14T17:09:38Z,China,on,Planned $ 7 Billion Investment
2011-02-14T17:12:51Z,European Stocks,Rise to,Highest
2011-02-14T17:12:51Z,Stocks,Rise Since,2008
2011-02-14T17:16:59Z,America Bond Program,Be,Revived
2011-02-14T17:17:00Z,Obama Budget,Would Exempt,IRA Distributions
2011-02-14T17:17:20Z,Rand,Reverses,Gains
2011-02-14T17:17:42Z,10 Years,in,Obama Budget
2011-02-14T17:20:47Z,Egypt,'s Rebellion,U.S. Competitiveness
2011-02-14T17:29:48Z,Harbert Management Largest Holdings,in,4th Quarter
2011-02-14T17:30:35Z,Kansas,Takes Over,Top Sopt From Ohio State
2011-02-14T17:32:29Z,Building Carbon Capture Plant,in,Poland
2011-02-14T17:33:00Z,Yemeni Protesters Clash,With,Police
2011-02-14T17:39:46Z,HBK Investments Largest Holdings,in,4th Quarter
2011-02-14T17:40:50Z,26.6 Million,Largest For,Show
2011-02-14T17:44:05Z,Seven,Charged in,Plot
2011-02-14T17:45:13Z,Aussie,Erases,Advance
2011-02-14T17:47:20Z,Kansas,Takes Over,Spot From Ohio State
2011-02-14T17:50:54Z,Obama,Would,Triple Guarantees
2011-02-14T17:50:54Z,Would,Guarantees for,Reactors
2011-02-14T17:55:00Z,Safety,Spending Up in,2012 Plan
2011-02-14T17:57:02Z,China,for,First Hedge Fund
2011-02-14T18:00:00Z,New Plant,in,Saudi Arabia
2011-02-14T18:01:39Z,Krona,Makes,Merkel 's Case
2011-02-14T18:01:39Z,Sweden Statist No,More Makes,Merkel 's Case for Europe
2011-02-14T18:03:14Z,Obama,Under,Budget Proposal
2011-02-14T18:03:28Z,Highway,Grows in,Obama Budget
2011-02-14T18:13:07Z,Ivory Coast Cashew-Monitoring Body Calls,in,Shipments of Nuts
2011-02-14T18:18:04Z,Orbimed Advisers Largest Holdings,in,4th Quarter
2011-02-14T18:29:19Z,Tiger Global Mgmt Largest Holdings,in,4th Quarter
2011-02-14T18:33:35Z,Microsoft ’s Ballmer,Sees Windows Phone Update in,March
2011-02-14T18:41:02Z,Whitebox Advisors Largest Holdings,in,4th Quarter
2011-02-14T18:43:12Z,Bill Murray 's Pro-Am Cinderella Story,Helps,Pebble Beach Golf Ratings Rise
2011-02-14T18:43:51Z,Tremblant Capital Group Largest Holdings,in,4th Qtr
2011-02-14T18:45:19Z,World Cup Record Scorer Ronaldo,Quits Soccer After,Losing Health Battle
2011-02-14T18:45:38Z,Investors,in,Power Plants
2011-02-14T18:45:38Z,Nigeria,Offers,Incentives
2011-02-14T18:46:21Z,Boeing Craft,Gain 's,in Pentagon
2011-02-14T18:46:32Z,Energy Trader,Sues,EU Commission
2011-02-14T18:46:32Z,Italian Energy Trader,Sues EU Commission Over,Carbon Thefts
2011-02-14T18:55:00Z,Housing Bank ’s Syrian Unit Posts Profit,Rises,7.6 %
2011-02-14T18:55:00Z,Housing Bank ’s Unit Posts Profit,Rises,7.6 %
2011-02-14T18:56:36Z,Copper,Rises,Record
2011-02-14T18:56:56Z,Pujols,Rejects,USA Today Reports
2011-02-14T19:01:48Z,TCW Group Incorporated Largest Holdings,in,4th Qtr
2011-02-14T19:15:52Z,Kayne Anderson Rudnick Largest Holdings,in,4th Qtr
2011-02-14T19:16:37Z,Decline,% to,84.87 Barrel
2011-02-14T19:16:37Z,N.Y. Crude Oil,Extends,Falls 0.8 %
2011-02-14T19:20:42Z,Gulf Coast Gasoline,Strengthens After,Hovensa Desulfurizer Fire
2011-02-14T19:25:47Z,Eton Park Capital Mgmt Largest Holdings,in,4th Qtr
2011-02-14T19:29:08Z,WL Ross,in,4th Qtr
2011-02-14T19:34:21Z,TPG-Axon Capital Mgmt Largest Holdings,in,4th Qtr
2011-02-14T19:35:58Z,Frontier Capital Mgmt Largest Holdings,in,4th Qtr
2011-02-14T19:37:51Z,Boehringer Blood Thinner,Added to,Heart Groups ' Treatment Guides
2011-02-14T19:44:00Z,Canyon Capital Advisors Largest Holdings,in,4th Qtr
2011-02-14T19:47:14Z,Cocoa,Rises to,One-Year High
2011-02-14T19:57:10Z,Caja Madrid,Cancels,25 Million-Euro Payment
2011-02-14T19:57:33Z,Hotchkis,in,4th Quarter
2011-02-14T19:58:39Z,Expedia,Warned,Sabre
2011-02-14T19:59:26Z,Capital B,in,Gas
2011-02-14T19:59:26Z,Hedge Funds,Bearish With,Capital B in Gas
2011-02-14T20:25:44Z,Vonn,Leaves,Ski Championships
2011-02-14T20:26:43Z,AQR Capital Mgmt Largest Holdings,in,4th Quarter
2011-02-14T20:28:16Z,Tiger Woods Will,Be Fined by,Tour
2011-02-14T20:37:08Z,Obama Budget Targets U.S. Technology,Spending,Growth
2011-02-14T20:42:52Z,45 % Funding Decrease,in,U.S. Proposal
2011-02-14T20:42:52Z,Small Business Administration,Faces,45 % Funding Decrease
2011-02-14T20:49:04Z,NFL Files Unfair-Labor Practices Complaint,in,Contract Talks
2011-02-14T20:49:25Z,Wheat,Rises Since,2008 on Demand Gain
2011-02-14T20:49:55Z,Adage Capital Partners Largest Holdings,in,4th Qtr
2011-02-14T21:06:19Z,Jeopardy Champion Jennings,Senses Target on,Back in IBM Match
2011-02-14T21:09:48Z,GLG Partners Largest Holdings,in,4th Quarter
2011-02-14T21:09:49Z,Environmental Agency,in,Budget
2011-02-14T21:09:49Z,Obama Seeks 13 % Cut,in,Environmental Agency ’s Budget
2011-02-14T21:13:16Z,It,May,May Takeover Target
2011-02-14T21:14:53Z,Obama,Submits Trillion Budget as,Republicans Pledge
2011-02-14T21:15:08Z,Glenview Capital Mgmt Largest Holdings,in,4th Qtr
2011-02-14T21:15:32Z,Appaloosa Management Largest Holdings,in,4th Qtr
2011-02-14T21:17:48Z,Lowest Level,to Falls,Egypt
2011-02-14T21:17:48Z,November,Since Falls,Egypt
2011-02-14T21:18:38Z,Merck,Wins,First Fosamax Jaw Death ’ State-Court Trial
2011-02-14T21:20:00Z,Basswood Capital Mgmt Largest Holdings,in,4th Qtr
2011-02-14T21:22:03Z,EchoStar,Rises on,Billion Deal
2011-02-14T21:26:58Z,Lockheed ’s Anti-Missile System,Be,Canceled
2011-02-14T21:27:13Z,Profit Forecast,Most Exceeds,Estimates
2011-02-14T21:34:51Z,Cyber-Intelligence Gathering,Would Get,More Funding in Obama Budget Plan
2011-02-14T21:34:51Z,More Funding,in,Obama Budget Plan
2011-02-14T21:38:30Z,EMS Falls,Meet,Expectation
2011-02-14T21:40:11Z,USDA Budget,Would,Rise 11 %
2011-02-14T21:43:26Z,Obama,in,Budget Proposal
2011-02-14T21:51:24Z,BP Capital Management Largest Holdings,in,4th Qtr
2011-02-14T21:52:15Z,Obama Seeks,Spending for,Exports
2011-02-14T21:54:46Z,Navy Budget,Spending,$ 74.7 Billion
2011-02-14T21:56:25Z,Fairholme Executives,Resign From,St. Joe Board on Governance Disagreements
2011-02-14T21:57:13Z,Private Capital Mgmt Largest Holdings,in,4th Qtr
2011-02-14T21:59:38Z,Fed,Should,Scrap Debit Caps
2011-02-14T21:59:38Z,U.S. Banks,Say,Fed
2011-02-14T22:07:52Z,China Optimism,Offsets,Valuation
2011-02-14T22:09:15Z,Motorola Mobility,Buys,3LM
2011-02-14T22:09:15Z,Tackle RIM,to,BlackBerry
2011-02-14T22:14:33Z,More Investments,in,Commercial Property
2011-02-14T22:15:31Z,State Bankruptcy Proposal,Criticized by,Parties
2011-02-14T22:19:08Z,Vinik Asset Mgmt Largest Holdings,in,4th Quarter
2011-02-14T22:19:22Z,Bernanke ’s 2009 Interview,Published in,Reversal
2011-02-14T22:19:22Z,Bernanke ’s Interview,Published in,Reversal
2011-02-14T22:19:30Z,Amtrak May Lose Money,in,Obama Transportation Plan
2011-02-14T22:22:22Z,Kurnell,in,First Half
2011-02-14T22:25:36Z,Moore Capital Hedge Fund,Buys Citigroup Stake in,Quarter
2011-02-14T22:35:11Z,Oil Volatility,Rises as,Futures Fall to Lowest
2011-02-14T22:43:35Z,State Department Plans Cuts,in,Security Funding
2011-02-14T22:45:37Z,New York City May,Get,$ 2 Billion
2011-02-14T22:46:20Z,Apple,Stakes in,Fourth Quarter
2011-02-14T22:46:20Z,Cohen ’s SAC,Added to,Genzyme Stakes
2011-02-14T22:46:20Z,Genzyme Stakes,in,Fourth Quarter
2011-02-14T22:47:09Z,Obama,Proposes User Fees in,Million CFTC Budget
2011-02-14T22:47:54Z,Coffee,Extends Rally Since,1997
2011-02-14T22:48:34Z,Tarullo,Says,Derivative Clearing Houses
2011-02-14T22:51:04Z,Berkshire Hathaway Inc Largest Holdings,in,4th Qtr
2011-02-14T22:54:45Z,Fairholme Capital Mgmt Largest Holdings,in,4th Qtr
2011-02-14T22:57:10Z,GE,Buy,$ 2.8 Billion Oil-Service Unit
2011-02-14T23:00:00Z,Australia,Reduces,Wheat After Floods
2011-02-14T23:00:01Z,Switzerland Fights Image,Freezing,Mubarak Assets
2011-02-14T23:00:01Z,Transition Woes,in,Egypt Leadership
2011-02-14T23:00:01Z,Tunisia Democracy Gridlock,Foreshadows,Transition Woes
2011-02-14T23:01:00Z,Carmakers,Set as,Value Swells
2011-02-14T23:01:00Z,German Carmakers,Set by,Billion
2011-02-14T23:01:00Z,German Top Carmakers,Set by,$ 90 Billion
2011-02-14T23:01:00Z,SocGen,Narrows,Earnings Gap
2011-02-14T23:01:00Z,Top Carmakers,Set by,Billion
2011-02-14T23:01:01Z,Daimler,of,Smart Brand Cars
2011-02-14T23:05:05Z,Cable Operators,Deals as,Digital Homes Ring in Profits
2011-02-14T23:05:05Z,Digital Homes Ring,in,Profits
2011-02-14T23:07:37Z,SAC Capital Advisors Largest Holdings,in,4th Qtr
2011-02-14T23:09:04Z,Japan ’s Bonds,Raise,Economic Assessment
2011-02-14T23:10:10Z,Nokia-Microsoft Accord Hard,Sell in,Barcelona
2011-02-14T23:13:26Z,Icahn Associates Corp Largest Holdings,in,4th Qtr
2011-02-14T23:19:56Z,Berkshire,Divests,Stakes
2011-02-14T23:19:56Z,Stakes,in,Bank of America
2011-02-14T23:29:14Z,Asian Coal Contracts,May Rise After,Rains
2011-02-14T23:29:14Z,Asian Thermal Coal Contracts,May Rise to,Record
2011-02-14T23:29:14Z,Coal Contracts,May Rise,36 %
2011-02-14T23:29:14Z,Thermal Coal Contracts,May Rise After,Rains
2011-02-14T23:30:44Z,Japanese Stock Futures Advance,Are,Little Changed
2011-02-14T23:30:54Z,Chilton Investment Co Largest Holdings,in,4th Qtr
2011-02-14T23:35:15Z,New Stakes,in,EOG Resources
2011-02-14T23:35:15Z,Pickens,Buys,New Stakes in EOG Resources
2011-02-14T23:49:11Z,Canadian Natural,Says,Horizon May Resume Operations
2011-02-14T23:49:11Z,Natural,Says,Horizon May Resume Operations
2011-02-14T23:49:18Z,Lowest,Near Trades,Egypt
2011-02-14T23:49:18Z,November,Since Trades,Egypt
2011-02-14T23:59:15Z,Auto Windscreens,Goes,Into Administration
2011-02-15T00:05:19Z,Egypt-Inspired Protesters Battle Security Forces,in,Bahrain
2011-02-15T00:25:49Z,Chelsea 's Premier League Title Defense Fades,Draw at,Fulham
2011-02-15T00:35:22Z,Silicon Valley Employment Posts Gains,Remains,Down
2011-02-15T01:10:21Z,Clinton,Lift,Focus Ahead
2011-02-15T03:00:40Z,GIC,Offers,$ 1.5 Billion
2011-02-15T05:00:01Z,Fight,With,Republicans
2011-02-15T05:00:01Z,Obama Budget Plan,Sets Stage for,Fight
2011-02-15T05:01:00Z,For-Profit-College Backers,Push,House Measure
2011-02-15T05:01:00Z,Zynga,Is,Said
2011-02-15T05:01:18Z,High Earners Return,in,Obama 's Budget Proposal
2011-02-15T05:01:18Z,Obama,in,Budget Proposal
2011-02-15T09:31:44Z,EU Delays,Pose,Euro Risk
2011-02-15T10:35:09Z,Dougan,Lowers,Returns Goal
2011-02-15T11:42:25Z,Bahrain Protests Swell,With,Second Death
2011-02-15T11:42:25Z,Second Death,Gas at,Funeral
2011-02-15T13:24:52Z,Ugandan,Vote,Oil Boom Nears
2011-02-15T14:50:16Z,NFL Files Unfair-Labor Practices Complaint,in,Contract Talks
2011-02-15T16:44:44Z,Mol,Misses,Earnings Estimate
2011-02-15T16:54:03Z,Weir Group,Retreats,Barclays Shares Climb
2011-02-15T16:58:29Z,Aussie,Appreciates,Versus Yen
2011-02-15T21:27:08Z,FedEx,Rises as,Investors Focus
2011-02-15T21:35:26Z,Marriott,Climbs After,Planned Spinoff
2011-02-16T22:37:13Z,Gates,Warns of,of Disruptions
2011-02-16T22:37:13Z,Military Budget Passage,Is,Delayed
2011-02-17T14:44:29Z,Aperam CEO,Would Consider,Stainless Steel Mergers
2011-04-13T23:42:00Z,68 %,in,Budget
