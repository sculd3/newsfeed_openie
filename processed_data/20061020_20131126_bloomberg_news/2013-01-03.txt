2013-01-03T00:00:00Z,Treasure Hidden,in,Berlin
2013-01-03T00:01:00Z,30 Million Pounds,in,Loans
2013-01-03T00:01:00Z,Farming Industry,Engage With,U.K. Study
2013-01-03T00:01:00Z,Industry,Engage With,Public
2013-01-03T01:00:00Z,Tax-Rate Boost,Sustains Longest Rally in,Five Years
2013-01-03T01:33:58Z,ConocoPhillips,Sued by,California
2013-01-03T02:24:10Z,Taiwan Dollar,Rises to,Eight-Week High
2013-01-03T02:31:43Z,Ex-Citadel Securities Head,Gets,Hong Kong License
2013-01-03T02:52:08Z,Copper,Slips From,Two-Month High Ahead
2013-01-03T03:13:03Z,U.S.,Lied,Megaupload Claims
2013-01-03T03:18:07Z,Peso,Touches,March 2008
2013-01-03T03:18:07Z,Philippine Peso,Touches,March 2008 High as Stocks Rise
2013-01-03T04:21:39Z,Gains,Cutting,Demand
2013-01-03T04:45:15Z,Hong Kong Short Selling Turnover,Recorded,01/03/2013
2013-01-03T05:00:02Z,Ex-Missile Defense Chief,Keep,His Rank
2013-01-03T05:01:00Z,Nets,in,Four Under Carlesimo
2013-01-03T05:01:01Z,AMR,Seeks Bankruptcy Court Approval for,Contracts
2013-01-03T05:01:01Z,Facebook Profit,Shows,Risk
2013-01-03T05:01:01Z,Google Said,Poised,Today
2013-01-03T05:01:03Z,Job Gains,Lure Investors to,Employment-Services Stocks
2013-01-03T05:18:34Z,$ 800 Million Casino IPO,in,Hong Kong
2013-01-03T05:29:08Z,Hyundai,Target on,Offshore Demand
2013-01-03T05:39:15Z,College Football,in,Sugar Bowl
2013-01-03T05:39:15Z,Louisville,Upsets,Florida 33-23
2013-01-03T06:13:37Z,Tottenham,Is Ready to,Rival Arsenal for Villa
2013-01-03T06:34:26Z,Taiwan Airlines,in,Alliance
2013-01-03T06:34:43Z,Aussie,Weaken on,U.S. Debt Ceiling Concern
2013-01-03T06:34:43Z,N.Z. Dollars,Weaken on,U.S. Debt Ceiling Concern
2013-01-03T06:46:45Z,Australia Swelters,in,Biggest Heat Wave
2013-01-03T06:49:07Z,Hyundai Falls,With,Korean Auto Stocks
2013-01-03T06:57:06Z,Gold-Loan Companies,Surge on,Central Bank Report
2013-01-03T07:00:00Z,U.K. House Prices,Fall,0.1 %
2013-01-03T07:10:39Z,Tin Seen,Advancing on,Fibonacci
2013-01-03T07:12:53Z,6-Day Wargame,in,Southern Waters
2013-01-03T08:00:00Z,Singapore Residue Fuel Stockpiles Fall,in,Three Months
2013-01-03T08:14:42Z,Marusan-Ai,Sued in,Australia
2013-01-03T08:19:04Z,Chinese Developers Restart Dollar Sales,in,Asia
2013-01-03T08:21:26Z,China,Running Lights Amid,Protests
2013-01-03T08:57:05Z,Next,Raises,Profit Target
2013-01-03T08:59:11Z,EU Carbon Supply May Peak,in,July
2013-01-03T09:00:06Z,Kenya Shilling,Weakens for,Fourth Day
2013-01-03T09:04:13Z,ECB,Says,Lending Shrank
2013-01-03T09:04:13Z,Private Lending Shrank,in,November
2013-01-03T09:09:54Z,Kazakhstan Government,Sell,Nurbank Stake
2013-01-03T09:10:29Z,African Republic Leader,Fires,Son
2013-01-03T09:10:29Z,Central African Republic Leader,Fires,Son
2013-01-03T09:10:29Z,Republic Leader,Fires Son After,Rebel Threat
2013-01-03T09:20:37Z,November Household Lending,Accelerates After,Rate Cuts
2013-01-03T09:20:37Z,Swedish November Household Lending,Accelerates After,Rate Cuts
2013-01-03T09:24:03Z,10-Year Yields,Changed at,1.44 %
2013-01-03T09:24:03Z,German 10-Year Yields,Changed at,1.44 %
2013-01-03T09:24:03Z,German Yields,Changed After,Jobless Data
2013-01-03T09:24:03Z,Yields,Changed at,1.44 %
2013-01-03T09:29:58Z,Spain Registered Unemployment Falls,in,Five
2013-01-03T09:43:47Z,Focus,Shifts to,U.S. Deficit
2013-01-03T09:43:47Z,German Stocks,Decline,Focus Shifts to U.S. Deficit
2013-01-03T09:43:47Z,Stocks,Decline,Focus Shifts to U.S. Deficit
2013-01-03T09:50:48Z,Exit U.K.,to CapitaLand,Reassess Australand
2013-01-03T09:54:22Z,Bickenbach,Jumps on,Handelsblatt Report
2013-01-03T09:54:22Z,Schmolz,Jumps on,Handelsblatt Report
2013-01-03T10:01:05Z,Vivendi ’s SFR-Iliad Deal May,Be Prohibited,BFM Reports
2013-01-03T10:08:55Z,Wellcome Trust,Starts,$ 326 Million Health Investment Arm
2013-01-03T10:11:13Z,Uganda,Leaves,Benchmark Rate Unchanged
2013-01-03T10:20:49Z,Wheat Futures Trade Little Changed,in,Chicago
2013-01-03T10:41:26Z,Russia,’s Lavrov,Unian Reports
2013-01-03T10:42:43Z,Forecast,in,December
2013-01-03T10:59:57Z,Dubai Stocks,Rise to,Nine-Month High Spurred by Bigger Dividends
2013-01-03T11:01:25Z,Gold,Declines,0.3 %
2013-01-03T11:01:25Z,Gold Declines,in,London Trading
2013-01-03T11:01:30Z,Economic Rebound,in,China
2013-01-03T11:04:10Z,India,Eases,Rules
2013-01-03T11:04:10Z,Jet,in,Talks
2013-01-03T11:08:23Z,Wacker,Jumps on,UBS Upgrade
2013-01-03T11:27:25Z,Corn Exports,Jumped,20 %
2013-01-03T11:27:25Z,Ukrainian Corn Exports,Jumped in,December
2013-01-03T11:38:08Z,Indian Stocks,Rise,3rd Day
2013-01-03T11:38:08Z,Stocks,Rise,Day
2013-01-03T11:39:59Z,Ghana,Adds,One Jubilee Crude Cargo
2013-01-03T11:51:16Z,Singh Steel Revival,Boosts,Tata-to-SAIL Outlook
2013-01-03T11:56:27Z,Fed Funds Projected,Open at,0.16 ICAP Says
2013-01-03T11:57:29Z,OIC,Urges France to,Act
2013-01-03T12:02:18Z,EDF ’s Clean-Energy Unit,Starts,Its Wind Project
2013-01-03T12:02:18Z,Its First Wind Project,in,Poland
2013-01-03T12:19:06Z,Crude Drops,in,Three Months
2013-01-03T12:19:35Z,Party,Decries,Tax Vote
2013-01-03T12:19:57Z,Emerging Stocks,Pare Gains,Utility Shares Decline
2013-01-03T12:19:57Z,Stocks,Pare,Gains
2013-01-03T12:33:43Z,India Police Files Charges,in,Court on Delhi Gang Rape Case
2013-01-03T12:56:12Z,Drug,as,Price Cut
2013-01-03T12:56:12Z,Novartis ’s Lucentis,Wins U.K. Backing as,Drug ’s Price Cut
2013-01-03T13:01:39Z,Brazil,Meet,2012 Fiscal Target
2013-01-03T13:09:28Z,Crude Oil Drops,in,Three Months
2013-01-03T13:11:08Z,Cliff Act,Gets,Dumb Deal
2013-01-03T13:16:23Z,Prospects,Improve for,American Crops
2013-01-03T13:16:23Z,Soybeans,Drop,Prospects Improve
2013-01-03T13:23:46Z,Investment,Is Sees,Lira Appreciation
2013-01-03T13:23:46Z,Turkey,Is Sees,ISE
2013-01-03T13:52:05Z,France,in,2014
2013-01-03T13:55:53Z,Debt Ceiling Fight,Could,Could Train Wreck
2013-01-03T13:56:38Z,Gross,Says,Government Financing Schemes
2013-01-03T13:59:14Z,Obama ’s Warning,Started,Road
2013-01-03T14:02:42Z,21 %,in,2012
2013-01-03T14:11:56Z,Treasury 10-Year Note,Erases Loss to,Yield 1.84 Percent
2013-01-03T14:11:56Z,Treasury Note,Erases,Loss
2013-01-03T14:14:25Z,EBRD Loan,Develop,Baltic Sea Port
2013-01-03T14:14:25Z,MSC Unit,Gets,EBRD Loan
2013-01-03T14:30:01Z,Polish Yields,Hit,High
2013-01-03T14:30:01Z,Yields,Hit,4-Week High
2013-01-03T14:35:13Z,Spain Deposits Rose,in,November
2013-01-03T14:45:00Z,Consumer Comfort,Climbed to,Eight-Month High
2013-01-03T14:49:29Z,Forecast,in,Holidays
2013-01-03T14:49:29Z,Jobless Claims,in,U.S. Rose
2013-01-03T14:57:38Z,Algeria ’s Salafis,Seek,Approval for New Political Party
2013-01-03T14:57:47Z,NHL Talks,Continue as,Jan. 11
2013-01-03T14:58:27Z,Barcelona Water Privatization Deal,Faces Action on,Appeal
2013-01-03T15:00:00Z,U.S. Mortgage Rates Little,Changed on,Record
2013-01-03T15:00:20Z,Cotton Disputes,Reach Record,Awards Surge
2013-01-03T15:09:31Z,Outlook,in,Northeast
2013-01-03T15:10:21Z,Canada,Stocks,Fall as Metals Slide
2013-01-03T15:11:42Z,Leu,Retreats as,Companies
2013-01-03T15:11:42Z,Romanian Leu,Retreats as,Companies
2013-01-03T15:14:37Z,Airbus,Wins,$ 2.8 Billion A350-900 Order From U.S. Lessor CIT
2013-01-03T15:33:22Z,Biggest Gain,in,West
2013-01-03T15:33:22Z,U.S. Nuclear Output,Led by,Gain in West
2013-01-03T15:33:22Z,U.S. Output,Led Higher by,Biggest Gain in West
2013-01-03T15:35:51Z,Chilean Peso,Rises as,Investors Cut Short Bets
2013-01-03T15:35:51Z,Peso,Rises to,Two-Month High
2013-01-03T15:51:09Z,Lower Saxony Opposition Lead Shrinks,in,Poll
2013-01-03T15:52:55Z,Dollar-Funding Stress,Holds at,16-Month Low
2013-01-03T15:52:55Z,Stress,Holds at,16-Month Low
2013-01-03T15:53:17Z,GM,Brings Truck Inventory in,Line
2013-01-03T16:00:01Z,OUE,Puts,Pressure
2013-01-03T16:00:01Z,Thai Billionaire,in,F&N Bid
2013-01-03T16:01:58Z,BBVA,Leads,Debt Sales
2013-01-03T16:01:58Z,Bank Bond Yields,Hold Near,Record Lows
2013-01-03T16:02:35Z,Oppenheimer,Says Sales to,Miss Analyst Estimates
2013-01-03T16:10:16Z,Manchester City ’s Mancini Clashes,With,Balotelli
2013-01-03T16:12:14Z,Lazard Revenue Seen,Jumping as,Budget Deal Fuels Mergers
2013-01-03T16:13:55Z,S&P,Erases Loss as,Retailers Gain
2013-01-03T16:13:55Z,S&P 500,Erases,Loss
2013-01-03T16:14:02Z,China,Cancels,Import
2013-01-03T16:14:36Z,Intercontinental,Has,Record 2012
2013-01-03T16:15:18Z,U.K. Pork Exports,Climbed in,October
2013-01-03T16:17:00Z,Fed,Buys,$ 5.098 Billion of Treasuries
2013-01-03T16:18:18Z,Wheat,Rises,Week
2013-01-03T16:32:32Z,Blackstone,Wins,Bid
2013-01-03T16:34:39Z,$ 889 Million,in,December
2013-01-03T16:35:04Z,Aqua America,Closes With,Texas
2013-01-03T16:41:22Z,to Five-Year Low,in,Texas Trading
2013-01-03T16:46:52Z,U.K. 10-Year Gilt Yield,Rises for,First Time
2013-01-03T16:47:59Z,More Savers,Switch to,Roth 401
2013-01-03T16:47:59Z,Savers,Switch to,Roth 401
2013-01-03T16:56:27Z,Stocks,Surge to,Four-Year High
2013-01-03T16:56:27Z,Swiss Stocks,Surge to,Four-Year High
2013-01-03T16:58:17Z,Vodafone Sells,Used to,Build Data Revenue
2013-01-03T17:00:00Z,IMF ’s Blanchard,Defends Findings on,Impact
2013-01-03T17:00:21Z,Change,Still Stand for,Something
2013-01-03T17:00:27Z,Republicans,Get,Angry
2013-01-03T17:02:09Z,Argentine Peso Falls,in,Parallel Market
2013-01-03T17:02:09Z,Record,to Falls,Clarin
2013-01-03T17:07:26Z,Solarworld,Expects,EU ’s China Anti-Dumping Duties
2013-01-03T17:09:04Z,Euro-Area,Stocks,Retreat Amid Concern
2013-01-03T17:10:33Z,Bovespa,Rises,20 %
2013-01-03T17:19:13Z,New Zealand,Faces Defeat After,Second Day of First Cricket Test
2013-01-03T17:35:08Z,Sugar,Tumbles,Most in 3 Weeks
2013-01-03T17:39:15Z,PPR,in,Talks
2013-01-03T17:42:10Z,Ghana,Adds,Jubilee Crude
2013-01-03T17:42:32Z,Candente Copper,Jumps on,Peru Exploration Drilling
2013-01-03T17:42:37Z,Carbon Market Value,Dropped,36 %
2013-01-03T17:45:06Z,EU Carbon,Tracking,2014 Power
2013-01-03T17:50:04Z,Morgan Stanley,Said to,Banks
2013-01-03T17:50:04Z,Prepping,Billion of,CMBS
2013-01-03T17:59:10Z,Shooter,Kills,Three Women
2013-01-03T17:59:10Z,Three Women,in,Swiss Mountain Village Spree
2013-01-03T18:34:35Z,Interest,in,New York Offshore Wind Projects
2013-01-03T18:34:35Z,U.S.,Gauges,Interest
2013-01-03T18:43:00Z,House,Vote,Month
2013-01-03T19:07:35Z,Fed,Sees,QE Efforts
2013-01-03T19:13:47Z,Zurich Insurance,Raises,$ 270 Million
2013-01-03T19:28:09Z,Manhattan Home Listings Plunge,in,Sign Prices
2013-01-03T19:33:21Z,Health Exchanges,in,Republican States
2013-01-03T19:51:26Z,Gold,Extends,Drop
2013-01-03T19:55:02Z,18 Months,in,Muni-Bond Bid-Rigging Case
2013-01-03T19:55:02Z,Ex-Broker,Gets,18 Months
2013-01-03T20:00:01Z,Nikkei Candle Pattern,Portends,Gains
2013-01-03T20:00:39Z,Ratan Tata,Retires With,$ 500 Billion Vision
2013-01-03T20:05:14Z,Brazil Water Utility Sabesp,Gets,Million River Cleanup Loan
2013-01-03T20:08:20Z,U.S. Treasury Inspector General,at,Home
2013-01-03T20:15:06Z,Congress Aids Oregon Electric Motorcycle Maker,in,Budget
2013-01-03T20:27:07Z,B2W,Leads,Consumer Stock Advance
2013-01-03T20:27:07Z,Bovespa,Enters,Bull Market
2013-01-03T20:33:55Z,Ex-Deutsche Bank Employee Parse,Loses,Bid
2013-01-03T20:36:35Z,We,Must Go Off,Platinum Coin Cliff
2013-01-03T20:38:26Z,Bond Yields,Drop as,Outlook
2013-01-03T20:38:26Z,Peruvian Bond Yields,Drop as,Outlook
2013-01-03T20:41:34Z,Devils Owner Vanderbeek,Buys Out,Partners
2013-01-03T20:48:03Z,Mairs,’s Frels,Henneman
2013-01-03T20:48:03Z,Mairs ’s Frels,Named,Manager of Year
2013-01-03T20:51:21Z,FOMC Participants,Saw QE3 Ending in,2013
2013-01-03T20:51:21Z,Most FOMC Participants,Saw QE3 Ending in,2013
2013-01-03T20:53:45Z,Mortgage-Bond Yields,Soar,Highest in Four Months
2013-01-03T21:00:33Z,55 %,in,October
2013-01-03T21:03:52Z,AquaChile,Surges After,Norway Salmon Prices Jump
2013-01-03T21:05:22Z,Hormel,Buy,Skippy Peanut Butter
2013-01-03T21:09:24Z,ALS Drug,Fails to,Show Efficacy
2013-01-03T21:13:16Z,Treasury Yield,Reaches,Highest Since May
2013-01-03T21:15:33Z,Quiksilver,Replaces,Founder
2013-01-03T21:17:01Z,Loblaw Buying Canada Safeway No Pipe Dream,in,Spinoff
2013-01-03T21:17:28Z,SunPower,Jumps as,Lazard Upgrades Shares
2013-01-03T21:23:18Z,Gap,Acquires,Luxury Womens ’ Apparel Maker Intermix
2013-01-03T21:24:48Z,FCC,Approves,Liberty Media Control of Sirius XM Licenses
2013-01-03T21:25:17Z,CEOs,Give U.S. Leaders Incomplete Grade on,Fiscal Cliff Work
2013-01-03T21:27:05Z,Futures Trade,in,Narrow Range
2013-01-03T21:31:25Z,Clean Energy Index,Rising in,2012
2013-01-03T21:32:48Z,Consumer Confidence,Improves in,U.S.
2013-01-03T21:38:42Z,Safeway CEO Burd,Retire After,20 Years
2013-01-03T21:41:46Z,Fed,Sees Bond Buying Ending in,2013
2013-01-03T21:44:15Z,AbbVie Plans Specialty Drug Focus,in,Sales
2013-01-03T21:57:32Z,Colombian Peso,Strengthens on,Foreign Investment Outlook
2013-01-03T21:57:32Z,Peso,Strengthens on,Foreign Investment Outlook
2013-01-03T22:00:28Z,Rape,Wakes,India
2013-01-03T22:04:14Z,CSI,Nears Bull Market After,ADR Jump
2013-01-03T22:04:14Z,CSI 300,Nears Bull Market After,ADR Jump
2013-01-03T22:05:58Z,Aussie,Fall on,U.S. Debt-Ceiling Speculation
2013-01-03T22:18:39Z,Fed,Favors QE3 Ending,Year
2013-01-03T22:27:02Z,Treasury 10-Year Note Yield,Touches 7-Month High on,Fed
2013-01-03T22:27:02Z,Treasury Note Yield,Touches,7-Month High
2013-01-03T22:41:01Z,Rambus,Barred From,Enforcing Chip Patents
2013-01-03T22:54:56Z,South African Olympic Cyclist Stander,Is Killed in,Road Accident
2013-01-03T22:57:08Z,N.J. Revenue May,Be,$ 700 Million Short Through Year End
2013-01-03T23:01:46Z,Sears,Settle,FTC Mislabelling Claims
2013-01-03T23:15:52Z,Qualcomm,Sued for,Records
2013-01-03T23:21:10Z,Allot ’s Record Stock Drop Emboldens,Buy Rating at,Needham
2013-01-03T23:23:27Z,Australian Stocks,Fall on,Fed Minutes
2013-01-03T23:23:27Z,Stocks,Fall on,Fed Minutes
2013-01-03T23:30:22Z,He,Battles,Republicans
2013-01-03T23:30:22Z,Liberals,Nip,Obama
2013-01-03T23:30:29Z,FTC,Spurn,Regulation
2013-01-03T23:43:10Z,BMW % December Gain,Overtakes,Mercedes
2013-01-03T23:43:10Z,BMW 39 % December Gain,Overtakes,Mercedes
2013-01-03T23:54:02Z,United Nations,Denies,Report Ban Ki Moon Will Visit North Korea
2013-01-04T00:25:11Z,Google Investigation,With,Voluntary Changes
2013-01-04T00:51:23Z,US Airways Merger Review,Concluded Within,Weeks
2013-01-04T01:09:27Z,Rig Grounding,Revives,Debate
2013-01-04T01:09:27Z,Shell,Over,Arctic Drilling
2013-01-04T01:21:57Z,Macy ’s Trial Over Martha Stewart Rights,Set for,February
2013-01-04T02:03:37Z,Toyota,Leads,2012 U.S. Vehicle Market
2013-01-04T04:35:38Z,Hong Kong Luxury Sales,Rebound on,Confidence
2013-01-04T05:00:01Z,Abbott,Said to,Show Bausch
2013-01-04T05:00:01Z,Firsts,in,New Congress
2013-01-04T05:00:01Z,Wal-Mart,Creates,New Role
2013-01-04T05:01:00Z,$ 700 Million,in,Loans
2013-01-04T05:01:01Z,Fitch Must Face Fraud Claims,in,SIV
2013-01-04T05:01:01Z,Moody,Claims in,SIV
2013-01-04T08:15:21Z,South Korea,Cites,Bias
2013-01-04T10:51:04Z,Chinese Tourists Lost,in,Thailand Mean Record Hotel
2013-01-04T11:07:17Z,Drugmaker Hub,Hit at,Six-Year High
2013-01-04T12:47:02Z,Pleads Guilty,in,U.S. Tax Probe
2013-01-04T16:59:26Z,Alstom Transport Orders,May Reach Record on,Canada Trams
2013-01-04T21:09:21Z,Final Tally,Shows Obama First Since,’56
2013-01-04T21:09:21Z,Tally,Shows Obama First Since,’56
2013-01-04T21:28:44Z,Chinese Web Gamers,Buy to,Opennheimer
2013-01-04T21:28:44Z,Web Gamers,Buy to,Opennheimer
2013-01-04T21:36:02Z,JPMorgan,Get,Delay
2013-01-04T22:04:07Z,Investors,Seek,Growth
2013-01-04T22:04:07Z,Obama,Fights Republicans on,Debt
