2011-08-29T00:00:00Z,Leap,Endorsing,Blank-Check Company
2011-08-29T00:00:00Z,Percentage,in,Your Tank
2011-08-29T00:11:09Z,Gain,Decline by,0.8 %
2011-08-29T00:11:09Z,Gold,Reverses,Gain
2011-08-29T00:19:46Z,Hynix Semiconductor ’s Shares Rise,in,Seoul
2011-08-29T00:37:41Z,China ’s Stocks,Raised to,Overweight
2011-08-29T00:59:03Z,New York,’s Commute,Mayor Bloomberg
2011-08-29T01:39:12Z,South Korea Indicates Cut,in,Growth Forecast Looming
2011-08-29T01:59:58Z,Taiwan Bonds Decline,Strengthens on,Bernanke Optimism
2011-08-29T03:52:03Z,1.8-Gigahertz Spectrum Auction,in,South Korea
2011-08-29T04:00:00Z,Bernanke,Turns Timid on,Japan
2011-08-29T04:00:01Z,Banks Bet Crude Oil Prices,Would,Fall
2011-08-29T04:00:01Z,Big Banks Bet Crude Oil Prices,Would,Fall
2011-08-29T04:00:01Z,Fall,in,2008 Run-Up
2011-08-29T04:00:01Z,Nino Rota,by,Scores
2011-08-29T04:00:09Z,Kelly Kraft,Wins,U.S. Golf Title Over Top-Ranked Patrick Cantlay
2011-08-29T04:00:10Z,Home Run,Lead as,Yankees Split Doubleheader
2011-08-29T04:01:00Z,AT&T Verizon-T Mobile,Sets Million for,Google Fight
2011-08-29T04:01:00Z,New York Local Government,Leads,Record Low Weekly Issuance
2011-08-29T04:01:00Z,You,Can Make,Wine
2011-08-29T04:01:16Z,Spending,Climbed,Car Purchases Rebounded
2011-08-29T04:09:02Z,California Team,Beats,Japan 2-1
2011-08-29T04:22:02Z,Focus International Expansion Plans,in,Australia
2011-08-29T04:34:06Z,Finance Minister Noda,in,Runoff Vote for Japan Leadership
2011-08-29T04:53:04Z,Some,Banned From,China Internet
2011-08-29T04:54:49Z,Vietnam,Raises,Dollar Reserve Ratios
2011-08-29T04:58:52Z,African Lawmakers,Seek,Business Day
2011-08-29T04:58:52Z,Lawmakers,Seek,Business Day
2011-08-29T04:58:52Z,South African Lawmakers,Seek,Mining Charter Changes
2011-08-29T05:00:17Z,Hong Kong Short Selling Turnover,Recorded,08/29/2011
2011-08-29T05:14:35Z,Kaieda Contest Run-Off Vote,in,Japan
2011-08-29T05:14:35Z,Noda,Vote in,Japan
2011-08-29T05:42:14Z,Supply Concern,Optimism on,Recovery
2011-08-29T06:00:00Z,South Korea ’s First Tidal-Power Plant,Begins,Operations
2011-08-29T06:02:34Z,10 %,in,Year
2011-08-29T06:11:13Z,Tanzania,Lowers,Gasoline Price
2011-08-29T06:11:16Z,FTD,Says With,Shareholder Sharp
2011-08-29T06:11:16Z,Loewe Eyes New Markets,Says With,Sharp
2011-08-29T06:22:58Z,It,Hampers,Competition
2011-08-29T06:22:58Z,Lufthansa,Sued,FTD Reports
2011-08-29T06:24:18Z,Israel,Considers,Building Nuclear-Power Plant
2011-08-29T06:29:12Z,Solar Millennium,Says,Subsidiary
2011-08-29T06:35:14Z,Japan ’s Bonds,Erasing,Losses
2011-08-29T06:35:14Z,Noda Win,Boosts,Reform Prospects
2011-08-29T07:07:22Z,Macquarie-Led Group,Makes,Conditonal Proposal
2011-08-29T07:10:41Z,Actelion Gains,in,Zurich
2011-08-29T07:10:42Z,Italy Government May,Raise,VAT
2011-08-29T07:18:17Z,Japan ’s Bond Market,Welcomes,Noda ’s Victory
2011-08-29T07:34:54Z,Bonds,Drop on,Measures
2011-08-29T07:35:50Z,Second Time,in,Apple Row
2011-08-29T08:15:00Z,Chocolate,Keeping,Healthy Heart
2011-08-29T08:15:00Z,Little Chocolate,Goes,Long Way
2011-08-29T08:32:33Z,Mauritius ’s Rupee,Weakens,Versus Dollar
2011-08-29T08:52:29Z,LG,Display,May Reduce
2011-08-29T08:52:29Z,May Reduce,Spending,Year
2011-08-29T08:52:29Z,Spending,Year,TV Demand Slows
2011-08-29T09:00:37Z,May,Decide,Aug. 31
2011-08-29T09:06:50Z,Jose,Weakens,North
2011-08-29T09:10:40Z,Taiwan Downgrades Nanmadol,Leaves,Land
2011-08-29T09:24:04Z,Quantum Asset ’s Dayal,Buy,Stocks
2011-08-29T09:27:20Z,Taiwan Hospital,Apologizes for,HIV Donor Organs
2011-08-29T09:48:18Z,Asian Currencies,Strengthen on,Bernanke Optimism
2011-08-29T09:48:18Z,Currencies,Strengthen on,Bernanke Optimism
2011-08-29T09:52:37Z,Airlines Stock Falls,Is,Double Estimates
2011-08-29T09:52:37Z,Turkish Airlines Stock Falls,Is,Double Estimates
2011-08-29T10:11:00Z,Nordea Will,Safeguard,Profitability
2011-08-29T10:16:12Z,Fed,on,Growth Outlook for U.S.
2011-08-29T10:16:12Z,Indian Stocks,Jump in,Asia
2011-08-29T10:16:12Z,Stocks,Jump Most in,Asia
2011-08-29T10:17:10Z,Kenya ’s Shilling,Slips,Most
2011-08-29T10:40:55Z,Downed Trees,in,New Jersey
2011-08-29T10:40:55Z,Niederauer,Fights,Trees
2011-08-29T10:45:15Z,Asia Stocks,Climb for,Third Day
2011-08-29T10:57:07Z,Reserve Bank,Recommends,Ownership Limit
2011-08-29T11:00:00Z,Decline,on Inflation,Food
2011-08-29T11:00:01Z,Allstate,in,Court News
2011-08-29T11:00:01Z,Merck,Allstate in,Court News
2011-08-29T11:02:21Z,Rise,in,October
2011-08-29T11:20:09Z,Italian Mayors,Demonstrate in,Milan
2011-08-29T11:20:09Z,Mayors,Demonstrate Against,Berlusconi Cuts
2011-08-29T11:29:04Z,Citic Bank Profit,Climbs,40 %
2011-08-29T11:31:49Z,Glencore,Buys,Extra Shares
2011-08-29T11:32:25Z,Chileans Stage Protests,in,City of Calama
2011-08-29T11:32:25Z,City,in Protests,Radio Bio-Bio Reports
2011-08-29T11:34:17Z,Taiwan Wealth,Business to,Far Eastern
2011-08-29T11:35:20Z,Bidvest,Buy,Topping Bid by Zeder Investments
2011-08-29T11:35:20Z,Capespan Shares,Bid by,Zeder Investments
2011-08-29T11:45:53Z,Olam Profit,Rises on,Higher Demand
2011-08-29T11:47:16Z,India ’s 10-Year Bonds,Drop,Third Day
2011-08-29T11:47:16Z,India ’s Bonds,Drop,Third Day
2011-08-29T12:04:00Z,South Africa ’s Malema,Says,Youth Wing to ANC Discipline
2011-08-29T12:10:03Z,World,of,Power
2011-08-29T12:14:24Z,Inflation,Slowed Than,More Forecast
2011-08-29T12:16:50Z,Foreign Funds,Sell,966 Million Rupees of Equities
2011-08-29T12:16:50Z,Funds,Sell,Net 966 Million Rupees
2011-08-29T12:19:55Z,Estonian Lawmakers,Approve,Term
2011-08-29T12:19:55Z,Lawmakers,Approve,Second Term
2011-08-29T12:27:38Z,SMA Solar May,Set Up Manufacturing Plant in,Asia
2011-08-29T12:43:53Z,18-Year Low,Bank of,Africa
2011-08-29T12:43:53Z,Uganda ’s Shilling,Extend Drop to,18-Year Low
2011-08-29T12:47:31Z,Crude Oil,Extends Gains in,New York
2011-08-29T12:47:31Z,Oil,Extends,Gains
2011-08-29T12:47:31Z,U.S. Consumer,Spending,Increases
2011-08-29T12:55:57Z,China Eastern Air,Boosts,Profit 30 %
2011-08-29T13:02:52Z,Start New Tax,to Senegal,Seneweb Reports
2011-08-29T13:04:37Z,U.S. Treasury Department,Schedule for,Monday
2011-08-29T13:08:38Z,Mumias Sugar,Rises to,High
2011-08-29T13:15:04Z,Solar Photovoltaic Plant,in,Eastern India
2011-08-29T13:15:04Z,Tata BP,Completes,Solar Photovoltaic Plant
2011-08-29T13:28:30Z,Novorossiysk Seaport,Resumes,Loadings Following Black Sea Storm
2011-08-29T13:49:10Z,Hartford,Leads,Insurer Rally
2011-08-29T13:49:10Z,Irene,as,Projected Costs Fall
2011-08-29T13:51:47Z,Complete Debt Refinancing Talks,in,Four Weeks
2011-08-29T13:56:48Z,Official Resigns,Permits,Sanction
2011-08-29T13:56:48Z,Romanian Official Resigns,Permits,Sanction
2011-08-29T13:58:21Z,Niger Rebels End Fight,in,Northeast
2011-08-29T14:02:17Z,U.S. Solar Product Exports Rose 83 %,in,2010
2011-08-29T14:02:33Z,Medvedev,Drive,Russian Economy
2011-08-29T14:15:10Z,n'ts,Dealing with,Disgruntled
2011-08-29T14:20:57Z,Venezuela ’s Guayana,Holds,$ 250 Billion
2011-08-29T14:23:48Z,1st Day,in,Seven
2011-08-29T14:23:54Z,Robles,Is,Disqualified
2011-08-29T14:27:00Z,Companies,Buy,Currency
2011-08-29T14:28:10Z,Irene,Headed for,Northeast
2011-08-29T14:28:10Z,NYSE Euronext,Weighed Arca Contingency Plan,Irene Headed
2011-08-29T14:29:56Z,Pending Sales,in,July
2011-08-29T14:42:42Z,Fillon,Says,France Wants Nuclear Crisis Management Center
2011-08-29T14:45:05Z,UPS,Agrees to,Expanded Multiyear College Sports Sponsorship
2011-08-29T14:48:11Z,China ’s Sohu,Surges on,Shares Repurchase Plan
2011-08-29T14:53:14Z,Irene,Stops Tourism at,Summer ’s End From Hatteras to New England
2011-08-29T14:53:14Z,Summer,at,End From Hatteras to New England
2011-08-29T14:55:27Z,Bernanke,Has New Delhi Under,Pressure
2011-08-29T14:55:59Z,Citigroup,Is,Bearish
2011-08-29T15:11:36Z,Losses,Seen,Overdone
2011-08-29T15:11:36Z,Turkey ’s Lira,Climbs,Strongest in Three Weeks
2011-08-29T15:20:50Z,Lagarde Snubbed,in,Push
2011-08-29T15:32:30Z,D’Antoni,on,Staff
2011-08-29T15:32:30Z,Former Hawks,Coach Woodson as,Assistant
2011-08-29T15:32:38Z,Greek Stocks,Soar,Most in Than Two Decades
2011-08-29T15:32:38Z,Stocks,Soar,Banks Merge
2011-08-29T15:40:23Z,Spain ’s Iberdrola Rules,Vote,Week
2011-08-29T15:45:00Z,Lotus Bakeries First-Half Profit,Climbs,20 %
2011-08-29T15:54:09Z,EU,Expanding,Companies ' Hostile-Takeover Defenses
2011-08-29T15:57:52Z,$ 221 Million,in,Five-Year Bonds
2011-08-29T16:01:00Z,Taiwan,Urges,Banks
2011-08-29T16:01:12Z,Trichet,Says,ECB Reassessing Inflation Risks
2011-08-29T16:01:36Z,Advance,in,Six
2011-08-29T16:03:38Z,Rusal Declines,in,Moscow
2011-08-29T16:03:59Z,Hon Hai ’s Income,Misses,Analysts ’ Estimates
2011-08-29T16:03:59Z,Hon Hai ’s Net Income,Misses,Analysts ’ Estimates
2011-08-29T16:06:01Z,Ruble,Strengthens,Most in Two Weeks
2011-08-29T16:06:01Z,Russian Ruble,Strengthens,Most
2011-08-29T16:12:20Z,Stocks,in,South Africa Head
2011-08-29T16:15:52Z,Fondiaria-SAI First-Half Loss,Narrows on,Non-Life
2011-08-29T16:17:50Z,14.1 % Stake,in,South Africa ’s Optimum Coal
2011-08-29T16:17:50Z,Glencore ’s Piruto Unit,Acquires,Stake in South Africa ’s Optimum Coal
2011-08-29T16:17:50Z,South Africa,in,Optimum Coal
2011-08-29T16:18:54Z,Export Tax,Lifts,Rosneft
2011-08-29T16:27:57Z,Nigerian Stocks,Fall to,19-Month Low
2011-08-29T16:27:57Z,Stocks,Fall to,19-Month Low
2011-08-29T16:29:00Z,Pulawy Shares,Move in,Warsaw
2011-08-29T16:35:17Z,Energy Future Unit,on,Solvency
2011-08-29T16:49:11Z,Assad,Stop Immediately,Violence
2011-08-29T16:49:11Z,Envoy,Urges,Assad
2011-08-29T16:49:11Z,Russian Envoy,in,Syria
2011-08-29T16:50:11Z,New York Harbor Gasoline,Strengthens on,Refinery Equipment Shut
2011-08-29T16:51:06Z,Hi-Media,of,Payment Business
2011-08-29T16:55:48Z,Chilean Peso,Climbs,U.S. Prospects Improve
2011-08-29T16:55:48Z,Peso,Climbs,U.S. Prospects Improve
2011-08-29T17:00:00Z,Fed Researcher,Sees,Little Harm
2011-08-29T17:03:26Z,Teva Patent Suit,Denied by,Federal Judge
2011-08-29T17:04:53Z,Tonight,for,Jets Game Due to Hurricane Irene
2011-08-29T17:22:13Z,Ghana,Raises Water Prices,Power-Generation Costs Climb
2011-08-29T17:26:50Z,Chicago Bears Cut Former New York Jets Draft,Pick,Vernon Gholston
2011-08-29T17:26:50Z,Chicago Bears Cut Former New York Jets Top Draft,Pick,Vernon Gholston
2011-08-29T17:29:45Z,U.S. Gulf Crude Premiums,Weaken,WTI-Brent Spread Narrows
2011-08-29T17:49:34Z,Copper Falls,in,New York
2011-08-29T18:03:40Z,MS Treatment,Denied by,Judge
2011-08-29T18:06:32Z,Nigerian Court,Frees Cleric Accused Over,Militant Attacks
2011-08-29T18:15:31Z,Gasoline,Fluctuates on,Oil Refinery Units Returning
2011-08-29T18:17:56Z,Gold Prices,Drop as,Equity Rally Cuts Demand
2011-08-29T18:26:50Z,$ 30 Billion,in,Iraq
2011-08-29T18:26:50Z,Waste,of Billion,Afghan Contracts
2011-08-29T18:27:52Z,Telemar Units Advance,Approve,Restructuring Plan
2011-08-29T18:44:38Z,Golf,in,Frys.Com Open
2011-08-29T19:00:01Z,Caviar,Served With,Harbor View
2011-08-29T19:00:01Z,Steak Chops,Served With,Harbor View
2011-08-29T19:01:35Z,Canada Prairie Harvest,Is Complete,14 %
2011-08-29T19:08:37Z,Yields,Sink as,Carstens Signals First Rate Cut
2011-08-29T19:09:36Z,Metro-North,Shut After,Irene
2011-08-29T19:11:13Z,Qaddafi Son Khamis,Accused of,Murdering Detainees
2011-08-29T19:19:48Z,JPMorgan,Waives,Fees for Tri-State Customers
2011-08-29T19:41:58Z,Wimbledon Champion Petra Kvitova,Loses in,First Round
2011-08-29T19:51:18Z,Cafes,With,Stranded Workers
2011-08-29T19:51:18Z,Wi-Fi Crowds Bookstores,Cafes With,Stranded Workers
2011-08-29T19:53:25Z,Crude Oil,Rises to,One-Week High in New York
2011-08-29T19:53:25Z,Oil,Rises to,One-Week High
2011-08-29T19:53:25Z,One-Week High,in,New York
2011-08-29T20:01:27Z,Chairman Face Probe,in,Argentina
2011-08-29T20:01:27Z,YPF CEO,Probe for,Accounting
2011-08-29T20:09:48Z,NASA ’s Hansen,Arrested,White House
2011-08-29T20:10:26Z,Saks CEO Sadove,Tightens,Inventory
2011-08-29T20:13:42Z,Labor Expert,Is Choice to,Obama ’s Economic Team
2011-08-29T20:32:12Z,CEO Buyout Bid,Valued,Million
2011-08-29T20:35:42Z,Dallas Cowboys,in,Salary Cap
2011-08-29T20:41:48Z,Consumer Spending,Climbs Than,Forecast on Purchases of Autos
2011-08-29T20:48:10Z,Obama,as,Top Economist
2011-08-29T20:48:10Z,Princeton,’s Krueger,Jobs-Credit Advocate
2011-08-29T20:48:10Z,Princeton ’s Krueger,Picked as,Obama ’s Economist
2011-08-29T20:50:54Z,SEC,Sues Florida Men for,Bilking Teachers
2011-08-29T20:53:04Z,Lojas Renner,Rises to,Six-Week High
2011-08-29T20:57:45Z,Little,Changed Since,July
2011-08-29T20:58:40Z,Utilities,Restore Power to,1.57 Million Customers Left by Irene
2011-08-29T21:07:57Z,Canadian Currency,Reaches Strongest in,3 Weeks on Boost
2011-08-29T21:07:57Z,Currency,Reaches Strongest in,3 Weeks on Boost
2011-08-29T21:10:44Z,Treasuries,Decline on,U.S. Evidence
2011-08-29T21:23:18Z,Qaddafi ’s Relatives,Arrive in,Algeria
2011-08-29T21:29:33Z,Arabs,Tell,NATO
2011-08-29T21:44:21Z,Building Companies,Surge After,Irene Damages U.S. East Coast
2011-08-29T21:46:29Z,Brazil ’s Rate Futures Yields,Spending,Halt Spurs Bets
2011-08-29T21:50:52Z,U.S. Nuclear Operators Survey Damage,Output After,Irene
2011-08-29T21:52:17Z,Wal-Mart Dispatches 800 Trucks,With,Storm Supplies
2011-08-29T21:55:06Z,Cerberus,Chatham for,Breach of Commitment
2011-08-29T21:55:06Z,Innkeepers Hotel Operator,Sues,Chatham for Breach
2011-08-29T22:00:52Z,Merkel,Tells,Voters Germany
2011-08-29T22:00:52Z,Voters Germany,Emerge,Stronger From Sovereign-Debt Crisis
2011-08-29T22:10:33Z,Help,Remains,No. 1 Movie With Million
2011-08-29T22:10:33Z,No. 1 Movie,With,$ 14.5 Million
2011-08-29T22:16:23Z,Alan Krueger 's Jobs Analysis,Was,Spot
2011-08-29T22:49:28Z,Building Approvals,Jump by,13 %
2011-08-29T22:49:28Z,New Zealand Dollar,Maintains,Gains
2011-08-29T23:00:01Z,Camera,Premiere at,Venice
2011-08-29T23:00:01Z,Madonna,Get Behind,Premiere
2011-08-29T23:00:01Z,Sudan,Violates,Southern Kordofan Cease-Fire
2011-08-29T23:01:00Z,U.K. Company Confidence,in,Economy Plunges
2011-08-29T23:26:50Z,Corn Futures,in,Decline
2011-08-29T23:29:08Z,CoreLogic,Explore,Potential Sale
2011-08-29T23:31:07Z,Barcelona Opens La Liga Defense,With,5-0 Rout as Fabregas
2011-08-29T23:50:43Z,MTV Video Music Awards,Draw Record Viewers With,Spears Tributes
2011-08-30T03:46:20Z,Deaths,Losses of,Power State-by-State
2011-08-30T04:01:01Z,BofA,to,$ 8.5 Billion Mortgage-Bond Settlement
2011-08-30T04:01:01Z,Google,Sued Over,Ads
2011-08-30T04:01:01Z,UAW,Wants,Wages for New Auto Workers
2011-08-30T04:07:09Z,Inflation Fight,Spurs,Record Swap-Curve Inversion
2011-08-30T04:39:58Z,Noda,Faces,Honeymoon
2011-08-30T05:07:32Z,Roger Federer,Win in,Their Thirties
2011-08-30T05:07:35Z,Foster,Up,Default Swaps
2011-08-30T05:07:35Z,SABMiller Bid,Drives Up,Foster ’s Default Swaps
2011-08-30T05:11:06Z,Pricing,Energy After,Bill
2011-08-30T05:48:11Z,Gazprom,Climbs as,U.S. Boosts Oil
2011-08-30T06:47:31Z,Japan Stocks Rise,Led by,Carmakers
2011-08-30T07:37:51Z,VTB May,Buy,B of Bank
2011-08-30T08:51:28Z,Nordea Job,Buoy,Profit
2011-08-30T08:53:31Z,Rise,to Premiums,CEO
2011-08-30T09:12:29Z,China,Faces,Dwindling Labor Supply
2011-08-30T10:28:56Z,Japan,’s Rises,Retail Sales Drop as Noda Picked for Premier
2011-08-30T10:28:56Z,Japan ’s Jobless Rate Rises,Drop as,Noda Picked
2011-08-30T10:33:44Z,Saudi Skyscraper Architect Grapples,With,Wind
2011-08-30T10:41:39Z,Syrian Troops,Kill,Seven Protesters
2011-08-30T10:41:39Z,Troops,Kill,Seven Protesters
2011-08-30T11:19:47Z,Vodafone May Merge Greek Mobile-Phone Unit,With,Wind Hellas
2011-08-30T11:51:15Z,Reserve Bank,Proposes,Sale Rules
2011-08-30T11:51:15Z,Tougher Capital,Rules for,New Banks
2011-08-30T12:15:44Z,Luxury-Home Prices,in,London Rise
2011-08-30T12:15:44Z,Overseas Investors,Seek,Secure Assets
2011-08-30T13:19:26Z,Finland ’s Collateral Demand,Leaves,EU Faced
2011-08-30T13:22:03Z,Berlusconi,Accepts,Allies ’ Demands
2011-08-30T15:23:30Z,Chan,Loses,Grip
2011-08-30T15:52:08Z,May,Decide,Zuma ’s Future
2011-08-30T15:52:40Z,Jain,Fails With,Equity
2011-08-30T16:49:18Z,Workers,Begin Restoring Power by,Irene
2011-08-30T16:50:50Z,IRS,in,Tax Court
2011-08-30T16:50:50Z,Kenneth Lay,Triumphs Over,IRS in Tax Court
2011-08-30T18:52:41Z,Gold Advances,Spur,Economy
