2013-04-01T00:01:00Z,Illinois Pension Bills,Squeeze Penalty to,2011 Low
2013-04-01T01:24:59Z,China ’s Stock Futures Drop,Misses,Estimates
2013-04-01T01:53:06Z,Bond Risk,in,Asia ex-Japan Increases
2013-04-01T02:09:04Z,3 %,in,Fiscal 2013
2013-04-01T02:37:52Z,One-Week High,Gain on,Debt Rating
2013-04-01T02:37:52Z,Peso,Touches,Bonds Gain on Debt Rating
2013-04-01T02:37:52Z,Philippine Peso,Touches,Bonds Gain
2013-04-01T02:39:30Z,December,Since Slumps,Inventories
2013-04-01T02:39:30Z,Lowest,to Slumps,Inventories
2013-04-01T02:50:18Z,Brent-WTI Crude Spread,Widens to,Texas
2013-04-01T03:17:04Z,Dollar Seen,Reducing,Demand
2013-04-01T03:17:04Z,Stronger Dollar Seen,Reducing,Demand
2013-04-01T04:00:01Z,Baylor,Ousted by,Louisville
2013-04-01T04:00:01Z,NCAA Women,From,Tournament
2013-04-01T04:00:13Z,Pinnacle,Proposes,Canada ’s Tallest Residential Tower
2013-04-01T04:00:33Z,Manufacturing,Grew on,Equipment Demand
2013-04-01T04:01:00Z,Aetna,Opposes,ERs
2013-04-01T04:01:00Z,FedEx,Joins Boeing In,Renewed Tax Rate Cut Pitch
2013-04-01T04:01:00Z,Washington Wines,Pack,Little Else
2013-04-01T04:01:15Z,$ 42 Million,in,Awards Questioned by Congress
2013-04-01T04:04:00Z,Points,Wins,Golf ’s Houston Open
2013-04-01T04:15:16Z,Astros Defeat Rangers 8-2,in,AL Debut
2013-04-01T04:24:45Z,Fantasy Baseball Teams,Make,Millions
2013-04-01T04:57:34Z,Nomura,Hires,Most Graduates
2013-04-01T05:00:00Z,Consumer Prices,Rise in,Five Years
2013-04-01T05:00:00Z,Peruvian Consumer Prices,Rise at,Fastest Pace
2013-04-01T05:00:00Z,Peruvian Consumer Prices Rise,in,Five Years
2013-04-01T05:10:51Z,Women Tourists,Avoid,India
2013-04-01T05:17:19Z,GD Midea,Jumps on,Parent Buyout
2013-04-01T06:09:38Z,6.8 %,in,April-Sept
2013-04-01T06:22:16Z,Tankan Survey,Misses,Estimates
2013-04-01T06:50:23Z,Japan ’s Business Pessimism,Shows,Challenges
2013-04-01T07:10:30Z,Export Data,Tensions With,North Korea
2013-04-01T07:10:30Z,Tensions,With,North Korea
2013-04-01T07:21:59Z,China Manufacturing,Expands at,Faster Pace
2013-04-01T07:23:16Z,Rubber Trades,in,Bear Market
2013-04-01T07:50:01Z,Serbia,Extends,Bids Deadline
2013-04-01T08:24:39Z,Indonesia,From,Sumatra Fall
2013-04-01T08:48:50Z,37.9 %,to Rose,Fuel
2013-04-01T08:48:50Z,February,in Rose,Fuel
2013-04-01T08:48:50Z,Malawi Inflation Rate Rose,in,February on Corn
2013-04-01T08:52:26Z,Yuan,Rises to,High on PBOC Fixing
2013-04-01T09:34:48Z,Singapore Home Prices Gain,in,Three Quarters
2013-04-01T10:00:15Z,Iran,Bristles for,Hosting Gathering
2013-04-01T10:11:31Z,South Korea,Offers,Tax Breaks
2013-04-01T10:21:48Z,Tunisia,Expects,$ 1.8 Billion IMF Accord
2013-04-01T10:30:22Z,Japan ’s Tankan,Misses,Estimates
2013-04-01T10:59:46Z,BP-Led Group,Weighing,Nabucco
2013-04-01T10:59:46Z,Nabucco,Plans for,EU Gas Link
2013-04-01T12:16:01Z,Fridman ’s Altimo May,Buy Out,Orascom Minorities
2013-04-01T12:32:48Z,Genworth,Wins Approval to,Distance Mortgage-Insurance Business
2013-04-01T12:35:10Z,Cold,Makes,Last Stand in East
2013-04-01T12:35:50Z,Bovespa-Index Futures Decline,in,Sao Paulo
2013-04-01T12:43:36Z,First Solar,Buys,150-Megawatt California Solar Power Project
2013-04-01T12:43:47Z,Petrobras,Avoiding Bond Market as,Selloff Deepens
2013-04-01T12:51:11Z,PMI,Says,Investor Ends Talks
2013-04-01T13:00:55Z,Moneyball Analytics,Will Kill,Loyalty
2013-04-01T13:08:21Z,Fortress,Hires,Goldman Sachs ’ Shell
2013-04-01T13:29:10Z,Egypt Bond Yield,Jumps to,7-Month High
2013-04-01T13:32:37Z,Whirlpool,Stop,Moldy Washer Suit
2013-04-01T13:34:20Z,Airlines,Rejected by,Top Court Price Advertising Rules
2013-04-01T13:36:04Z,Oil Industry,Rebuffed on,Air-Quality Rules
2013-04-01T13:53:49Z,Brazilian Stocks,Falls on,Inflation Outlook
2013-04-01T13:53:49Z,Most Brazilian Stocks,Falls as,Renner Drops
2013-04-01T13:54:53Z,Lincoln Tunnel,Opened After,Truck Overturns
2013-04-01T14:00:01Z,Construction Spending,Rises on,Gain
2013-04-01T14:03:22Z,Treasuries,Erase,Decline
2013-04-01T14:04:45Z,Chelsea,Beats,Manchester United
2013-04-01T14:04:45Z,Manchester United,Reach,F.A. Cup Semifinal
2013-04-01T14:06:03Z,Currency Swings,Drop on,Foreign Demand
2013-04-01T14:06:03Z,Mexican Currency Swings,Drop on,Foreign Demand
2013-04-01T14:10:28Z,Ride Bicycles,in,Public
2013-04-01T14:12:28Z,Nicotine Gum Labels,Revised by,FDA
2013-04-01T14:24:08Z,Insiders,Turn Angel Investors in,Lobbying Twist
2013-04-01T14:24:08Z,Political Insiders,Turn,Angel Investors
2013-04-01T14:25:15Z,Suncor,Goes,Direct to Refiners
2013-04-01T14:32:34Z,Serbia Wants Kosovo Pledge,in,Northern Serb Areas
2013-04-01T14:55:42Z,China,Behind,Avian Flu
2013-04-01T15:02:20Z,Lower Corn,Lifts,Output
2013-04-01T15:07:41Z,Hogs,Slide on,Signs
2013-04-01T15:08:05Z,Gulf Capital,Raises,$ 215 Million SME Lending Fund
2013-04-01T15:11:58Z,Prosecutors,in,Aurora Massacre
2013-04-01T15:15:04Z,Treasuries ’ Lure,Abroad Grows in,$ 3 Trillion Fed Account
2013-04-01T15:30:09Z,Biggest Changes,in,Implied Volatility
2013-04-01T15:30:09Z,U.S. Stock Options,With,Biggest Changes in Implied Volatility
2013-04-01T15:37:27Z,Heat Beat,Spurs Without,Wade
2013-04-01T15:38:27Z,It,Keep Farms on,Care Concern
2013-04-01T15:38:27Z,Want It,Keep,Farms
2013-04-01T15:51:46Z,Majority,Get,Education on Immigration
2013-04-01T15:55:56Z,Invermar,Plunges in,Santiago
2013-04-01T16:02:21Z,Finland,’s Outotec,Metso
2013-04-01T16:02:21Z,Serb RTB Bor Eyes Copper Research,With,Finland ’s Outotec
2013-04-01T16:37:31Z,CSN,Is,Steelmaker
2013-04-01T16:37:31Z,Profit,Beats,Forecasts
2013-04-01T16:41:05Z,Brazil,Provides,$ 1.5 Billion
2013-04-01T16:48:37Z,Mandela,Stays After,Fluid Drained
2013-04-01T17:14:33Z,Fannie Mae Profits,Complicate,U.S. Housing-Finance Overhaul
2013-04-01T17:36:09Z,African Republic Leader,Appoints,Government
2013-04-01T17:36:09Z,Central African Republic Leader,Appoints,Government
2013-04-01T17:36:09Z,Republic Leader,Appoints,Government
2013-04-01T18:18:44Z,Canadian Crude,Weakens After,Exxon Shuts Southbound Pipeline
2013-04-01T18:18:44Z,Crude,Weakens After,Exxon Shuts Southbound Pipeline
2013-04-01T18:19:25Z,Caterpillar CEO,Says,Visa Reform Needed
2013-04-01T18:19:25Z,Visa Reform Needed,Ease,Workforce Gaps
2013-04-01T18:39:32Z,SEC,Backs,Forcing Bank Disclosures on Muni-Bond Campaigns
2013-04-01T18:48:33Z,Rubiales,Hits Two-Week High on,Brazilian Oil Block Deal
2013-04-01T18:59:46Z,Anglo,Discusses Port With,Batista
2013-04-01T19:00:35Z,Ellington,Buy,Rentals
2013-04-01T19:02:06Z,Ultra-Low Sulfur Diesel Futures Contract,Replaces,Heating Oil
2013-04-01T19:44:17Z,Brokest,in,Cyprus Bailout
2013-04-01T20:00:20Z,Traders ’ Pranks,Were,Routine on U.S. Exchanges
2013-04-01T20:04:53Z,Corn,Enters Bear Market on,Signs of Ample Supply
2013-04-01T20:04:53Z,Signs,in,U.S.
2013-04-01T20:05:49Z,American Greetings,Agrees by,Weiss Family
2013-04-01T20:05:49Z,Greetings,Agrees by,Weiss Family
2013-04-01T20:12:04Z,Manufacturing,Cools as,Government Cuts Loom
2013-04-01T20:14:08Z,Oil Options Volatility,Rises as,Futures Fall
2013-04-01T20:16:05Z,Chesapeake,Taps,COO Dixon
2013-04-01T20:17:04Z,Cowgill,Behind,Grand Slam
2013-04-01T20:17:04Z,Mets Beat Padres 11-2,in,MLB Opener Behind Cowgill 's Grand Slam
2013-04-01T20:17:15Z,Novartis Cancer-Drug Patent,Denied by,India Supreme Court
2013-04-01T20:19:38Z,Quicksilver,Selling,Stake
2013-04-01T20:25:50Z,GameStop,Climbs Since,2009 on New Console Outlook
2013-04-01T20:27:41Z,Rallying Stocks,Generate,Return
2013-04-01T20:27:41Z,Stocks,Generate,Best Return
2013-04-01T20:27:59Z,China,Cited by,U.S.
2013-04-01T20:28:11Z,Lear,Reaches Accord With,Dissident Shareholders
2013-04-01T20:39:55Z,Mumbai,Gets,Closure
2013-04-01T20:48:59Z,Palestinian Prime Minister Fayyad,Hospitalized With,Infection
2013-04-01T20:48:59Z,Prime Minister Fayyad,Hospitalized With,Infection
2013-04-01T20:54:46Z,Diller-Backed Aereo,Beats,Network Bid
2013-04-01T20:54:49Z,Watchdog,Says,U.S. Regulator Needs Image Polishing
2013-04-01T20:57:15Z,First Time,in,Week
2013-04-01T21:04:16Z,Visa ’s Partridge,Stays as,Consultant
2013-04-01T21:04:56Z,LDK,Tops Slump as,SouFun Drops on Housing Curbs
2013-04-01T21:10:10Z,Global Cotton-Production Forecast,Raised,Demand Cut
2013-04-01T21:11:08Z,Demand,Holding,Strong
2013-04-01T21:11:08Z,Holding,Strong in,Asia
2013-04-01T21:11:08Z,Saudi Arabia ’s Al-Naimi,Sees,Demand
2013-04-01T21:12:16Z,Burkle ’s Yucaipa,Gets,Delano Hotel Stake in Morgans Swap
2013-04-01T21:12:16Z,Delano Hotel Stake,in,Morgans Swap
2013-04-01T21:24:06Z,U.S. Stocks,Fall,American Manufacturing Index Slips
2013-04-01T22:03:55Z,NFL,from,Browns
2013-04-01T22:03:55Z,San Francisco 49ers,Get,Quarterback Colt McCoy
2013-04-01T22:17:43Z,Copyright Ruling,in,Used-Digital-Song Case
2013-04-01T22:17:43Z,Vivendi,Wins,Copyright Ruling in Used-Digital-Song Case
2013-04-01T22:18:21Z,U.S. Output,Hits,Metals
2013-04-01T22:27:05Z,Belanich Brothers,Discuss,Growth
2013-04-01T22:29:05Z,Pipeline Spill,in,Arkansas
2013-04-01T22:29:30Z,Facebook,Must,Face Trademark Trial Over Timeline
2013-04-01T22:30:15Z,Silence,Can,For Supreme Court Can Golden
2013-04-01T22:30:41Z,NRA,Opposes,Hunting
2013-04-01T22:42:55Z,$ 165.8 Bln,in,Week
2013-04-01T23:00:00Z,Corn,Markets on,Supply
2013-04-01T23:00:00Z,Michelin Heaven,Lures U.K. Gourmets to,Cambridge
2013-04-01T23:00:00Z,Motown Pulls,in,Broadway Crowds
2013-04-01T23:00:00Z,Stalin,Splits Georgia as,Statues Smashed
2013-04-01T23:00:00Z,U.K.,Replaces,Failed FSA
2013-04-01T23:22:19Z,China Gold Tibet Mine Rescue,Suspended,Death Toll Rises
2013-04-01T23:22:19Z,Death Toll,Rises to,36
2013-04-01T23:32:23Z,Fulham,Push,QPR
2013-04-01T23:32:23Z,QPR,Closer to,Premier League Relegation
2013-04-01T23:47:39Z,Morgans Hotel Group Director,Sues Over,Recapitalization
2013-04-02T00:01:03Z,BP,Halt,Gulf Oil-Spill Settlement Payments
2013-04-02T00:13:39Z,AMC ’s Dead Finale,Draws,12.4 Million Viewers
2013-04-02T00:13:39Z,AMC ’s Finale,Draws,12.4 Million Viewers
2013-04-02T00:13:39Z,AMC ’s Walking Dead Finale,Draws,12.4 Million Viewers
2013-04-02T00:13:39Z,AMC ’s Walking Finale,Draws,12.4 Million Viewers
2013-04-02T01:16:52Z,Foreigners,Sell,Korean Stocks
2013-04-02T01:53:54Z,Taiwan,Let,Mainland Lenders Own Bigger Stakes
2013-04-02T02:54:20Z,Australian Dollar Gains,Will Hold,Rates
2013-04-02T02:54:20Z,Dollar Gains,Will Hold,Rates
2013-04-02T04:00:00Z,Small-Business Insurance Market,Delayed,Year
2013-04-02T04:00:02Z,Relativity,Raises,$ 115 Million Loan
2013-04-02T04:00:54Z,Arkansas Oil Spill,Raises,Scrutiny of Pipeline Network
2013-04-02T04:01:01Z,Colorado Prosecutors,Seek,Death Penalty
2013-04-02T04:01:01Z,Death Penalty,in,Movie Massacre
2013-04-02T05:41:59Z,Dodgers,Open With,MLB
2013-04-02T05:41:59Z,Nationals Open,With,MLB
2013-04-02T05:41:59Z,Yankees,Lose to,Red Sox
2013-04-02T08:03:45Z,Apple CEO Cook,Apologizes for,China IPhone Warranties
2013-04-02T08:32:45Z,Hong Kong Businesses,Vanish,Rents Soar
2013-04-02T08:39:32Z,German Inflation,Probably Slowed in,March
2013-04-02T08:39:32Z,Inflation,Slowed in,March
2013-04-02T08:47:24Z,World-Beater Cut,in,London Metal
2013-04-02T11:13:13Z,Denmark,Drop,Risky Mortgages
2013-04-02T11:13:13Z,IMF,Urges,Denmark
2013-04-02T11:50:42Z,Targets,in,Talks
2013-04-02T13:42:36Z,Nuance,Jumps,9.3 %
2013-04-02T13:53:45Z,Napolitano,Extends,Italy Impasse
2013-04-02T14:31:32Z,Dell CEO,Says,Business Unit
2013-04-02T14:31:32Z,Making,Progress Amid,LBO Talks
2013-04-02T16:30:14Z,Jazztel,Bucking,Slump Seen Luring Vodafone Bid
2013-04-02T16:36:24Z,MTS,Fight,VTB ’s Billion Tele2 Russian Takeover
2013-04-02T19:19:48Z,Stay,in,Bankruptcy
2013-04-02T20:10:35Z,U.S.,Reverses,Medicare Rate Cut Decision
2013-04-02T21:18:31Z,Unemployment,Rises,Record
