2010-11-16T00:00:00Z,German Investor Confidence,in,Seven Months on Growth
2010-11-16T00:00:01Z,Chef Albert Roux,Has,Advice for Protege Ramsay
2010-11-16T00:00:01Z,Saint Laurent Record,Challenged by,Million Design-Art Auction
2010-11-16T00:00:01Z,Springsteen,Opens,Vaults
2010-11-16T00:01:00Z,Bondholders,Looms,G-20 Agrees on Basel Rules
2010-11-16T00:01:00Z,G-20,Agrees on,Basel Rules
2010-11-16T00:04:12Z,Credit Suisse,Recommends,Switching
2010-11-16T00:21:23Z,UBS,Raises,Dollar Forecasts on U.S. Recovery Signs
2010-11-16T00:34:16Z,Dolphins,Lose Chad Pennington for,Season With Shoulder Injury
2010-11-16T00:34:16Z,Season,With,Shoulder Injury
2010-11-16T00:42:32Z,Coal Exports,Fall as,Queue
2010-11-16T00:59:25Z,Nickel,Heading by,2013
2010-11-16T01:06:42Z,Copper,Resumes Decline on,Concern China Demand May Slow
2010-11-16T01:15:12Z,India,With,Maruti
2010-11-16T01:15:12Z,India 's Maruti,With Partner,Line Reports
2010-11-16T01:15:12Z,Skoda May Partner,With,India 's Maruti
2010-11-16T01:18:00Z,China,Meet,80 % of Demand
2010-11-16T01:25:19Z,Onion Prices,in,India Rise
2010-11-16T01:26:43Z,Australian Dollar,Is Near,Two-Week Low
2010-11-16T01:26:43Z,Dollar,Is Near,Two-Week Low
2010-11-16T01:26:43Z,Minutes,Say,Consumers Cautious
2010-11-16T01:37:06Z,7 % Stake,in,Yunnan Copper 's Parent
2010-11-16T01:37:06Z,Yunnan Copper,'s Parent,21st Century Reports
2010-11-16T01:41:15Z,Chesapeake,Closes on,CNOOC Unit Deal for Eagle Shale Holdings
2010-11-16T01:58:26Z,Lloyds TSB Unit Plans Benchmark Australian Dollar Bond Sale,in,Debut Offer
2010-11-16T02:00:00Z,U.S.,Earns Blame for,Trade Mess
2010-11-16T02:01:13Z,Union Leader,Striking,Collahuasi Miners Ready
2010-11-16T02:02:51Z,China Sells Sugar,Pork From,Stockpiles
2010-11-16T02:02:51Z,Pork,in,Inflation Fight
2010-11-16T02:03:24Z,Hyundai Group,Named,Preferred Bidder
2010-11-16T02:14:38Z,Construction Bank Chairman Guo,Says,Halt of Loans for Developers Unlikely
2010-11-16T02:22:15Z,China 's Economic Index,Rises for,Fifth Month
2010-11-16T02:22:15Z,China 's Leading Economic Index,Rises Amid,Sustained Growth
2010-11-16T02:24:43Z,Japan,Citing,Trade Minister
2010-11-16T02:26:33Z,Boston Generating,Backs,$ 1.1 Billion Constellation Sale
2010-11-16T02:31:55Z,China Vanke,Fall to,Six-Week Low on New Property Curbs
2010-11-16T02:31:58Z,Poland Files Papers,in,Japan
2010-11-16T02:45:53Z,$ 3.6 Billion,in,Assets
2010-11-16T02:50:36Z,Palm Oil Futures,in,Malaysia Gain for Second Day
2010-11-16T02:51:25Z,Yuan Little,Changed on,Concern
2010-11-16T02:59:20Z,Chinese Aluminum Demand May,in,Double Decade Spurred by Economy
2010-11-16T02:59:20Z,Rusal,Says,Aluminum Demand May in Decade Spurred
2010-11-16T03:08:49Z,China,Sees,Faster Gains in Foreign Direct Investment
2010-11-16T03:08:49Z,Faster Gains,in,Foreign Direct Investment
2010-11-16T03:26:59Z,BHP,Says,Potash Corp
2010-11-16T03:26:59Z,Bid Potential Returns,Outweighed,Risk
2010-11-16T03:30:00Z,Pacino 's Fiery Shylock Burns,in,Broadway
2010-11-16T03:33:11Z,China,'s Producers,Anhui Conch Rally on Low-Income Housing Report
2010-11-16T03:33:11Z,China 's Cement Producers,Rally on,Low-Income Housing Report
2010-11-16T03:38:53Z,Google,With,Software
2010-11-16T03:51:27Z,Cathay Pacific,Climbs to,Record
2010-11-16T03:52:31Z,More Projects,in,Advanced Economies
2010-11-16T03:54:38Z,Rubber Advances,in,Three Days on Weaker Yen
2010-11-16T04:08:38Z,Big C,Surges After,Announcing Acquisition
2010-11-16T04:08:38Z,C,Surges After,Announcing Acquisition of Carrefour Stores
2010-11-16T04:18:37Z,Building Collapse,in,Eastern New Delhi
2010-11-16T04:23:11Z,Expects No,Move,Year
2010-11-16T04:31:21Z,Rupee Rebounds,Are Repatriating,Income
2010-11-16T04:33:18Z,South Korea,Raises,Interest Rate
2010-11-16T04:38:14Z,Gold,Reversing,Decline
2010-11-16T04:45:37Z,Mexico 's Lower House,Spending,Part
2010-11-16T04:53:16Z,Asian Stocks,Fluctuate as,U.S. Retail Sales Temper Tightening Concerns
2010-11-16T04:53:16Z,Stocks,Fluctuate as,U.S. Retail Sales Temper Tightening Concerns
2010-11-16T05:00:00Z,Singapore Export Growth,Quickens as,Shipments
2010-11-16T05:00:35Z,Agency Bond Transparency,Derailed by,Brokers
2010-11-16T05:01:00Z,Bailout Panel Warns,Urges,Stress Tests
2010-11-16T05:01:00Z,Taxidermist,Stuffs,Barnum 's Elephant
2010-11-16T05:01:06Z,Industrial Production,Probably Increased in,October
2010-11-16T05:22:53Z,Adviser Basu Expects Sharp Decline,in,India 's Inflation
2010-11-16T05:22:53Z,India,in,Inflation in Next Few Weeks
2010-11-16T05:22:53Z,India 's Inflation,in,Next Few Weeks
2010-11-16T05:27:16Z,Eiffage 's Board Will,Have,Le Figaro Reports
2010-11-16T05:27:28Z,Mortgages,in,October
2010-11-16T05:27:28Z,Pimco ’s Gross Cuts Government Debt,Adds,Mortgages
2010-11-16T05:29:36Z,Japan 's Lower House,Approves Funding for,Kan 's Economic Stimulus Package
2010-11-16T05:29:36Z,Kan,for,Economic Stimulus Package
2010-11-16T05:30:52Z,IMF,From,Shocks Facility
2010-11-16T05:38:21Z,Papandreou,Tells,Figaro
2010-11-16T05:42:09Z,Metorex May,Move in,Early Johannesburg Trading
2010-11-16T05:48:25Z,Aristocrat Viridian WS Slot Machines Infringe Patents,Claims in,Suit
2010-11-16T05:48:25Z,IGT Claims,in,Suit
2010-11-16T05:53:59Z,Low Cost Bank,in,Senegal
2010-11-16T06:01:22Z,Spymaster,'s Defection,Kommersant
2010-11-16T06:03:58Z,Dudley,Says,QE2 Critics
2010-11-16T06:05:20Z,Kabel Deutschland 's Second-Quarter Loss,Narrows,64 Percent
2010-11-16T06:29:34Z,Nigerian Rebel Group,Confirms Attack on,Claims 7 Hostages
2010-11-16T06:29:34Z,Rebel Group,Confirms Attack on,Claims 7 Hostages
2010-11-16T06:30:12Z,U.K. Author Shadrake,Gets,Fine for Singapore Contempt
2010-11-16T06:32:16Z,Infineon,Proposes Dividend,Quarterly Earnings Increase
2010-11-16T06:35:02Z,China Guodian May Invest 20 Billion Yuan,in,Solar
2010-11-16T06:36:20Z,Atos Origin,Gets,U.K. Government Contract Extension Worth
2010-11-16T06:36:24Z,Rwanda Central Bank,Cuts Reuters Reports to,6 %
2010-11-16T06:40:10Z,Lithuanian Prime Minister Kubilius,Is,Country 's Least Popular Politician
2010-11-16T06:40:10Z,Prime Minister Kubilius,Is,Country 's Least Popular Politician
2010-11-16T06:45:46Z,Banco Popolare Rights Offer,Save,Million
2010-11-16T06:57:21Z,Barclays,Says,U.K. Breakeven Rates at High
2010-11-16T06:57:21Z,U.K. Breakeven Rates,Widen on,CPI Data
2010-11-16T07:00:00Z,Michelin Guide Crowns New Three-Star Restaurant,in,Switzerland
2010-11-16T07:02:05Z,Euro,Rises on,German Data
2010-11-16T07:03:25Z,Lithuania,'s Plant,VZ Reports
2010-11-16T07:03:36Z,Burberry Group Reports Pretax Profit,in,Latest Period
2010-11-16T07:03:36Z,Wheat Declines,May,May Easing
2010-11-16T07:05:41Z,10-Year Yield,Rises to,2.59 Percent
2010-11-16T07:06:16Z,Ruble,Slips,0.6 %
2010-11-16T07:06:57Z,Its,Unit,MF Reports
2010-11-16T07:06:57Z,Monte Paschi di Siena,Received,Offer
2010-11-16T07:09:02Z,Alcatel,Sees,Fortune
2010-11-16T07:09:02Z,India,End,Poverty
2010-11-16T07:09:02Z,Wal-Mart,Urges,India
2010-11-16T07:17:53Z,Euro,Erases,Gains
2010-11-16T07:23:00Z,Record Quarter,Have Says,May
2010-11-16T07:23:00Z,Salmon Farming Industry,Have Says,May
2010-11-16T07:25:35Z,Vestas Wind Systems,Wins U.S. Order With,90 Megawatt Capacity
2010-11-16T07:27:47Z,Trading Firms,Drop on,Commodities
2010-11-16T07:33:27Z,Gains,Losses Amid,Signs of U.S. Recovery
2010-11-16T07:34:11Z,Dubai Holding,Gets,Billion Fresh Capital From Government
2010-11-16T07:40:44Z,Hana Financial Said,in,Talks With Lone Star Buy
2010-11-16T07:40:44Z,Talks,With,Lone Star
2010-11-16T07:45:03Z,Rio,Testify to,Australia Senate Committee
2010-11-16T07:51:32Z,Renaissance Capital CEO Jennings,Sees,Russian African IPOs
2010-11-16T07:51:32Z,Russian African IPOs,in,Hong Kong
2010-11-16T07:52:03Z,Zhou Targets Liquidity,Could Impose,Price Controls
2010-11-16T07:52:32Z,China Stocks,Plunge,Controls
2010-11-16T07:55:41Z,Chile May,Develop Geothermal Energy Projects With,Bolivia
2010-11-16T07:57:41Z,Eisai Plans Sale,in,U.S.
2010-11-16T08:02:56Z,Biotech-Crop Approval Plan,Gets,Analysis by European Lawyers
2010-11-16T08:03:49Z,European Stocks,Slide on,Ireland Debt Concern
2010-11-16T08:03:49Z,Stocks,Slide on,Ireland Debt Concern
2010-11-16T08:04:57Z,Hypo Real Estate Loss,Reducing,Provisions
2010-11-16T08:10:06Z,Metorex,Heads on,Ruashi Electrical Faults
2010-11-16T08:18:46Z,Rates,Curb,Inflation
2010-11-16T08:18:46Z,Stocks,Decline,Rates Raised
2010-11-16T08:19:48Z,China,Damping,Demand
2010-11-16T08:30:13Z,Asian Currencies,Strengthen on,Bank of Korea 's Rate Decision
2010-11-16T08:30:13Z,Currencies,Strengthen on,Bank of Korea 's Rate Decision
2010-11-16T08:30:13Z,Korea,of,Rate Decision
2010-11-16T08:40:02Z,Data May Show Inflation,Stayed,High
2010-11-16T08:45:45Z,Factory,in,Ulsan
2010-11-16T08:46:16Z,UBS Chief Gruebel,Reiterates,Targets
2010-11-16T08:47:51Z,Collapse,Kills,66
2010-11-16T08:47:51Z,New Delhi Authorities,Search for,Building Owner
2010-11-16T08:55:45Z,Solar Millennium,Receives,Approvals
2010-11-16T08:56:04Z,Miner,Dies at,Ezulwini
2010-11-16T09:04:49Z,Zambia Kwacha,for Shilling,Investec
2010-11-16T09:08:35Z,Hyundai Group,Named,Preferred Bidder
2010-11-16T09:10:07Z,Beat Sensex Index,'s Gain,CLSA Analyst
2010-11-16T09:19:20Z,Carbon Permit Prices,Decline Near,Two-Year High
2010-11-16T09:22:58Z,Polish Bonds Rise,Drops on,Central Banker Bratkowski Rate Comments
2010-11-16T09:24:45Z,Poland,Starts,Marketing
2010-11-16T09:34:10Z,Equities,Snap,Three-Day Gain Led
2010-11-16T09:34:10Z,Three-Day Gain,Led by,Anglo
2010-11-16T09:38:37Z,U.K. Pound,Rises,Versus Dollar
2010-11-16T09:46:13Z,Taylor Wimpey,Gets Land-Buying Flexibility With,Billion Loan Facility
2010-11-16T09:46:49Z,Angola 's Economy,is Likely Expand IMF Forecasts by,2.5 % Year
2010-11-16T09:52:11Z,Airbus,Says Rolls to,Ship Engines From A380 Factory
2010-11-16T09:52:12Z,Deutsche Telekom,Reaches Settlement at,Company
2010-11-16T09:54:12Z,Israel,Eases,Restrictions
2010-11-16T09:54:12Z,Restrictions,in,West Bank
2010-11-16T09:54:12Z,West Bank,Jerusalem for,Eid Al-Adha Holiday
2010-11-16T09:54:16Z,Intel,Working With,TV Makers
2010-11-16T10:00:01Z,Little,Changed in,4th Quarter
2010-11-16T10:00:38Z,Axa,Seeks Billion in,Cost Cuts to Free
2010-11-16T10:01:22Z,Spain Falls Short,Set for,Bills Auction
2010-11-16T10:03:28Z,Euro Jumps Versus Dollar,Yen After,ZEW Report
2010-11-16T10:05:45Z,$ 10 Billion Hydropower Project,for Myanmar,China Thailand Agree to Study
2010-11-16T10:05:45Z,Myanmar,Agree to,Study
2010-11-16T10:10:51Z,Stay Little,Changed,Investor Confidence Unexpectedly Rises
2010-11-16T10:13:02Z,Sberbank,Hires Banks for,$ 2 Billion Loan
2010-11-16T10:13:46Z,European October Inflation,Accelerates on,Surging Energy Costs
2010-11-16T10:13:46Z,October Inflation,Accelerates on,Surging Energy Costs
2010-11-16T10:15:24Z,Angola Plans,in,January
2010-11-16T10:15:24Z,Ship,to Plans,Plan
2010-11-16T10:19:19Z,Most Sales,in,Six Weeks
2010-11-16T10:28:09Z,High-Yield Bond Risk,Rises to,Four-Week High in Europe
2010-11-16T10:30:01Z,King,Says,U.K.
2010-11-16T10:31:38Z,Airbus Said,in,2011
2010-11-16T10:33:37Z,Chelsea 's John Terry May,Be Out,Out Months
2010-11-16T10:33:37Z,Out Months,With,Leg Injury
2010-11-16T10:37:10Z,Nedbank,Says,Pattern Signals Reverse
2010-11-16T10:37:10Z,Rand,Says,Pattern Signals Reverse
2010-11-16T10:37:48Z,Gome Drops,in,Hong Kong
2010-11-16T10:37:49Z,Burberry,Beats,Profit Estimates
2010-11-16T10:46:32Z,United Nations,Urges,Calm
2010-11-16T10:46:39Z,Greece,in,Hidden Debt
2010-11-16T10:54:04Z,Asian Stocks,Drop on,Signs Governments
2010-11-16T10:54:04Z,Stocks,Drop on,Signs Governments
2010-11-16T11:02:24Z,Face,Opposition to,Debt-Crisis Plan
2010-11-16T11:02:24Z,Merkel Resilient,in,Face of European
2010-11-16T11:03:07Z,Microfinance Companies,in,India
2010-11-16T11:03:31Z,Hong Kong 's Unemployment Rate,Remains at,20-Month Low
2010-11-16T11:04:56Z,Southern Sudanese,Registering for,Independence Referendum
2010-11-16T11:08:48Z,Casino,Seek,Billion Loan
2010-11-16T11:15:26Z,British Land Earnings,Increase,3.7 %
2010-11-16T11:15:26Z,Company,as,Real Estate Gains Value
2010-11-16T11:15:26Z,Land Earnings,Increase,3.7 %
2010-11-16T11:16:54Z,Coffee Harvest Delays Lengthen,in,Vietnam
2010-11-16T11:17:34Z,King,Predicts,Elevated U.K. Inflation After Rate Breaches
2010-11-16T11:27:55Z,Deutsche Bank 's Jain,Says,Unlikely
2010-11-16T11:27:55Z,Unlikely,Default of,Euro Countries
2010-11-16T11:40:06Z,Kenya,in,Talks With Reliance
2010-11-16T11:40:06Z,Reliance,Tatas for,Investments
2010-11-16T11:40:06Z,Talks,With,Reliance
2010-11-16T11:40:46Z,Lenovo Falls Most,Said to,Exit
2010-11-16T11:40:56Z,SPDR Gold Holdings,Invests in,iShares
2010-11-16T11:40:56Z,Soros,Reduces,Invests
2010-11-16T11:47:37Z,October Producer Prices,Accelerate,Level Since May
2010-11-16T11:47:37Z,Russian October Producer Prices,Accelerate,Level
2010-11-16T11:47:53Z,Bond Yields,Hold,Cash Crunch
2010-11-16T11:47:53Z,Cash,Crunch at,Indian Banks
2010-11-16T11:50:21Z,Jain,Backs,Postbank Takeover
2010-11-16T11:52:53Z,$ 436 Million,in,Mozambique Mobile Operation
2010-11-16T12:05:26Z,Foschini Group,Says,First-Half Headline Earnings Advance
2010-11-16T12:06:09Z,Asda,Says,Third-Quarter Sales
2010-11-16T12:06:09Z,VAT,Increased by,1.3 Percent
2010-11-16T12:16:55Z,Audi,in,Europe
2010-11-16T12:16:55Z,Mercedes-Benz,Loses Ground to,October Luxury-Car Sales
2010-11-16T12:17:22Z,Lead-Recycler Gravita India,Jumps,68 %
2010-11-16T12:30:23Z,Wen,Says on,State TV
2010-11-16T12:42:13Z,Auditor,Says,State Lost 1.4 Trillion Rupees
2010-11-16T12:42:13Z,Indian Auditor,Says,State Lost 1.4 Trillion Rupees in Mobile Airwaves Sale
2010-11-16T12:42:13Z,State Lost 1.4 Trillion Rupees,in,Mobile Airwaves Sale
2010-11-16T12:47:27Z,Tam Third-Quarter Net,Eliminating,$ 339 Million Provisions
2010-11-16T12:50:30Z,Wheat Slides,May,May Easing
2010-11-16T12:50:59Z,Wal-Mart 's Asda,in,Good Shape for Christmas After Sales Increase
2010-11-16T12:57:24Z,Brasil Third-Quarter Net Income,Rises on,Increased Lending
2010-11-16T13:00:37Z,Hydro Prices,for Relief,Globe
2010-11-16T13:01:39Z,Mauritius Stocks,Climb to,Highest
2010-11-16T13:05:18Z,America,of Bank,Netschi in Court News
2010-11-16T13:05:18Z,Bank,Netschi in,Court News
2010-11-16T13:05:18Z,Foreclosure Suits,Bank of,America
2010-11-16T13:05:18Z,Netschi,in,Court News
2010-11-16T13:09:23Z,Emerging-Market Stocks,Fall,Day Led by China
2010-11-16T13:14:18Z,Home-Pitch Advantage May Tip Ashes Series Australia,'s Way,Ex-Players
2010-11-16T13:25:59Z,S&P 500 May Rise,O'Neill 's,Goldman
2010-11-16T13:26:11Z,U.S. Economy,in,Slow Recovery
2010-11-16T13:26:18Z,Additional U.K. Spectrum,in,First Quarter of 2012
2010-11-16T13:26:18Z,Vodafone,Bid for,Additional U.K. Spectrum in First Quarter of 2012
2010-11-16T13:33:08Z,Dollar,Erases,Gain Versus Yen
2010-11-16T13:33:42Z,Djokovic,Roddick While,Federer Will Face Murray
2010-11-16T13:33:42Z,Nadal,Draws,Roddick
2010-11-16T13:45:00Z,3.8 %,in,Third Quarter
2010-11-16T13:45:00Z,Economic Growth,Slows as,Exports Fall
2010-11-16T13:45:00Z,Growth,Slows to,3.8 % in Third Quarter
2010-11-16T13:45:00Z,Israeli Economic Growth,Slows to,3.8 % in Third Quarter
2010-11-16T13:45:00Z,Israeli Growth,Slows to,3.8 % in Third Quarter
2010-11-16T13:50:47Z,Talks,With,China
2010-11-16T13:50:47Z,Venezuela,in,Talks With China on Power Projects
2010-11-16T13:51:03Z,Rose 0.4 %,in,October
2010-11-16T13:54:44Z,Selling Covered Bonds Denominated,in,Euros
2010-11-16T13:58:28Z,Allied Irish,Must Pay,$ 1.8 Billion
2010-11-16T13:58:54Z,Rothschild,Buys,Indonesian Coal Stakes
2010-11-16T14:03:36Z,Rand,in,Two Weeks Against Dollar on Commodities
2010-11-16T14:03:36Z,Two Weeks,in Rand,Europe Crisis
2010-11-16T14:03:36Z,Weakest,at Rand,Europe Crisis
2010-11-16T14:11:20Z,Wheat,Called in,Chicago
2010-11-16T14:16:32Z,Brazil-Beating Growth,Sparks Decline in,Bond Yield Gap
2010-11-16T14:16:32Z,Growth,Sparks,Decline
2010-11-16T14:22:21Z,October Industrial Output,Increased,6.6 %
2010-11-16T14:22:21Z,Russian October Industrial Output,Increased,6.6 %
2010-11-16T14:30:14Z,Ireland Said,in,Talks Get for Government
2010-11-16T14:30:14Z,Talks,in Said,Banks
2010-11-16T14:38:09Z,Exxon Mobil,Closes Oso Platform After,Attack
2010-11-16T14:38:27Z,Lan Airlines Falls,in,Month
2010-11-16T14:38:48Z,City,of,Casino Industry
2010-11-16T14:41:19Z,Dynegy Debate,Intensifies in,Hours
2010-11-16T14:44:53Z,Dougherty,Quits,Soros-Seeded Pharos
2010-11-16T14:44:53Z,Soros-Seeded Pharos,Set Up,Emerging
2010-11-16T14:48:17Z,Lipstick Building ' Owner,in,New York City Files
2010-11-16T14:50:33Z,African Treasury,Says,Eskom Equity
2010-11-16T14:50:33Z,Eskom Equity,Be Considered in,Budget
2010-11-16T14:50:33Z,South African Treasury,Says,Eskom Equity
2010-11-16T14:50:33Z,Treasury,Says,Eskom Equity
2010-11-16T14:54:42Z,International Demand,Declined in,September
2010-11-16T14:59:57Z,Sales Growth,Abroad Offsets,Decline
2010-11-16T15:11:45Z,Blankfein,Emerging for,Growth
2010-11-16T15:11:45Z,Markets Goldman,',Biggest Opportunity
2010-11-16T15:15:15Z,U.K. Lawmaker Woolas,Says,Voiding Election Result
2010-11-16T15:15:21Z,Iceland Bank Creditors Warn,Will Fight,Debt Relief Measures
2010-11-16T15:15:21Z,They,Will Fight,Debt Relief Measures
2010-11-16T15:20:56Z,China,Breaking,Airbus-Boeing Grip
2010-11-16T15:21:32Z,ATB,Says at,Weekly Mombasa
2010-11-16T15:26:58Z,Canada September Factory Sales,Decline,0.6 Percent Less Expected
2010-11-16T15:27:12Z,London Hedge-Fund Managers May,Be,Allowed
2010-11-16T15:30:00Z,Chicago,'s Alinea,Top Honors for Hometown
2010-11-16T15:30:00Z,Chicago 's $ 195 Alinea,Honors for,Obama 's Hometown
2010-11-16T15:30:00Z,Obama,for,Hometown
2010-11-16T15:32:07Z,Angola 's Annual Inflation,Accelerates to,16 %
2010-11-16T15:32:07Z,Angola 's Inflation,Accelerates in,October
2010-11-16T15:33:38Z,Emerging-Market Stocks,Fall,Day Led by China
2010-11-16T15:35:31Z,Borders CEO LeBow,Gives,Record
2010-11-16T15:39:48Z,Corn,Slides,Their
2010-11-16T15:42:35Z,America,of Bank,CEO
2010-11-16T15:42:35Z,in Hand-to-Hand Combat,' Bank,CEO
2010-11-16T15:43:04Z,Austria,Says,Aid Withheld
2010-11-16T15:43:04Z,Greece,Leads,Jump in Sovereign Debt Swaps
2010-11-16T15:43:04Z,Jump,in,Sovereign Debt Swaps
2010-11-16T15:45:59Z,Vienna Insurance,Sued on,Policy
2010-11-16T15:47:18Z,French Gastronomy,Voted for,Cuisine
2010-11-16T15:47:18Z,Gastronomy,Voted Unesco Heritage for,Cuisine
2010-11-16T15:47:18Z,Unesco Intangible Heritage,in,First
2010-11-16T15:50:35Z,Nabucco,Stream Among,Topics in Italy-Germany Meetings
2010-11-16T15:50:35Z,Topics,in,Italy-Germany Meetings
2010-11-16T15:54:56Z,Eyes Stake Sale,in,2011
2010-11-16T15:59:01Z,Stark,Says After,End
2010-11-16T16:01:00Z,Inflation Pressures,Set,Escalate
2010-11-16T16:01:00Z,Taiwan Growth,Probably Surpassed,8 %
2010-11-16T16:03:32Z,Panamericano Rescue May Cut Loan Purchases,Bank of,America
2010-11-16T16:03:51Z,African Tea Prices Rise,in,Three
2010-11-16T16:03:51Z,First Week,for Rise,Broker
2010-11-16T16:03:51Z,Three,in Rise,Broker
2010-11-16T16:04:56Z,Head Gasoline Trader Christophilopoulos,in,London
2010-11-16T16:09:10Z,South Africa,'s Market,Investec
2010-11-16T16:14:25Z,Group,Argues,Foreclosure Hearings Begin
2010-11-16T16:14:25Z,Mortgage Transfers,Are,Valid
2010-11-16T16:16:07Z,IMF 's Ukraine Mission,Recommends,$ 1.6 Billion Payment
2010-11-16T16:26:36Z,Yuan IPOs,Are,Beijing 's Export for Hong Kong Exchange CEO Charles Li
2010-11-16T16:30:23Z,ATP Chief Helfant,Is,Optimistic Board Will Favor Shorter Tennis Season
2010-11-16T16:41:57Z,Indianapolis,Approves,$ 640 Million Parking-Meter Contract
2010-11-16T16:51:08Z,They,Were,Tortured
2010-11-16T17:00:15Z,Petra Fund REIT,Wins,Bankruptcy Stay Against Enforcement
2010-11-16T17:02:31Z,C-Bass,Use,Cash Collateral
2010-11-16T17:04:12Z,Avenir,Are Active in,Paris Trading
2010-11-16T17:04:12Z,Lottomatica,Hires Banks for,Senior
2010-11-16T17:05:53Z,10-Year Bond Yields,Rise to,Highest
2010-11-16T17:05:53Z,10-Year Municipal Bond Yields,Rise to,Highest
2010-11-16T17:05:53Z,Bond Yields,Rise in,Four Months
2010-11-16T17:05:53Z,Municipal Bond Yields,Rise in,Four Months
2010-11-16T17:05:53Z,Top-Rated 10-Year Bond Yields,Rise to,Highest
2010-11-16T17:05:53Z,Top-Rated 10-Year Municipal Bond Yields,Rise in,Four Months
2010-11-16T17:05:53Z,Top-Rated 10-Year Municipal Bond Yields Rise,in,Four Months
2010-11-16T17:05:53Z,Top-Rated Bond Yields,Rise to,Highest
2010-11-16T17:05:53Z,Top-Rated Municipal Bond Yields,Rise in,Four Months
2010-11-16T17:16:10Z,Franc,Weakens in,Longest Run of Declines Against Dollar
2010-11-16T17:16:10Z,Swiss Franc,Weakens in,Longest Run
2010-11-16T17:27:31Z,European Stocks,Drop as,Rio Tinto
2010-11-16T17:27:31Z,Stocks,Drop as,Rio Tinto
2010-11-16T17:27:33Z,Lawmakers,Modify,Fed 's Mandate
2010-11-16T17:27:33Z,Republican Lawmakers,Modify,Fed 's Mandate
2010-11-16T17:34:09Z,NYC Pension Funds,Call for,Independent Audit
2010-11-16T17:35:29Z,Fair Price,in,2011
2010-11-16T17:38:51Z,K1 Hedge Fund Founder Kiener,Defrauding,Barclays
2010-11-16T17:39:27Z,BP,Buys,Jet Fuel
2010-11-16T17:44:14Z,Ireland,Weighs,Aid
2010-11-16T17:53:19Z,Patients,Need,Talks
2010-11-16T17:59:46Z,Miss Weeks,to Alex,Club
2010-11-16T18:09:38Z,Fiat,Says,Ferrari
2010-11-16T18:11:23Z,18.7,to Drops,CTV Network
2010-11-16T18:13:44Z,Fed,Raise Inflation Expectations With,QE2
2010-11-16T18:25:12Z,Panamericano,Climbs,5 %
2010-11-16T18:27:15Z,Petra Fund Creditor KBS,Loses,Bid for Probe of Assets
2010-11-16T18:33:36Z,King,Says,U.K. ’s Exposure to Ireland
2010-11-16T18:36:04Z,L20,Win,Three Michelin Stars
2010-11-16T18:37:54Z,Five-Day Wait,Lures,U.S. Airlines
2010-11-16T18:37:54Z,Wait,Lures,U.S. Airlines
2010-11-16T18:52:12Z,Chicago,'s Alinea,Top Honors for Hometown
2010-11-16T18:52:12Z,Chicago 's $ 195 Alinea,Honors for,Obama 's Hometown
2010-11-16T18:52:12Z,Obama,for,Hometown
2010-11-16T18:59:14Z,Regions Financial,Plunges,Risk Managers Depart
2010-11-16T19:05:16Z,Southern California Home Sales,Decline on,Drop
2010-11-16T19:05:42Z,Fed 's Rosengren,Sees,Billion Stimulus Program
2010-11-16T19:06:13Z,GDP Rebound,Threatens,2011 Inflation Target
2010-11-16T19:07:26Z,Said,Pay for,Investment Bankers
2010-11-16T19:16:15Z,Canada,Extends Afghan Mission for,Three Years With New Focus
2010-11-16T19:16:15Z,Three Years,With,New Focus
2010-11-16T19:30:00Z,Edmund de Waal Become Finalists,in,Costa Literary Contest
2010-11-16T19:30:00Z,Michael Frayn,Finalists in,Costa Literary Contest
2010-11-16T19:30:58Z,Estonia,Join,Euro
2010-11-16T19:33:48Z,Gold Falls,in,Almost Two Weeks as Dollar Gain Curbs Demand
2010-11-16T19:36:02Z,Wall Street,'s Earnings,Cash Bonus Pools May Shrink
2010-11-16T19:36:11Z,New Zealand,Says,Kiwifruit Disease Spreads Beyond Where Discovered
2010-11-16T19:54:07Z,Ireland,Over,Finances
2010-11-16T19:55:05Z,Morgan Stanley,Questioned Over,Timing of 97 % Loss on Stake in Revel
2010-11-16T19:55:05Z,Timing,in,Revel
2010-11-16T20:05:15Z,China,on,Inflation Concerns
2010-11-16T20:05:31Z,California,Offers,Billion
2010-11-16T20:13:30Z,Lachlan Murdoch,Set to,Complete Purchase of Ten Stake
2010-11-16T20:14:51Z,Berlusconi,Vote After,Passage of Budget Law
2010-11-16T20:21:27Z,SAP,Owes,Damages Expert
2010-11-16T20:31:57Z,Cotton Prices,in,New York Drop on India 's Crop Outlook
2010-11-16T20:31:57Z,India,on,Crop Outlook
2010-11-16T20:34:56Z,UN,Weighs Troops for,Southern Sudan
2010-11-16T20:36:08Z,Bondholders,Lose Billion on,Bill
2010-11-16T20:37:11Z,Miller Tabak 's Pietronico,Says,Muni-Bond Supply Surge Means Bargains
2010-11-16T20:38:55Z,Facebook,on First,Herald
2010-11-16T20:45:53Z,Fifth Day,for Slumps,Brazil Interest Rate Concern
2010-11-16T20:49:09Z,Afghan War 's Giunta,Gets First Nonposthumous Medal Since,Vietnam
2010-11-16T20:51:54Z,Chicago,in Slump,Inflation
2010-11-16T20:51:54Z,China Moves,as Slump,Inflation
2010-11-16T20:51:54Z,Soybeans Slump,in,Chicago
2010-11-16T20:55:38Z,Brazilian Interest-Rate Futures Yield,Rises on,Central Bank Speculation
2010-11-16T20:55:38Z,Interest-Rate Futures Yield,Rises on,Central Bank Speculation
2010-11-16T21:00:19Z,Panamericano Rallies,Be,Acquired
2010-11-16T21:04:13Z,Alcoholic Energy Drinks Will,Be,Barred
2010-11-16T21:04:13Z,Energy Drinks Will,Be Barred by,U.S
2010-11-16T21:04:13Z,Schumer,Says After,Deaths
2010-11-16T21:04:38Z,TVGuide.com,Adds,Tools
2010-11-16T21:04:38Z,Talk,Shows on,Facebook
2010-11-16T21:08:33Z,LLX Advance,With,Usiminas
2010-11-16T21:09:35Z,Home Depot Profit,Beats,Analysts ' Estimate
2010-11-16T21:13:05Z,U.S. Review,Tied to,Its Sole Product
2010-11-16T21:14:01Z,Stride Rite Children 's Footwear Stores,in,China
2010-11-16T21:22:48Z,Clothing Retailer,on,Earnings
2010-11-16T21:22:48Z,Urban Outfitters,Climbs,Most in Two Years on Clothing Retailer 's Earnings
2010-11-16T21:25:30Z,Bradesco,Offerings as,Global Market Rout Curbs Demand
2010-11-16T21:27:09Z,Opportunities,Share,Costs for Faster Network
2010-11-16T21:27:09Z,Rogers CEO,Would Consider,Opportunities
2010-11-16T21:27:52Z,Qualcomm,Sees,Prices
2010-11-16T21:31:13Z,Caterpillar CEO,Steps Deal-Making to,Target Emerging Markets
2010-11-16T21:34:19Z,Hong Kong Stock Trading,Reaches Record Volume on,Concern
2010-11-16T21:38:08Z,General Motors,Raises Price Range in,Initial Stock Sale
2010-11-16T21:42:05Z,AT&T,Executive for,Mobile Venture
2010-11-16T21:43:43Z,Factory Production,Climbed by,Most Three Months October
2010-11-16T21:43:43Z,Three Months,in,October
2010-11-16T21:44:03Z,Oracle,Wins,Ruling Upholding Investor Suit Dismissal
2010-11-16T21:46:50Z,FCC,Reject,Comcast-NBC Deal
2010-11-16T21:46:50Z,Senator Sanders,Urges,FCC
2010-11-16T21:50:15Z,Chile,Raises Rate to,3 %
2010-11-16T21:50:15Z,Economy,as,Rebound Threatens Inflation Targets
2010-11-16T21:54:28Z,Canadian Stocks,Fall as,Commodity Producers Decline
2010-11-16T21:54:28Z,Stocks,Fall as,Commodity Producers Decline on China Rate Concern
2010-11-16T21:58:01Z,Bullard,Say,Rosengren
2010-11-16T22:08:36Z,Stocks,Price of,Crude Oil Drop
2010-11-16T22:08:48Z,Idea,Breaking,Company
2010-11-16T22:11:07Z,U.S. Senators Urge,Earlier Inspections for,Inbound Air-Cargo
2010-11-16T22:18:56Z,Dollar,Strengthens Amid,Irish Debt Turmoil
2010-11-16T22:26:21Z,May,Break Up,Jim Beam Maker
2010-11-16T22:27:18Z,Blackstone,Raises Dynegy Bid Before,Vote
2010-11-16T22:31:17Z,Government Panel,Break Up,NTT
2010-11-16T22:31:17Z,Japanese Government Panel,Abandons,Plan
2010-11-16T22:31:33Z,Emerging Markets ETF,Put,Trading Jumps
2010-11-16T22:31:33Z,Markets ETF,Put,Trading Jumps
2010-11-16T22:33:00Z,Education Chief Duncan,Working With,Boehner
2010-11-16T22:34:20Z,0.4 %,in,Third Quarter
2010-11-16T22:35:23Z,Warhol Foundation 's $ 7 Million Defense,Beats,Collector
2010-11-16T22:35:23Z,Warhol Foundation 's $ Million Defense,Beats,Collector
2010-11-16T22:35:23Z,Warhol Foundation 's Million Defense,Beats,Collector
2010-11-16T22:42:02Z,Online Ad Sales,in,China Surge
2010-11-16T22:45:11Z,Fletcher,Buildings,Earnings Ahead
2010-11-16T22:57:09Z,Infineon Plans Buyback,Dividend After,Profit Gain
2010-11-16T23:00:39Z,Bipartisan Support,Without Difficult,JPMorgan
2010-11-16T23:01:10Z,Knicks Will Play Magic March 28,in,Rescheduled Madison Square Garden Game
2010-11-16T23:04:28Z,Australian Carbon Price,Unlock,Free Market Genius
2010-11-16T23:04:28Z,Carbon Price,Unlock,Free Market Genius
2010-11-16T23:24:30Z,R.J. Reynolds,Pay,$ 80 Million
2010-11-16T23:25:01Z,Sarkozy Plans Tax-System Overhaul,Levy on,Wealth
2010-11-16T23:28:30Z,Glaxo,Backing on,Lupus Drug
2010-11-16T23:30:07Z,Australian Westpac Index Little,Changed,Dwelling Approvals Fell
2010-11-16T23:30:07Z,Australian Westpac Leading Index Little,Changed,Dwelling Approvals Fell
2010-11-16T23:30:07Z,Westpac Index Little,Changed,Dwelling Approvals Fell
2010-11-16T23:30:07Z,Westpac Leading Index Little,Changed,Dwelling Approvals Fell
2010-11-16T23:56:53Z,Former Grant Thornton Hong Kong Partner,Is Arrested,Morning Post Reports
2010-11-17T00:03:56Z,Ireland,Over,Debt Crisis
2010-11-17T00:07:53Z,Yahoo Tests Local Commerce Service,With,Groupon
2010-11-17T00:20:45Z,U.S. Video-Game Industry Sales,Fell for,Seventh Month
2010-11-17T00:46:33Z,Ambac,Faced,State Official
2010-11-17T01:12:23Z,Morgan Stanley 's Net Queen Meeker Back,Picks,Mobile-Web Stars
2010-11-17T01:12:23Z,Morgan Stanley 's Queen Meeker Back,Picks,Mobile-Web Stars
2010-11-17T03:06:28Z,Fleet,Parked After,Fire
2010-11-17T03:21:05Z,China,Considers,Gradual Increase
2010-11-17T03:21:05Z,Gradual Increase,in,Gold Reserve Holdings
2010-11-17T03:23:38Z,KEB May Derail South Korea,for,Plan Sell
2010-11-17T04:07:36Z,Philippines May,Hold Interest Rate,Inflation Eases
2010-11-17T05:00:01Z,Talks,Are,Delayed
2010-11-17T05:55:28Z,Qantas Plane,Turns Back After,Bird Strike
2010-11-17T09:22:28Z,Aid Talks,Stop Short as,EU Lauds Austerity
2010-11-17T09:22:28Z,Irish Aid Talks,Stop Short as,EU Lauds Austerity
2010-11-17T09:27:53Z,Berlusconi,Vote After,Passage of Budget Law
2010-11-17T10:12:01Z,France Telecom,Regain,Edge
2010-11-17T10:37:24Z,African Rand 's Rally,Bolsters,Chances of Reduction in Interest Rates
2010-11-17T10:37:24Z,Chances,in,Interest Rates
2010-11-17T10:37:24Z,Rand 's Rally,Bolsters,Chances of Reduction in Interest Rates
2010-11-17T10:37:24Z,South African Rand 's Rally,Bolsters,Chances in Interest Rates
2010-11-17T11:43:04Z,'22 World Cup Games,in,North Korea
2010-11-17T11:43:04Z,South Korea,Would Hold,'22 World Cup Games
2010-11-17T14:05:54Z,Iceland,Closer to,Dutch Depositor Deal That May Spark Rating Upgrade
2010-11-17T14:05:54Z,U.K.,Deal That,May Spark Rating Upgrade
2010-11-17T15:36:41Z,China Orders Stricter Fire Controls,Kills,53 People
2010-11-17T16:45:28Z,Berlusconi Companies Mediaset,Slide on,Premier 's Political Woes
2010-11-17T16:45:28Z,Premier,on,Political Woes
2010-11-17T18:09:16Z,Ireland Crisis,Punishes,Borrowers
2010-11-17T18:24:01Z,Billionaire Eskenazi Said,in,YPF Unit
2010-11-17T20:15:04Z,Republicans,Balk at,Approval
2010-11-17T21:56:56Z,Discounts,Push Down,Closed-End Funds
2010-11-17T21:56:56Z,Widening Discounts,Push After,Weeklong Bond Selloff
2010-11-17T23:27:06Z,BHP,Revives,Buyback
2010-11-17T23:55:31Z,GM IPO,Raises,Preferred
2010-11-19T17:11:09Z,Stocks,Decline as,UBS
2010-11-19T17:11:09Z,Swiss Stocks,Decline as,UBS
