2011-04-14T00:06:12Z,Copper,in,London Declines
2011-04-14T00:29:08Z,Canada ’s Harper,in,French Debate
2011-04-14T00:31:45Z,Singapore MAS,Allows,Stronger Currency
2011-04-14T00:52:36Z,Asian Stock Declines,Demand for,Safer Assets
2011-04-14T01:00:00Z,RIM ’s PlayBook,Makes,BlackBerry Cool Again
2011-04-14T01:17:44Z,Great Barrier Reef,on Grounding,Report
2011-04-14T01:22:32Z,GDP,Twice Estimated,Pace
2011-04-14T01:22:32Z,Singapore,Allows Currency Gains,GDP Grows
2011-04-14T01:44:20Z,Statistics N.Z. Resumes Canterbury Surveys,Will Delay,GDP
2011-04-14T01:51:59Z,Olympic Medals,for Gift,FT
2011-04-14T01:54:20Z,Galletley,in,London
2011-04-14T01:54:20Z,UBS ’s Bourdeau,Galletley in,London
2011-04-14T01:54:20Z,Westpac,Hires,UBS ’s Bourdeau
2011-04-14T02:09:42Z,Goldman Traders,Manipulate,Derivatives Market
2011-04-14T02:11:18Z,Market Loan Program,to RBS,FT Reports
2011-04-14T02:23:53Z,Firm,of,2007 Revenue
2011-04-14T02:37:11Z,Talks,in Tepco,Yomiuri Reports
2011-04-14T02:37:11Z,Tepco,in,Talks With Government on Compensation
2011-04-14T02:41:30Z,Companies,Can Argue,Treasury
2011-04-14T02:42:29Z,Make,Text,Messages Free
2011-04-14T02:42:29Z,South Korea,Text,Messages Free
2011-04-14T02:56:08Z,Capitals Defeat Rangers 2-1,in,Overtime
2011-04-14T02:56:08Z,Opening Game,in,NHL Playoffs
2011-04-14T02:56:08Z,Overtime,in,Opening Game
2011-04-14T03:07:33Z,Banks,Today in,Mumbai
2011-04-14T03:07:33Z,Today,in,Mumbai
2011-04-14T03:13:49Z,Treasuries,Withstand,End of Fed Buying Program
2011-04-14T03:13:53Z,North Korea,Prepares,Indict
2011-04-14T03:13:53Z,U.S. Citizen,Arrested for,Unspecified Crime
2011-04-14T03:15:24Z,Overtime,in,NHL Opening Round
2011-04-14T03:15:24Z,Top-Seeded Capitals Defeat Rangers 2-1,in,Overtime
2011-04-14T03:18:04Z,Pakistan Will Resume,Playing,Cricket
2011-04-14T03:44:52Z,Kings Fans,Let,Emotions Before Likely Anaheim
2011-04-14T03:57:10Z,Big Banks,in,Libor Manipulation
2011-04-14T04:00:01Z,Ex-Senator Santorum,Begins,Raising Money
2011-04-14T04:00:01Z,Football Show Scores,With,Non-Sports Fans
2011-04-14T04:00:01Z,Obama 's Money Pump,Primed by,Chicago Friends
2011-04-14T04:00:03Z,Insurance,in,Brazil Increases
2011-04-14T04:01:00Z,Arcos,Raises Billion in,Expanded Initial Public Offering
2011-04-14T04:01:00Z,Challenges BMW,With,Superman Sedan
2011-04-14T04:01:00Z,Christie Bid,to Cool,Poll
2011-04-14T04:01:00Z,DHL Reboots,in,U.S
2011-04-14T04:01:00Z,Deficit-Cut Deal,in,Challenge
2011-04-14T04:01:00Z,Foreclosure Filings,in,U.S. Drop
2011-04-14T04:01:00Z,Kentucky,Refuses to,Issue License
2011-04-14T04:01:00Z,Obama,Sets,June Deadline
2011-04-14T04:01:00Z,Suicide Rates,in,U.S. Increase as Economy Declines
2011-04-14T04:01:21Z,Weather,Improves After,Sandstorm
2011-04-14T04:42:15Z,Lennon ’s Lucy,in,Sky ’ Draft
2011-04-14T04:47:06Z,Pakistan Protests U.S. Missile,Strikes Near,Border
2011-04-14T04:47:06Z,Ties,With,CIA Sour
2011-04-14T04:48:29Z,South Africa 's Zuma,Says BRICS Agree to,Reform UN Security Council
2011-04-14T04:55:35Z,Tauron,Be,Gas-Fired
2011-04-14T05:00:18Z,Hong Kong Short Selling Turnover,Recorded,04/14/2011
2011-04-14T05:00:34Z,$ 4.3 Billion Wind,Investment in,Turkey
2011-04-14T05:00:34Z,Hydro Investment,in,Turkey
2011-04-14T05:03:48Z,African Efforts,Spur,Finance Minister
2011-04-14T05:03:48Z,Efforts,Spur,New Growth Trajectory
2011-04-14T05:03:48Z,South African Efforts,Spur,New Growth Trajectory
2011-04-14T05:19:09Z,Aftershocks,Threaten Damage to,Crippled Japan Nuclear Power Station
2011-04-14T05:22:05Z,Bank Muscat First-Quarter Profit,Climbs,13 %
2011-04-14T05:22:50Z,PKN Orlen,Wants,Drill for Oil
2011-04-14T05:27:07Z,Portugal Fall,To Exports,AIM
2011-04-14T05:29:14Z,Det Norske Veritas May,Hire,DN Reports
2011-04-14T05:32:15Z,Khasis,Buy into,Retailer Lenta
2011-04-14T05:40:20Z,Tokyo Apartments,Put Up for,Sales
2011-04-14T05:43:35Z,$ 10 Million,in,Solar Company Tigo Energy
2011-04-14T05:43:35Z,Gore ’s Generation Fund,Invests,$ 10 Million
2011-04-14T05:44:18Z,Canucks Overcome Blackhawks,in,NHL Playoff Openers
2011-04-14T05:44:18Z,Capitals Beat Rangers,Blackhawks in,NHL Playoff Openers
2011-04-14T05:45:02Z,Carrefour,Makes,New Pay Offer
2011-04-14T05:49:16Z,BBVA,Give,350 Employees
2011-04-14T06:01:47Z,Glencore,Confirms,Plan
2011-04-14T06:05:11Z,Denmark Fights Lundbeck Drug ’s Use,in,Executions
2011-04-14T06:06:31Z,Hungary May,Save Billion on,Reorganization
2011-04-14T06:09:05Z,EU50 Million,in,German Settlement
2011-04-14T06:09:05Z,Julius Baer,Pays,EU50 Million
2011-04-14T06:32:28Z,Groupon-Like Discount Service,in,South Korea
2011-04-14T06:32:30Z,2011 Deficit May,Be Below,3 %
2011-04-14T06:32:30Z,Belgian 2011 Deficit May,Be Below,3 %
2011-04-14T06:32:30Z,Belgian Deficit May,Be Below,3 %
2011-04-14T06:32:30Z,Deficit May,Be Below,3 %
2011-04-14T06:32:30Z,Deputy Premier,Tells,Echo
2011-04-14T06:40:19Z,Greenko Group,Wins,Hydroelectric Projects in India
2011-04-14T06:40:19Z,Hydroelectric Projects,in,India
2011-04-14T06:47:04Z,10-Year Bund Yield,Reaches One-Week Low Before,Economic Forecasts
2011-04-14T06:47:04Z,Bund Yield,Reaches One-Week Low Before,Economic Forecasts
2011-04-14T06:47:04Z,German 10-Year Bund Yield,Reaches One-Week Low Before,Economic Forecasts
2011-04-14T06:47:04Z,German Bund Yield,Reaches One-Week Low Before,Economic Forecasts
2011-04-14T06:54:15Z,Fourth Day,in,London Trade
2011-04-14T06:55:22Z,ProLogis Increases PEPR Stake,Company at,1.7
2011-04-14T06:56:03Z,New Zealand Government,Puts Onus,Prove
2011-04-14T07:04:05Z,Japanese Stocks,Rise for,Day
2011-04-14T07:04:05Z,Stocks,Rise for,Second Day
2011-04-14T07:05:17Z,European Stocks,Fall as,Carmakers
2011-04-14T07:05:17Z,Stocks,Fall as,Carmakers
2011-04-14T07:07:25Z,Bond Risk,Rises in,Europe
2011-04-14T07:07:25Z,Corporate Bond Risk,Rises in,Europe
2011-04-14T07:13:08Z,Brent Futures Decline,in,London
2011-04-14T07:13:08Z,Crude Oil,Erases,Gain in New York
2011-04-14T07:13:08Z,Earlier Gain,in,New York
2011-04-14T07:13:08Z,Oil,Erases,Earlier Gain
2011-04-14T07:15:35Z,Aero Vodochody Signs Contract,with,Embraer
2011-04-14T07:15:40Z,Singh,Says BRICS Banks to,Boost Use
2011-04-14T07:17:15Z,3.5 %,in,1st Quarter
2011-04-14T07:17:40Z,Sells $ 221 Million,in,Bonds
2011-04-14T07:20:08Z,Ostrich Meat,Birds After,Flu Found
2011-04-14T07:21:21Z,Danone Reports Fastest Sales Growth,in,Three Years on Water
2011-04-14T07:24:22Z,Forint Firms,Snapping,3 Days
2011-04-14T07:24:22Z,Hungarian Forint Firms,Snapping,3 Days of Losses
2011-04-14T07:28:20Z,Greece May,Need Debt Restructuring After,June
2011-04-14T07:28:20Z,Schaeuble,Die,Welt
2011-04-14T07:28:53Z,7.7 %,in,Third Quarter
2011-04-14T07:40:16Z,Leighton,Tumbles,Most in Two Years
2011-04-14T07:48:42Z,$ 870 Million European Carbon,Permits,VAT Fraud
2011-04-14T07:48:42Z,$ Million European Carbon,Permits,VAT Fraud
2011-04-14T07:48:42Z,Million European Carbon,Permits,VAT Fraud
2011-04-14T07:55:10Z,Treasuries,Snap Two-Day Gain Before,Bond Sale
2011-04-14T07:58:47Z,Uganda Opposition Leader Besigye,in,Standoff
2011-04-14T08:05:38Z,Namakwa Diamonds,Says Gem Production as,Prices Increase
2011-04-14T08:28:51Z,Government Term Ends,in,August
2011-04-14T08:34:14Z,Brent Crude ’s Premium,Widens for,Fifth Day
2011-04-14T08:41:10Z,10-Year Bond Yield,Rises Above,13 % for First Time
2011-04-14T08:41:10Z,Bond Yield,Rises Above,13 % for First Time
2011-04-14T08:41:10Z,Greek 10-Year Bond Yield,Rises Above,13 % for First Time Amid Debt Crisis
2011-04-14T08:41:10Z,Greek Bond Yield,Rises Above,13 % for First Time
2011-04-14T08:49:29Z,Wind Power,in,Kazakhstan
2011-04-14T08:52:11Z,EU,’s Plans,BNEF Says in Report
2011-04-14T08:52:11Z,EU ’s Energy-Efficiency Plans,Says in,Report
2011-04-14T08:55:03Z,ZTE,Provide,High-Speed Access
2011-04-14T08:58:23Z,Vestas,Wins,Inner Mongolia Contract
2011-04-14T09:00:00Z,Swiss Investor Confidence Increases,in,Eight Months
2011-04-14T09:02:49Z,$ 3 Trillion,in,Policy Challenge
2011-04-14T09:02:49Z,China Reserves,Pass,$ 3 Trillion
2011-04-14T09:02:49Z,World-Record China Reserves,Pass,Trillion in Policy Challenge
2011-04-14T09:06:34Z,U.S. 10-Year Treasury Notes,Erase,Decline
2011-04-14T09:06:34Z,Yield,Little Changed at,3.46 %
2011-04-14T09:09:45Z,Korea ’s Won,Reverses,Earlier Declines on Singapore Policy
2011-04-14T09:17:29Z,Drop,in,Seismic Studies
2011-04-14T09:19:54Z,36 % Boost,in,Sugar Output
2011-04-14T09:19:54Z,Omnicane,Sees,Boost in Sugar Output
2011-04-14T09:25:52Z,Ugandan Opposition Leader,Arrested After,Police Standoff
2011-04-14T09:30:21Z,2.8 %,in,February
2011-04-14T09:30:52Z,CEZ,Extends,Retreat
2011-04-14T09:35:43Z,Citigroup,Sued in,Branch
2011-04-14T09:46:40Z,Gains,in,Most Asian Currencies
2011-04-14T09:46:40Z,Singapore 's Dollar,Leads,Gains in Most Asian Currencies
2011-04-14T09:49:30Z,Japan,Should Consider,Quake Tax
2011-04-14T09:50:07Z,Japan National Soccer Team,Play at,Copa America Tournament
2011-04-14T09:52:29Z,Rupiah Little,Changed on,Speculation Central Bank Intervening
2011-04-14T09:53:39Z,$ 7.1 Billion,in,Rights Offering
2011-04-14T10:00:03Z,Evercore,Hires,Goldman ’s Raymond Strong
2011-04-14T10:00:59Z,Petrobras May,Seek,China Development Bank Loan
2011-04-14T10:06:00Z,Croatia Aims,in,June
2011-04-14T10:16:42Z,European Coal Derivatives Gain,Advance in,Four Days
2011-04-14T10:16:42Z,First Advance,in,Four Days
2011-04-14T10:17:17Z,Fourth Day,for Drops,Debt Overhaul
2011-04-14T10:17:18Z,Statoil,Says,Major Find Near Peregrino May Warrant Platform
2011-04-14T10:19:42Z,Eskom 's Workers,Threaten,Full Strike
2011-04-14T10:23:11Z,President,Says,Profit May Beat Forecast
2011-04-14T10:23:11Z,Toshiba,Rises,President Says
2011-04-14T10:24:36Z,888,Says,First-Quarter Gambling Revenue Increases
2011-04-14T10:24:36Z,9 Percent,Led By,Bingo
2011-04-14T10:38:24Z,50 Places,in,Business Ranking
2011-04-14T11:00:00Z,Boost Liquidity,With,Mortgage Bonds
2011-04-14T11:00:00Z,Fisher,Says,U.K. Lenders
2011-04-14T11:00:09Z,RSA Insurance CEO Andy Haste,Gets,6 %
2011-04-14T11:01:29Z,Woody Allen ’s Romantic Movie,With,Bruni
2011-04-14T11:08:12Z,Ackermann,Is,Dangerous
2011-04-14T11:08:12Z,Ex-IMF Economist Johnson,Tells,TAZ
2011-04-14T11:08:24Z,Credit Suisse ’s CoCos Plan,Opposed by,Shareholder Group Ethos
2011-04-14T11:10:18Z,Borg,Says,Difficult
2011-04-14T11:20:39Z,Iceland President,Defends,Pre-Crisis Tours Promoting Bank Model
2011-04-14T11:21:20Z,Optimum,Says,Transnet
2011-04-14T11:21:20Z,Transnet,Close Richard Bay Line For,20-Days
2011-04-14T11:23:45Z,Investec Asset Management,Says South Africa May Raise Rates in,September
2011-04-14T11:31:19Z,Namibia ’s Annual Inflation Rate,Climbs to,3.8 %
2011-04-14T11:31:19Z,Namibia ’s Inflation Rate,Climbs From,Previous 3.1 %
2011-04-14T11:35:02Z,Mortgage Servicers,Settle,FDIC
2011-04-14T11:42:21Z,Shell,Says After,Maintenance
2011-04-14T11:46:49Z,UN,Encourage Energy-Efficiency Projects in,Carbon Market
2011-04-14T11:52:09Z,Ukraine ’s TMM,Returned in,2010
2011-04-14T11:58:31Z,Graft Charges,Were,Dropped
2011-04-14T11:58:50Z,Russia May,Sell,12 % of Rail Monopoly
2011-04-14T12:00:00Z,Czech Finance Ministry,Lowers,Growth Forecast
2011-04-14T12:05:40Z,Arab,Bid for,$ 1.2 Billion Iraq Plants
2011-04-14T12:12:41Z,Second Day,for Rise,Concern About Inflation
2011-04-14T12:15:49Z,U.S. Treasury Department,Schedule for,April 14
2011-04-14T12:19:13Z,Hedge Funds,With,Short Lockups Win Investors
2011-04-14T12:22:43Z,FrontPoint,Fargo in,Court News
2011-04-14T12:22:43Z,Wells Fargo,in,Court News
2011-04-14T12:24:04Z,Liberty Mutual,Move on,Insurer
2011-04-14T12:28:49Z,Ahluwalia Contracts,Wins,5.35 Billion Rupees of Building Orders
2011-04-14T12:34:28Z,Zambia Sugar Signs,With,Group of Banks for Expansion
2011-04-14T12:34:46Z,Stocks,in,U.S. Retreat
2011-04-14T12:34:46Z,Surprise Advance,in,First-Time Jobless Claims
2011-04-14T12:36:25Z,Bloomberg LP Opens,Expanded,Shanghai Office
2011-04-14T12:42:36Z,March,in,Kampala
2011-04-14T12:46:25Z,Wholesale Prices,in,Rise
2011-04-14T12:48:36Z,Crude Falls,in,Two Weeks
2011-04-14T12:52:17Z,U.S. Jobless Claims,Reflecting,Quarter-End Volatility
2011-04-14T12:52:24Z,Forecasters,Split Over,Extent
2011-04-14T12:52:36Z,Fukushima Radiation Leaks Will Continue,Says Through,June
2011-04-14T12:52:36Z,Tokyo Electric,Says Through,June
2011-04-14T13:00:37Z,Terna Denies Report,With,Snam Rete Gas
2011-04-14T13:02:57Z,Union Properties,Surges,Most in 3 Years
2011-04-14T13:16:14Z,Mining,in,Guinea
2011-04-14T13:16:52Z,Azerbaijan ’s Crude-Oil Production,Declined,1.9 %
2011-04-14T13:20:20Z,Crude Oil,Is,Changed
2011-04-14T13:20:20Z,Oil,Is Little Changed in,New York
2011-04-14T13:33:15Z,Birmingham City Owner Carson Yeung Increases Stake,in,Premier League Team
2011-04-14T13:33:45Z,Limit Acquisitions,to Springer,Online Operators
2011-04-14T13:34:17Z,Liberty Mutual,Move on,Insurer
2011-04-14T13:34:27Z,Financial Markets Showing Confidence,in,U.S. Fiscal Position
2011-04-14T13:34:27Z,Geithner,Says,Financial Markets Showing Confidence
2011-04-14T13:36:25Z,EDF,Terminates,Involvement in Carbon Project
2011-04-14T13:36:25Z,Involvement,in,Carbon Project
2011-04-14T13:45:04Z,Kenya,Raises,Maximum Price
2011-04-14T13:46:55Z,Profit,Climbs,23 %
2011-04-14T13:46:55Z,Progressive Profit,Climbs,23 %
2011-04-14T13:49:12Z,Crude Oil,Reverses Decline in,New York
2011-04-14T13:49:12Z,Oil,Reverses,Decline
2011-04-14T13:50:48Z,Credit-Default Swaps,Jump,52 Basis Points
2011-04-14T13:50:48Z,Greek Credit-Default Swaps,Jump,52 Basis Points
2011-04-14T14:00:19Z,Fruit Bowl ’ Will Honor German Reunification,in,Berlin
2011-04-14T14:01:39Z,Mortgage Rates,Climb for,Fourth Week
2011-04-14T14:04:48Z,FDA,Puts,Stroke Medication
2011-04-14T14:11:38Z,Lyondell Reports,Flaring Gases at,Houston Chemical Plant
2011-04-14T14:14:04Z,Bullish Peso Bets,Jump,35 %
2011-04-14T14:14:04Z,Peso Bets,Jump,35 %
2011-04-14T14:21:01Z,Consumer Comfort,Rises for,Third Straight Week
2011-04-14T14:22:51Z,Canadian Dollar,Erases,Its Losses Against U.S. Dollar in Toronto Trading
2011-04-14T14:22:51Z,Dollar,Erases,Its Losses Against U.S. Dollar in Toronto Trading
2011-04-14T14:22:51Z,U.S. Dollar,in,Toronto Trading
2011-04-14T14:23:07Z,Egyptian Resorts Falls,in,5 Years After Land Rescinded
2011-04-14T14:29:24Z,Polish Day-Ahead Power,Rises Amid,Increased Demand
2011-04-14T14:34:03Z,Natural Gas,Rises on,Smaller-Than-Expected Inventory Increase
2011-04-14T14:35:02Z,Treasuries,Pare,Advance
2011-04-14T14:43:34Z,Funding $ 99 Million Power Plant,Using,Waste
2011-04-14T14:43:34Z,Funding Million Power Plant,Using Waste From,Whisky
2011-04-14T14:43:34Z,Helius,Completes,Funding $ 99 Million Power Plant
2011-04-14T14:45:12Z,He,Stability in,Canadian Election Debate
2011-04-14T14:45:12Z,Stability,in,Canadian Election Debate
2011-04-14T14:48:58Z,AT&T T-Mobile Deal,Faces,Coordinated Review
2011-04-14T14:55:55Z,Noonan Signals,Forced,Losses
2011-04-14T14:59:59Z,Drop First Day,in,Three
2011-04-14T15:01:19Z,Latvian Parliament,Passes,Supplemental Budget
2011-04-14T15:02:46Z,Debt Restructuring,Would Cause,Chain Reaction
2011-04-14T15:08:40Z,Ireland ’s NAMA,Takes,Control of Derek Quinlan Real Estate
2011-04-14T15:10:43Z,Swaziland Labor Unions,Call Off,Protests
2011-04-14T15:14:11Z,Assets,Exceed,Share Price
2011-04-14T15:14:11Z,Uranium Producers,in,Takeover
2011-04-14T15:22:04Z,First Bank,Rises,Sevenfold
2011-04-14T15:26:47Z,Food Prices,Says Near,2008 Levels
2011-04-14T15:26:47Z,World Bank,Says Near,2008 Levels
2011-04-14T15:27:31Z,Three-Week Low,to Tumble,Rates
2011-04-14T15:30:10Z,European Debt Overshadows Mol,as,Oil Output Boost
2011-04-14T15:31:36Z,46 %,in,2010
2011-04-14T15:36:39Z,Van Eck Global ’s Russia Small-Cap ETF,Begins,New York Trading
2011-04-14T15:37:28Z,NFL,Begins,Mediation Talks
2011-04-14T15:41:30Z,Croatian Government,Appoints Galic as,New Head of Selloff Agency
2011-04-14T15:41:30Z,Government,Appoints,Galic
2011-04-14T15:44:25Z,European Content Creators,in Invests,Ads
2011-04-14T15:44:25Z,YouTube Invests,in,European Content Creators
2011-04-14T15:45:44Z,China ’s Li,Meets,Xinhua
2011-04-14T15:45:44Z,Greece,’s Pamboukis,Xinhua
2011-04-14T15:45:53Z,Obama Administration,in,Six Months
2011-04-14T15:48:34Z,Dunelm Group,Says,Sales Rise as New Superstores Open
2011-04-14T15:49:43Z,U.K. Royal Mint Gold Output,Slips,Silver Output Gains
2011-04-14T15:51:25Z,Europe Jet Fuel Stockpiles Plunge,in,Two Years
2011-04-14T15:53:14Z,WH Smith,Rises,Tax Rate
2011-04-14T15:54:39Z,Iamgold,for,Minority Stakes in Ghana Gold Mines
2011-04-14T15:54:39Z,Iamgold ’s Minority Stakes,in,Its Ghana Gold Mines
2011-04-14T15:55:19Z,Telefonica May Reduce Workforce,in,Spain
2011-04-14T15:55:59Z,WestLB Bad Bank Cuts Assets,Loss After,Provisions
2011-04-14T15:59:40Z,NYSE,Expects,Record Emerging-Nation Listings
2011-04-14T16:00:01Z,Wen,Resists,Yuan Pressure
2011-04-14T16:05:08Z,Carrefour,Confirms,2011 Targets
2011-04-14T16:08:05Z,Harvard Fund,Hires,Parikh
2011-04-14T16:10:48Z,OSX,Sees Brazil Market for,Platforms
2011-04-14T16:15:13Z,Pound,Climbs as,U.K. Confidence Rebounds
2011-04-14T16:15:18Z,Portuguese Yields,Surge,Record
2011-04-14T16:15:18Z,Schaeuble,Raises,Restructuring
2011-04-14T16:17:28Z,51 %,in,Season
2011-04-14T16:17:39Z,Jefferies,Pays,$ 2 Million
2011-04-14T16:20:35Z,Tepco,Move Fukushima Backup Power to,Higher Ground
2011-04-14T16:20:35Z,Tsunami Risk,Prompts,Tepco
2011-04-14T16:22:01Z,African Stocks,Advance For,Second Day
2011-04-14T16:22:01Z,South African Stocks,Advance For,Second Day
2011-04-14T16:22:01Z,Stocks,Advance For,Day
2011-04-14T16:25:40Z,BlueNext,Beyond,Tracking
2011-04-14T16:26:07Z,Orascom Telecom,Approves Measures to,VimpelCom Merger
2011-04-14T16:26:56Z,Copper Fall,Terms of,Trade
2011-04-14T16:27:58Z,CEO,Steps,Down
2011-04-14T16:31:36Z,Deutsche Bank,Hire in,Poland
2011-04-14T16:31:36Z,Goldman,Sued,Deutsche Bank Hire
2011-04-14T16:33:35Z,Treasuries,Erase,Advance Before U.S. Auctions
2011-04-14T16:34:12Z,European Stocks Decline,in,Four
2011-04-14T16:34:54Z,Glencore IPO,Helps Bonds as,Holders Double Money
2011-04-14T16:36:44Z,Viceroy Anguilla Creditors,Seek Trustee to,Run Bankruptcy Case
2011-04-14T16:42:12Z,Chavez Policy,Sends,Venezuelans
2011-04-14T16:43:31Z,London Police Arrest Third Man,in,Tabloid Phone-Hack Probe
2011-04-14T16:44:46Z,Run,in,2012 Elections
2011-04-14T16:45:59Z,13,at %,Survey
2011-04-14T16:47:59Z,Chemical-Plant Acquisitions,in,U.S.
2011-04-14T16:50:55Z,Erdem Basci,Is,Named
2011-04-14T16:50:55Z,Head Turkey,to,Central Bank
2011-04-14T16:59:06Z,Conoco,Buys,Diesel
2011-04-14T17:01:44Z,Grillo,Join,Boards Next Month
2011-04-14T17:01:44Z,LME,Says,Grillo
2011-04-14T17:06:26Z,Moody,Puts,Nykredit Ratings
2011-04-14T17:06:34Z,BP,Extends,Rosneft Share Swap Deadline
2011-04-14T17:06:34Z,Rosneft Share Swap Deadline,Keep,Deal Alive
2011-04-14T17:07:53Z,U.S. Stocks,Pare Losses as,Consumer-Staples Shares Gain
2011-04-14T17:09:09Z,Isovoltaic,Cancels,Million Solar IPO
2011-04-14T17:11:44Z,Moscow City Telephone,Recommends,197.94 Ruble Dividend
2011-04-14T17:17:52Z,FASB,Rules on,Leasing
2011-04-14T17:24:48Z,Al-Qaeda Group,in,Yemen
2011-04-14T17:24:58Z,IMF,EU for,300 Million Euros
2011-04-14T17:24:58Z,Latvia,Completes,Talks
2011-04-14T17:25:04Z,Harvest,Nears,36-Year High
2011-04-14T17:28:07Z,U.S. Companies,Pay,Study
2011-04-14T17:30:05Z,Andreae-Noris,Says,First-Half Pretax Profit Falls
2011-04-14T17:31:10Z,Boehner,Suggests,Pass
2011-04-14T17:32:22Z,Libya TV,Shows,Qaddafi Touring Tripoli in Open Car
2011-04-14T17:32:22Z,Qaddafi Touring Tripoli,in,Open Car
2011-04-14T17:36:04Z,Duke,Builds Largest Storage System at,Wind Farm
2011-04-14T17:43:22Z,Kenneth Marsh Pleads Guilty,in,Gryphon Stock-Tip Case
2011-04-14T17:43:46Z,Kornasiewicz,Quits as,Chief Executive of Pekao
2011-04-14T17:48:13Z,Bonds Jury,Agrees on,Obstruction
2011-04-14T17:59:06Z,U.S. Gulf Crude Premiums,Weaken,WTI-Brent Spread Narrows
2011-04-14T18:06:02Z,European Officials,Reject,Idea Restructuring
2011-04-14T18:06:02Z,Officials,Reject,Idea Greek Restructuring
2011-04-14T18:06:55Z,Ghana Ex-First Lady,in,Primary
2011-04-14T18:13:56Z,April 13,for Update,Text
2011-04-14T18:30:08Z,Treasury,2 to,30-Year Spread
2011-04-14T18:47:10Z,Roche,'s %,Avastin
2011-04-14T18:47:10Z,Swiss Franc,on,Strength
2011-04-14T18:48:37Z,Rajaratnam Family,Invested in,Schutte ’s Fund Weeks
2011-04-14T18:48:37Z,Schutte,in,Fund Weeks
2011-04-14T18:49:12Z,Daughter,from,Laptop
2011-04-14T18:55:06Z,African Coal Plant,Wins,U.S. Backing Over Environmentalist Protests
2011-04-14T18:55:06Z,Coal Plant,Wins,U.S. Backing
2011-04-14T18:55:06Z,South African Coal Plant,Wins,U.S. Backing
2011-04-14T18:55:51Z,Freenet,Replaces,Company Financing
2011-04-14T18:58:58Z,Poland,in,10-Year Dollar Bond Sale
2011-04-14T19:07:46Z,U.S. Stocks,Erase Drop as,Supervalu Shares Lead Gains of Consumer Staples
2011-04-14T19:09:08Z,Sugar Falls,Slump Since,May
2011-04-14T19:19:49Z,California Panel Cuts Lawmakers,’ Money,Sacramento Bee
2011-04-14T19:20:59Z,Japan,'s Economy,Shirakawa
2011-04-14T19:20:59Z,Next Quarter,in,Quake Recovery
2011-04-14T19:35:36Z,Zazove Associates Largest Holdings,in,1st Quarter
2011-04-14T19:40:01Z,AP,Says on,Outfield Traps in 2012
2011-04-14T19:40:01Z,Foul Lines,Traps in,2012
2011-04-14T19:40:01Z,Outfield Traps,in,2012
2011-04-14T19:40:08Z,BTG Signs Accord,With,Sumitomo Mitsui
2011-04-14T19:40:17Z,Libya,Pressure on,Pentagon
2011-04-14T19:40:17Z,NATO Chief Seeks,More Attack,Jets for Libya
2011-04-14T19:45:22Z,Assad,Approves Cabinet in,Wake of Protests
2011-04-14T19:46:03Z,Israel ’s Fischer,Says for,Concern
2011-04-14T19:47:54Z,Minsheng,Loans for,Shanghai Daily
2011-04-14T19:55:25Z,Canadian Natural Gas,Rises on,Lower-Than-Expected Storage Gain
2011-04-14T19:59:14Z,Germany ’s Asmussen,Sees,Reuters
2011-04-14T20:08:02Z,High Floors,in,Top Midtown Manhattan Office Towers
2011-04-14T20:18:36Z,May,Be Helping,Syria Regime Quell Protesters
2011-04-14T20:21:23Z,Valero,Starts,Texas Crude Unit
2011-04-14T20:24:20Z,Consumer Confidence,Rises,Straight Week on Job-Market Gains
2011-04-14T20:26:54Z,Greece Yields,Surge,Portugal
2011-04-14T20:26:54Z,Stocks,Climb,House Passes
2011-04-14T20:27:06Z,Saudi Arabia,Reduced,Crude Oil Advances
2011-04-14T20:31:04Z,SuperValu,Jumps as,Profit Forecast Tops Analysts ’ Estimates
2011-04-14T20:35:17Z,California,Begs Texas for,Job-Creating Recipe
2011-04-14T20:35:17Z,Job-Creating Recipe,With,Growth Trading Places
2011-04-14T20:35:24Z,Reviewers,Criticize,PlayBook Tablet
2011-04-14T20:36:30Z,Colombia,Yields,Fall to 3-Month Low on Inflation Bets
2011-04-14T20:38:24Z,Rhode Island Pension Board,Widens,Funding Gap
2011-04-14T20:38:58Z,Canada,Stocks,Fall
2011-04-14T20:38:58Z,Research,In,Motion Slides
2011-04-14T20:40:53Z,Hammerson,’s Tipped,Sky
2011-04-14T20:40:53Z,Lupatech,Dismissing,Chief Financial Officer
2011-04-14T20:40:53Z,for Lloyd,’s Tipped,Sky
2011-04-14T20:42:03Z,Aeromexico Declines,in,Initial Public Offering
2011-04-14T20:45:49Z,Acorda,Jumps for,Ampyra
2011-04-14T20:59:03Z,Porto,Advance to,Europa League Semifinals
2011-04-14T20:59:38Z,Zipcar,Surges in,First Day
2011-04-14T21:00:13Z,New York ’s Nassau County,Going,Broke
2011-04-14T21:00:13Z,No,Share,Pain
2011-04-14T21:00:13Z,No One,Share,Pain
2011-04-14T21:01:40Z,Noda,Shirakawa on,Growth Outlook
2011-04-14T21:10:49Z,Tyco Becomes Target,With,Schneider Talks Viewed as Sale Sign
2011-04-14T21:14:04Z,Peru Central Bank Sells Dollars,in,Two Years
2011-04-14T21:17:17Z,Euro Advances,Be,Contained
2011-04-14T21:27:02Z,Itau,Buys,49 %
2011-04-14T21:30:40Z,Emerging-Market Stocks,Fall Amid,Concern
2011-04-14T21:32:56Z,Brazil Lula Field May,Hold,6.7 Billion Barrels
2011-04-14T21:36:03Z,Treasury Notes Decline,in,Three Days
2011-04-14T21:42:18Z,Google Profit,Misses Marketing Costs on,Hiring
2011-04-14T21:55:27Z,Congress,Spending Bill With,$ 38.5 Billion Cut
2011-04-14T21:58:30Z,JPMorgan,Pushes Microchip Cards in,Race With Wells Fargo
2011-04-14T21:58:30Z,Race,With,Wells Fargo
2011-04-14T22:01:00Z,Deutsche,Expands Focus as,Bank Lands Merger Deal
2011-04-14T22:06:45Z,U.S.,Completes,Fraud Case
2011-04-14T22:16:30Z,House,Approves,Bill
2011-04-14T22:17:43Z,Borders,Amend,Bonuses
2011-04-14T22:21:04Z,Madoff Trustee 's Filing,Alleges,JPMorgan Thoroughly Complicit
2011-04-14T22:23:18Z,Geithner,Leaders on,Mideast Plan
2011-04-14T22:27:40Z,Orchard Brands Retailer Unit,Wins,Approval of Reorganization Plan
2011-04-14T22:30:00Z,Japanese Nuclear Workers ’ Blood,Should,Should Saved
2011-04-14T22:30:00Z,Japanese Workers ’ Blood,Should,Should Saved
2011-04-14T22:30:00Z,Nuclear Workers ’ Blood,Should,Should Saved
2011-04-14T22:30:00Z,Workers ’ Blood,Help in,Exposure Cure
2011-04-14T22:35:16Z,Portuguese Trio,Joined by,Spain Villarreal
2011-04-14T22:35:16Z,Spain,by,Villarreal in Europa League Semifinals
2011-04-14T22:35:16Z,Spain 's Villarreal,in,Europa League Semifinals
2011-04-14T22:36:35Z,Coeur D’Alene,Fall on,Proposed Bolivia Changes
2011-04-14T22:50:52Z,Georgia Gulf,Declares Force Majeure on,Its Shipments
2011-04-14T23:00:00Z,Confidence,on Week,Survey
2011-04-14T23:00:00Z,Republican Horror Movie Sequel,Hits,Theaters
2011-04-14T23:00:00Z,Roots,Buried,Deep
2011-04-14T23:00:01Z,World ’s 50 Best Restaurant Awards,Draw,Leading Chefs
2011-04-14T23:00:01Z,World ’s Best Restaurant Awards,Draw,Leading Chefs
2011-04-14T23:00:49Z,Reckitt Benckiser 's CEO,Has,Tough Act Follow
2011-04-14T23:00:49Z,Reckitt Benckiser 's Next CEO,Has,Act Follow
2011-04-14T23:01:00Z,Goldman Sachs,Misses Out,Out Glencore 's $ 11 Billion Public Offering
2011-04-14T23:29:16Z,Intermune Ex-CEO Harkonen Sentenced,in,Fraud Case
2011-04-14T23:37:06Z,Yen Falls Versus Dollar,Euro Before,China Growth Data
2011-04-15T00:21:25Z,Fed Must Act,Remove Stimulus Before,Inflation
2011-04-15T00:21:25Z,Lacker,Says,Fed Must Act
2011-04-15T02:11:02Z,Record High,Near Aussie,Consumer Prices Rise
2011-04-15T04:00:01Z,Fresenius Intensify Effort,With,FDA
2011-04-15T04:00:01Z,Hospira,Effort With,FDA
2011-04-15T04:00:18Z,Morgan Stanley ’s Gorman,Gets,Million Pay for First Year
2011-04-15T04:00:24Z,Lakers,Win,Straight NBA Championship
2011-04-15T04:00:44Z,U.S. Senate,Kills,Planned Parenthood
2011-04-15T04:01:00Z,Sacramento Mayor Pitches NBA Owners,in,Bid
2011-04-15T04:01:01Z,AllianceBernstein,Hires,Caxton ’s Feuerman
2011-04-15T04:01:38Z,Defense Witness,in,Fund Weeks
2011-04-15T04:01:38Z,Rajaratnam 's Family Invested,in,Defense Witness ’s Fund Weeks
2011-04-15T06:03:58Z,Colorado Rockies Sweep Double-Header,With,Mets for MLB Best Record
2011-04-15T06:51:06Z,Yen,Weigh on,Stocks
2011-04-15T07:59:58Z,Shocks,Slow,Global Growth
2011-04-15T08:03:29Z,He,Considers,Tyco
2011-04-15T08:03:29Z,Schneider CEO Tricoire,Buoyed by,Rewards
2011-04-15T08:26:48Z,Run Central Bank,to Basci,Architect of Cheap-Money Policy
2011-04-15T08:26:48Z,Turkey Names Basci,Architect of,Cheap-Money Policy
2011-04-15T09:40:01Z,CICC,Says,Banks Too
2011-04-15T09:40:01Z,China,on,Stock Market
2011-04-15T10:38:18Z,China Economic Growth,Curtail,Bad Loans
2011-04-15T10:38:18Z,ICBC ’s Jiang,Says,China Economic Growth
2011-04-15T11:12:16Z,Isovoltaic,Cancels,IPO
2011-04-15T12:41:19Z,State Street ’s $ 885 Million Tax Refund,Sparks,Boston Protest
2011-04-15T12:41:19Z,State Street ’s $ Million Tax Refund,Sparks,Boston Protest
2011-04-15T12:41:19Z,State Street ’s Million Tax Refund,Sparks,Boston Protest
2011-04-15T13:54:51Z,RIM Chiefs,Defend,PlayBook Against Critics
2011-04-15T15:08:23Z,Lockheed Call Options Volume,Jumps as,Shares Decline
2011-04-15T15:28:49Z,Merkel,Faces,Nuclear Exit Bill
2011-04-15T15:28:49Z,States,Step Up,Pressure
2011-04-15T16:31:38Z,Canadian Stocks,Rise as,U.S. Manufacturing Activity Advances
2011-04-15T16:31:38Z,Stocks,Rise as,U.S. Manufacturing Activity Advances
2011-04-16T04:01:14Z,Varney,Says,Congress May Want to Study Antitrust Overlap
2011-05-05T15:30:38Z,Caterpillar,Faces,EU Probe Into Plan
2011-05-05T15:30:38Z,EU Probe Into Plan,Buy,MWM
2011-05-05T15:30:38Z,In-Depth EU Probe Into Plan,Buy,MWM
2011-05-13T09:10:31Z,Spain,Finds,One
