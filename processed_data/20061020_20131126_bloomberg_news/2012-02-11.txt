2012-02-11T00:00:01Z,CDU-FDP Coalition,Win,Majority
2012-02-11T00:00:01Z,Coalition,Win Majority in,2013
2012-02-11T00:00:01Z,Seehofer,Tells,Welt
2012-02-11T00:51:33Z,Post,Report of,Departure
2012-02-11T04:00:01Z,Hapag-Lloyd,on Deal,Hamburger
2012-02-11T04:26:46Z,Vietnam Premier,Citing,Transaction Mistakes
2012-02-11T05:00:01Z,NFL,of,49ers
2012-02-11T05:00:19Z,Canadian Dollar Ends Longest Winning Streak,in,3 Months on Greece Turmoil
2012-02-11T05:02:02Z,New England Patriots ’ Gronkowski,Faces,Ankle-Surgery Recovery
2012-02-11T05:28:40Z,DLF Profit,Misses Estimates as,Sales Decline
2012-02-11T06:00:14Z,Tokyo,Ease,Traffic
2012-02-11T07:00:00Z,Central Bank,Expands,Bond Buying
2012-02-11T08:29:31Z,Bumi,From,Board
2012-02-11T08:54:02Z,Saudi Cement Output,Rises,23 %
2012-02-11T09:00:58Z,Wuhu,Waives,Tax
2012-02-11T09:43:13Z,Nigerian Security Chief,Sees,Boko Haram Government Link
2012-02-11T10:20:42Z,Odier,Tells,NZZ
2012-02-11T10:27:01Z,Boost Investment,in,Canadian Oil Industry
2012-02-11T10:34:01Z,EFSF Will Supply Aid,in,Rescue
2012-02-11T10:34:01Z,Greece,to Aid,WirtschaftsWoche
2012-02-11T10:45:00Z,Bulgaria,Wants,Join
2012-02-11T11:00:34Z,$ 395 Million,in,Medical Contracts
2012-02-11T11:14:55Z,Germany,Keep,Support
2012-02-11T11:14:55Z,Westerwelle,Writes in,Bild
2012-02-11T11:17:59Z,Former A2A Chairman Giuliano Zuccoli,Dies,Ansa Reports
2012-02-11T11:54:02Z,France,’s Candidacy,Parisien
2012-02-11T12:20:26Z,Lobao,Tells,Estado
2012-02-11T12:21:01Z,Ghizzoni,Tells,WirtschaftsWoche
2012-02-11T12:49:18Z,PACE,Tells Interfax in,Campaign
2012-02-11T12:49:18Z,Putin Abuses Power Resources,Tells Interfax in,Campaign
2012-02-11T13:23:50Z,Italy,in,Different Situation
2012-02-11T13:23:50Z,Napolitano,Italy in,Different Situation
2012-02-11T13:37:05Z,Christie,at,Sale
2012-02-11T13:50:19Z,Saudi Shares Advance,Approves,Deal
2012-02-11T13:54:37Z,Rally,in,His Support
2012-02-11T14:41:47Z,Manchester United,Beats,Liverpool 2-1
2012-02-11T14:52:52Z,Greek Leaders,Urge,Passage of Austerity Bill
2012-02-11T14:52:52Z,Leaders,Avoid,Chaos
2012-02-11T15:17:54Z,Gunmen,Kill,General Day
2012-02-11T15:58:19Z,6 %,in,2011
2012-02-11T16:04:50Z,Manchester United,Beats,Liverpool 2-1
2012-02-11T16:07:03Z,Medtronic Seeks Royalties,Interest From,NuVasive
2012-02-11T17:00:00Z,Westerwelle,Tells,Spiegel
2012-02-11T17:03:37Z,Venezuela,Gives,Pepsi Venture
2012-02-11T18:11:19Z,Federer,Switzerland After,Davis Cup Doubles
2012-02-11T18:55:16Z,Five Sun Newspaper Staff,Arrested as,News Corp
2012-02-11T18:55:16Z,Sun Newspaper Staff,Arrested as,News Corp
2012-02-11T20:38:40Z,Guinea ’s Opposition Groups,Call,Monday
2012-02-11T21:33:24Z,Romney,Wins,Straw Poll
2012-02-11T22:00:32Z,Candidate Romney,Wins,CPAC Straw Poll
2012-02-11T22:00:32Z,Republican Candidate Romney,Wins,CPAC Straw Poll
2012-02-12T03:41:44Z,SEC,Looking,Into PE Firms ’ Valuation
2012-02-12T17:23:10Z,Romney,Regains,Momentum
2012-02-12T21:24:44Z,Rioters,Burn Buildings as,Parliament Votes
