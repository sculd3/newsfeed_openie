2012-09-18T00:42:37Z,Taiwan Intervention,Gives,Exporters Edge
2012-09-18T01:02:24Z,Cognizant,Beats,Infosys
2012-09-18T01:31:05Z,India ’s ONGC,Offers,Naphtha for October Loading From Mumbai
2012-09-18T02:57:47Z,Egyptian Prosecutors,Charge,325 People
2012-09-18T02:57:47Z,Prosecutors,Charge,325 People
2012-09-18T03:00:00Z,Warren Gains Edge Over Brown,in,Poll Following Convention
2012-09-18T03:16:28Z,Machinery,Strikes,Pipeline
2012-09-18T03:22:18Z,China Money Rate,Jumps on,Cash Demand Outlook
2012-09-18T03:53:23Z,Hong Kong Swing,Losses Amid,China Concern
2012-09-18T04:00:01Z,Demand,Spawns,Fatshion Category
2012-09-18T04:01:00Z,Brooke Astor Dog Art,Head ’s,to Sotheby
2012-09-18T04:01:00Z,Vilma,Meets About,Bounty Case Ban
2012-09-18T04:01:01Z,$ 30 Million Round,in,Alternative Funder Kabbage
2012-09-18T04:01:01Z,Thomvest,Leads,Million Round
2012-09-18T04:01:01Z,UNC Chapel Hill,Emerging,Markets
2012-09-18T04:04:41Z,Pacific Rubiales May,Get,Environment Permit
2012-09-18T04:05:29Z,Funds,Buy,19.8 Billion Rupees of Indian Derivatives
2012-09-18T04:05:29Z,Global Funds,Buy,Net 19.8 Billion Rupees of Derivatives
2012-09-18T04:06:24Z,Waratahs,Coach After,Rugby Season
2012-09-18T04:08:48Z,Said,Seek,U.S. Investors
2012-09-18T04:44:06Z,China,as Baidu,Tencent Show Patriotism
2012-09-18T04:45:31Z,Hong Kong Short Selling Turnover,Recorded,09/18/2012
2012-09-18T04:47:46Z,Japan Company Bond Risk,Rises on,China Tension
2012-09-18T04:49:28Z,RBA,Saw,High Currency Dimming Growth Outlook
2012-09-18T04:55:30Z,Successor,Boring Time,Departure Nears
2012-09-18T04:56:09Z,Philippines,Gets,Approval
2012-09-18T05:00:05Z,LG,Keeps Smartphone Sales Goal After,Shipments Lag
2012-09-18T05:32:24Z,Meijin Energy Group,Makes,Million Bid
2012-09-18T05:47:34Z,China Home-Price Gains,Cooling,May Limit Curbs
2012-09-18T05:48:41Z,Kuwait Energy,Considers Puls Biznesu Reports in,Poland
2012-09-18T05:54:46Z,India Consumer-Price Inflation,Quickens to,High
2012-09-18T06:05:18Z,Its Decline,in,Two Months
2012-09-18T06:31:25Z,Kazi,Says,Rate Move
2012-09-18T06:31:25Z,Pakistan,Weigh,Growth
2012-09-18T06:32:05Z,Hungary,’s Armenians,Blikk
2012-09-18T06:34:16Z,Rosneft,Approves,Extra Dividend
2012-09-18T06:53:08Z,Stock Losses,Deter,Risk-Taking
2012-09-18T06:53:08Z,Won,Weakens,Bonds Gain
2012-09-18T07:21:00Z,Dana Gas Shareholder,Says,Optimistic
2012-09-18T07:22:04Z,Sony Shares Rise,With,Olympus
2012-09-18T07:39:10Z,Stem Yen,to,Rally
2012-09-18T07:41:47Z,Biggest Drop,in,Two Months
2012-09-18T07:41:47Z,Decline,in,New York
2012-09-18T07:41:47Z,Oil,Extends Decline After,Drop
2012-09-18T08:00:00Z,ABN Amro,Buys Stocks on,ECB Crisis-Fighting
2012-09-18T08:05:33Z,China ’s Stocks,Fall on,Concern Japan Tensions May Hurt Economy
2012-09-18T08:06:12Z,Neas Energy Plans U.K. Power Trading,in,2012
2012-09-18T08:19:08Z,Mobile Competitor,on,Setback
2012-09-18T08:21:05Z,Ringgit,Leads Drop,Export Outlook Dims
2012-09-18T08:24:40Z,Western Lenders Boosted Romanian Units Capital,in,First Half
2012-09-18T08:25:54Z,Standard Bank Plans,in,Two Months
2012-09-18T08:26:20Z,Telefonica Czech Unit,Facing,Million Anti-Monopoly Lawsuit
2012-09-18T08:42:34Z,Japan,Sets Up,New Nuclear Regulator
2012-09-18T08:51:28Z,Bill Yields,Fall Since,Draghi Plan
2012-09-18T08:51:28Z,Spanish Bill Yields,Fall Since,Draghi Plan
2012-09-18T08:59:55Z,25 % Increase,in,Water Rates
2012-09-18T08:59:55Z,California City Council,Rejects,Increase in Water Rates
2012-09-18T09:03:57Z,Inflation Rate,Drop in,September
2012-09-18T09:06:23Z,First Software,Buy,Expand in Cloud Computing
2012-09-18T09:06:38Z,U.K. Inflation,Slows on,Energy Prices
2012-09-18T09:11:12Z,Wallabies Fly-Half Cooper Will Miss,Rest of,Rugby Championship
2012-09-18T09:12:01Z,Euro,Stays,Weaker Versus Dollar
2012-09-18T09:19:37Z,Lebanon Court Charges Members,With,Terrorism
2012-09-18T09:25:11Z,U.K.,Begins Tax-Evasion Probe With,Focus on Lawyers
2012-09-18T09:28:53Z,Ingves,Says,Small Likelihood of Riksbank FX Intervention
2012-09-18T09:29:34Z,Tropical Storm Moves,in,Atlantic
2012-09-18T09:30:00Z,Intel-Based Motorola Smartphone,in,Europe
2012-09-18T09:32:58Z,JX Nippon,Buys Stake in,North Sea License
2012-09-18T09:45:06Z,Pakistan Will,Seek,Zardari Graft Probe
2012-09-18T09:54:08Z,EU Lawmakers,Seek Company Disclosure on,Mining Payments
2012-09-18T10:00:00Z,South Africa Consumer Confidence,Remains Close to,2008 Low
2012-09-18T10:13:28Z,England,Leaves,Pietersen
2012-09-18T10:14:27Z,JFK,Fought,DDT
2012-09-18T10:14:27Z,Rachel Carson,in,Environmental Crusade
2012-09-18T10:15:24Z,Indian Stocks,Losses on,Reforms
2012-09-18T10:15:24Z,Stocks,Swing Between,Gains
2012-09-18T10:46:11Z,German Investor Confidence,Rises,First Time
2012-09-18T10:46:11Z,Investor Confidence,Rises First Time Since,April
2012-09-18T10:47:38Z,Rose 51 %,in,First Half
2012-09-18T10:53:51Z,Fed Funds Projected,Open at,0.14 ICAP Says
2012-09-18T11:07:48Z,Million,in,New Contracts
2012-09-18T11:08:53Z,Foreign Investors,Buy,Net 23.1 Billion Rupees of Indian Stocks
2012-09-18T11:27:42Z,Ministry,Declining,17 %
2012-09-18T11:28:14Z,Students,in,Siemens Backyard
2012-09-18T11:36:14Z,NATO,Restricts,Afghan Joint Operations
2012-09-18T11:41:49Z,Imerys CEO,Focuses as,Europe Suffers Doldrums
2012-09-18T11:53:35Z,European Union,Expands Duties on,Chinese Aluminum Foil
2012-09-18T12:10:24Z,Czech Liquor Ban,in,4th Day
2012-09-18T12:13:20Z,Leu,Weakens in,Three Days
2012-09-18T12:19:21Z,Kurdish Militants,Kill,Four Soldiers
2012-09-18T12:23:38Z,Alcentra,Boosts,High-Yield Bond Team
2012-09-18T12:24:03Z,U.K. Royal Family,Wins,French Ruling
2012-09-18T12:27:56Z,German Minister,Sees,Euro Crisis
2012-09-18T12:39:09Z,Otkritie Strategist Mundy,Quits After,After Less Two Years
2012-09-18T12:53:03Z,Health Care REIT,Buys Out,Sunrise Joint Venture Partners
2012-09-18T12:54:36Z,Egypt,Creates Housemaid Union in,Bid
2012-09-18T12:55:11Z,Oil,Extends Drop in,Two Months
2012-09-18T12:55:29Z,United ’s Tarmac Delays,in,Chicago Storms Probed
2012-09-18T12:57:23Z,Current-Account Gap,in,U.S. Narrowed
2012-09-18T12:57:23Z,U.S. Narrowed,in,Second
2012-09-18T13:04:15Z,Bank,Says,Mexico Chief Reich de Polignac Resigns
2012-09-18T13:25:00Z,Cameron,Examines,U.K. Pension Overhaul
2012-09-18T13:37:24Z,Turkey Cuts Rates First Time,in,7 Months
2012-09-18T13:56:43Z,Sri Lanka,in,World Twenty20
2012-09-18T13:56:43Z,Toss,Fields Against,Sri Lanka in World Twenty20
2012-09-18T13:56:43Z,Zimbabwe,Wins,Fields
2012-09-18T13:57:35Z,Man,Arrested After,Two Officers Killed in Manchester Shooting
2012-09-18T14:07:14Z,63 %,in,First Eight Months
2012-09-18T14:14:31Z,Clinton,Welcomes,Myanmar ’s Suu Kyi
2012-09-18T14:18:01Z,UN,Says,Liberia Security Threats Persist
2012-09-18T14:18:58Z,Kuwait Mobile-Phone Users,Can,Change Operators
2012-09-18T14:20:49Z,1.7 %,in,Survey
2012-09-18T14:29:48Z,Defaults,Exceed,Nevada
2012-09-18T14:35:41Z,Belgrade Airport,Expects,Post-Sanction Record
2012-09-18T14:41:24Z,Obama Administration,Wins,Stay of Detention Law Rejection
2012-09-18T14:43:22Z,IMF,Send,Team
2012-09-18T14:46:04Z,Krona,Climbs as,Ingves Damps Intervention Speculation
2012-09-18T14:46:04Z,Swedish Krona,Climbs as,Ingves Damps Intervention Speculation
2012-09-18T14:49:20Z,Citigroup Executives,Depart After,Mortgage-Fraud Case
2012-09-18T14:54:37Z,Kurdish Oil Minister,Says,Pipeline Exports
2012-09-18T14:54:37Z,Oil Minister,Says,Pipeline Exports
2012-09-18T14:59:47Z,Falcons ’ Michael Turner,Arrested After,Win
2012-09-18T15:00:00Z,Mortgage Lending,Dropped,10 %
2012-09-18T15:03:57Z,EBRD,Approves,Jordan Investments
2012-09-18T15:07:36Z,International Demand,Rises on,Europe
2012-09-18T15:08:33Z,Pakistan Twin Explosions,Kill,Hurt 20 Near Karachi Mall
2012-09-18T15:17:15Z,Komercni,Lower on,Crisis
2012-09-18T15:20:55Z,NEI ’s Fertel,Says in,in Future
2012-09-18T15:30:00Z,Court,Reinstates,Rule
2012-09-18T15:32:48Z,Forint,Weakens,Euro Crisis Concerns Increase
2012-09-18T15:50:06Z,Plutocratic Dinner,Doomed,Campaign
2012-09-18T15:52:14Z,Norilsk,Sees,Nickel
2012-09-18T15:54:02Z,Car Sales,Drop as,Demand Falls
2012-09-18T15:54:02Z,European Car Sales,Drop as,German Demand Falls
2012-09-18T15:54:44Z,Russia 's Beet-Sugar Harvest Seen,Reaching,11 Million Tons
2012-09-18T15:55:20Z,Google YouTube,Will Sell,Fox Movies
2012-09-18T15:55:20Z,Other Sites,Will Sell,Fox Movies
2012-09-18T15:55:20Z,Sites,Will Sell,Fox Movies
2012-09-18T15:56:37Z,Singh,Sell Indians on,Liberalization
2012-09-18T16:00:14Z,European Stocks,Decline for,Day
2012-09-18T16:00:14Z,Stocks,Decline for,Second Day
2012-09-18T16:04:07Z,Stakes,in,Oil Exploration Projects
2012-09-18T16:04:37Z,German Stocks,Fall for,Day
2012-09-18T16:04:37Z,Stocks,Fall as,Deutsche Bank Drops
2012-09-18T16:06:31Z,Bonds,Gain After,Nation ’s First Auction
2012-09-18T16:06:31Z,Nation,After,First Auction
2012-09-18T16:06:31Z,Spanish Bonds,Gain After,Nation ’s First Auction
2012-09-18T16:08:09Z,Manchester United,Has,Loss
2012-09-18T16:12:23Z,Gilts Advance,Boosts,Safety Demand
2012-09-18T16:26:32Z,Zloty,Slips,Euro Crisis
2012-09-18T16:39:15Z,Lawyer,Tells,Court
2012-09-18T16:39:16Z,Russia,in,KHL
2012-09-18T16:39:16Z,Season,in,Russia ’s KHL
2012-09-18T16:45:10Z,CF Partners,Starts,Voluntary Carbon Unit
2012-09-18T16:58:11Z,Nigerian Central Bank,Keeps Rates at,Record High
2012-09-18T16:58:16Z,Pfizer Unit,Must,Face Investor Class Action Over Pristiq
2012-09-18T17:20:32Z,Romney,Ignorance of,Facts
2012-09-18T17:29:00Z,Scotland ’s Salmond,Sees,Renewables Investments
2012-09-18T17:30:26Z,Mets,Moving Minor-League Affiliate to,Las Vegas
2012-09-18T17:31:42Z,Angolan Catholic Church,Wants,Autonomy for Oil-Rich Cabinda
2012-09-18T17:31:42Z,Catholic Church,Wants,More Autonomy for Oil-Rich Cabinda
2012-09-18T17:32:22Z,Virginia Voters,Crediting,Obama
2012-09-18T17:36:54Z,New York Gasoline,Strengthens After,Irving Refinery Shuts Unit
2012-09-18T17:37:12Z,Shell,Unveils,British Columbia Water Treatment Facility
2012-09-18T17:55:55Z,Florida,Loses,Bid
2012-09-18T18:18:56Z,Pacific Rubiales,Sees,CPE-6 Drilling Beginning
2012-09-18T18:30:33Z,Wisconsin AG Seeks Delay,in,Ruling on Bargaining Rights
2012-09-18T18:42:12Z,Usmanov ’s Voting Stake,in,Mail.ru
2012-09-18T19:01:18Z,NFL Films President Steve Sabol,Dies of,Brain Cancer
2012-09-18T19:14:35Z,Gas-Export Study Delay,Puts U.S. Projects for,Year
2012-09-18T19:21:24Z,Cancer,in,Dense Breast Tissue
2012-09-18T19:23:19Z,Pimco ’s QE3 Distaste,Grows,Peso Bond Bet Fades
2012-09-18T20:04:53Z,Ex-CBS Executive,Oversee,Xbox Content
2012-09-18T20:04:53Z,Microsoft,Hires,Ex-CBS Executive
2012-09-18T20:10:49Z,Mitt Romney,Are,Alike
2012-09-18T20:10:49Z,Occupy Wall Street,Are,Alike
2012-09-18T20:11:39Z,Goldcorp,Says,M&A Valuations Attractive
2012-09-18T20:11:44Z,Solid Footing,With,Fox Post-Rothman
2012-09-18T20:13:14Z,Ethanol Falls,Will Slow,Demand
2012-09-18T20:16:42Z,Illumina CEO,Says,Roche ’s $ 6.7 Billion Bid Limited Talks
2012-09-18T20:24:11Z,Deal,With,Lehman Brokerage
2012-09-18T20:27:51Z,IGT Folding Online Poker,in,Europe
2012-09-18T20:29:17Z,Firm,as,CFO
2012-09-18T20:34:33Z,Stealth Lobbying,Tout,Sugar
2012-09-18T20:37:25Z,Chrysler Talks,With,Canada Auto Union Extended
2012-09-18T20:37:25Z,GM,Talks With,Canada Auto Union Extended
2012-09-18T20:41:15Z,Builders ’ Cash,Crunch,Ebbs
2012-09-18T20:41:15Z,Gafisa,Gains,70 %
2012-09-18T20:42:57Z,National Enquirer Owner,Invites,Default Talk
2012-09-18T20:52:50Z,Argentina ’s Dollar Bonds,Tumble After,IMF Threatens Censure
2012-09-18T20:55:22Z,L.A. Administrator,Urges,Older Retirement
2012-09-18T20:59:07Z,FedEx,Slumps Amid,Europe Concern
2012-09-18T20:59:07Z,Most U.S. Stocks,Fall,FedEx Slumps Amid Europe Concern
2012-09-18T20:59:07Z,U.S. Stocks,Fall,FedEx Slumps
2012-09-18T21:00:54Z,Outrage,Is,Crazy
2012-09-18T21:02:29Z,U.S.,Prompts,Border Alert
2012-09-18T21:03:38Z,Emerging Stocks,Drop,Most in 2 Weeks on China
2012-09-18T21:03:38Z,Stocks,Drop,Most
2012-09-18T21:04:11Z,Alibaba,Completes,$ 7.6 Billion Repurchase of Yahoo Stake
2012-09-18T21:08:32Z,Aussie Dollar Declines,Slowing,Growth
2012-09-18T21:12:19Z,RIM,Forges Licensing Deal for,Memory Storage
2012-09-18T21:15:35Z,Repair Work,With,Lockheed Needed
2012-09-18T21:17:47Z,Storms May Bring Tornadoes,Delays to,Northeast
2012-09-18T21:20:03Z,Novartis,Billion of,U.S. Bond Issuance
2012-09-18T21:33:38Z,Billion,Buy Vivint Inc. for,$ 2
2012-09-18T21:33:38Z,Blackstone Group,Agrees,Billion
2012-09-18T21:40:42Z,LS Power,Gets,Prudential
2012-09-18T21:40:42Z,Prudential,Funding for,California Solar
2012-09-18T21:45:17Z,Spain,Floats,Bailout Prospect
2012-09-18T21:49:53Z,Blackstone,Buy,Vivint
2012-09-18T21:53:52Z,Allies Bemoan,Lost,Days
2012-09-18T21:53:52Z,Romney,Regain,Control
2012-09-18T21:55:41Z,Apollo ’s Berry Plastics Said,Starting,IPO Week
2012-09-18T22:00:05Z,Labor,Set to,Talk
2012-09-18T22:00:05Z,Rutte,Urges,Austerity
2012-09-18T22:01:00Z,EU,Over,Crisis Fight
2012-09-18T22:05:22Z,Fed ’s Dudley,Easing,Vital
2012-09-18T22:10:30Z,Arizona,Can,Start Immigrant Status Checks
2012-09-18T22:30:07Z,Empire,Built on,Short-Armed Shirts
2012-09-18T22:30:22Z,Ryan ’s Proposal,Would Shrink,Medicare ’s Doctor Pool
2012-09-18T22:33:12Z,Ohio,Vote,Ruling
2012-09-18T23:00:01Z,Erotic Lessons,Gems ’s,Jackson
2012-09-18T23:00:01Z,Hidden U.S. Software Billionaire Ragon Surfaces,in,Boston
2012-09-18T23:00:36Z,Danone,Touts,Yogurt
2012-09-18T23:00:36Z,Europe,Tightens,Ad Rules
2012-09-18T23:01:00Z,Censors,Leave Imprint on,Exiled Myanmar Artist
2012-09-18T23:01:00Z,Desperate Housewives,Gets,Turkish Twist
2012-09-18T23:01:00Z,German City Needing Aid,Shows,Debt-Crisis Tentacles
2012-09-18T23:01:00Z,Housewives,Gets,Twist
2012-09-18T23:01:22Z,Real Madrid Rallies Past Manchester City 3-2,in,Champions League
2012-09-19T00:18:53Z,Mendis ’s Bowling Record,Sparks,Sri Lanka Rout in Twenty20 Opener
2012-09-19T00:18:53Z,Sri Lanka Rout,in,Twenty20 Opener
2012-09-19T01:53:20Z,Game Maker,Expands,Beyond Facebook
2012-09-19T01:53:20Z,Kabam,Weighs,IPO
2012-09-19T02:03:22Z,Peso Forecasts,Raised,Rest of Region Cut
2012-09-19T04:01:01Z,Chinese-Owned Company,on,Wind-Farm Bid
2012-09-19T04:01:01Z,Pennsylvania Court,Sets,Voter-ID Ruling
2012-09-19T04:01:01Z,Samsung,Defeat Galaxy Tab Sale Ban in,Apple Case
2012-09-19T04:10:29Z,Anti-Japan Protests,Raise,Risks for China ’s Leadership Shift
2012-09-19T04:10:29Z,China,for,Leadership Shift
2012-09-19T04:10:29Z,Protests,Raise,Risks
2012-09-19T05:17:42Z,China Resumed Treasury,Buys,Trade Surplus Widened
2012-09-19T07:56:02Z,Vivendi,for,GVT Unit
2012-09-19T08:33:10Z,LG,Sued by,Illinois
2012-09-19T09:08:12Z,UBS,Is Said to,Plan
2012-09-19T12:21:03Z,Czech Liquor Ban,Hurts,Pernod
2012-09-19T12:35:04Z,Merkel Resolve,Tested With,EADS
2012-09-19T12:52:44Z,Oil,Pares,Gains
2012-09-19T15:36:04Z,Romney,Invites,Attacks on Class Warfare Charge
2012-09-19T16:37:22Z,Czech Yields,Drop Before,Bond Auction
2012-09-19T16:47:45Z,Platinum Holdings,Expand,Record on Mine Disruptions
2012-09-19T20:35:20Z,Uranium Recovery,Postponed to,Low
2012-10-16T14:27:04Z,Confidence,Climbs for,Sixth Month
