2012-11-06T00:00:01Z,Duck,Serves,Snacks
2012-11-06T00:00:01Z,May,Consider,Share Buybacks
2012-11-06T00:00:01Z,Waffle,Serves,Expensive Wines
2012-11-06T00:01:00Z,Murray,Win at,ATP Finals
2012-11-06T00:11:35Z,ResCap Buyer,Must,U.S.
2012-11-06T00:27:07Z,China,Overtakes U.K.,N.Z. ’s Second-Biggest Tourism Draw
2012-11-06T00:54:48Z,ECB ’s Constancio Rebuffs Report Spain,Violated,Debt Rules
2012-11-06T01:00:00Z,Women Donors,Shun,Super-PACs
2012-11-06T02:00:13Z,Karachi Gangs,Prepare for,Polls
2012-11-06T02:00:37Z,Elliott Carter,Methuselah of,American Composers
2012-11-06T02:02:33Z,Strength,in,Mining States
2012-11-06T02:34:12Z,Lending May Help,Weaken,BOJ Official
2012-11-06T02:34:12Z,Unlimited Lending May Help,Weaken,BOJ Official
2012-11-06T03:32:18Z,Funds,Buy,1.14 Billion Rupees of Derivatives
2012-11-06T03:32:18Z,Global Funds,Buy,1.14 Billion Rupees of Indian Derivatives
2012-11-06T04:38:09Z,Palm Oil Inventories,in,Malaysia Jumped
2012-11-06T04:45:17Z,Hong Kong Short Selling Turnover,Recorded,11/06/2012
2012-11-06T05:00:01Z,Obama,Finds,Voice
2012-11-06T05:00:33Z,Storm Art Damage,Overwhelms Conservators in,Wake of Sandy
2012-11-06T05:01:00Z,4.67 %,in,Third Quarter
2012-11-06T05:01:00Z,Cardinals ’ Hitting,Coach After,Three Years
2012-11-06T05:01:00Z,Cash,Was,Sent
2012-11-06T05:01:00Z,Cats,Await,Saviors Drive
2012-11-06T05:01:00Z,Live Nation Profit,Climbs,12 %
2012-11-06T05:01:00Z,Mark McGwire,Exits,Cardinals ’ Hitting Coach
2012-11-06T05:01:00Z,My DNA Results,Spur,Alzheimer ’s Anxiety
2012-11-06T05:01:00Z,Pacino,Opening to,December
2012-11-06T05:01:00Z,Romney,Remains,Mystery
2012-11-06T05:01:00Z,Saviors,Drive to,Rescue
2012-11-06T05:01:00Z,Secret Cash,Was,Sent
2012-11-06T05:01:01Z,2012,as,Top Player
2012-11-06T05:01:01Z,G-20,Ignores,Climate
2012-11-06T05:01:01Z,Miguel Cabrera,Is,Selected by MLB Peers as 2012 ’s Player
2012-11-06T05:01:01Z,World Bank ’s Kim,Raises,Sandy
2012-11-06T05:01:03Z,Driver Shortage,Boosting,Dedicated U.S. Trucking Business
2012-11-06T05:01:03Z,Green Jobs Depend,Win As,Fiscal Cliff Approaches
2012-11-06T05:01:03Z,Obama Mirror Image,With,Lessons
2012-11-06T05:01:06Z,Patients,Suffer as,Money Overwhelms Therapy
2012-11-06T05:09:51Z,Atlantic City,Faces,New Blows
2012-11-06T05:09:51Z,Struggling,in,Best Times
2012-11-06T05:22:35Z,China,Initiates Case With,WTO
2012-11-06T05:41:48Z,Brees,Helps,Vick to Fourth Straight NFL Loss
2012-11-06T05:41:48Z,Saints Send Eagles,Vick to,Fourth Straight NFL Loss
2012-11-06T05:51:49Z,Debt,With Project,BofA
2012-11-06T05:51:49Z,Origin May Fund LNG Project,With,Debt
2012-11-06T05:54:48Z,Plans,Sell,Convertible Bonds
2012-11-06T06:00:00Z,Clues,in,Three Eastern States
2012-11-06T06:05:54Z,Australia,Sparks Currency Gain by,Holding Key Rate
2012-11-06T06:20:12Z,New York Knicks Beat 76ers,in,13 Years
2012-11-06T06:20:12Z,Team,for,Best NBA Start
2012-11-06T06:37:28Z,Chelsea,Turns,Remy as Cheaper Striker Option
2012-11-06T06:45:00Z,Consumer Confidence,Remains Unchanged in,October
2012-11-06T06:45:00Z,Swiss Consumer Confidence,Remains,Unchanged
2012-11-06T07:04:41Z,Bunds,Open,Changed
2012-11-06T07:04:41Z,German Bunds,Leaving,10-Year Yield
2012-11-06T07:09:16Z,Mauritius October Inflation,Accelerates to,9-Month High
2012-11-06T07:16:51Z,South Africa ’s Marcus,Spends,New Mandela Banknotes
2012-11-06T07:18:54Z,Egypt,Eases,Curfew
2012-11-06T07:18:54Z,Shops,Cafes Amid,Opposition
2012-11-06T07:42:08Z,Green Moon,Upstages,Runners
2012-11-06T07:58:01Z,Green Energy Push,Helps,Huaneng Bond Sales
2012-11-06T08:29:17Z,Crude Oil,Fluctuates Before,Election
2012-11-06T08:29:17Z,Oil,Fluctuates Before,Election
2012-11-06T08:29:17Z,Ruble,Weakens Day,Oil Fluctuates Before Election
2012-11-06T08:35:17Z,Ericsson,Says,Telecom Services
2012-11-06T08:35:17Z,Telecom Services,Outpace,Equipment Sales
2012-11-06T08:35:54Z,Estonia Unions,Seek,Higher Minimum Wage
2012-11-06T08:37:59Z,U.S. Fiscal Cliff,Priced,Smead Capital
2012-11-06T08:40:01Z,Hannover Re Targets Record Profit,Rises,63 %
2012-11-06T08:41:17Z,Andritz,Cautions After,Hydropower Turbine Intake Drops
2012-11-06T08:42:59Z,African Mine,Strikes by,12.5 Billion Rand
2012-11-06T08:42:59Z,Mine,Strikes by,12.5 Billion Rand
2012-11-06T08:42:59Z,South African Mine,Strikes by,12.5 Billion Rand
2012-11-06T09:25:49Z,Disaster Planning,Takes Seat to,Debt Crisis
2012-11-06T09:27:55Z,Taiwan Five-Year Yield,Rises on,Rate Outlook
2012-11-06T09:32:45Z,LDK,End,Wafer Supply
2012-11-06T09:36:42Z,Finances Quest,Spare,Education
2012-11-06T09:36:42Z,Sound Finances Quest,Spare,Education
2012-11-06T09:36:42Z,Van Rompuy,Says,Finances Quest
2012-11-06T09:50:27Z,Spain,Rise as,Spanish 10-Year Yield Falls
2012-11-06T09:50:33Z,Emirates NBD,Sees,2013 Retail Revenue
2012-11-06T09:58:58Z,Credit Risk,in,Portugal
2012-11-06T09:59:06Z,Iceland,Sees Mortgage Bubble Threat From,Foreign Cash
2012-11-06T10:00:00Z,Euro-Region Producer-Price Inflation,Held,Steady
2012-11-06T10:08:44Z,Equity Bank,Climbs,14 %
2012-11-06T10:08:54Z,Bank Dollar Funding Cost Increases,in,European Money Markets
2012-11-06T10:16:58Z,60 Billion Yen Bonds,in,Japan
2012-11-06T10:17:37Z,Asian Stocks,Rise Before,U.S. Vote
2012-11-06T10:17:37Z,Stocks,Rise as,Utilities Advance
2012-11-06T10:26:00Z,Euro-Area Factory,Contracts for,9th Month
2012-11-06T10:33:18Z,Fall,to Jobs,Study
2012-11-06T10:33:22Z,Imagination,Buys,MIPS Operations
2012-11-06T10:39:09Z,Fifth Day,for Advance,Rupee Strength
2012-11-06T10:45:00Z,Cyprus,Wins EU Approval to,Offer Bank Guarantees
2012-11-06T10:54:03Z,Dominican Republic,Raise,Taxes
2012-11-06T11:40:51Z,Russia,Starts,Tests
2012-11-06T11:41:33Z,Chile Policy Makers,Were in,October Rate Decision
2012-11-06T11:42:30Z,Barclays,Add,Nigeria Bonds
2012-11-06T11:42:30Z,Naira,Snaps Retreat,Barclays Set
2012-11-06T11:52:48Z,Trinidad,’s Contracts,Guardian
2012-11-06T12:05:10Z,U.K. Manufacturing,Increased in,September
2012-11-06T12:07:43Z,Renaissance Capital,’s Varawalla,Konigsberg Said Leave
2012-11-06T12:10:25Z,Slovenia,Put on,Vote Call
2012-11-06T12:18:43Z,Factory Orders,Slump,Most
2012-11-06T12:18:43Z,German Factory Orders,Slump,Most
2012-11-06T12:25:34Z,France,Had October as,Rain Flooding
2012-11-06T12:44:45Z,DirecTV ’s Profit,Trails,Estimates
2012-11-06T12:49:08Z,Cliffhanger,Vote,General Strike Begins
2012-11-06T12:56:10Z,2.5 %,in,2012
2012-11-06T12:56:10Z,Contract,to Economy,Economics Institute
2012-11-06T13:05:08Z,Middle East Supertanker Surplus Seen Unchanged,in,Broker Survey
2012-11-06T13:12:09Z,PGNiG,Has,Record Jump
2012-11-06T13:27:45Z,2013 Coal,Highest as,Crude Gains
2012-11-06T13:27:45Z,Coal,Rises,Highest in Two Weeks as Gains
2012-11-06T13:27:45Z,European 2013 Coal,Rises,Highest in Two Weeks
2012-11-06T13:27:45Z,European Coal,Rises,Highest in Two Weeks as Gains
2012-11-06T13:35:54Z,Netoil,Submits,Offer for Petroplus Refinery
2012-11-06T13:35:54Z,Revised Offer,in,France
2012-11-06T13:43:46Z,Eatery,in,Istanbul
2012-11-06T13:57:49Z,South Africa Vineyards,Burned for,Second Day Protests
2012-11-06T14:02:18Z,Increases Gasoline,in,CPI
2012-11-06T14:02:18Z,S. Africa Cuts Weight,Gasoline in,CPI
2012-11-06T14:07:48Z,Treasuries Investors,Neutral Since,August
2012-11-06T14:13:07Z,Fed Funds,Open Within,Target Range
2012-11-06T14:17:33Z,May Continue,in,2013
2012-11-06T14:17:33Z,Poland,in,December
2012-11-06T14:17:33Z,Start Carbon Sales,to Poland,May Continue in 2013
2012-11-06T14:19:22Z,Deutsche Bank,Rejects,FERC Penalty
2012-11-06T14:24:40Z,Ukraine,Increases,UkrAgroConsult
2012-11-06T14:24:40Z,Wheat,Surplus,UkrAgroConsult
2012-11-06T14:30:00Z,Regulators,Seek,Tougher Trading Book Rules in Third Basel Wave
2012-11-06T14:30:00Z,Tougher Trading Book Rules,in,Third Basel Wave
2012-11-06T14:41:21Z,May,Urges,Restraint
2012-11-06T14:41:21Z,U.K.,Begins,Child Abuse Investigation
2012-11-06T14:41:46Z,BTA Ex-Chairman,Loses Appeal Against,Jail Term
2012-11-06T14:41:46Z,Fugitive BTA Ex-Chairman,Loses Appeal Against,Jail Term
2012-11-06T14:46:42Z,BMW,Sticks After,Quarterly Profit
2012-11-06T14:48:23Z,Storm Aftereffects,Curb,East Coast Supplies
2012-11-06T14:49:47Z,Google Partner Babylon,Seeks,$ 115 Million From Nasdaq IPO
2012-11-06T14:51:05Z,Senior Egyptian Police Officer Shot,in,Latest Sinai Attack
2012-11-06T15:06:47Z,BNP Paribas Names Serebriakov Currency Strategist,in,New York
2012-11-06T15:14:43Z,Zillow,Tumbles as,Forecast Trails Estimates
2012-11-06T15:19:37Z,That,Puts Burden on,Consumers
2012-11-06T15:19:37Z,U.K.,Will Reject,EDF Nuclear Deal
2012-11-06T15:19:46Z,South America Crop Outlook Seen,Deteriorated by,Oil World
2012-11-06T15:20:27Z,Federer,Beats,Tipsarevic
2012-11-06T15:30:17Z,Bancolombia,Assumes,$ 881 Million Interbolsa Bond Operations
2012-11-06T15:46:00Z,Amgen ’s Heart Drug Lowers Patient Cholesterol,in,Study
2012-11-06T15:46:12Z,Dana Gas,Asks Bondholders After,Payment Miss
2012-11-06T15:47:19Z,Turkey Yields,Drop to,Record Low
2012-11-06T15:51:20Z,Morning,in,India
2012-11-06T15:51:20Z,U.S. Votes,Are,Counted
2012-11-06T15:55:24Z,Housing-Market Recovery,in,U.S.
2012-11-06T15:56:55Z,French Sugar-Beet Harvest,Slowed by,Wet October
2012-11-06T15:56:55Z,Sugar-Beet Harvest,Slowed by,Unusually Wet October
2012-11-06T15:58:19Z,Perrigo Premium,Rises on,Bets OTC Sales Gained
2012-11-06T16:00:00Z,Singer ’s Elliott Sells Home Loan Bonds,in,Latest Quarter
2012-11-06T16:04:49Z,Federer,Beats Tipsarevic for,Record ATP Finals Match Win
2012-11-06T16:05:37Z,Thor Industries,Surges as,Revenue Increases
2012-11-06T16:17:55Z,Citigroup,Says,Singapore Monetary Authority Probes Rates
2012-11-06T16:20:59Z,M&S First-Half Net,Tops Estimates,Sales Decline Eases
2012-11-06T16:21:35Z,Talks,With,Airbus
2012-11-06T16:21:57Z,Romney,Defy History in,Election
2012-11-06T16:22:08Z,Ukraine Central Bank,Control,Scope
2012-11-06T16:22:55Z,Gary Cohn,Craig for,New Bond Film
2012-11-06T16:23:18Z,Syria,’s Fight,Activists
2012-11-06T16:23:18Z,Syria ’s Rebels Fight,With,Pro-Assad Palestinians
2012-11-06T16:27:28Z,Ohio Candidate,Sues to,Block Electronic Voting Machines
2012-11-06T16:28:02Z,Air Berlin,Sues Over,Opening of Capital ’s Airport
2012-11-06T16:28:02Z,Capital,of,Airport
2012-11-06T16:32:09Z,Former Armstrong Teammate,Apologizes After,Positive Drug Test
2012-11-06T16:37:19Z,Erdogan Crowning Decade,With,Fitch Rewards Bonds
2012-11-06T16:41:36Z,Hollande,Raises,Signaling Economic Shift
2012-11-06T16:44:34Z,Ukraine ’s Astarta,Gets,IFC Loan
2012-11-06T16:51:54Z,Damages,in,Elevator Cartel
2012-11-06T16:51:54Z,EU Regulator,Can Seek,Damages in Elevator Cartel
2012-11-06T16:52:01Z,Investments,in,Fiscal Target
2012-11-06T16:52:53Z,Million-Dollar Traders,Replaced With,Machines
2012-11-06T16:52:53Z,Traders,Replaced Amid,Cuts
2012-11-06T16:53:42Z,Treasuries,Drop,Election
2012-11-06T16:58:01Z,Debt,Leads,Advance
2012-11-06T16:58:01Z,Greek Debt,Leads,Advance
2012-11-06T16:59:12Z,Serbia,Agrees With,Wholesalers
2012-11-06T17:05:35Z,Election,Settle,Republicans ’ Ayn Rand Debate
2012-11-06T17:08:32Z,Ex-UBS Banker Bagios Pleads Guilty,in,Tax-Evasion Case
2012-11-06T17:10:17Z,U.K. Stocks,Climb as,U.S. Votes
2012-11-06T17:17:29Z,Bats Europe,Seek,U.K. Exchange License
2012-11-06T17:26:26Z,U.S.,Raises,Gasoline Demand Forecast
2012-11-06T17:32:57Z,London,in,Talks Buy to Form REIT
2012-11-06T17:34:48Z,BowLeven,Rises,Most in Four Months on Accord
2012-11-06T17:53:10Z,Liffe Commodities Trade Rose,in,October
2012-11-06T17:54:28Z,EU Carbon,Rises as,Delays Cut Auction Supply
2012-11-06T18:08:50Z,Obama Will Win,Say,Expectation Polls
2012-11-06T18:59:25Z,Lakers Guard Steve Blake,Abusing,Fan
2012-11-06T19:00:01Z,Eight-Month Low,to Falls,MasterCard
2012-11-06T19:00:08Z,Copper Futures Post Biggest Gain,in,Almost Three Weeks
2012-11-06T19:23:51Z,Colombia ’s Peso,Surges,Seized Brokerage ’s Trading Taken Over
2012-11-06T19:26:11Z,Pennon Subsidiary Viridor,in,25-Year South London Waste Contract
2012-11-06T19:38:46Z,L’Oreal,Confirms,2012 Goal
2012-11-06T19:38:46Z,Sales,Disappoint,Analysts
2012-11-06T19:51:38Z,Wells Fargo,Makes Canada Priority in,Global Expansion
2012-11-06T19:55:33Z,Christie,Says,Know-Nothing Romney Aides Spread Reports
2012-11-06T19:56:37Z,Ohio Candidate,Loses,Bid for Voting Machine Ban
2012-11-06T20:09:58Z,Manhattan Top-Tier Office Rents,Drop,Most
2012-11-06T20:19:22Z,RBNZ,Says,Kiwi Currency May Remain Strong on Economic Outlook
2012-11-06T20:28:53Z,Crimson Tide,With,Win as Election-Year Omen
2012-11-06T20:28:53Z,Redskins Rule Vies,With,Crimson Tide ’s Win as Election-Year Omen
2012-11-06T20:34:38Z,It,May Sell,Units
2012-11-06T20:57:23Z,Mets ’ Duda,Breaking,His Wrist
2012-11-06T20:58:27Z,Hamptons Chef Tourondel,Settles,Lawsuit
2012-11-06T21:09:19Z,Egypt Government,Raises Payments for,2013 Season
2012-11-06T21:09:45Z,Amazon Monthly Prime Option,Steps Challenge to,Netflix
2012-11-06T21:09:45Z,Amazon Prime Option,Steps Challenge to,Netflix
2012-11-06T21:14:02Z,THQ,Plunges Ever,Loss
2012-11-06T21:14:16Z,Georgia,'s Detained,Lawyer
2012-11-06T21:18:06Z,Crude Oil,Moving by,Trains
2012-11-06T21:21:53Z,New York Court,Dies at,68
2012-11-06T21:28:13Z,Arab Bank,Wins,Dismissal
2012-11-06T21:28:37Z,Baidu,Rises,Earnings Drive
2012-11-06T21:28:37Z,Earnings,Drive,Higher
2012-11-06T21:28:56Z,Market,Prefers,Obama
2012-11-06T21:39:58Z,AOL,Posting,Profit
2012-11-06T21:40:51Z,India,Talks on,Uranium Sales
2012-11-06T21:42:27Z,Cost,Lost to,Mortgage Deal
2012-11-06T21:52:20Z,Security,Is,Questioned
2012-11-06T21:54:28Z,Romney Supporters,Tout,Pew Reports
2012-11-06T21:55:47Z,Crude Oil,Rises on,Gasoline Supply Concern
2012-11-06T21:55:47Z,Oil,Rises on,Gasoline Supply Concern
2012-11-06T22:06:45Z,3-Year Yield,Exceeds,Forecast
2012-11-06T22:06:45Z,Yield,Exceeds,Forecast
2012-11-06T22:11:01Z,Canadian Dollar Gains,With,Stocks
2012-11-06T22:12:08Z,Dish,Rises,Customer Growth Stalls
2012-11-06T22:12:39Z,Job Openings,Decreased,Hiring Fell
2012-11-06T22:14:13Z,Stocks Rise,Drop Amid,U.S. Voting
2012-11-06T22:23:49Z,Pfizer ’s Arthritis Treatment,Wins,Approval
2012-11-06T22:25:59Z,David Ferrer,Beats Del Potro at,ATP World Tour Finals
2012-11-06T22:30:00Z,Fastest Pace,in,Year
2012-11-06T22:53:22Z,Casinos,Utilities,Suffer
2012-11-06T22:53:22Z,Moody,Says,Utilities Suffer After Sandy
2012-11-06T23:13:00Z,Mix,With,Court Battles
2012-11-06T23:13:23Z,Dollar,Weakens,Ahead
2012-11-06T23:15:19Z,Australian Stocks,Climb,U.S. Voting Continues
2012-11-06T23:15:19Z,Stocks,Climb,U.S. Voting Continues
2012-11-06T23:29:44Z,Dominican Republic Congress,Backs,Tax Hikes
2012-11-06T23:30:00Z,New EU Threat,Offers,Tale
2012-11-06T23:30:53Z,Form,With,Qatari Fund
2012-11-06T23:31:38Z,New York,Looks,Into Craigslist Gasoline Sales
2012-11-06T23:36:26Z,India Belies Ford-GM Comeback,With,Combined Share of 6.5 %
2012-11-07T00:00:00Z,Thomas Lynch,Dies at,88
2012-11-07T00:35:41Z,Bain,Says to,Rebound
2012-11-07T01:30:41Z,ACLU,Loses Bid for,Emergency Access to Ballots
2012-11-07T01:41:12Z,E-Mail Votes Overwhelm Election Officials,in,Storm Zone
2012-11-07T02:00:00Z,Diageo,Shrinking,Johnnie Walker
2012-11-07T02:00:01Z,BM&FB ovespa Profit,Misses,Estimates on Lower Trading Volume
2012-11-07T02:25:50Z,Kiwi Staying,Strong on,Nation ’s Growth Outlook
2012-11-07T02:25:50Z,Nation,on,Growth Outlook
2012-11-07T02:25:50Z,RBNZ,Sees,Kiwi Staying Strong on Nation ’s Growth Outlook
2012-11-07T03:11:40Z,South Korea,’s Ahn,Moon Merge
2012-11-07T05:00:01Z,Apple,Says,Jelly Bean Infringe Patent
2012-11-07T05:00:01Z,Samsung,’s Note,Jelly Bean Infringe Patent
2012-11-07T07:46:15Z,Blackstone,Buys Loans Backed by,Stake
2012-11-07T07:46:15Z,Paris,in,Gecina
2012-11-07T08:07:34Z,Election Day Voting Target Voter ID,Mural of,Obama
2012-11-07T12:01:44Z,Opponents,Tap,Resentmen
2012-11-07T12:01:44Z,Putin,Faces,Tide
2012-11-07T13:15:38Z,New Storm,Deepens,Woe
2012-11-07T13:40:12Z,Samaras,Faces,Down Parliament Dissenter
2012-11-07T14:01:35Z,Gazprom,Awaits Arbitration,RWE Gas Price Talks Stall
2012-11-07T14:25:54Z,Hollande,Introduces,Same-Sex Marriage Bill in French Cabinet
2012-11-07T14:25:54Z,Same-Sex Marriage Bill,in,French Cabinet
2012-11-07T20:39:17Z,Fairfield Greenwich,Settles,Claims
2012-11-07T21:23:51Z,Plexus,Plunges on,Loss of Biggest Client Juniper Networks
2012-11-07T21:36:50Z,Profit,Tops Analyst Estimates on,TV Ad Gains
2012-11-07T21:50:55Z,Dow,Tumbles,Most
2012-11-08T04:06:57Z,Central Petroleum,Sees,Exploration
2012-11-08T04:06:57Z,Exploration,Starting by,April
