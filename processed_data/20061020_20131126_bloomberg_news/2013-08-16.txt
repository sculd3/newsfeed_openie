2013-08-16T00:16:11Z,Aussie Dollar,Poised for,Weekly Drop
2013-08-16T00:26:51Z,Jesus,Was,Just One Messiah
2013-08-16T01:45:40Z,Hong Kong Stocks,Fall on,U.S. Concern
2013-08-16T01:46:20Z,South Korea Bonds,Set for,Weekly Drops
2013-08-16T01:53:02Z,BHP,Says,May Face Regulator Sanctions in Anti-Corruption Probe
2013-08-16T01:53:02Z,May Face Regulator Sanctions,in,Anti-Corruption Probe
2013-08-16T02:39:41Z,China ’s Stocks Fall,Paring,Weekly Gain
2013-08-16T02:44:05Z,Rubber,Pares,Weekly Advance
2013-08-16T03:17:25Z,Rebar,Pares Third Weekly Gain on,Concern Fed May Taper Stimulus
2013-08-16T03:28:25Z,China,Has Live-Fire Exercises With,Japan
2013-08-16T03:31:02Z,Rupiah Falls,in,Four Years Before Current Account Data
2013-08-16T03:35:51Z,Copper,Poised for,Third Weekly Advance
2013-08-16T03:43:03Z,Rio May Gains,Should,Australia Change Government
2013-08-16T03:43:03Z,Rio May Lead Gains,Should,Australia Change Government
2013-08-16T03:53:15Z,It,Avoid,Third Straight Loss
2013-08-16T03:53:15Z,Tepco Bond Risk,Rises,It Seeks
2013-08-16T04:00:01Z,Florida Pain Victims,Trapped by,Prescription Crackdown
2013-08-16T04:00:01Z,Marisha Pessl,’,Night Film
2013-08-16T04:00:01Z,Marisha Pessl ’s Night Film,Could Use,Darkness
2013-08-16T04:01:00Z,AMR-US Airways Merger Limbo,Puts CEOs,Hold
2013-08-16T04:01:00Z,Detroit General-Obligation Bonds Seen,Paying,100 %
2013-08-16T04:01:00Z,Disney,Lure,Activision ’s Gamers
2013-08-16T04:01:00Z,Kutcher ’s Jobs,Beats Sony to,Cinemas
2013-08-16T04:01:07Z,Home,Help Angels Avoid Sweep by,Yankees
2013-08-16T04:01:07Z,Two Home,Help Angels Avoid Sweep by,Yankees
2013-08-16T04:03:53Z,Anne Doss,Dies at,56
2013-08-16T04:20:06Z,Indonesian President Yudhoyono,Defends,Economic Record
2013-08-16T04:20:06Z,President Yudhoyono,Defends,Economic Record
2013-08-16T04:26:45Z,Goldman,in,Lead
2013-08-16T04:44:41Z,Jakarta Stocks,Fall,Most
2013-08-16T04:45:25Z,Hong Kong Short Selling Turnover,Recorded,08/16/2013
2013-08-16T05:10:07Z,Korea Finance ’s Biggest Dollar Bonds,in,Year Rise
2013-08-16T05:10:07Z,Year Rise,in,Trading
2013-08-16T05:23:20Z,Duhart-Milon,Climbs to,Two-Month High
2013-08-16T05:23:20Z,Duhart-Milon ’09,Climbs to,Two-Month High in London
2013-08-16T05:23:20Z,Two-Month High,in,London
2013-08-16T05:24:13Z,Gold Fields,Says,Investors Flee
2013-08-16T05:24:13Z,Mining,Says,Investors Flee
2013-08-16T05:31:20Z,United,Joins Chase for,Midfielder Willian
2013-08-16T05:40:30Z,Palm,Heads Since,2010
2013-08-16T05:44:28Z,Commonwealth Bank,Raises Billion in,Post-2007 RMBS
2013-08-16T05:46:16Z,Soybeans,Poised for,Week
2013-08-16T06:34:07Z,Magnitude 6.6 Earthquake,Strikes,Central New Zealand
2013-08-16T06:34:07Z,Magnitude Earthquake,Strikes,Central New Zealand
2013-08-16T06:38:14Z,Maersk,Raises Profit Forecast,Earnings Double
2013-08-16T06:40:33Z,China,on,Upper Yangtze
2013-08-16T07:04:41Z,Barclays,Sees Record Profit in,Pakistan
2013-08-16T07:13:29Z,Malaysia,Keeps,Palm Oil Export Tax Unchanged
2013-08-16T07:22:35Z,Soilbuild Business Space REIT Drops,in,Debut
2013-08-16T07:33:27Z,Record,in,China
2013-08-16T07:55:05Z,Rouen Grain Exports,Increase,77 %
2013-08-16T08:01:22Z,Cuadrilla Halts Well,in,Southern England
2013-08-16T08:07:58Z,Philippine Bonds,Fall,Week
2013-08-16T08:12:52Z,Deutsche Bank Sells Subordinated Notes,Tied to,Credit Agricole
2013-08-16T08:25:12Z,Stocks,Retreat for,Second Day Ahead of U.S. Reports
2013-08-16T08:25:12Z,Swiss Stocks,Retreat for,Second Day Ahead
2013-08-16T08:26:53Z,Rupee,Plunges With,Currencies
2013-08-16T08:32:42Z,Longest,Winning Streak in,7 Months
2013-08-16T08:36:15Z,U.K. Stocks,Fall as,Aviva Decline
2013-08-16T08:44:46Z,Thai Bonds,Fall,Week
2013-08-16T08:55:12Z,Titan,Leads,Drop
2013-08-16T09:03:49Z,Euro-Area Exports,Increase,3 %
2013-08-16T09:03:49Z,Inflation,Holds Below,2 %
2013-08-16T09:14:42Z,Nomura,Hires,Gupta From BofA to Boost Asia Equity Sales
2013-08-16T09:14:42Z,Wong,Gupta to,Boost Asia Equity Sales
2013-08-16T09:29:37Z,African Gold Stocks,Rise as,Metal Rebounds
2013-08-16T09:29:37Z,Gold Stocks,Rise to,High
2013-08-16T09:29:37Z,South African Gold Stocks,Rise to,High
2013-08-16T09:29:49Z,Rothschild,On,Currency Bets
2013-08-16T09:59:53Z,Asia Stocks,Fall as,U.S. Jobs Data Spurs Stimulus Concern
2013-08-16T10:01:40Z,England ’s Bresnan,With,Injury
2013-08-16T10:12:44Z,German Rent Rally,Stalls After,Years
2013-08-16T10:12:44Z,Rent Rally,Stalls After,Years of Gains
2013-08-16T10:41:18Z,Line,Raises,Forecast
2013-08-16T10:41:18Z,Maersk,Climbs to,18-Month High
2013-08-16T10:42:56Z,London Cops,Used,Version
2013-08-16T11:01:00Z,Natural Gas Futures Gain,in,Survey on Hotter Weather Outlook
2013-08-16T11:08:22Z,Crackdown,Worsens,Pollution
2013-08-16T11:08:22Z,Military Crackdown,Worsens Pollution in,Nigeria ’s Oil Region
2013-08-16T11:08:22Z,Nigeria,in,Oil Region
2013-08-16T11:17:16Z,AAA Yields,Exceed,AAs
2013-08-16T11:17:17Z,Jain Irrigation Falls,in,India
2013-08-16T11:17:30Z,Snowden,After,Disclosures
2013-08-16T11:27:09Z,Mercedes,Raises Production in,Pursuit
2013-08-16T11:31:09Z,Arsenal ’s Arteta May Miss Six Weeks,With,Injury
2013-08-16T11:39:05Z,Airports,in,Wallonia Region
2013-08-16T11:39:05Z,EU,Investigating,Belgian Aid
2013-08-16T11:57:17Z,Hamburg Port,Turns to,Baltics
2013-08-16T12:00:39Z,Customer Service,in,Age of Internet of Things
2013-08-16T12:14:58Z,Operator Business,Boosts,Sales
2013-08-16T12:19:54Z,African Car Industry,Strike to,Cost 700 Million Rand
2013-08-16T12:19:54Z,Car Industry,Strike,Day
2013-08-16T12:19:54Z,South African Car Industry,Strike to,Cost 700 Million Rand
2013-08-16T12:20:04Z,Travancore CD,of,Deals
2013-08-16T12:27:48Z,Safaricom,Rises,Record on Data Demand Jump
2013-08-16T12:34:00Z,African Corn,Rises Following,Gain
2013-08-16T12:34:00Z,Corn,Rises Following,Gain
2013-08-16T12:34:00Z,Gain,in,U.S.
2013-08-16T12:34:00Z,South African Corn,Rises for,Day
2013-08-16T12:40:29Z,EUR Gains,in,New York Trading
2013-08-16T12:40:29Z,USD,Gains in,New York Trading
2013-08-16T13:03:35Z,Productivity,in,U.S.
2013-08-16T13:08:04Z,Wells Fargo,Buys,Billion
2013-08-16T13:27:59Z,Markel,Acquires Eagle as,Insurer Bets on Virginia Homebuilding
2013-08-16T13:33:11Z,We,Salvaging,Its Parts
2013-08-16T13:48:36Z,Telekom Austria Said,With,Funds for Serbia Broadband
2013-08-16T13:56:44Z,Europe,Changes,Ryder Cup Captain Selection Starting in 2016
2013-08-16T13:56:44Z,Ryder Cup Captain Selection,Starting in,2016
2013-08-16T14:00:17Z,You,Get,Job
2013-08-16T14:14:59Z,Couples,Lacking,IRS Guidance Risk
2013-08-16T14:14:59Z,Gay,Married,Couples
2013-08-16T14:19:03Z,Company Bond Sales,in,U.S. Fall Most as Yields
2013-08-16T14:33:45Z,Indonesia ’s Coffee Deliveries,Seen by,Volcafe Jumping After Eid
2013-08-16T14:45:43Z,Forint,Weakens on,Foreign Bond Selling Concern
2013-08-16T14:57:58Z,Mexico Peso,Heads to,Weekly Drop
2013-08-16T14:57:58Z,Oil Law,Disappoints,Traders
2013-08-16T15:05:10Z,Metalist Kharkiv ’s Champions League Match-Fixing Ban,Is,Upheld
2013-08-16T15:20:43Z,Ugandan Coffee Exports,Climb on,Bigger Crop
2013-08-16T15:24:01Z,Flyers ’ Giroux,Has,Surgery on Finger
2013-08-16T15:24:53Z,ICE,Clears SEC Regulatory Approval for,NYSE Euronext Deal
2013-08-16T15:25:05Z,Vietnam ’s Coffee Farmers,Seen by,Nedcoffee Holding
2013-08-16T15:38:33Z,Canada June Factory Sales Fall Third Time,in,Four Months
2013-08-16T15:54:26Z,Pole-Vaulter,Says,Misunderstood Over Gays
2013-08-16T15:54:26Z,Russian Pole-Vaulter,Says,Misunderstood
2013-08-16T15:58:05Z,Potential System,Has,Energy Companies on Alert
2013-08-16T15:58:05Z,Potential Tropical System,Has,Energy Companies on Alert
2013-08-16T15:58:43Z,European Stocks,Rise as,Maersk Rallies
2013-08-16T15:58:43Z,Stocks,Rise as,Maersk Rallies
2013-08-16T16:00:01Z,Everbright Securities Trading Error,Roils,Shanghai Stock Market
2013-08-16T16:04:30Z,Gold Traders,Most Bullish as,Paulson Cuts
2013-08-16T16:05:40Z,Hagstrom,Oversee Prime Brokerage in,U.S.
2013-08-16T16:05:40Z,UBS,Promotes,Hagstrom
2013-08-16T16:19:35Z,Chevron,Sell,Forties
2013-08-16T16:49:16Z,Cirque du Soleil Show Tanks,in,China
2013-08-16T17:04:14Z,Best Places,in,Africa
2013-08-16T17:19:51Z,Republicans,Punish,Networks
2013-08-16T17:21:00Z,Obama May,Have Point On,Priorities
2013-08-16T17:23:06Z,Beech Street,in,Commercial Property Loan
2013-08-16T17:23:06Z,Capital,Buys,Beech Street
2013-08-16T17:23:06Z,Capital One,Buys,Beech Street
2013-08-16T17:27:25Z,Hong Kong,Raises,2013 Growth Outlook
2013-08-16T18:00:00Z,Morgan Stanley,Hires,Vlaad
2013-08-16T18:19:08Z,Costa Rica Central Bank,Steps Up,Dollar Purchases
2013-08-16T18:19:44Z,L.A. ’s Trophy Homes,Starring in,Sequel
2013-08-16T18:35:46Z,Sandberg,Takes Over as,Interim Manager
2013-08-16T18:50:03Z,Whose Intellectual Property,Can,I Rip
2013-08-16T18:50:37Z,Curb,Is,First-Ever
2013-08-16T18:50:37Z,Drought-Induced Curb,Is,First-Ever
2013-08-16T18:54:32Z,Reasons,Maintain,Aid
2013-08-16T18:56:18Z,Gold,Rises Since,2008
2013-08-16T18:56:38Z,Ecuador Seeks Crude,in,Amazon Rainforest
2013-08-16T18:57:54Z,Midwest Gasoline,Climbs on,Coffeyville Repairs
2013-08-16T19:27:12Z,$ 300 Million,in,Data-Center REIT Initial Public Offer
2013-08-16T19:27:12Z,QTS,Seeks,Million in Data-Center REIT Public Offer
2013-08-16T19:29:57Z,Norquist,Says,Obama
2013-08-16T19:29:57Z,Obama,Accept,Health-Law Delays
2013-08-16T19:51:11Z,Things,Go Without,Cash
2013-08-16T20:02:29Z,Christie Will Ease Kids ’ Access,With,Doctor Notes
2013-08-16T20:10:37Z,Housing,Starts in,U.S. Rise
2013-08-16T20:12:56Z,Merck,Halts Animal-Drug Sale After,Cattle Problem
2013-08-16T20:30:18Z,Cincinnati Financial Increases,Dividend,3.1 %
2013-08-16T20:35:57Z,Spain,of,Richest Woman
2013-08-16T20:37:54Z,AMR-US Airways Block,Sends,Ripples Through M&A Market
2013-08-16T20:38:23Z,Dell,Results,Underscore Challenges
2013-08-16T20:46:39Z,Qaddafi-Era Libya Work,Haunts,SNC ’s Turnaround
2013-08-16T20:50:08Z,New York,Hunts,Investor-Protection Head
2013-08-16T20:53:13Z,Investors,Weigh,Confidence Data
2013-08-16T20:53:36Z,Latam Airlines Gains,in,Chile
2013-08-16T21:01:04Z,Credit Swaps,in,U.S. Record Largest Weekly Increase
2013-08-16T21:01:09Z,Canada Stocks Rise,Capping,Week
2013-08-16T21:12:34Z,Brazil Real,Tumbles,Most in World
2013-08-16T21:12:34Z,Mantega,Says,Drop Helpful
2013-08-16T21:13:42Z,Fed ’s Fisher,Says,Bond Yields
2013-08-16T21:13:42Z,Investors,See,QE Waning
2013-08-16T21:21:21Z,JPMorgan Suit,With,Bear Stearns Liquidators
2013-08-16T21:39:23Z,Samsung,Gets to,Your Wrist
2013-08-16T21:43:21Z,Dole CEO Murdock,Sued by,Oklahoma Police Fund
2013-08-16T21:49:44Z,Mayer,in,Turnaround Push
2013-08-16T21:49:44Z,Yahoo Names Webb,in,Mayer ’s Turnaround Push
2013-08-16T22:00:25Z,It,Readies,Plug-in i8
2013-08-16T22:36:18Z,Ethanol Declines,With,Corn
2013-08-16T22:45:45Z,China Trading Error,Reduces,Investor Confidence
2013-08-16T22:54:08Z,Worst 3-Day Drop,in,14 Months
2013-08-16T23:00:00Z,Romanee-Conti,Joins,Meursault
2013-08-16T23:01:00Z,Cairo Morgues,Turn,Battleground
2013-08-17T00:01:53Z,Swiss Lawyer Pleads Guilty,Helps in,U.S. Tax Probe
2013-08-17T03:06:04Z,Egypt Islamists,Call Amid,Deaths Rise
2013-08-17T04:00:01Z,Dow Posts,in,Year on Fed Concern
2013-08-17T04:00:01Z,Hospitals,May Absorb,Risk
2013-08-17T04:00:19Z,Rodriguez Implicated Braun,in,Biogenesis Doping
2013-08-17T04:00:22Z,Iraq Open,Strikes on,Terrorists
2013-08-17T04:00:25Z,Plane,Was,Fast
2013-08-17T04:00:25Z,Sinking Too,Was,Fast
2013-08-17T04:01:00Z,Economic Boon,With,Even Detours
2013-08-17T04:01:00Z,Jobs,Survives,Kutcher
2013-08-17T04:01:00Z,NSA,Defends,Spying Amid Anger Over Privacy Violations
2013-08-17T04:01:00Z,Texas Sushi Chef,Pair to,Aid Houston Museum
2013-08-17T04:01:01Z,Bernanke Order,Appealed by,U.S.
2013-08-17T04:01:01Z,Icahn,Loses,Bid
2013-08-17T04:01:01Z,Ryne Sandberg,Gets,Phillies Manager Job
2013-08-17T18:47:07Z,Health Delays,Avoid,Shutdown
2013-08-17T18:47:07Z,Norquist,Says,Obama
2013-08-17T18:47:07Z,Obama,Take,Health Delays
