2011-09-30T00:00:39Z,Real U.S. Map,Country of,Regions
2011-09-30T00:00:53Z,Books,in,Football Realignment
2011-09-30T00:00:53Z,Colleges,Must Keep Focus on,Books in Football Realignment
2011-09-30T00:00:55Z,Keynes,Preside at,Jobs Debate
2011-09-30T00:05:21Z,Pinera,Confirms,Creation of $ 4 Billion Higher Education Fund
2011-09-30T00:32:51Z,McGraw-Hill,in,Talks Combine
2011-09-30T00:32:51Z,Talks,in McGraw-Hill,CME
2011-09-30T01:01:31Z,Meg Whitman,Leaves,Kleiner Perkins
2011-09-30T01:01:31Z,She,Takes CEO Role at,HP
2011-09-30T01:09:00Z,Gamuda,Rises on,Quarterly Profit Increase
2011-09-30T01:09:22Z,Australian Home Values,Fall,3.2 %
2011-09-30T01:09:22Z,Australian Home Values Fall,in,Year
2011-09-30T01:17:23Z,Astino,Rises,Most
2011-09-30T01:30:00Z,Renaissance Learning ’s Deal Math Means Investors,Lose,41 % Payoff
2011-09-30T01:32:30Z,Australia August Private Credit,Rises,0.2 %
2011-09-30T01:33:03Z,Hang Lung Properties,Rise on,Property Purchase
2011-09-30T01:45:27Z,Longfor Properties Shares Fall,in,Early Trading
2011-09-30T02:00:00Z,Rogen,Uses,Pal ’s Cancer
2011-09-30T02:18:20Z,DMCI,Rise on,Philippine Infrastructure Projects
2011-09-30T02:29:08Z,Strong Growth,in,Next Few Years
2011-09-30T02:31:06Z,China Stocks,Sink,Low on Slowdown
2011-09-30T02:46:32Z,Corn Advances,Shrinking,Supply
2011-09-30T02:46:32Z,Report,in,U.S. Stockpiles
2011-09-30T03:00:00Z,Embraer,Sees,2012 Demand
2011-09-30T03:13:33Z,O’Gara,Gets,Ireland Start
2011-09-30T03:22:33Z,Favor,in Philippines,Reviews NDFs
2011-09-30T03:22:33Z,Philippines,in,Favor of Speculative Inflows
2011-09-30T03:36:19Z,China Local Governments,Can Raise,Money
2011-09-30T04:00:00Z,Fed ’s Operation Twist,Bring Down Unemployment in,Poll
2011-09-30T04:00:01Z,Global Investors,Reverses,57 %
2011-09-30T04:00:06Z,Red Sox,Had Probability in,Ninth Inning
2011-09-30T04:00:06Z,Taka-q Parent,Announces,Planned FY Dividend
2011-09-30T04:00:30Z,Black-White Kids Surge,in,South
2011-09-30T04:00:36Z,Economist Hyman,Makes,ISI No. 1
2011-09-30T04:00:36Z,Top Economist Hyman,Makes Too,ISI No. 1
2011-09-30T04:01:01Z,Dallas Stars,Court for,Sport Bankruptcies
2011-09-30T04:01:01Z,Debt Firms,Play,Whack-a-Mole
2011-09-30T04:01:01Z,Whack-a-Mole,Using Lawyers to,Skirt Fee Ban
2011-09-30T04:01:09Z,Consumer Spending,Probably Slowed in,August
2011-09-30T04:01:09Z,Wage Gains,in,U.S.
2011-09-30T04:16:49Z,Torrent Pharma,Gets FDA Tentative Approval for,Memantine Pill
2011-09-30T04:30:00Z,Philippines,Open to,Swap
2011-09-30T04:34:13Z,Milk,in,Australia
2011-09-30T04:34:13Z,Parmalat Issues Voluntary,Recall of,Milk
2011-09-30T04:43:44Z,Philippine Peso Volatility,in,Middle of Range
2011-09-30T04:48:41Z,China ’s Manufacturing Contracts,Streak Since,2009
2011-09-30T04:58:08Z,Reliance Capital,Gets RBI Approval for,Unit Stake Sale
2011-09-30T05:00:06Z,Godo Steel,Announces,Planned 1H Group Dividend
2011-09-30T05:00:06Z,Kyokuto Co Ltd Parent,Announces,Planned FY Dividend
2011-09-30T05:00:06Z,Sagami Co Ltd,Announces,Planned FY Group Dividend
2011-09-30T05:00:19Z,Hong Kong Short Selling Turnover,Recorded,09/30/2011
2011-09-30T05:17:37Z,iShares Silver Trust Holdings,Dropped,22.71 Tons
2011-09-30T05:24:46Z,Japan Vehicle Production,Rises for,First Time
2011-09-30T05:25:46Z,Berlusconi,by Backed,Corriere
2011-09-30T05:25:46Z,Head Bank,to Backed,Corriere
2011-09-30T05:33:31Z,Talks,With,Leighton
2011-09-30T05:33:40Z,Time,for Groep,Parkiet
2011-09-30T05:35:01Z,Sonova Reviews Middle Ear Implant Activities,in,Overhaul
2011-09-30T05:36:45Z,Sonova May Close Phonak Acoustic Implants Center,in,Switzerland
2011-09-30T06:00:07Z,Okayama Paper Parent,Announces,Planned FY Dividend
2011-09-30T06:00:08Z,Gulliver Intl,Announces,Planned FY Group Dividend of 148.00 Yen
2011-09-30T06:00:08Z,Hiday Hidaka C Parent,Announces,Planned FY Dividend
2011-09-30T06:00:08Z,J-COM Holdings,Announces,Planned FY Group Dividend of 25.00 Yen
2011-09-30T06:00:08Z,Japan MDM,Announces,Planned FY Group Dividend
2011-09-30T06:00:08Z,Mitachi Co Ltd,Announces,Planned FY Group Dividend
2011-09-30T06:00:08Z,Okano Valve Mfg,Announces,Planned FY Group Dividend of 8.00 Yen
2011-09-30T06:00:08Z,Piped Bits Co L Parent,Announces,Planned FY Dividend
2011-09-30T06:00:08Z,Point Inc,Announces,Planned FY Group Dividend of 120.00 Yen
2011-09-30T06:00:08Z,Tri Stage Parent,Announces,Planned FY Dividend of 20.00 Yen
2011-09-30T06:05:35Z,Ruble,Weakens for,Losing 0.7 %
2011-09-30T06:05:35Z,Third Straight Day,% Against,Dollar
2011-09-30T06:08:16Z,Warehouses,in,Poland
2011-09-30T06:10:06Z,Cybele Co Lt Parent,Announces,Planned FY Dividend
2011-09-30T06:10:06Z,Halows Co Ltd Parent,Announces,Planned FY Dividend of 11.00 Yen
2011-09-30T06:10:06Z,Japan ERI Co Lt,Announces,Planned FY Group Dividend
2011-09-30T06:10:06Z,Oomitsu,Announces,Planned FY Group Dividend of 10.00 Yen
2011-09-30T06:10:06Z,Techno Alpha Co,Announces,Planned FY Group Dividend of 21.00 Yen
2011-09-30T06:10:06Z,ktk Inc,Announces,Planned FY Group Dividend
2011-09-30T06:28:02Z,Formosa,Operates,No. 1 Naphtha Cracker
2011-09-30T06:28:02Z,ICIS,% to,90 %
2011-09-30T06:28:08Z,Taiwan May Receive Spot LNG Cargo,in,October
2011-09-30T06:30:07Z,Com Parent,Announces,Planned FY Dividend
2011-09-30T06:30:07Z,E-SUPPORTLINK Lt,Announces,Planned FY Group Dividend of 0.00 Yen
2011-09-30T06:30:07Z,Ichirokudo Co,Announces,Planned FY Group Dividend of 1050.00 Yen
2011-09-30T06:30:07Z,Kitakei,Announces,Planned FY Group Dividend
2011-09-30T06:30:07Z,Medical Com Parent,Announces,Planned FY Dividend
2011-09-30T06:30:07Z,Medical Net Com Parent,Announces,Planned FY Dividend
2011-09-30T06:30:07Z,Net Com Parent,Announces,Planned FY Dividend of 4.00 Yen
2011-09-30T06:30:07Z,Star Mica Co,Announces,Planned FY Group Dividend
2011-09-30T06:34:27Z,Japan ’s Topix Falls,Rally Amid,U.S. Growth Concern
2011-09-30T06:35:10Z,1st Day,in,3
2011-09-30T06:40:09Z,Fels,Says,Higher Inflation Part of Crisis Solution
2011-09-30T06:45:00Z,Consumer,Spending,Climbed After July Drop
2011-09-30T06:45:00Z,French Consumer,Spending,Climbed
2011-09-30T06:45:00Z,Spending,Climbed in,August
2011-09-30T06:45:12Z,64 Megawatts,in,Brazil
2011-09-30T06:46:46Z,UniCredit,Regulators to,Review SIFI Rules
2011-09-30T06:51:50Z,Bulgarian State Pension Fund Gap Seen,in,2011
2011-09-30T06:53:47Z,Hungary August,Seasonally Adjusted,M3 Money Supply
2011-09-30T06:59:44Z,Next Week,Before,Auction
2011-09-30T06:59:44Z,Next Week ’s Auction,Before Fall,Post Weekly Drop
2011-09-30T07:00:00Z,Czech Central Bank,Sees Risk to,Inflation
2011-09-30T07:00:06Z,Nihon Enterpri,Announces,Planned FY Group Dividend
2011-09-30T07:00:06Z,Sobal Corp,Announces,Planned FY Group Dividend of 15.00 Yen
2011-09-30T07:08:12Z,Ramayana Gains,in,12 Days After Report on Sales
2011-09-30T07:15:06Z,Soko Seiren Co L,Announces,Planned FY Group Dividend
2011-09-30T07:16:44Z,India Mukherjee,Says,Too Early
2011-09-30T07:16:53Z,Bayer,Helped,Syndrome Patients
2011-09-30T07:20:36Z,Crude Fluctuates,in,New York
2011-09-30T07:29:31Z,Russian Train Accident,Kills,5
2011-09-30T07:29:31Z,Train Accident,Kills,5
2011-09-30T07:30:33Z,Tepco,Faces Zombie Future,Fukushima Claims Set
2011-09-30T07:31:05Z,1.1 %,in,August
2011-09-30T07:32:55Z,Madeira Region,Has,Debt of 6.3 Billion Euros
2011-09-30T07:36:03Z,India ’s Cabinet,Approves Mining Bill on,Profit
2011-09-30T07:37:42Z,Hong Kong Home Prices Rise,in,Sept. 19-25
2011-09-30T07:37:42Z,Sept. 19-25,in Rise,Centaline
2011-09-30T07:38:53Z,China,Plans to,Limit Mining
2011-09-30T07:41:04Z,Credit Agricole Cuts,Fewer Assets Than,Rivals
2011-09-30T07:42:04Z,Gulf Keystone Chief Executive Gained,in,Unusual Share Deal
2011-09-30T07:42:06Z,QRxPharma,in,Talks With Potential Partners
2011-09-30T07:42:06Z,Talks,With,Potential Partners
2011-09-30T07:45:48Z,Turkey ’s Trade Deficit,Widened in,August
2011-09-30T07:47:25Z,India,Extends Stock-Holding Rule by,2 Months
2011-09-30T07:47:42Z,Second Day,in,New York Trading
2011-09-30T07:47:54Z,Doctors,Quitting,Hospitals
2011-09-30T07:47:54Z,Quitting,Hospitals Over,Pay Demands
2011-09-30T07:47:54Z,Slovak Doctors,Quitting,Hospitals
2011-09-30T07:52:18Z,Japan,in Output,Mitsui
2011-09-30T07:52:18Z,Rise,to Output,Mitsui
2011-09-30T07:52:18Z,Zinc Output,in,Japan
2011-09-30T07:52:49Z,MedTech,Narrows Through,June
2011-09-30T07:52:49Z,Zimbabwe,of,Loss
2011-09-30T07:57:46Z,GDP,Expands,Final 2.4 %
2011-09-30T07:57:46Z,Serbian GDP,Expands,2.4 %
2011-09-30T07:57:46Z,Year,in,Second Quarter
2011-09-30T07:59:22Z,Rubber,in,Tokyo Climbs
2011-09-30T08:00:00Z,European Money-Market Turnover Rose,in,Second
2011-09-30T08:00:00Z,Italy ’s Jobless Rate Fell,in,August
2011-09-30T08:03:07Z,Inflation Slowdown,in,September Will
2011-09-30T08:04:38Z,Czech Economic Growth,Revised by,Statistics Office
2011-09-30T08:08:50Z,60 Megawatt Wind Park,in,Poland
2011-09-30T08:09:11Z,China ’s Stocks,Sink to,to April 2009 Low
2011-09-30T08:12:51Z,Serbia 2Q,Revised,Gross Domestic Product
2011-09-30T08:16:58Z,Further Decline,Against Bank,Basci
2011-09-30T08:17:05Z,Taiwan Dollar,Has,Biggest Drop
2011-09-30T08:17:54Z,China,Orders,Sugar Sales
2011-09-30T08:18:08Z,Goldman,Advises,Sumitomo Mitsui
2011-09-30T08:18:19Z,Armenian Inflation,Accelerated to,6.2 %
2011-09-30T08:18:19Z,Inflation,Accelerated on,Higher Food Costs
2011-09-30T08:20:12Z,Bubble,in,Japan
2011-09-30T08:20:12Z,It,’s 1987 Without,Bubble
2011-09-30T08:20:12Z,Job Losses,Spur,Hollowing-Out Concern
2011-09-30T08:23:20Z,South Korea,Get,May
2011-09-30T08:24:41Z,Baht,Completes Drop on,Fund Outflows
2011-09-30T08:26:31Z,Commerzbank,Says,ECB to Lower Benchmark Twice Through Early 2012
2011-09-30T08:30:39Z,China Developers ’ Shares Slump,in,Hong Kong
2011-09-30T08:34:01Z,CapitaMalls,More in,2012
2011-09-30T08:37:38Z,National Grid,Raises,260 Million Pounds
2011-09-30T08:40:32Z,Scottish,Says,Second-Half
2011-09-30T08:40:32Z,Second-Half,Make Up for,Profit Slide
2011-09-30T08:40:32Z,Southern,Says,Second-Half
2011-09-30T08:46:47Z,Asian Currencies,Set Since,1997 Crisis Caused IMF Bailouts
2011-09-30T08:46:47Z,Currencies,Set for,Month
2011-09-30T08:47:27Z,Hong Kong,’s Supply,Deposits
2011-09-30T08:47:35Z,Copper Rout,Outpaces,Analysts
2011-09-30T08:48:36Z,Finland,Completes EFSF Ratification as,President Signs Measures
2011-09-30T08:49:03Z,Taiwan Central Bank Sells NT$ 70.35 Billion,in,Certificates
2011-09-30T08:49:18Z,Lawmakers,Curbing Risks at,Credit Suisse
2011-09-30T08:49:18Z,Swiss Lawmakers,Curbing,Risks
2011-09-30T08:50:44Z,Hoopp,Buy,2 Czech Malls
2011-09-30T08:50:44Z,Meyer Bergman,Buy,2 Czech Malls
2011-09-30T08:51:04Z,Bernie Ecclestone,Set for,November Testimony
2011-09-30T08:51:04Z,Gribkowsky,in,F-1 Trial
2011-09-30T08:52:33Z,Fall,Loss Since,2001
2011-09-30T08:55:00Z,Balda,Says,Unit Plans
2011-09-30T08:55:41Z,Central Bank,Adds,Funds
2011-09-30T08:55:41Z,China ’s Swaps,Slide,Most Since 2008
2011-09-30T08:56:21Z,Great Stagnation,Looms,Confidence Lags
2011-09-30T08:56:21Z,Stagnation,Looms,Confidence Lags
2011-09-30T08:56:45Z,14 %,in,Zurich
2011-09-30T08:57:42Z,Apple Proposal,End,Australia Patent Dispute
2011-09-30T08:57:42Z,Samsung Electronics,Gives,Apple Proposal
2011-09-30T08:57:45Z,HSBC Alternative,Buys,Rest in Manhattan ’s Times Square
2011-09-30T08:57:45Z,Manhattan,in,Times Square
2011-09-30T09:00:00Z,Austria ’s 2012 GDP Growth Outlook,Slashed,Second Time
2011-09-30T09:00:00Z,Austria ’s GDP Growth Outlook,Slashed,Second Time
2011-09-30T09:05:02Z,Hochtief,Raises,2011 Guidance
2011-09-30T09:05:26Z,Reynders,Tells,RTBF
2011-09-30T09:05:41Z,Peso,Has,Month
2011-09-30T09:05:41Z,Philippine Peso,Has Month Since,May 2010
2011-09-30T09:07:13Z,Bunds,Pare Advance After,Euro-Area Inflation
2011-09-30T09:07:13Z,German Bunds,Pare,Advance
2011-09-30T09:13:17Z,India,’s Drops,Misses
2011-09-30T09:17:45Z,Zambian Trade,Surplus Widens in,August
2011-09-30T09:17:45Z,Zambian Trade Surplus Widens,in,August
2011-09-30T09:22:40Z,Mirvac,Sell,Stake
2011-09-30T09:25:35Z,Food Wheat,in,Tender
2011-09-30T09:29:44Z,Chinese Stocks,in,Hong Kong Cap
2011-09-30T09:31:37Z,German States,Vote on,EFSF
2011-09-30T09:31:37Z,Schaeuble,Cites Contagion Risk,German States Vote on EFSF
2011-09-30T09:33:37Z,Norway Credit Growth,Accelerates,Unemployment Declines
2011-09-30T09:38:01Z,Bills,in,Fourth Quarter
2011-09-30T09:38:47Z,Japan Firms,Looking for,Vietnam Expansion
2011-09-30T09:38:47Z,Mizuho CEO Sato,Says,Japan Firms Looking for Vietnam Expansion
2011-09-30T09:38:53Z,Pound,Erases Decline Versus Dollar to,Trade Little
2011-09-30T09:40:47Z,London Stock,Decline to,Comment on LME
2011-09-30T09:42:27Z,More Soccer Stadiums,in,Brazil
2011-09-30T09:43:07Z,Serbian Fiscal Council,Says,Deficit Justified
2011-09-30T09:45:00Z,Unnim Banc,Get,Temporary Approval
2011-09-30T09:45:17Z,Mauritius Rupee,Sets for,Biggest Quarterly Drop
2011-09-30T09:49:22Z,Hungary Bank Group,Still Decide,Turning to EU on Loan Plan
2011-09-30T09:52:02Z,Hochtief,Says,Qatar Holding Voting Rights Rose
2011-09-30T09:57:53Z,Lebanon ’s 7-Month Budget Gap,Narrows,33 %
2011-09-30T09:59:35Z,Sri Lankan Inflation,Slows,Boosting Scope
2011-09-30T10:00:00Z,U.S. Recession Risk,Rises on,April Oil Price Jump
2011-09-30T10:00:25Z,Banks ’ Private Sector Deposits Fell,Annual,10.4 %
2011-09-30T10:00:25Z,Banks ’ Sector Deposits Fell,Annual,10.4 %
2011-09-30T10:00:25Z,Irish Banks ’ Private Sector Deposits Fell,Annual,10.4 %
2011-09-30T10:00:25Z,Irish Banks ’ Sector Deposits Fell,Annual,10.4 %
2011-09-30T10:06:13Z,Vietnam ’s Bonds,Gain for,Third Week
2011-09-30T10:06:50Z,Foreign Maids Win Court Challenge,Seeking,Residency
2011-09-30T10:07:59Z,$ 5 Billion Expansion,in,U.S.
2011-09-30T10:07:59Z,3i,Seeks,$ 5 Billion Expansion in U.S.
2011-09-30T10:10:38Z,Basci,Says,Economy Slows
2011-09-30T10:10:38Z,Turkish Warning Lights,Still Says,Economy Slows
2011-09-30T10:10:38Z,Warning Lights,Says,Economy Slows
2011-09-30T10:13:59Z,Finland ’s Katainen Wins First Confidence Motion,in,Parliament
2011-09-30T10:14:18Z,Bank,Says,Unnim
2011-09-30T10:14:18Z,Unnim,Get,EU568 Million
2011-09-30T10:16:17Z,Coal,Heads Since,March 2010
2011-09-30T10:16:17Z,European Coal,Heads for,Biggest Monthly Drop
2011-09-30T10:27:25Z,India,’s Drop,Interocean Data Show
2011-09-30T10:28:49Z,Entire 50 % Stake,in,TransContainer
2011-09-30T10:28:49Z,Railways,May Sell,Entire 50 % Stake
2011-09-30T10:28:49Z,Russian Railways,May Sell,Entire Stake
2011-09-30T10:33:44Z,Foreign Investors Increase Holdings,in,August
2011-09-30T10:35:24Z,Standard Bank,Lends,$ 500 Million
2011-09-30T10:35:24Z,Vedanta,to,Konkola Copper
2011-09-30T10:36:34Z,South Africa,Beats,Samoa
2011-09-30T10:37:53Z,Spain Rescue Fund,% of,Novacaixagalicia
2011-09-30T10:45:09Z,Barclays,Halt Part for,10 Days
2011-09-30T10:47:12Z,EFSF Expansion,Clears,Last Hurdle
2011-09-30T10:48:14Z,Shoprite CEO Basson,Earns,36.5 Million Rand
2011-09-30T10:48:42Z,CEO,Starting,Oct. 1
2011-09-30T10:50:35Z,Slovak Politicians,Must Find,EFSF Solution
2011-09-30T10:53:21Z,ECB Effort,Is Complicated by,Surprise Surge
2011-09-30T10:54:54Z,Hella S. Haasse,Dies,Aged 93
2011-09-30T10:55:35Z,Governor,Bank of,Spain
2011-09-30T10:55:48Z,Norway,Seeks,$ 176 Million
2011-09-30T11:00:18Z,Daimler,Says,Mercedes Truck Orders
2011-09-30T11:01:23Z,Board,Discuss,Fund Raising
2011-09-30T11:01:44Z,UBA Nigeria ’s Teudor-Matthews,Quits as,Deputy Managing Director
2011-09-30T11:04:22Z,Inflation Rate,Probably Fell in,September
2011-09-30T11:04:22Z,Turkish Inflation Rate,Fell in,September
2011-09-30T11:06:20Z,German Retail Sales,Drop,Most in Than Four Years on Crisis Concerns
2011-09-30T11:06:20Z,Retail Sales,Drop,Most in Than Four Years on Crisis Concerns
2011-09-30T11:07:59Z,Fed Funds Projected,Open at,0.09 ICAP Says
2011-09-30T11:08:32Z,U.S. Average Gasoline Price,Fell,Yesterday
2011-09-30T11:08:32Z,U.S. Average Regular Gasoline Price,Fell,Yesterday
2011-09-30T11:08:32Z,U.S. Gasoline Price,Fell,Yesterday
2011-09-30T11:08:32Z,U.S. Regular Gasoline Price,Fell,Yesterday
2011-09-30T11:09:29Z,Mauritius Stocks,Trim,Quarterly Drop
2011-09-30T11:22:14Z,Greek Protesters Block Ministry Access,Complicating,Loan Review
2011-09-30T11:22:14Z,Protesters Block Ministry Access,Complicating,Loan Review
2011-09-30T11:24:57Z,Polymetal Plans London Listing,Sale of,Shares
2011-09-30T11:31:54Z,India ’s April-June Current Account Gap,Widens,Billion
2011-09-30T11:32:00Z,India ’s Foreign Exchange Reserves Week,Ended,Sept. 23
2011-09-30T11:34:20Z,National Level,’ Design,Re-Define
2011-09-30T11:40:00Z,India April-August Budget Deficit,Reaches,66.3 % of Annual Target
2011-09-30T11:40:02Z,Kenya Bonds Fall 3rd Day,Has,Quarter
2011-09-30T11:40:02Z,Kenya Bonds Fall Day,Has Quarter in,3 Years
2011-09-30T11:44:23Z,Uganda Shilling,Pares,Quarterly Drop
2011-09-30T11:45:05Z,Caps Holdings,Narrowing of,Six-Month Losses
2011-09-30T11:49:29Z,Nokia,Completes Accord Out,Symbian
2011-09-30T11:49:43Z,Three-Month Dollar Libor Increases,Reaches,0.374 %
2011-09-30T11:51:02Z,317,at Debt,RBI
2011-09-30T11:51:02Z,End,at Debt,RBI
2011-09-30T11:56:26Z,Barroso,Says,EU
2011-09-30T11:56:36Z,Opap,Rises After,License
2011-09-30T11:56:55Z,Credit Markets,Heading for,Worst Quarter
2011-09-30T12:00:00Z,South Africa Trade Deficit,Narrows as,Metal Prices Boost Exports
2011-09-30T12:03:12Z,India Budget Gap,Reaches,66.3 % of Target
2011-09-30T12:05:37Z,Austrian Parliament,Approves,Europe ’s Expanded Rescue Fund EFSF
2011-09-30T12:05:37Z,Parliament,Approves,Europe ’s Expanded Rescue Fund EFSF
2011-09-30T12:13:41Z,School,Spending,7.2 %
2011-09-30T12:13:49Z,Anil Ambani ’s Reliance Communications,Tumbles,Record
2011-09-30T12:19:43Z,Eircom ’s Senior Lenders May Gain 25 % Stake,in,Debt-Equity Swap
2011-09-30T12:22:13Z,BC Partners May,Seek,Raise for LBOs
2011-09-30T12:24:27Z,BofA,in,Court News
2011-09-30T12:24:27Z,Rajaratnam,BofA in,Court News
2011-09-30T12:30:00Z,U.S. August Market,Based,Consumption Price Indexes
2011-09-30T12:31:44Z,U.S. Corn Inventory Drops,Less Expected,Livestock-Feed Use Eases
2011-09-30T12:33:39Z,GlaxoSmithKline ’s Lupus Drug,Rejected by,U.K. Regulator
2011-09-30T12:33:47Z,U.S. Stock Futures Maintain Losses,Shows,Slowdown
2011-09-30T12:37:04Z,S&P 500 Dividend Health,Changes for,Sept. 30
2011-09-30T12:37:26Z,Microsemi,Pursuing,$ 7.69 Billion of Loans
2011-09-30T12:40:42Z,CLO Market,Would Weather Default by,Wells Fargo
2011-09-30T12:42:44Z,Merge Power Plants,Mines in,Two Companies
2011-09-30T12:42:44Z,Mines,in,Two Companies
2011-09-30T12:50:50Z,Deadline,Offers to,Oct. 31
2011-09-30T12:56:04Z,CEO,Director of,Polymetal
2011-09-30T12:56:04Z,Nesis,Posts of,Director
2011-09-30T12:58:53Z,Crude Oil,Heads for,Its Decline
2011-09-30T12:58:53Z,Its Biggest Quarterly Decline,in,New York
2011-09-30T12:58:53Z,Oil,Heads Since,2008
2011-09-30T12:59:12Z,Chaoda Modern,Says,Chairman Kwok Disputes Hong Kong Tribunal ’s Allegations
2011-09-30T13:08:30Z,Central Bank,Expands,Assets
2011-09-30T13:08:30Z,Romanian Central Bank,Expands,Assets for Open-Market Operations
2011-09-30T13:08:44Z,$ 40 Million,in,Third Quarter
2011-09-30T13:12:53Z,Finland,of,Hakkarainen Seeks Third Term
2011-09-30T13:14:32Z,Zambian Budget May,Following,Week ’s Election
2011-09-30T13:14:58Z,Chinese Students,Salvage,Cargo Containers
2011-09-30T13:14:58Z,Students,Salvage,Cargo Containers
2011-09-30T13:20:57Z,Ugandan Inflation,Surges to,28.3 %
2011-09-30T13:22:07Z,20.6 %,in,August
2011-09-30T13:26:53Z,$ 20Million Grain Silo,With,EBRD Loan
2011-09-30T13:29:57Z,Alitalia,Starting on,Nov. 2
2011-09-30T13:30:09Z,Deutsche Wohnen,Says,Wittan
2011-09-30T13:30:09Z,Wittan,Gradually Succeed,CFO Ullrich
2011-09-30T13:32:52Z,ABN,Joins,Deutsche Bank
2011-09-30T13:32:52Z,Deutsche Bank,in,Reviving Unsecured Bond Sales
2011-09-30T13:40:10Z,Rand,Is,Headed for Worst Quarter in Decade on Global Growth Concern
2011-09-30T13:51:20Z,$ 842 Million,in,Inflation-Linked Bonds
2011-09-30T13:51:20Z,Chile BCI May,Sell,$ 842 Million
2011-09-30T13:53:55Z,Turkey,Offers,Women
2011-09-30T13:54:20Z,Ocean,Talking to,African
2011-09-30T13:55:04Z,Cordray Nomination,Set,Review
2011-09-30T13:59:47Z,Betfair,Says,Hacker Attempt
2011-09-30T13:59:47Z,Hacker Attempt,Steal,Customer Data
2011-09-30T14:00:59Z,Declared Dividend Changes,in,September
2011-09-30T14:04:52Z,4.4 Billion Liras,in,Debt
2011-09-30T14:05:46Z,Leaders,Switch,Roles
2011-09-30T14:05:46Z,Russian Leaders,Switch,Roles
2011-09-30T14:09:47Z,Troika,of Focus,Official
2011-09-30T14:10:03Z,Nigeria ’s Stock Index,Heads for,Quarter
2011-09-30T14:13:52Z,Buffett,Says,Tax on Wealthy Akin
2011-09-30T14:15:00Z,BNP Drop,in,Paris
2011-09-30T14:15:00Z,SocGen,Drop in,Paris
2011-09-30T14:15:00Z,Tail Risk,on SocGen,BNP Drop in Paris
2011-09-30T14:16:08Z,Consumer Confidence,in,U.S.
2011-09-30T14:24:27Z,Global Women,Have,Heroine
2011-09-30T14:24:27Z,Heroine,in,Hong Kong
2011-09-30T14:24:27Z,Women,Have,Heroine
2011-09-30T14:25:03Z,Ackermann,Sees,Sort
2011-09-30T14:25:34Z,Business Activity,in,U.S.
2011-09-30T14:27:36Z,Dallas Fed,Trimmed,Mean U.S. PCE
2011-09-30T14:39:17Z,SATS,Enters,Discussions Over Potential Sale of Daniels Group
2011-09-30T14:41:56Z,Canada July Gross Domestic Product,Rises,0.3 %
2011-09-30T14:42:18Z,Basketball,in,Italy
2011-09-30T14:42:43Z,Ophelia,Intensifies to,Category 3 Hurricane
2011-09-30T14:43:42Z,Tax-Exempt Securities Show Longest,Winning Streak Since,2002
2011-09-30T14:44:25Z,Auditors ' Auditor,Faces,Moment
2011-09-30T14:44:37Z,Fitch,Says,Jury Still Out on Turkey Credit Rating Upgrade
2011-09-30T14:44:37Z,Jury,Still Out on,Turkey Credit Rating Upgrade
2011-09-30T14:44:49Z,India RBI,Revises Overdraft Limit for,Third
2011-09-30T14:48:08Z,Corn Futures Plunge 40-Cent Limit,in,Chicago
2011-09-30T14:50:47Z,Polish Central Bank,Supports Zloty for,Second Time
2011-09-30T14:51:50Z,Serbia Bans All Weekend Public Gatherings,Including,Gay Pride
2011-09-30T14:52:40Z,Author,’,Death Porn
2011-09-30T14:56:02Z,Czech Koruna Slides Versus Euro,Set for,Worst Month
2011-09-30T14:57:30Z,Canada Canola,Drops in,Week Ended Sept. 28
2011-09-30T14:57:30Z,Soybean Processing Drops,in,Week Ended Sept. 28
2011-09-30T15:04:40Z,Disclosures,in,SEC Report
2011-09-30T15:04:40Z,Processes,Disclosures in,SEC Report
2011-09-30T15:10:58Z,Hovnanian Debt Yields,Soar After,Exchange Offer
2011-09-30T15:10:58Z,Hovnanian Secured Debt Yields,Soar to,18.5 %
2011-09-30T15:11:58Z,Kenyan President,Stabilize,Shilling
2011-09-30T15:11:58Z,President,Vows,Measures
2011-09-30T15:25:05Z,Forint,Extends Loss in,2 Years on Budget Worry
2011-09-30T15:29:16Z,Erste,Sees Forint in,2012
2011-09-30T15:30:19Z,Biggest Changes,in,Implied Volatility
2011-09-30T15:30:19Z,U.S. Stock Options,With,Biggest Changes in Implied Volatility
2011-09-30T15:30:37Z,Consumer Spending,Cooled in,August
2011-09-30T15:37:51Z,U.S. Regulators,Eased TARP Repayment Terms for,Bank Exits
2011-09-30T15:42:39Z,U.S. Stocks,Pare Declines as,Consumer Staple Companies Rise
2011-09-30T15:49:51Z,Chris Christie,in,His Own Words
2011-09-30T15:50:17Z,Russian Stocks,Extending,Biggest Drop
2011-09-30T15:50:17Z,Stocks,Extending,Quarterly Drop
2011-09-30T15:54:22Z,A-Tec Plummets,in,Vienna
2011-09-30T15:56:28Z,Jupiter Hotels,Buys,24 Hotels
2011-09-30T16:00:00Z,Canada ’s Budget Deficit Triples,in,July
2011-09-30T16:02:13Z,Stocks,Posting,Quarterly Loss
2011-09-30T16:02:13Z,Swiss Stocks,Posting,Loss
2011-09-30T16:03:25Z,Asian Citrus,Tumbles on,Record
2011-09-30T16:03:25Z,Citrus,Tumbles in,Hong Kong
2011-09-30T16:06:00Z,KGHM,Move in,Warsaw
2011-09-30T16:06:23Z,Medvedev,Defends,Putin ’s Return
2011-09-30T16:07:14Z,Grueninger,Steps Downs as,Chief Executive Officer
2011-09-30T16:08:25Z,Tigers,in,AL Playoffs Means Detroit Bonus
2011-09-30T16:08:36Z,Breweries,Buys,Life
2011-09-30T16:08:36Z,Nigerian Breweries,Buys,Sona
2011-09-30T16:09:57Z,Ruble,Extends,Drop
2011-09-30T16:13:07Z,Larry Ellison,of,Company
2011-09-30T16:13:07Z,Oracle,Sued Over,Purchase of Larry Ellison ’s Company
2011-09-30T16:15:49Z,Merkel,in,Coming Days
2011-09-30T16:15:49Z,Sarkozy,Meet,Merkel
2011-09-30T16:16:35Z,Germany Stocks Fall,Extending Biggest Quarterly Drop Since,2002
2011-09-30T16:20:39Z,U.K. Stocks,Trimming,Weekly Gain
2011-09-30T16:22:51Z,U.S. Corporate Credit Risk Benchmark,Climbs to,Highest
2011-09-30T16:24:45Z,Omega Pharma,Spends,26.9 Million Euros
2011-09-30T16:28:44Z,European Stocks,Decline on,Economy
2011-09-30T16:28:44Z,Stocks,Decline on,Extending Slump
2011-09-30T16:29:51Z,Ethanol,Prices for,Week of Sept. 30
2011-09-30T16:31:45Z,Cephalon,Gets U.S. Justice Department Subpoena for,Top-Sellers
2011-09-30T16:35:00Z,James Bianco,Urges,Defensive
2011-09-30T16:36:01Z,Vietnam,in,2011-12
2011-09-30T16:38:11Z,Expected Earnings Growth,in,S&P 500
2011-09-30T16:39:15Z,Analyst Estimate Changes,in,S&P 500
2011-09-30T16:41:26Z,Al-Awlaki,’s Blow,Obama
2011-09-30T16:44:08Z,BP,Buys,Jet Fuel Cargo
2011-09-30T16:54:50Z,Archie MacAllaster,Is Dead at,82
2011-09-30T16:54:50Z,Barron,of,Investor Roundtable
2011-09-30T16:55:09Z,FCStone,Approved as,Cat
2011-09-30T16:57:06Z,Buffett,Says,Berkshire $ 4 Billion of Stock Investments
2011-09-30T17:00:56Z,Newt Gingrich 's Contract,With,Future
2011-09-30T17:10:31Z,Sales,Increase From,Surveillance
2011-09-30T17:17:44Z,Percentage,Feeling,Healthy Rises
2011-09-30T17:18:00Z,Canada,Agrees,C$ 2.2 Billion
2011-09-30T17:18:19Z,Expected Revenue Growth,in,S&P 500
2011-09-30T17:28:24Z,Biggest Gap,With Stocks,Estimate
2011-09-30T17:28:24Z,Biggest Weekly Changes,in,Target Price
2011-09-30T17:28:24Z,S&P 500 Stocks,With,Biggest Gap Between Market Price
2011-09-30T17:30:25Z,Patron,Pay Million for,24 Jarvis Hotels
2011-09-30T17:32:33Z,Investment,Opposes,News Corp
2011-09-30T17:35:48Z,Union Bank Nigeria Shareholders,Approve,Recapitalization
2011-09-30T17:36:54Z,Morgan Stanley,Wins,Dismissal
2011-09-30T17:36:54Z,Virgin Islands Pension Fund,of,CDO Lawsuit
2011-09-30T17:42:36Z,America,of,Problems
2011-09-30T17:42:36Z,Bank,Take,Longer
2011-09-30T17:42:36Z,Buffett,Says,Bank
2011-09-30T17:43:49Z,Durant Join NBA Talks,With,Basketball Season Start at Stake
2011-09-30T17:43:49Z,LeBron James,Talks With,Basketball Season Start at Stake
2011-09-30T17:46:16Z,Graceway Pharmaceuticals,Wins,Approval of Bankruptcy Loan
2011-09-30T17:51:23Z,Portugal ’s Madeira Region,Has,Debt of 6.3 Billion Euros
2011-09-30T18:15:28Z,Puerto Rico,Joins,U.S. Complaint
2011-09-30T18:27:46Z,Portugal ’s 2010 Deficit,Was,9.8 %
2011-09-30T18:27:46Z,Portugal ’s Deficit,Was,9.8 %
2011-09-30T18:36:56Z,Ex-Harvard Student,Gets Three-Years In,Prison Related
2011-09-30T18:36:57Z,Gulf Coast,Rise Amid,Norco Refinery Issues
2011-09-30T18:37:14Z,Fergo Aisa,Reaches,Agreement
2011-09-30T18:53:24Z,Misconduct Claims,in,Cuban Probe
2011-09-30T18:53:24Z,SEC Watchdog,Rejects,Misconduct Claims in Cuban Probe
2011-09-30T18:59:45Z,AT&T,Cites,South Overture
2011-09-30T19:02:42Z,It,More Inspections at,North Anna
2011-09-30T19:13:18Z,U.S. Peanut Prices,Received by,Farmers
2011-09-30T19:14:20Z,$ 1.2 Billion,in,Lyondell LBO
2011-09-30T19:25:11Z,Schumer,Says,Support
2011-09-30T19:28:08Z,Mets Ruling,Used Within,Days
2011-09-30T19:29:42Z,Wells Fargo,Settle,Lawsuits
2011-09-30T19:32:55Z,CFTC Traders,in,Financial Futures Positions
2011-09-30T19:35:11Z,Goldman ’s O’Neill,Sees G-20 Coordinated Rate Cuts at,November
2011-09-30T19:36:27Z,Kodak Debt Swaps,Weigh,Bankruptcy
2011-09-30T19:45:35Z,11,in Drops,Germany
2011-09-30T19:45:35Z,Colombia Peso Drops,in,11 on China
2011-09-30T19:45:35Z,Weakest Level,to Drops,Germany
2011-09-30T19:47:02Z,Romaine Lettuce Recalled,in,21 States
2011-09-30T19:53:06Z,California ’s Debt Service,Rises to,7.8 % of General Fund
2011-09-30T20:06:58Z,Toyota-Honda Rebound,Ignites,Call
2011-09-30T20:12:20Z,Netflix,With,Telefonica
2011-09-30T20:12:20Z,Slim,in,Latin America
2011-09-30T20:12:20Z,Telefonica,Slim in,Latin America
2011-09-30T20:12:39Z,Minmetals,Acquires,Congolese Copper Producer Anvil Mining
2011-09-30T20:13:11Z,Analyst,After,Warning
2011-09-30T20:14:17Z,Alere,Raises,Stake
2011-09-30T20:15:08Z,James,Re-Election to,News Corp
2011-09-30T20:15:47Z,Pharmasset,Rises on,Results for Hepatitis C Drug
2011-09-30T20:16:33Z,GE Survey,Finds,Midmarket Growth Amid Souring Economic Outlook
2011-09-30T20:20:12Z,Buffett,Says,Tax Money Shufflers
2011-09-30T20:26:24Z,ProLogis,Gets,$ 1.4 Billion Guarantee for Rooftop Solar Project
2011-09-30T20:32:58Z,Brazil,Keep,Tax
2011-09-30T20:44:14Z,Fall,Extending,Biggest Quarterly Drop
2011-09-30T20:46:02Z,Cost $ 11.6 Billion,in,Current Fiscal Year
2011-09-30T20:58:46Z,Italian Banks,in,Swaps Market
2011-09-30T21:05:21Z,Ingersoll Rand,Cutting,Sales
2011-09-30T21:06:58Z,Central Fund,are,Unchanged
2011-09-30T21:09:10Z,Verizon,Sues U.S. to,Block Internet Rules
2011-09-30T21:15:45Z,State Department,Read Riot Act Over,Attack
2011-09-30T21:20:31Z,Bank ’s Countrywide,Sued by,Sealink Funding
2011-09-30T21:28:27Z,IBM,Tops,Microsoft
2011-09-30T21:28:27Z,Microsoft,Be,Second-Most Valuable in Technology
2011-09-30T21:28:27Z,Second-Most Valuable,in,Technology
2011-09-30T21:37:15Z,Mexico,’s Drop,Europe Outlook
2011-09-30T21:54:05Z,$ 534 Million,in,Brazil Wood Pellet Production
2011-09-30T21:54:58Z,Hedge Fund,Must Pay,Wachovia Million
2011-09-30T21:56:37Z,State Street,Launches,Audio
2011-09-30T21:57:16Z,First Solar,Gets,Loan Guarantees
2011-09-30T21:59:18Z,SunPower Sells California Plant,With,U.S. Guarantee to NRG
2011-09-30T22:08:53Z,Chinese Stocks,in,U.S.
2011-09-30T22:25:55Z,Asian Currencies,Have Month Since,1997 on Signs of Slowdown
2011-09-30T22:25:55Z,Currencies,Have Month Since,1997 on Signs of Slowdown
2011-09-30T22:33:46Z,It,’s Appealing,Ruling Allowing
2011-09-30T22:33:46Z,Ruling,Allowing,Alabama Immigration Law
2011-09-30T22:54:30Z,Florida Prison Privatization,Is,Blocked
2011-09-30T23:00:01Z,European Stocks,Post,Biggest Weekly Gain
2011-09-30T23:00:01Z,European Stocks Post,in,14 Months
2011-09-30T23:00:01Z,Stocks,Post,Biggest Weekly Gain
2011-09-30T23:08:12Z,$ 4.75 Billion,in,Solar Power Loans
2011-09-30T23:08:12Z,U.S.,Closes,$ 4.75 Billion
2011-09-30T23:13:38Z,Brzezinski,Says,U.S. Transcript
2011-09-30T23:36:12Z,Asia Stocks Gain,in,Week on Speculation Europe Will
2011-10-01T00:10:46Z,Colombia,Keeps,4.5 % Rate
2011-10-01T04:00:01Z,U.S. Stocks,Capping,S&P 500 ’s Quarter
2011-10-01T04:00:06Z,21 Arrests,Are,Made
2011-10-01T04:00:06Z,Arrests,Are,Made
2011-10-01T04:00:09Z,NFL,Refuses,Reduce
2011-10-01T04:01:00Z,Jackie Collins,Buys,Sells 400 Million Books
2011-10-01T04:01:00Z,Lady Gaga,Joins,Sting
2011-10-01T04:01:01Z,California,by,Harris
2011-10-01T04:01:01Z,Facebook,Sued for,Tracking Users
2011-10-01T04:01:01Z,New Texas Election Map Proposals Sought,in,Perry Case
2011-10-01T04:01:01Z,Team,for,Fight With League
2011-10-01T04:01:01Z,Team ’s Fight,With,League
2011-10-01T04:01:03Z,FL Panel,Sets,Jan. 31
2011-10-01T04:01:16Z,Mideast Peace Impasse May,Pose,Israel Existential Threat
2011-10-01T04:01:16Z,Peace Impasse May,Pose,Israel Existential Threat
2011-10-01T17:29:45Z,Europe,Weighs,Stepped-Up Effort
2011-10-01T17:56:54Z,CIA Drone Strike,in,Yemen Silences al-Qaeda ’s Most-Effective English Voice
2011-10-01T17:56:54Z,Yemen Silences al-Qaeda,in,Most-Effective English Voice
2011-10-04T06:41:32Z,Bond Risk,Rises in,Europe
2011-10-04T06:41:32Z,Corporate Bond Risk,Rises in,Europe
2011-10-07T01:54:40Z,Bets,in,Afghanistan
