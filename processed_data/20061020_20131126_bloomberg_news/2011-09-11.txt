2011-09-11T00:06:20Z,Australian Stosur,Beats Unseeded Kerber,Reach
2011-09-11T00:06:20Z,Stosur,Beats,Unseeded Kerber
2011-09-11T00:06:20Z,U.S.,Open,Women ’s Final
2011-09-11T00:19:06Z,Andy Murray,Wins,Set of Men ’s Semifinal Vs. Rafael Nadal
2011-09-11T00:19:06Z,Men,of,Semifinal Vs. Rafael Nadal
2011-09-11T01:10:37Z,Djokovic,in,U.S. Tennis
2011-09-11T01:10:37Z,Men ’s Final,Djokovic in,U.S. Tennis
2011-09-11T02:57:26Z,Serena Williams,Wins,First Set of U.S. Open Semifinal
2011-09-11T03:38:10Z,Top Seed Caroline Wozniacki,in,U.S. Open Semifinal
2011-09-11T03:39:17Z,BHP Billiton ’s Coal Miners Resume,Strikes After,Talks Fail
2011-09-11T04:01:00Z,World Trade Center,Left Safety Design Legacy in,Buildings
2011-09-11T04:01:08Z,Retail Sales,Probably Slowed on,Growth
2011-09-11T04:17:52Z,Djokovic,Nadal in,U.S. Open
2011-09-11T04:17:52Z,Nadal,in,U.S. Open
2011-09-11T05:07:21Z,Australia ’s First Woman U.S.,Is Open,Finalist
2011-09-11T05:07:21Z,Samantha Stosur,Is Open,Finalist
2011-09-11T05:10:21Z,Yankees,Lose,Fourth Straight
2011-09-11T06:00:01Z,Violence Fuels Former General,as,Election Bid
2011-09-11T06:14:38Z,Virgin Money,Mulls,Sunday Telegraph
2011-09-11T07:17:31Z,Iran,Says,Boosting Oil Output Among Top Priorities
2011-09-11T07:19:00Z,Dubai Shares Decline,in,Week Low on Europe Debt Crisis Concern
2011-09-11T07:38:32Z,Iran,Install Largest Oil Platform in,Gulf
2011-09-11T07:43:39Z,Garcia,Tells,El Mundo
2011-09-11T08:07:12Z,PetroSA Plans Oil,Exploration Around,Cape Coast
2011-09-11T08:42:08Z,Saudi SIDC,Rises After,Sleep High Stake Ruling
2011-09-11T08:49:17Z,Medvedev Seeks Stricter Laws,Kills,Hockey Team
2011-09-11T09:30:47Z,Amcon,Writes Off,ThisDay Reports
2011-09-11T09:42:17Z,Ex-Bundesbank Member,Says,Stark Exit
2011-09-11T09:54:12Z,Assad,Agrees on,Reform
2011-09-11T09:54:12Z,Syria Forces,Kill,18 Protesters
2011-09-11T10:17:50Z,Fertilizer Supplier Origin,Gets,400 Million Pound Bid
2011-09-11T10:32:48Z,Czech Finance Minister,Sees Revenue on,Growth
2011-09-11T10:35:28Z,Fischer,Tells,Bild
2011-09-11T10:36:00Z,Citadel,Heads in,Month
2011-09-11T10:36:00Z,Unit,Secures,Funds
2011-09-11T10:55:21Z,Groupon Investor Funds Hoberman,’s Mydeco,Sunday Telegraph
2011-09-11T11:21:01Z,Export Minivans,in,U.S.
2011-09-11T11:21:01Z,Fiat ’s Turkish Unit,Agrees to,Export Minivans
2011-09-11T11:21:01Z,Fiat ’s Unit,Agrees to,Export Minivans
2011-09-11T11:51:13Z,Lloyds,Considers,Sale of Loan Portfolio
2011-09-11T12:33:16Z,Yemen,Sets,November Masila Oil
2011-09-11T12:38:09Z,Sri Lanka,Fights Back to,Boost Hopes Draw
2011-09-11T13:05:26Z,Metro Investor,Opposes,Spiegel
2011-09-11T13:08:01Z,GM,Asked BMW About,Cooperation
2011-09-11T13:09:39Z,Egypt,Cancels,Treasury Bills Sale
2011-09-11T13:18:31Z,Terror Threat,Spurs,NYC Frozen Zones
2011-09-11T13:40:36Z,Starbucks,Expand,Food Business
2011-09-11T13:58:50Z,Alexander,Says With,Fiscal Plans
2011-09-11T14:01:00Z,Nation,for,Workers
2011-09-11T14:57:07Z,Aramco,CEO for,Gas Venture in Saudi Arabia
2011-09-11T14:57:07Z,Gas Venture,in,Saudi Arabia
2011-09-11T15:01:00Z,Germany May,Surrender Over,Greece
2011-09-11T15:01:32Z,Obama,Wins,Argument
2011-09-11T15:15:08Z,Miss Four Weeks,With,Foot InjuryH
2011-09-11T15:26:05Z,England ’s Dyson,Wins,Golf ’s KLM Open
2011-09-11T15:28:14Z,Volkswagen,Says,Suzuki Broke Partnership Rules
2011-09-11T15:30:21Z,French Yachtsman Killed,Rescued After,Pirate Attack
2011-09-11T15:33:40Z,Malema Disciplinary Hearings,Postponed to,Sept. 13
2011-09-11T15:43:38Z,Hungary,’s Orban,MTI
2011-09-11T16:04:09Z,Officials,Information on,Adviser Brennan
2011-09-11T16:33:55Z,Fuel Oil,With Iraq,Petra
2011-09-11T16:33:55Z,Iraq,With,Fuel Oil
2011-09-11T16:33:55Z,Supply Jordan,to Iraq,Petra
2011-09-11T16:41:33Z,Storm Nate,Reaches,Mexican Coast
2011-09-11T16:41:33Z,Weakened Storm Nate,Reaches,Mexican Coast
2011-09-11T18:17:47Z,Hungary,’s Details,MTI Reports
2011-09-11T19:48:19Z,9/11,Remembered as,U.S. Officials Monitor Threat
2011-09-11T20:00:03Z,Saudi Lingerie Stores,Can Hire,Women
2011-09-11T20:07:14Z,Baltimore Ravens Beat Pittsburgh Steelers,in,NFL Opening Week
2011-09-11T21:00:00Z,Palestinian Authority ’s Progress,in,Governance Threatened
2011-09-11T21:22:36Z,First,Set,6-2 Vs. Serena Williams
2011-09-11T21:32:25Z,50 Civilians,in,Nuba Mountains
2011-09-11T21:57:03Z,Dollar Index,Rises,Highest In Than Six Months Amid Europe Concerns
2011-09-11T22:00:12Z,Red Bull ’s Vettel,Wins Grand Prix,Close on Formula One Title
2011-09-11T22:25:50Z,Cochlear,Recalls,Nucleus CI500 Implants
2011-09-11T22:45:00Z,Soybeans,May Rise,Yields Shrink
2011-09-11T22:45:00Z,Yields,Shrink After,Weather
2011-09-11T23:00:01Z,Femmes Fatales,Take,Paris
2011-09-11T23:00:01Z,Jewish Museum,Shows,Lost Collection
2011-09-11T23:00:01Z,Museum,Shows Lost Collection in,Berlin
2011-09-11T23:00:01Z,Nazi-Plundered Jewish Museum,Shows Lost Collection in,Berlin
2011-09-11T23:00:01Z,Nazi-Plundered Museum,Shows,Lost Collection
2011-09-11T23:01:00Z,Saint-Emilion,Starts,Harvest Fest
2011-09-11T23:01:00Z,Spending,Holds,Back Recovery
2011-09-11T23:01:00Z,Spending Growth,in,Scandinavia
2011-09-11T23:01:52Z,Lord,After,Tie
2011-09-11T23:29:39Z,Betis,Wins in,Spain
2011-09-11T23:29:39Z,Real Betis,Wins Again in,Spain
2011-09-12T01:26:15Z,Aussie Resilience,Spurs,Rally
2011-09-12T01:26:15Z,Resilience,Spurs Rally in,Bonds
2011-09-12T02:37:56Z,Asia-Pacific Bond Risk,Rises to,Highest
2011-09-12T04:00:07Z,Contagion,Is,Weekend ’s Top Movie
2011-09-12T04:00:07Z,Weekend ’s Movie,Ending,Three-Week Reign
2011-09-12T04:00:07Z,Weekend ’s Top Movie,Ending,Three-Week Reign
2011-09-12T04:08:10Z,May,Be Distant Scenario as,Data Show Demand Strength
2011-09-12T04:26:57Z,Jets Cowboys,Lose to,Eagles Win as NFL Season Start
2011-09-12T04:26:57Z,Jets Top Cowboys,Lose to,Redskins
2011-09-12T04:26:57Z,Redskins,Win as,NFL Season Start
2011-09-12T06:35:48Z,Home-Shortage Myth,Pits,Blogs Versus Banks
2011-09-12T06:35:48Z,Myth,Pits,Blogs Versus Banks
2011-09-12T07:08:15Z,Toyota Losing Market No.,Makes Prius Vehicle to,Escape Fading Japan Inc
2011-09-12T07:08:15Z,Toyota Losing Market No. 1,Makes,Prius Vehicle
2011-09-12T08:58:26Z,Currency,Slows,Inflation
2011-09-12T08:58:26Z,May,Keep,Rate
2011-09-12T09:10:39Z,Gillard Plans,Revised,Australia Migration Law
2011-09-12T10:32:54Z,Help,With Crisis,Domingo
2011-09-12T10:32:54Z,Rolex Oysters Navigate Swiss Franc Crisis,With,Help From Federer
2011-09-12T10:39:20Z,Party,Pushes,National Needs
2011-09-12T10:45:34Z,Papandreou,Avert,Default
2011-09-12T11:31:15Z,Europe Banks,Valued,Intensifying
2011-09-12T12:03:54Z,Tata Motors,Forced,Search
2011-09-12T12:07:22Z,Storm Nate,Downgraded Over,Mexico
2011-09-12T12:07:22Z,Tropical Storm Nate,Downgraded Over,Mexico
2011-09-12T13:18:08Z,Fiat,Frets,Volkswagen
2011-09-12T16:26:25Z,DAX,Divides,Analysts From Investors
2011-09-12T16:26:25Z,Valuations,Tumble,54 %
2011-09-12T20:15:02Z,Rush,Brings Increase in,Valuation
2011-09-12T20:35:04Z,Bartz,Has Resigned as,Yahoo Director
2011-09-12T21:18:37Z,Erasing Loss,in,Last Hour
2011-09-12T21:18:37Z,U.S. Stocks Rise,Loss in,Last Hour
2011-09-12T21:31:17Z,Cover Losses,in,Equity Slump
2011-09-13T00:26:16Z,Fighters,Push Toward,Sirte
2011-09-14T13:48:18Z,AIDS,Starts as,Donors Pull Back
