2012-10-12T00:01:05Z,JPMorgan,Sees,Surge
2012-10-12T00:22:58Z,G-7,Discusses,Potential Fiscal Measures
2012-10-12T00:33:29Z,Biggest Drop,in,12 Years
2012-10-12T01:00:00Z,Weidmann,Sees German Economy Stagnating in,Second Half of 2012
2012-10-12T02:46:50Z,Mo,After,Nobel Prize
2012-10-12T02:46:50Z,Shanghai Xinhua,Jumps,10 %
2012-10-12T03:26:41Z,China ’s Stocks Fall,Erasing,Gains
2012-10-12T03:33:17Z,Funds,Sell,Net 6.29 Billion Rupees of Indian Derivatives
2012-10-12T03:33:17Z,Global Funds,Sell,6.29 Billion Rupees of Derivatives
2012-10-12T03:50:18Z,O’Neill,Quits as,Australian Rugby Chief
2012-10-12T03:58:35Z,48 Percent,Say in,CNN Poll
2012-10-12T03:58:35Z,Percent,Say in,CNN Poll
2012-10-12T04:00:01Z,Cyberattacks,Could Become,as Destructive as 9/11
2012-10-12T04:00:01Z,Food-Stamp Overpayments Drop,With,Record Usage
2012-10-12T04:00:01Z,Ivy Leaders,Thank,One Man
2012-10-12T04:00:02Z,Affleck,Joins,Jackman
2012-10-12T04:00:02Z,Jackman,Put Studios in,Best-Picture Race
2012-10-12T04:01:00Z,Attacks,in,Connecticut Race
2012-10-12T04:01:00Z,CFTC ’s Gensler Interested,in,Second-Term
2012-10-12T04:01:00Z,Confusion Sown,in,Pennsylvania
2012-10-12T04:01:00Z,Corvette-Size Electric Motor Seen,Changing,How Jets Taxi
2012-10-12T04:01:00Z,Jets Tickets,Discounted,Online 50 % of Giants
2012-10-12T04:01:00Z,Puerto Rico Debt,Extends Slump Before,Governor Vote
2012-10-12T04:01:00Z,Texas Windmills Make Schools Share,With,Poorer Districts
2012-10-12T04:01:00Z,Wrestling Magnate,Keeps Up,Attacks in Connecticut Race
2012-10-12T04:07:54Z,Tabcorp,Appeal to,Ban Online Bar Bets
2012-10-12T04:08:33Z,Gillard,Hails Indonesia Ties as,Australia Remembers Bali Attack
2012-10-12T04:09:37Z,Emerging-Stock Funds Post Inflows,Led by,India
2012-10-12T04:36:52Z,World Bank ’s Kim Wants Fast Loans,Emerging,Market Clout
2012-10-12T04:36:52Z,World Bank ’s Kim Wants Loans,Emerging,Market Clout
2012-10-12T04:37:47Z,Tigers Beat Athletics 6-0,in,MLB
2012-10-12T04:45:31Z,Hong Kong Short Selling Turnover,Recorded,10/12/2012
2012-10-12T05:07:17Z,U.K.,Woos,Indian Leader Decade
2012-10-12T05:14:03Z,RusHydro,Snaps,Four-Day Slump
2012-10-12T05:35:07Z,Titans 26-23 NFL,Win Over,Steelers
2012-10-12T05:35:07Z,Titans NFL,Win Over,Steelers
2012-10-12T05:43:32Z,Tigers Advance,in,MLB
2012-10-12T05:43:32Z,Yankees Face Game 5,With,Orioles
2012-10-12T06:29:23Z,Uniqlo Retailer,Plunges as,Forecast Below Estimate
2012-10-12T06:30:00Z,IMF,Urges,Latin America
2012-10-12T06:30:00Z,Latin America,Maintain,Fiscal Rigor
2012-10-12T06:33:42Z,Rule,to EU,CTK
2012-10-12T06:46:07Z,Japan,of,Richest
2012-10-12T06:48:35Z,Daimler Investor Aabar,Shifts to,Voting Stake
2012-10-12T07:09:12Z,Davids,Joins,English Fourth-Tier Club Barnet
2012-10-12T07:27:22Z,Seven West,Tempts,Buyout on Advertising Rebound
2012-10-12T07:27:22Z,West,Tempts,Buyout
2012-10-12T07:29:55Z,Akzo CEO ’s Fatigue,Has,Investors
2012-10-12T07:29:55Z,Investors,in,Limbo
2012-10-12T07:30:00Z,External Imbalances,in,Middle East Oil Importers
2012-10-12T07:30:00Z,Iran Budget Gap,Widen as,Sanctions Hurt Oil Exports
2012-10-12T07:31:45Z,India Factory Output,Beats Estimates,Outlook Stays
2012-10-12T07:31:45Z,Outlook,Stays,Cloudy
2012-10-12T07:33:37Z,Banks ’ Dollar Funding Costs,Drop for,First Time
2012-10-12T07:33:37Z,European Banks ’ Dollar Funding Costs,Drop in,Week
2012-10-12T07:43:34Z,China ’s Stocks Rise,Capping,Weekly Gains
2012-10-12T07:49:36Z,Shale Gas Drilling,in,Poland
2012-10-12T07:52:00Z,France 's Rapeseed,Improves in,Northeast
2012-10-12T08:00:00Z,Lower Demand,in,Fourth Quarter
2012-10-12T08:05:35Z,Bulgarian Inflation,Accelerates Foods in,September
2012-10-12T08:05:35Z,Inflation,Accelerates Foods on,Fuel
2012-10-12T08:08:53Z,Bain,in,EU1B Deal
2012-10-12T08:10:12Z,Sony Rating,Lowered ’s,Moody
2012-10-12T08:14:27Z,Retailer Li Ning,Surges on,Management Changes
2012-10-12T08:27:05Z,Saudi Arabian Grain Imports Seen,Declining to,12.8 Million Tons
2012-10-12T08:43:57Z,Taiwan Bonds,Have Weekly Gain as,Growth Estimates
2012-10-12T08:47:43Z,EU,Awarded,2012 Nobel Peace Prize
2012-10-12T08:48:07Z,Biden,Shows Obama to,How Debate
2012-10-12T08:53:23Z,Mills,in,India
2012-10-12T08:59:00Z,Daimler Investor Aabar,Shifts to,Indirect Voting Holding
2012-10-12T09:10:02Z,Sinosteel Contractors,in,Zimbabwe Told
2012-10-12T09:59:15Z,NIS,Buys Fuel Storage in,Bulgaria
2012-10-12T09:59:15Z,Serbian NIS,Buys Fuel Storage in,Bulgaria
2012-10-12T10:04:52Z,Infosys Results,Outweigh,Output Data
2012-10-12T10:09:17Z,Hungary Wants Change,in,IMF Discussions
2012-10-12T10:17:29Z,0.8 Percent,in,August
2012-10-12T10:22:53Z,Azeri Inflation,Accelerates to,1.5 %
2012-10-12T10:29:47Z,Oerlikon,Faces,Chinese Delay
2012-10-12T10:33:12Z,Brazil 's Arabica Discount,Narrows,ICE Futures Slide
2012-10-12T10:33:17Z,Rosatom Interested,in,Co-Funding
2012-10-12T10:37:25Z,Google German Criminal Street View Probe Said,Be,Dropped
2012-10-12T10:38:35Z,Asian Stocks,Rise on,Japan-China Dispute Talks
2012-10-12T10:38:35Z,Stocks,Rise on,Japan-China Dispute Talks
2012-10-12T10:52:37Z,Forint Gains,Falls on,IMF Aid Outlook
2012-10-12T10:52:48Z,MAN,Sees,Tough 2013
2012-10-12T11:08:58Z,EU,Wins,Nobel Peace Prize
2012-10-12T11:09:40Z,London,Jumps to,Second Behind NYC
2012-10-12T11:28:10Z,JPMorgan Investment Bank,Pay Pool Falls Per,Person
2012-10-12T11:29:12Z,EU ’s Bowles,Include,Binding Limit
2012-10-12T11:38:31Z,PKO,Leads,Drop
2012-10-12T11:38:31Z,Tusk,Unveils,Share Transfer Plan
2012-10-12T11:38:42Z,Total,Loses,EU Top Court Appeal of 78.6 Mlne-Euro Fine
2012-10-12T11:44:28Z,Peace Prize,Is Good Choice Despite,It
2012-10-12T11:44:28Z,Prize,Is,Good
2012-10-12T11:47:22Z,Bonds,Fall,Third Day on Concern Inflation
2012-10-12T11:47:22Z,Indian Bonds,Fall,Day
2012-10-12T11:54:48Z,Maersk,Takes,19 Ships Off Asia-Europe Route
2012-10-12T11:58:48Z,Czech Republic,Sell Military Jets to,Iraq
2012-10-12T12:01:00Z,Globalfoundries CEO,Sees,Investment in Client Supply Lock
2012-10-12T12:01:00Z,Investment,in,Client Supply Lock
2012-10-12T12:04:34Z,EU,’s Market,Danish Lobby Group
2012-10-12T12:04:34Z,EU ’s Internal Energy Market,in,Crisis
2012-10-12T12:04:47Z,Fed Funds,Open Within,Target Range
2012-10-12T12:07:19Z,Oil,Heads in,Month
2012-10-12T12:10:21Z,African Corn,Snaps,Four-Day Drop
2012-10-12T12:10:21Z,Corn,Snaps,Drop
2012-10-12T12:10:21Z,South African Corn,Snaps Drop as,Stocks
2012-10-12T12:13:53Z,Soybeans,Drop,Supply Concerns Ease
2012-10-12T12:13:53Z,Supply Concerns,Ease After,USDA Report
2012-10-12T12:27:31Z,Djakovic,Agreement on,Weapon Stations
2012-10-12T12:40:33Z,Van Rompuy,Seeks,Balance for Non-Euro Nations
2012-10-12T12:45:05Z,It,Can Leave,Asset Protection Scheme
2012-10-12T12:45:05Z,RBS,Nears,Point
2012-10-12T12:47:30Z,Wholesale Prices,in,U.S. Rise
2012-10-12T12:49:41Z,Canada,Revises Quarter Labor Cost Rise to,0.6 %
2012-10-12T12:56:38Z,Foreign Investors,Buy,9.88 Billion Rupees of Stocks
2012-10-12T13:16:33Z,May,Take,Earnings Charge
2012-10-12T13:29:34Z,Ukraine Wheat Exporters,Arrange Quick,Shipments
2012-10-12T13:45:31Z,Lufthansa,Says,No Gulf Pact Before Savings
2012-10-12T13:45:48Z,African Truckers,Agree to,End Strike
2012-10-12T13:45:48Z,South African Truckers,Agree to,Wage Settlement
2012-10-12T13:45:48Z,Truckers,Agree to,End Strike
2012-10-12T13:46:44Z,Company Bond Sales,in,U.S.
2012-10-12T13:59:24Z,Consumer Confidence,Tops,Estimates
2012-10-12T13:59:24Z,U.S. Stocks,Extend,Gains
2012-10-12T14:00:53Z,Demand,Weakens Across,Europe
2012-10-12T14:00:53Z,Dutch Flower Exports,Slump,6.6 %
2012-10-12T14:00:53Z,Flower Exports,Slump,6.6 %
2012-10-12T14:14:28Z,U.K. Commission,Set to,Review Derivatives
2012-10-12T14:17:32Z,Consumer Confidence,Tops,Estimates
2012-10-12T14:19:44Z,Davis Rea,Buys,U.S.
2012-10-12T14:19:44Z,U.S.,RBC on,Economy Risk
2012-10-12T14:25:20Z,Eco City May,See,Mercedes Opportunity
2012-10-12T14:26:28Z,Canadian Dollar,in,Narrowest Range
2012-10-12T14:33:11Z,%,Gathered as,Wheat
2012-10-12T14:33:11Z,16 %,Gathered as,Wheat
2012-10-12T14:40:02Z,Clarkson Data,Shows,Iron-Ore Swaps Advanced
2012-10-12T14:44:03Z,Consumer Sentiment,Rises to,Pre-Recession High
2012-10-12T14:47:38Z,Serb Dinar,Hits,5-Month High
2012-10-12T14:49:48Z,Vattenfall,Starts,Testing
2012-10-12T14:52:20Z,Clinton,Help,Vilsack
2012-10-12T14:53:29Z,Euro Joint Bills,Urged as,Path Out Debt Crisis
2012-10-12T15:01:16Z,Pattern,Gets,Financing for California Wind Project
2012-10-12T15:08:56Z,Bonds,Climb as,Irish Debt Gains
2012-10-12T15:08:56Z,Spanish Bonds,Climb as,Debt Gains
2012-10-12T15:18:19Z,Biden,Could,Could Obama ’s Improbable Henry Higgins
2012-10-12T15:40:58Z,EU,Reaches,Preliminary Deal
2012-10-12T15:43:50Z,German Stocks Decline,Exending,DAX Index ’s Weekly Drop
2012-10-12T15:43:50Z,Stocks Decline,Exending,DAX Index ’s Weekly Drop
2012-10-12T15:56:53Z,Decline,Followed by,Gasoline
2012-10-12T15:56:53Z,Nickel,Leads,Decline Followed by Gasoline
2012-10-12T16:02:28Z,Romania ’s 2018 Euro Bonds Extend Longest Rally,in,Six Months
2012-10-12T16:03:57Z,Lagarde Warning,Offsets,U.S. Data
2012-10-12T16:09:55Z,Biden ’s Combative Ryan Debate,Gives,Obama Chance
2012-10-12T16:09:55Z,Biden ’s Ryan Debate,Gives Obama Chance,Reset
2012-10-12T16:21:05Z,STMicro,Evaluate,Digital Unit Sale
2012-10-12T16:26:06Z,Nedbank,Are Lending,Most
2012-10-12T16:27:33Z,Saints ’ Players,in,Bounty Case Appeal Latest Goodell Suspensions
2012-10-12T16:32:36Z,Ruble,Snaps,Two Days
2012-10-12T16:33:25Z,Vitol,Buys,More Naphtha
2012-10-12T16:34:25Z,Mitt Romney,on,Tax Plan
2012-10-12T16:40:59Z,Munger Siblings,Spend,$ 54 Million
2012-10-12T16:42:36Z,Canadian Stocks,Decline as,Financial Shares Retreat
2012-10-12T16:42:36Z,Stocks,Decline as,Financial Shares Retreat
2012-10-12T16:47:30Z,Treasury,Asks,Bond Dealers
2012-10-12T16:57:23Z,EPA Worries Dilbit Pipeline Spill,Threatens,Kalamazoo River
2012-10-12T17:00:00Z,King,’s Candidates,Osborne
2012-10-12T17:00:00Z,Osborne,Blames Germany for,BAE/EADS Merger Talks Collapse
2012-10-12T17:00:00Z,U.S.,Avoid From,Cliff
2012-10-12T17:38:38Z,Citigroup,Setting Up,Credit Fund
2012-10-12T17:48:15Z,Solyndra,Sues Suntech Holdings Over,Antitrust Claims
2012-10-12T17:49:12Z,China,Concerns,Mount
2012-10-12T17:57:02Z,Turkey May,Hold,Referendum Over Early Local Elections
2012-10-12T17:58:54Z,S&P Cuts South Africa,as,Rating
2012-10-12T18:31:00Z,Billionaire Mallya,Is Issued,Warrant on Kingfisher Checks
2012-10-12T19:00:37Z,Gold,Is Advancing as,Dollar Spurs Investment
2012-10-12T19:00:37Z,Seen,Is Advancing as,Weaker Dollar Spurs Investment
2012-10-12T19:01:47Z,Nigeria ’s September Revenue,Rises to,594.7 Billion Naira
2012-10-12T19:05:20Z,Harvard Business School,Gets,$ 40 Million Gift From Chao Family
2012-10-12T19:17:08Z,Peru,Keeps,4.25 % Rate
2012-10-12T19:50:01Z,Russia Will,Keep,Purchasing Canada Dollars
2012-10-12T19:55:20Z,Hollande,Pushes,Intervention
2012-10-12T19:56:31Z,Dish,Turn,Over Data
2012-10-12T20:01:17Z,Ryan,Truth Over,Iran
2012-10-12T20:02:11Z,Soybeans,Decline on,Slowing Demand for U.S. Supplies
2012-10-12T20:04:04Z,Ex-Mentor CEO Levine,Named,Successor
2012-10-12T20:05:00Z,Drop,in,U.S. Exports
2012-10-12T20:05:00Z,Wheat,Tumbles as,Drop
2012-10-12T20:08:20Z,Interim CEO,in,First Major
2012-10-12T20:08:20Z,WellPoint Reorganize,in,Interim CEO ’s First Major
2012-10-12T20:12:19Z,Micron,Says,PC Chip Prices
2012-10-12T20:14:25Z,FDA Staff,Backs,Risk Plan for Bowel Drug
2012-10-12T20:16:08Z,JPMorgan Profit,Beats,Estimates
2012-10-12T20:16:29Z,Groups,See,Loopholes
2012-10-12T20:16:29Z,Outside Groups,See,Loopholes
2012-10-12T20:26:48Z,Canadian Oils,Weaken,BP ’s Whiting Refinery Shutdown Nears
2012-10-12T20:26:48Z,Oils,Weaken,BP ’s Whiting Refinery Shutdown Nears
2012-10-12T20:30:17Z,MTN-Iran Deal,Postponed by,U.S. Judge
2012-10-12T20:31:30Z,U.S. Stocks,Decline as,Banks Offset Economy
2012-10-12T20:31:34Z,Ecolab,Buy,Champion
2012-10-12T20:34:21Z,3 Months,in,New York
2012-10-12T20:35:06Z,Plug,Receives,Delisting Warning From Nasdaq
2012-10-12T20:43:06Z,Turkey,Moves,Tanks
2012-10-12T20:49:29Z,California Debate,Sparks,Scuffle Between Political Rivals
2012-10-12T20:51:12Z,Romney,of,Math
2012-10-12T20:58:54Z,Biden Focuses,With,Ryan in Republican ’s Home State
2012-10-12T20:58:54Z,Republican,in,Home State
2012-10-12T20:58:54Z,Ryan,in,Republican ’s Home State
2012-10-12T21:00:00Z,Honda Record Sales,Led in,Markets
2012-10-12T21:00:00Z,U.S.,Postpones,China Currency Report
2012-10-12T21:00:30Z,Sununu,Says,Obama Imitating Biden
2012-10-12T21:03:48Z,Company Credit-Default Swaps,in,U.S. Rise
2012-10-12T21:05:52Z,General Dynamics,Wins Pentagon Backing for,Radios
2012-10-12T21:09:33Z,Fed,Buys,$ 1.889 Billion
2012-10-12T21:56:23Z,Patty,Weakens,May Form
2012-10-12T21:59:54Z,Plantronics,Sued for,Headset Market Monopoly
2012-10-12T22:01:00Z,EU,Stabilize,Crisis
2012-10-12T22:21:05Z,Liquor Maker Beam,Investigates,Operations
2012-10-12T22:21:05Z,Operations,Practices in,India
2012-10-12T22:21:05Z,Practices,in,India
2012-10-12T22:23:17Z,American,Adds Seat Clamps on,767s as Precautionary
2012-10-12T22:29:11Z,Biden-Ryan Debate,Draws,27 % Decline
2012-10-12T22:29:32Z,Emerging Stocks Fall Commodities,Drop on,Growth View
2012-10-12T22:29:32Z,Most Emerging Stocks Fall Commodities,Drop on,Growth View
2012-10-12T23:00:01Z,Europe Stocks,Drop in,Four
2012-10-12T23:33:48Z,IMF Europe Reforms,Are,Promising Strategy
2012-10-13T04:00:01Z,Cable Operators,Can Fight,Theft
2012-10-13T04:00:01Z,June,Since Fall,Profits
2012-10-13T04:00:01Z,U.S. Stocks,Fall,Most
2012-10-13T04:00:19Z,Harvard ’s Basketball Team May,Get New Arena Under,Allston Plan
2012-10-13T04:00:54Z,Bruyneel,Leaves,RadioShack Cycling
2012-10-13T04:01:00Z,Elvis Family,Lived in,One Room
2012-10-13T04:01:00Z,Home,in,East River
2012-10-13T04:01:00Z,Kahn ’s Memorial,Finds,Home
2012-10-13T04:01:00Z,Postal Service,Woos,Web Retailers
2012-10-13T04:01:00Z,Surprise Jack Welch Missed,Shows,Better U.S. Growth
2012-10-13T04:01:01Z,Alex Rodriguez,Stays on,Bench
2012-10-13T04:01:01Z,MLB Yankees,Oust,Orioles
2012-10-13T04:01:01Z,NYC Big Soda Ban Challenged,in,Court by Industry
2012-10-13T04:01:01Z,U.S. Lessees,Skip,Payments
2012-10-13T04:01:42Z,Biden,in,Denial
2012-10-13T04:01:42Z,Romney,Says Biden as,Carney Offers Defense
2012-10-13T04:14:20Z,BP Workers,’ Refusal,U.S.
2012-10-13T12:10:00Z,Clapton ’s Richter,Fetches,30 Times Purchase
2012-10-13T12:24:26Z,RBS,Says,Santander Drops
