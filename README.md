This repo is to process the newsfeed data. The data was used in this paper [Deep Learning for Event-Driven Stock Prediction](https://www.ijcai.org/Proceedings/15/Papers/329.pdf)

> The [link](http://ir.hit.edu.cn/∼xding/index_english.htm) provided by the paper seems to be no longer available as of 1st Oct. 2018. 

# Run
> mvn compile exec:java -Dexec.mainClass=newsfeed_openie.Process -Dexec.args=""
