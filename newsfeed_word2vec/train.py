import sentences, gensim

#reuter = sentences.FeedSentences('/Users/hjunlim/Documents/projects/financial-news-dataset/ReutersNews106521') # a memory-friendly iterator
ss = sentences.FeedSentences() # a memory-friendly iterator

cts = sentences.CorpusTitleSentences()
tm = gensim.models.Word2Vec(cts, min_count = 4, iter=2)
tm.save('newsfeedtitlemodel')

ccs = sentences.CorpusContentSentences()
cm = gensim.models.Word2Vec(ccs, min_count = 4, iter=1)
cm.save('newsfeedcontentmodel')
