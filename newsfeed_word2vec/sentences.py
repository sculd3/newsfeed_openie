import os

DIR_BLOOMBERG = '/Users/hjunlim/Documents/projects/financial-news-dataset/20061020_20131126_bloomberg_news'
DIR_REUTERS = '/Users/hjunlim/Documents/projects/financial-news-dataset/ReutersNews106521'
DIR_CORPUS = '/Users/hjunlim/Documents/projects/newsfeed_openie/corpus'

class FeedSentences(object):
    def __iter__(self):
        for dir_name in [DIR_REUTERS]:
            for date_dir in os.listdir(dir_name):
                if date_dir == ".DS_Store": continue
                print(date_dir)
                date_dir_path = os.path.join(dir_name, date_dir)
                for fname in os.listdir(date_dir_path):
                    f_path = os.path.join(date_dir_path, fname)
                    if '.tar' in f_path: continue
                    lines = ''
                    for line in open(f_path):
                        if '=========' in str(line): continue
                        lines += line
                    if len(lines.strip()) == 0: continue

                    title = ''
                    if len(lines.split('--')) > 1:
                        title = lines.split('--')[1].replace('\n', ' ').strip()
                    content = lines.split('.html')[-1]
                    content = content.replace('\n', ' ').strip()
                    yield (title + ' ' + content).split()

class CorpusTitleSentences(object):
    def __iter__(self):
        fname = os.path.join(DIR_CORPUS, 'titles.txt')
        lines = ''
        for line in open(fname):
            yield line.split()

class CorpusContentSentences(object):
    def __iter__(self):
        fname = os.path.join(DIR_CORPUS, 'sentences_pruned.txt')
        lines = ''
        for line in open(fname):
            yield line.split()
