package newsfeed_openie;

import financialnewsdataset.*;

import java.util.*;

public class App
{
    public static void main( String[] args )
    {
        Calendar fromCal = Calendar.getInstance();
        fromCal.set(2010, 1, 1);
        Calendar toCal = Calendar.getInstance();
        toCal.set(2010, 2, 1);

        Date from = fromCal.getTime();
        Date to = toCal.getTime();

        List<Tuple> tuples = ProcessedDataset.readRange(from, to, FinancialNewsDataset.SOURCE_TYPE_BLOOMBERG);
        for (Tuple tuple : tuples) {
            System.out.println(tuple.toString());
        }
        System.out.println( "done." );
    }
}
