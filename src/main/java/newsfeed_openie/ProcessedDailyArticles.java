package newsfeed_openie;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class ProcessedDailyArticles {
    public List<Tuple> tuples;
    public Date date;

    public ProcessedDailyArticles() {
        tuples = new ArrayList<Tuple>();
    }

    static final ThreadLocal<DateFormat> DATE_FORMAT = new ThreadLocal<DateFormat>(){
        @Override
        protected DateFormat initialValue() {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            return sdf;
        }
    };

    static public ProcessedDailyArticles fromFile(String filename, Date date) {
        ProcessedDailyArticles daily = new ProcessedDailyArticles();
        daily.date = date;

        try {
            FileReader fileReader = new FileReader(filename);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                Tuple tuple = Tuple.fromString(line);
                if (tuple != null) {
                    daily.tuples.add(tuple);
                }
            }
        } catch(FileNotFoundException ex) {
            ex.printStackTrace();
        } catch(IOException ex) {
            ex.printStackTrace();
        }
        return daily;
    }

    public void prune() {
        List<Tuple> pruned = new ArrayList<Tuple>();
        pruned.addAll(tuples);
        tuples.clear();
        for (Tuple tuple : pruned) {
            if (tuples.size() > 0 && tuples.get(tuples.size()-1).subject().equals(tuple.subject())) {
                continue;
            }
            if (tuple.relation().equals("’s") || tuple.relation().equals("'s")) {
                continue;
            }
            tuples.add(tuple);
        }
    }

    public void toFile(String filename) {
        try {
            PrintWriter writer = new PrintWriter(filename, "UTF-8");
            for (Tuple t : tuples) {
                writer.println(t.toString());
            }
            writer.close();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (UnsupportedEncodingException ex) {
            ex.printStackTrace();
        }
    }
}
