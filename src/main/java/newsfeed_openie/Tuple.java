package newsfeed_openie;

import com.google.auto.value.AutoValue;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@AutoValue
abstract public class Tuple {
    static private final ThreadLocal<DateFormat> DATE_FORMAT = new ThreadLocal<DateFormat>(){
        @Override
        protected DateFormat initialValue() {
            // 2006-10-27T13:21:05Z
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            return sdf;
        }
    };

    static final String DELIMITER = ",";
    public abstract String subject();
    public abstract String relation();
    public abstract String object();
    public abstract Date date();

    public static Tuple.Builder builder() {
        return new AutoValue_Tuple.Builder()
                .setSubject("")
                .setRelation("")
                .setObject("")
                .setDate(new Date())
                ;
    }

    @AutoValue.Builder
    public abstract static class Builder {
        public abstract Tuple.Builder setSubject(String v);
        public abstract Tuple.Builder setRelation(String v);
        public abstract Tuple.Builder setObject(String v);
        public abstract Tuple.Builder setDate(Date date);
        public abstract Tuple build();
    }

    static public Tuple fromString(String str) {
        String[] tokens = str.split(DELIMITER);
        if (tokens.length != 4) {
            return null;
        }

        try {
            Tuple tuple = Tuple.builder()
                    .setDate(DATE_FORMAT.get().parse(tokens[0]))
                    .setSubject(tokens[1])
                    .setRelation(tokens[2])
                    .setObject(tokens[3])
                    .build();
            return tuple;
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public String toString() {
        return String.join(DELIMITER, Arrays.asList(DATE_FORMAT.get().format(date()),subject(),relation(),object()));
    }

    public static Comparator<Tuple> TupleComparator  = new Comparator<Tuple>() {
        public int compare(Tuple t1, Tuple t2) {
            int c = t1.date().compareTo(t2.date());
            if (c == 0) {
                c = t1.subject().compareTo(t2.subject());
            }
            if (c == 0) {
                c = t2.toString().length() - t1.toString().length();
            }
            return c;
        }

    };
}
