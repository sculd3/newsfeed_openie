package newsfeed_openie;

import edu.stanford.nlp.ie.util.RelationTriple;
import edu.stanford.nlp.simple.Document;
import edu.stanford.nlp.simple.Sentence;
import financialnewsdataset.Article;
import financialnewsdataset.FinancialNewsDataset;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.io.*;

public class Process {

    static private final ThreadLocal<DateFormat> DATE_FORMAT = new ThreadLocal<DateFormat>(){
        @Override
        protected DateFormat initialValue() {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            return sdf;
        }
    };

    static void run(Date from, Date to, String sourceType) {
        File dir = new File(ProcessedDataset.PATH_PROCESSED_DATA + "/" + sourceType);
        dir.mkdirs();

        Date date = from;
        while (date.before(to) || date.equals(to)) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);

            String dateStr = DATE_FORMAT.get().format(date);
            try {
                List<Article> articles = FinancialNewsDataset.readDate(date, sourceType);
                if (articles.size() > 0) {
                    PrintWriter writer = new PrintWriter(ProcessedDataset.PATH_PROCESSED_DATA + "/" + sourceType + "/" + dateStr + ".txt", "UTF-8");
                    List<Tuple> dailyTuples = new ArrayList<Tuple>();
                    for (Article article : articles) {
                        System.out.println("processing " +  article.date().toString() +  " : " + article.title());
                        List<Tuple> tuples = article.process();
                        dailyTuples.addAll(tuples);
                    }
                    dailyTuples.sort(Tuple.TupleComparator);
                    for (Tuple tuple : dailyTuples) {
                        writer.println(tuple.toString());
                    }
                    writer.close();
                }
            } catch (FileNotFoundException ex) {
                ex.printStackTrace();
            } catch (UnsupportedEncodingException ex) {
                ex.printStackTrace();
            }
            cal.add(Calendar.DATE, 1);
            date = cal.getTime();
        }
    }

    static void titlesIntoSinglefile(Date from, Date to, String sourceType, String outFileName) {
        File dir = new File(ProcessedDataset.PATH_PROCESSED_DATA + "/" + sourceType);
        dir.mkdirs();

        Date date = from;
        try {
            PrintWriter writer = new PrintWriter(outFileName, "UTF-8");
            while (date.before(to) || date.equals(to)) {
                Calendar cal = Calendar.getInstance();
                cal.setTime(date);

                String dateStr = DATE_FORMAT.get().format(date);
                writer.println(dateStr);
                List<Article> articles = FinancialNewsDataset.readDate(date, sourceType);
                articles.sort(new Comparator<Article>() {
                    @Override
                    public int compare(Article o1, Article o2) {
                        return o1.date().compareTo(o2.date());
                    }
                });
                if (articles.size() > 0) {
                    List<Tuple> dailyTuples = new ArrayList<Tuple>();
                    for (Article article : articles) {
                        System.out.println("processing " +  article.date().toString() +  " : " + article.title());
                        if (article.title().trim().length() == 0) {
                            continue;
                        }
                        writer.println(article.date() + "," + article.title().trim());
                    }
                }
                cal.add(Calendar.DATE, 1);
                date = cal.getTime();
            }
            writer.close();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (UnsupportedEncodingException ex) {
            ex.printStackTrace();
        }
    }

    static void lemmaTizeTitles(String dir) {
        try {
            FileReader fileReader = new FileReader(dir + "/titles.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            PrintWriter writer = new PrintWriter(dir + "/titles_lemmatized.txt", "UTF-8");

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                if (!line.contains(",")) {
                    writer.println(line);
                } else {
                    String[] commaSplits = line.split(",", 2);
                    Sentence sentence = new Sentence(commaSplits[1].toLowerCase());
                    List<String> lemmas = sentence.lemmas();
                    writer.println(commaSplits[0] + "," + String.join(" ", lemmas));
                }
            }

            bufferedReader.close();
            fileReader.close();
            writer.close();
        } catch(FileNotFoundException ex) {
            ex.printStackTrace();
        } catch(IOException ex) {
            ex.printStackTrace();
        } finally {
        }
    }

    static void batchRun() {
        Calendar fromCal = Calendar.getInstance();
        Calendar toCal = Calendar.getInstance();
        fromCal.set(2006, 9, 22); // month is 0 indexed
        //fromCal.set(2012, 10, 25); // month is 0 indexed
        toCal.set(2013, 10, 26);
        //Process.run(fromCal.getTime(), toCal.getTime(), FinancialNewsDataset.SOURCE_TYPE_BLOOMBERG);
        Process.run(fromCal.getTime(), toCal.getTime(), FinancialNewsDataset.SOURCE_TYPE_REUTERS);
    }

    static void titlesIntoSinglefile() {
        Calendar fromCal = Calendar.getInstance();
        Calendar toCal = Calendar.getInstance();
        fromCal.set(2006, 9, 22); // month is 0 indexed
        toCal.set(2013, 10, 26);

        Process.titlesIntoSinglefile(fromCal.getTime(), toCal.getTime(),
                FinancialNewsDataset.SOURCE_TYPE_BLOOMBERG,
                ProcessedDataset.PATH_TITLES + "/" + FinancialNewsDataset.SOURCE_TYPE_BLOOMBERG + "/titles.txt");

        Process.titlesIntoSinglefile(fromCal.getTime(), toCal.getTime(),
                FinancialNewsDataset.SOURCE_TYPE_REUTERS,
                ProcessedDataset.PATH_TITLES + "/" + FinancialNewsDataset.SOURCE_TYPE_REUTERS + "/titles.txt");
    }

    static void test() {
        Iterator<Tuple> it = new ProcessedDataset.TupleIterator(FinancialNewsDataset.SOURCE_TYPE_BLOOMBERG);
        while (it.hasNext()) {
            Tuple t = it.next();
            System.out.println(t.toString());
        }
    }

    public static void main( String[] args )
    {
        //ProcessedDataset.prune(FinancialNewsDataset.SOURCE_TYPE_BLOOMBERG);
        //ProcessedDataset.prune(FinancialNewsDataset.SOURCE_TYPE_REUTERS);
        //Process.test();
        //Process.batchRun();
        //Process.titlesIntoSinglefile();
        Process.lemmaTizeTitles(ProcessedDataset.PATH_TITLES + "/" + FinancialNewsDataset.SOURCE_TYPE_BLOOMBERG);
        //Process.lemmaTizeTitles(ProcessedDataset.PATH_TITLES + "/" + FinancialNewsDataset.SOURCE_TYPE_REUTERS);
    }
}

