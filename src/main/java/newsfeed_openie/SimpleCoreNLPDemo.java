package newsfeed_openie;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.simple.*;
import edu.stanford.nlp.ie.util.*;

import java.util.stream.Collectors;

public class SimpleCoreNLPDemo {
    public static void main(String[] args) {
        // Create a document. No computation is done yet.
        Document doc = new Document("Russia’s Gazprom Neft Boosts 2011 Omsk Refinery Output by 5%");
        for (Sentence sent : doc.sentences()) {  // Will iterate over two sentences
            /*
            // We're only asking for words -- no need to load any models yet
            System.out.println("The second word of the sentence '" + sent + "' is " + sent.word(1));
            // When we ask for the lemma, it will load and run the part of speech tagger
            System.out.println("The third lemma of the sentence '" + sent + "' is " + sent.lemma(2));
            // When we ask for the parse, it will load and run the parser
            System.out.println("The parse of the sentence '" + sent + "' is " + sent.parse());
            */
            // ...
            System.out.println("sentence");

            for (RelationTriple trie : sent.openieTriples()) {
                System.out.println(String.format("confidence: %f", trie.confidence));
                System.out.println("subject");
                System.out.println(String.join(" ", trie.subject.stream().map(cl->cl.originalText()).collect(Collectors.toList())));
                System.out.println("relation");
                System.out.println(String.join(" ", trie.relation.stream().map(cl->cl.originalText()).collect(Collectors.toList())));
                System.out.println("object");
                System.out.println(String.join(" ", trie.object.stream().map(cl->cl.originalText()).collect(Collectors.toList())));
            }
        }
    }
}