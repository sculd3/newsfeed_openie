package newsfeed_openie;

import financialnewsdataset.Article;
import financialnewsdataset.FinancialNewsDataset;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class ProcessedDataset {
    static final Date DATE_FIRST, DATE_LAST;

    static public final String PATH_PROCESSED_DATA = "processed_data/";
    static public final String PATH_TITLES = "titles/";

    static final ThreadLocal<DateFormat> DATE_FORMAT = new ThreadLocal<DateFormat>(){
        @Override
        protected DateFormat initialValue() {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            return sdf;
        }
    };

    static {
        Calendar first = Calendar.getInstance();
        Calendar last = Calendar.getInstance();
        first.set(2006, 9, 20); // month is 0 indexed
        last.set(2013, 10, 26);
        DATE_FIRST = first.getTime();
        DATE_LAST = last.getTime();
    }

    static public class TupleIterator implements Iterator<Tuple> {
        Date date =  null;
        Queue<Tuple> tuples;
        String dirLoc;

        public TupleIterator(String sourceType) {
            date = DATE_FIRST;
            tuples = new LinkedList<Tuple>();

            if (sourceType == FinancialNewsDataset.SOURCE_TYPE_BLOOMBERG) {
                dirLoc = PATH_PROCESSED_DATA + FinancialNewsDataset.SOURCE_TYPE_BLOOMBERG;
            } else {
                dirLoc = PATH_PROCESSED_DATA + FinancialNewsDataset.SOURCE_TYPE_REUTERS;
            }
        }

        public boolean hasNext() {
            if (tuples.size() > 0) {
                return true;
            }
            boolean found = false;
            while (!date.after(DATE_LAST) && !found) {
                String daily = DATE_FORMAT.get().format(date);
                String filename = dirLoc + "/" + daily + ".txt";
                File loc = new File(filename);
                if (loc.exists()) {
                    ProcessedDailyArticles dailyArticles = ProcessedDailyArticles.fromFile(filename, date);
                    if (dailyArticles.tuples.size() > 0) {
                        tuples.addAll(dailyArticles.tuples);
                        found = true;
                    }
                }
                Calendar cal = Calendar.getInstance();
                cal.setTime(date);
                cal.add(Calendar.DATE, 1);
                date = cal.getTime();
            }
            return found;
        }

        public Tuple next() {
            return tuples.remove();
        }
    }

    // drop duple with "'s" relation.
    static public void prune(String sourceType) {
        String dirLoc;
        if (sourceType == FinancialNewsDataset.SOURCE_TYPE_BLOOMBERG) {
            dirLoc = PATH_PROCESSED_DATA + FinancialNewsDataset.SOURCE_TYPE_BLOOMBERG;
        } else {
            dirLoc = PATH_PROCESSED_DATA + FinancialNewsDataset.SOURCE_TYPE_REUTERS;
        }
        Date date =  DATE_FIRST;
        while(!date.after(DATE_LAST)) {
            String daily = DATE_FORMAT.get().format(date);
            String filename = dirLoc + "/" + daily + ".txt";
            File loc = new File(filename);
            if (loc.exists()) {
                ProcessedDailyArticles dailyArticles = ProcessedDailyArticles.fromFile(filename, date);
                if (dailyArticles.tuples.size() > 0) {
                    int prevSize = dailyArticles.tuples.size();
                    dailyArticles.prune();
                    if (dailyArticles.tuples.size() < prevSize) {
                        dailyArticles.toFile(filename);
                    }
                }
            }

            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.add(Calendar.DATE, 1);
            date = cal.getTime();
        }
    }

    static public List<Tuple> readRange(Date from, Date to, String sourceType) {
        List<Tuple> res = new ArrayList<Tuple>();
        Iterator<Tuple> it = new TupleIterator(sourceType);
        while(it.hasNext()) {
            Tuple tuple = it.next();
            if (tuple.date().before(from)) {
                continue;
            }
            if (tuple.date().after(to)) {
                continue; // sometimes articles are in the wrong folder
            }
            res.add(tuple);
        }
        return res;
    }

    public static void main( String[] args )
    {
        Calendar fromCal = Calendar.getInstance();
        fromCal.set(2012, 0, 0);
        Calendar toCal = Calendar.getInstance();
        toCal.set(2012, 0, 2);

        Date from = fromCal.getTime();
        Date to = toCal.getTime();

        List<Tuple> tuples = ProcessedDataset.readRange(from, to, FinancialNewsDataset.SOURCE_TYPE_BLOOMBERG);
        for (Tuple tuple : tuples) {
            System.out.println(tuple.toString());
        }
        tuples = ProcessedDataset.readRange(from, to, FinancialNewsDataset.SOURCE_TYPE_REUTERS);
        for (Tuple tuple : tuples) {
            System.out.println(tuple.toString());
        }
        System.out.println( "done." );
    }
}
