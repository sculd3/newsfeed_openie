package financialnewsdataset;

import com.google.auto.value.AutoValue;
import java.util.*;
import java.io.*;
import java.nio.file.Files;

@AutoValue
abstract public class DailyArticles {
    public abstract List<Article> articles();

    public static Builder builder() {
        return new AutoValue_DailyArticles.Builder()
                .setArticles(new ArrayList<Article>())
                ;
    }

    @AutoValue.Builder
    public abstract static class Builder {
        public abstract Builder setArticles(List<Article> articles);
        public abstract DailyArticles build();
    }

    static public DailyArticles fromDir(String dir, String sourceType) {
        DailyArticles daily = DailyArticles.builder().build();
        File loc = new File(dir);
        if (!loc.exists()) {
            System.out.println("dir " + dir + " does not exist.");
            return daily;
        }
        for (String file : loc.list())
        {
            Article article = Article.fromFile(dir + "/" + file, sourceType);
            if (article == null) {
                System.out.println("result from " + dir + " " + file + " is null.");
                continue;
            }
            daily.articles().add(article);
        }

        return daily;
    }
}
