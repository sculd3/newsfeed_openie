package financialnewsdataset;

import newsfeed_openie.ProcessedDataset;
import newsfeed_openie.Tuple;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.nio.file.*;
import java.io.IOException;

public class Corpus {
    public static void main( String[] args )
    {
        Calendar fromCal = Calendar.getInstance();
        Calendar toCal = Calendar.getInstance();
        fromCal.set(2006, 9, 22); // month is 0 indexed
        toCal.set(2013, 10, 26);
        Date from = fromCal.getTime();
        Date to = toCal.getTime();
        Date date = from;

        while (date.before(to) || date.equals(to)) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);

            List<Article> bloomberg = FinancialNewsDataset.readDate(date, FinancialNewsDataset.SOURCE_TYPE_BLOOMBERG);
            List<Article> reuter = FinancialNewsDataset.readDate(date, FinancialNewsDataset.SOURCE_TYPE_REUTERS);
            List<Article> articles = new ArrayList<Article>();
            articles.addAll(bloomberg);
            articles.addAll(reuter);

            if (articles.size() > 0) {
                try {
                    for (Article article : articles) {
                        Files.write(Paths.get("corpus/titles.txt"), article.title().getBytes(), StandardOpenOption.APPEND);
                        //Files.write(Paths.get("corpus/sentences.txt"), article.title().getBytes(), StandardOpenOption.APPEND);
                        //Files.write(Paths.get("corpus/sentences.txt"), article.content().getBytes(), StandardOpenOption.APPEND);

                    }
                }catch (IOException e) {
                    System.out.println(e.getMessage());
                }
            }
            cal.add(Calendar.DATE, 1);
            date = cal.getTime();
        }



        System.out.println( "done." );
    }
}
