package financialnewsdataset;

import newsfeed_openie.ProcessedDataset;
import newsfeed_openie.Tuple;
import org.apache.commons.lang3.ObjectUtils;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class FinancialNewsDataset {
    static private final ThreadLocal<DateFormat> DATE_FORMAT_REUTER = new ThreadLocal<DateFormat>(){
        @Override
        protected DateFormat initialValue() {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            return sdf;
        }
    };

    static private final ThreadLocal<DateFormat> DATE_FORMAT_BLOOMBERG = new ThreadLocal<DateFormat>(){
        @Override
        protected DateFormat initialValue() {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            return sdf;
        }
    };

    static final String PATH_REPOSITORY = "/Users/hjunlim/Documents/projects/financial-news-dataset/";
    static public final String SOURCE_TYPE_BLOOMBERG = "20061020_20131126_bloomberg_news";
    static public final String SOURCE_TYPE_REUTERS = "ReutersNews106521";

    static public List<Article> readDate(Date at, String sourceType) {
        List<Article> res = new ArrayList<Article>();
        String dirLoc;
        if (sourceType == SOURCE_TYPE_BLOOMBERG) {
            dirLoc = PATH_REPOSITORY + SOURCE_TYPE_BLOOMBERG;
        } else {
            dirLoc = PATH_REPOSITORY + SOURCE_TYPE_REUTERS;
        }

        String dateStr;
        if (sourceType == SOURCE_TYPE_BLOOMBERG) {
            dateStr = DATE_FORMAT_BLOOMBERG.get().format(at);
        } else {
            dateStr = DATE_FORMAT_REUTER.get().format(at);
        }
        DailyArticles dailyArticles = DailyArticles.fromDir(dirLoc + "/" + dateStr, sourceType);
        if (dailyArticles == null) {
            return res;
        }
        res.addAll(dailyArticles.articles());
        return res;
    }

    static public List<Article> readRange(Date from, Date to, String sourceType) {
        List<Article> res = new ArrayList<Article>();
        String dirLoc;
        if (sourceType == SOURCE_TYPE_BLOOMBERG) {
            dirLoc = PATH_REPOSITORY + SOURCE_TYPE_BLOOMBERG;
        } else {
            dirLoc = PATH_REPOSITORY + SOURCE_TYPE_REUTERS;
        }
        File loc = new File(dirLoc);

        for (String daily : loc.list())
        {
            if (daily.equals(".DS_Store"))  {
                continue;
            }
            try {
                Date date = new Date();
                if (sourceType == SOURCE_TYPE_BLOOMBERG) {
                    date = DATE_FORMAT_BLOOMBERG.get().parse(daily);
                } else {
                    date = DATE_FORMAT_REUTER.get().parse(daily);
                }
                if (date.before(from) || date.after(to)) {
                    continue;
                }

                res.addAll(FinancialNewsDataset.readDate(date, sourceType));
            } catch (ParseException ex) {
                ex.printStackTrace();
            }
        }
        return res;
    }

    public static void main( String[] args )
    {
        Calendar fromCal = Calendar.getInstance();
        fromCal.set(2012, 0, 1);
        Calendar toCal = Calendar.getInstance();
        toCal.set(2012, 0, 10);

        Date from = fromCal.getTime();
        Date to = toCal.getTime();

        List<Article> articles = FinancialNewsDataset.readRange(from, to, FinancialNewsDataset.SOURCE_TYPE_BLOOMBERG);
        for (Article article : articles) {
            System.out.println(article.title());
        }
        System.out.println( "done." );
    }
}
