package financialnewsdataset;

import com.google.auto.value.AutoValue;
import edu.stanford.nlp.ie.util.RelationTriple;
import edu.stanford.nlp.simple.Document;
import edu.stanford.nlp.simple.Sentence;
import newsfeed_openie.Tuple;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.nio.charset.StandardCharsets;

@AutoValue
abstract public class Article {
    static final private Pattern ARTICLE_PATTERN = Pattern.compile(
            ".*/organizations/(.*)/environments/(.*)/apiproxies/([^/]*)(/.+)?");

    static private final ThreadLocal<DateFormat> DATE_FORMAT_REUTER = new ThreadLocal<DateFormat>(){
        @Override
        protected DateFormat initialValue() {
            // Sat Oct 21, 2006 8:11pm EDT
            SimpleDateFormat sdf = new SimpleDateFormat("E MMM dd',' yyyy K:mma z");
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            return sdf;
        }
    };

    static private final ThreadLocal<DateFormat> DATE_FORMAT_BLOOMBERG = new ThreadLocal<DateFormat>(){
        @Override
        protected DateFormat initialValue() {
            // 2006-10-27T13:21:05Z
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            return sdf;
        }
    };

    public abstract String title();
    public abstract String author();
    public abstract Date date();
    public abstract String url();
    public abstract String content();

    public static Article.Builder builder() {
        return new AutoValue_Article.Builder()
                .setTitle("")
                .setAuthor("")
                .setUrl("")
                .setDate(new Date())
                .setContent("")
                ;
    }

    @AutoValue.Builder
    public abstract static class Builder {
        public abstract Article.Builder setTitle(String title);
        public abstract Article.Builder setAuthor(String author);
        public abstract Article.Builder setUrl(String url);
        public abstract Article.Builder setDate(Date date);
        public abstract Article.Builder setContent(String content);
        public abstract Article build();
    }

    static public Article fromFile(String filename, String sourceType) {
        try {
            FileReader fileReader = new FileReader(filename);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String line;
            String content = "";
            while ((line = bufferedReader.readLine()) != null) {
                content += line + "\n";
            }

            String[] splits = content.split("--");
            if (splits.length < 5) {
                return null;
            }
            String title = splits[1];
            String author = splits[2];
            String dateStr = splits[3].trim();
            if (dateStr.replaceAll(" ", "").toLowerCase().contains("by")) {
                title = splits[1] + " " + splits[2];
                author = dateStr;
                dateStr = splits[4].trim();
            }
            Date date = new Date();
            if (sourceType == FinancialNewsDataset.SOURCE_TYPE_BLOOMBERG) {
                date = DATE_FORMAT_BLOOMBERG.get().parse(dateStr);
            } else {
                date = DATE_FORMAT_REUTER.get().parse(dateStr);
            }

            bufferedReader.close();

            String url = "";
            String[] httpSplit = content.split("http://");
            if (httpSplit.length > 1) {
                String[] urlRestSplit = httpSplit[1].split("\n");
                url = "http://" + urlRestSplit[0];
            }

            String contentStr = "";
            String[] htmlSplit = content.split(".html");
            if (htmlSplit.length > 1) {
                contentStr = htmlSplit[htmlSplit.length-1].replaceAll("\n", " ").trim();
            }

            return Article.builder()
                    .setTitle(title)
                    .setAuthor(author)
                    .setUrl(url)
                    .setDate(date)
                    .setContent(contentStr)
                    .build();
        } catch (ParseException ex) {
            System.out.println("Parse error: " +  ex.getMessage() + ", filename: " + filename);
        } catch(FileNotFoundException ex) {
            ex.printStackTrace();
        } catch(IOException ex) {
            ex.printStackTrace();
        } finally {
        }

        return null;
    }

    public List<Tuple> process() {
        List<Tuple> res = new ArrayList<Tuple>();
        Document doc = new Document(title());
        for (Sentence sent : doc.sentences()) {
            for (RelationTriple trie : sent.openieTriples()) {
                Tuple tuple = Tuple.builder()
                        .setDate(date())
                        .setSubject(String.join(" ", trie.subject.stream().map(cl->cl.originalText()).collect(Collectors.toList())))
                        .setRelation(String.join(" ", trie.relation.stream().map(cl->cl.originalText()).collect(Collectors.toList())))
                        .setObject(String.join(" ", trie.object.stream().map(cl->cl.originalText()).collect(Collectors.toList())))
                        .build()
                        ;
                if (tuple.relation().equals("’s")) {
                    continue;
                }
                res.add(tuple);
                res.sort(Tuple.TupleComparator);
            }
        }
        if (res.size() == 0) {
            return res;
        }
        List<Tuple> pruned = new ArrayList<Tuple>();
        pruned.add(res.get(0));
        /*
        for (Tuple tuple : res) {
            if (pruned.size() > 0 && pruned.get(pruned.size()-1).subject().equals(tuple.subject())) {
                continue;
            }
            pruned.add(tuple);
        }
        */
        return pruned;
    }
}
