import os
import pattern.en as en

def unify_word(word): # went -> go, apples -> apple, BIG -> big
    return str(en.conjugate(word)).lower()

of = open('pruned.txt', 'w')
for line in open('titles.txt'):

	nl = ' '.join(map(unify_word, line.strip().split()))
	nl = nl.replace('u.s.', 'us').replace('u.s', 'us').replace('u.k.', 'uk').replace('u.k', 'uk')
	nl = nl.replace('.', ' ').replace(',', ' ').replace(':', ' ').replace(';', ' ').replace('-', ' ')
	nl = nl.replace('"', '').replace("'", '')
	nl = nl.replace('inc.', '').replace('``', '').replace("''", '')
	nls = nl.split('. ')
	for l in nls:
		if '@bloomberg' in l: continue
		if '@reuter' in l: continue
		of.write(l.strip() + '\n')